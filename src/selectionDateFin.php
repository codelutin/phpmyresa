<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File selectionDateFin.php
*
* This file is used to select the end date for a periodical reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only)	exit($exit_message_authentification);

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 3) && (count($_GET) != 3)) exitWrongSignature('selectionDateFin.php');

if (isset($_GET['mois'])) $mois = $_GET['mois'];
else if (isset($_POST['mois'])) $mois = $_POST['mois'];
else $mois = $moisCourant;
if ( ! ctype_digit($mois) || ($mois == '') ) exitWrongSignature('selectionDateFin.php');
if ($mois > 12) exitWrongSignature('selectionDateFin.php');

if (isset($_GET['annee'])) $annee = $_GET['annee'];
else if (isset($_POST['annee'])) $annee = $_POST['annee'];
else $annee = $anneeCourante;
if ( ! ctype_digit($annee) || ($annee == '') ) exitWrongSignature('selectionDateFin.php');
if (strlen($annee) > 4) exitWrongSignature('selectionDateFin.php');

if (isset($_GET['type'])) $type = $_GET['type'];
else if (isset($_POST['type'])) $type = $_POST['type'];
else $type = 0;
if ( ! in_array($type, array('0', '1', '2')) ) exitWrongSignature('selectionDateFin.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e	 	  **************
**********************************************************************************************/

echo $entete;
?>

<body>

<script type='text/javascript'> <!--

window.onblur =  window.focus();

function startResa(jour){

	var indexMois = document.forms.genererCalendrier.mois.selectedIndex;
	var indexAnnee = document.forms.genererCalendrier.annee.selectedIndex;
	var moisSelectionne = document.forms.genererCalendrier.mois.options[indexMois].value;
	var anneeSelectionnee = document.forms.genererCalendrier.annee.options[indexAnnee].value;

	var alertDate='<?php echo $_SESSION['s_language']['selectionDateFin_alert_J'];?>';
	var alertDate2='<?php echo $_SESSION['s_language']['selectionDateFin_alert2_J'];?>';
	var alertDate3='<?php echo $_SESSION['s_language']['calendrier_error2_J'];?>';
	lesMois = new Array(<?php
	            $inter = '';
	            for ($i = 0 ; $i < count($_SESSION['s_les_mois']); $i++) $inter .= "'".$_SESSION['s_les_mois'][$i]."',";
	            echo substr($inter,0,-1);
	            ?>);
	lesJours = new Array(<?php
	            $inter = '';
	            for ($i = 0 ; $i < count($_SESSION['s_les_jours']); $i++) $inter .= "'".$_SESSION['s_les_jours'][$i]."',";
	            echo substr($inter,0,-1);
	            ?>);

	<?php if ($type == 0){
	// Cas du choix d'une date de fin pour la crÃ©ation d'une rÃ©servation
	    echo "
	    var dateDebut = window.opener.document.forms.resa.date.value;
	    var res = dateDebut.split('-');
	    anneeDebut = res[0];
	    moisDebut = res[1];
	    jourDebut = res[2];
	    if (moisDebut.substring(0,1) == '0') moisDebut = moisDebut.substring(1,moisDebut.length);
	    if (jourDebut.substring(0,1) == '0') jourDebut = jourDebut.substring(1,jourDebut.length);

	    if (compareDate(anneeSelectionnee, anneeDebut, moisSelectionne, moisDebut, jour, jourDebut, 'true') == 'false'){
	        alert(alertDate);
	        return;
	    }

	    mois = moisSelectionne;
	    annee = anneeSelectionnee;
	    window.opener.document.forms.resa.mm.value = mois;
	    window.opener.document.forms.resa.jj.value = jour;
	    window.opener.document.forms.resa.aa.value = annee;
	    mois = mois -1;
	    var enddate = new Date();
	    enddate.setDate(jour);
	    enddate.setMonth(mois);
	    enddate.setFullYear(annee);
	    var inter = enddate.getDay()+".$_SESSION['s_jour_offset'].";
	    if (inter < 0) inter = inter +7;
	    var jourdelasemaine = lesJours[inter];
	    mois = lesMois[mois];
	    window.opener.document.forms.resa.dateFin.value = jourdelasemaine+' '+jour+' '+mois+' '+annee;
	    self.close();
	    ";
	} else if ($type == 1){
	// Cas du choix d'une date de dÃ©but pour la modification d'une rÃ©servation
	    echo "
	    anneeFin = window.opener.document.forms.resa.aa2.value;
	    moisFin = window.opener.document.forms.resa.mm2.value;
	    jourFin = window.opener.document.forms.resa.jj2.value;

	    var date1 = new Date();
	    var anneeCourante = parseInt(date1.getFullYear());
	    var moisCourant = parseInt(date1.getMonth()+1);
	    var jourCourant = parseInt(date1.getDate());

	    if (compareDate(anneeSelectionnee, anneeCourante, moisSelectionne, moisCourant, jour, jourCourant, 'false') == 'false'){
	        alert(alertDate3);
	        return;
	    }

	    if (compareDate(anneeFin, anneeSelectionnee, moisFin, moisSelectionne, jourFin, jour, 'true') == 'false'){
	        alert(alertDate2);
	        return;
	    }

	    mois = moisSelectionne;
	    annee = anneeSelectionnee;
	    window.opener.document.forms.resa.mm1.value = mois;
	    window.opener.document.forms.resa.jj1.value = jour;
	    window.opener.document.forms.resa.aa1.value = annee;
	    mois = mois -1;
	    var enddate = new Date();
	    enddate.setDate(jour);
	    enddate.setMonth(mois);
	    enddate.setFullYear(annee);
	    var inter = enddate.getDay()+".$_SESSION['s_jour_offset'].";
	    if (inter < 0) inter = inter +7;
	    var jourdelasemaine = lesJours[inter];
	    mois = lesMois[mois];
	    window.opener.document.forms.resa.dateDebut.value = jourdelasemaine+' '+jour+' '+mois+' '+annee;
	    self.close();
	    ";
	} else {
	// Cas du choix d'une date de fin pour la modification d'une rÃ©servation
	    echo "
	    anneeDebut = window.opener.document.forms.resa.aa1.value;
	    moisDebut = window.opener.document.forms.resa.mm1.value;
	    jourDebut = window.opener.document.forms.resa.jj1.value;

	    var date1 = new Date();
	    var anneeCourante = parseInt(date1.getFullYear());
	    var moisCourant = parseInt(date1.getMonth()+1);
	    var jourCourant = parseInt(date1.getDate());

	    if (compareDate(anneeSelectionnee, anneeCourante, moisSelectionne, moisCourant, jour, jourCourant, 'false') == 'false'){
	        alert(alertDate3);
	        return;
	    }

	    if (compareDate(anneeSelectionnee, anneeDebut, moisSelectionne, moisDebut, jour, jourDebut, 'true') == 'false'){
	        alert(alertDate);
	        return;
	    }

	    mois = moisSelectionne;
	    annee = anneeSelectionnee;
	    window.opener.document.forms.resa.mm2.value = mois;
	    window.opener.document.forms.resa.jj2.value = jour;
	    window.opener.document.forms.resa.aa2.value = annee;
	    mois = mois -1;
	    var enddate = new Date();
	    enddate.setDate(jour);
	    enddate.setMonth(mois);
	    enddate.setFullYear(annee);
	    var inter = enddate.getDay()+".$_SESSION['s_jour_offset'].";
	    if (inter < 0) inter = inter +7;
	    var jourdelasemaine = lesJours[inter];
	    mois = lesMois[mois];
	    window.opener.document.forms.resa.dateFin.value = jourdelasemaine+' '+jour+' '+mois+' '+annee;
	    self.close();
	    ";
	}?>

}
//-->
</script>

<form id='genererCalendrier' method='post' action='selectionDateFin.php'>

<table class='all' style="padding:1px; border-spacing:1px;" >
	<tr style='background-color:#eeeeee'>
	     <td>
	        <input type="hidden" name="type" value="<?php echo $type;?>" />
	        <select name="mois" onchange="javascript:document.forms.genererCalendrier.submit();">
<?php
	for ($i=1; $i<13; $i++) {
	    if ($i == $mois) {
	            echo "<option value=\"".$i."\" selected=\"selected\">".$_SESSION['s_les_mois'][$i-1]."</option>\n";
	    } else {
	            echo "<option value=\"".$i."\">".$_SESSION['s_les_mois'][$i-1]."</option>\n";
	    }
	}
?>
	        </select>

	        <select name="annee" onchange="javascript:document.forms.genererCalendrier.submit();">
<?php
	for ($i=$anneeCourante; $i<$anneeCourante+4; $i++) {
	    if ($i == $annee) {
	            echo "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n";
	    } else {
	            echo "<option value=\"".$i."\">".$i."</option>\n";
	    }
	}
?>
	        </select>
	    </td>
	</tr>

	<tr>
	    <td>
	        <?php calendrier($mois,$annee); ?>
	    </td>
	</tr>

</table>

</form>


<?php echo $body_end;?>
