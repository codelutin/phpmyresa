<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File redirectCookie.php
*
* This file is used to disallow access to users who disabled cookies
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('./commun/commun.php');


/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_GET);
if ((($nbpar != 1) && ($nbpar != 2)) || (count($_POST) != 0)) exitWrongSignature('redirectCookie.php');

if ( ! isset($_GET['from'])) exitWrongSignature('redirectCookie.php');
else {
	$from = $_GET['from'];
	if (strrpos($from, '/') !== false){
		$from = strrchr($from, '/');
		$from = substr($from, 1);
	}
}

if ( ! in_array($from, array('affichePDF.php', 'calendrier.php', 'changeLanguage.php', 'chercher.php', 'efface.php', 'execefface.php', 'execmodifie.php', 'execresa.php', 'execresa2.php', 'execvalide.php', 'genexcel.php', 'index.php', 'invitevalide.php', 'modifie.php', 'reservation.php', 'rss.php', 'selectionDateFin.php', 'today.php', 'tree.php', 'valide.php', 'vide.php', 'vueMois.php', 'vueSemaine.php', 'wiCal.php'))) exitWrongSignature('redirectCookie.php');

if (isset($_GET['id'])){
	$id = $_GET['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('redirectCookie.php');
}
if (isset($_GET['idmulti'])){
	$idmulti = $_GET['idmulti'];
	if ( ! ctype_digit(idmulti) || (idmulti == '') ) exitWrongSignature('redirectCookie.php');
}

/***********************************************************************************************
**************		  Fin de vÃ©rification des paramÃštres en entrÃ©e	 	  **************
**********************************************************************************************/

$var = ini_get('session.name');
if (isset($_COOKIE[$var])){
	if (isset($id)) header("Location: $from?id=$id");
	else if (isset($idmulti)) header("Location: $from?idmulti=$idmulti");
	else header("Location: $from");
	exit();
} else{
	$emailCrypte = PHPcrypt("mailto:".$technical_email."?subject=".$_SESSION['s_language']['help_25']);
	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<a href='\", \"$emailCrypte\", \"'>$technical_contact</a>\")</script>";

	$redirect = $error_message_begin."</p>\n";
	$redirect .= "<script src='commun/function.js' type='text/javascript'></script>\n";
	$redirect .= "<h1 class='centered'>".$_SESSION['s_language']['commun_redirect1']." ".$var.".</h1>\n";
	$redirect .= "<h2 class='centered'>".$_SESSION['s_language']['commun_redirect2']." ".$afficheEmailCrypte." - ".$_SESSION['s_language']['help_23']." ".$technical_tel."\n</h2>";
	$redirect .= $body_end;
	exit($redirect);
}
?>
