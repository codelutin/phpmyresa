<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File wiCal.php
*
* This file is used to generale ical files, if the icalendar functionnality is enabled
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 Frédéric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


function wiCal($classe){
	global $connexionDB, $CALENDARS_file_beginning_of_name, $CALENDARS_file_location, $CALENDARS_end_of_uid, $iCal_delay;

	$calendrier = $CALENDARS_file_beginning_of_name.$classe;
	$iCal = $CALENDARS_file_location.$calendrier.".ics";
	$fp = fopen($iCal,"w");
	if (!$fp){ return $fp;}
	$enTete = "BEGIN:VCALENDAR\nCALSCALE:GREGORIAN\nX-WR-TIMEZONE;VALUE=TEXT:Europe/Paris\nX-WR-CALNAME;VALUE=TEXT:".$calendrier."\n";
	$r = fwrite($fp, $enTete);

	$DB_request = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email FROM reservation R, objet O, classe C ";
	$DB_request .= "WHERE R.idobjet = O.id AND O.id_classe = C.id AND C.nom = '$classe' AND O.pubical = 1 AND R.state=0 AND R.diffusion=1 ";
	if ($iCal_delay != -1){
		$old_date = strtotime('now') - $iCal_delay * 7 * 24 * 60 * 60;
		$old_date = date("Y-m-d", $old_date);
		$DB_request .= " AND R.jour >= '$old_date'";
	}
	$DB_request .= "ORDER BY R.jour";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	while (( $reservation = database_fetch_object($resultat)) != NULL){
		$r = fwrite($fp,"BEGIN:VEVENT\n");
		$uid = "UID:".database_get_from_object($reservation, 'id').$CALENDARS_end_of_uid;
		$r = fwrite($fp,"$uid\n");
		$dtstartJour = ereg_replace("-","",database_get_from_object($reservation, 'jour'));
		$dtstart = "DTSTART:".$dtstartJour."T";
		$deb = substr(database_get_from_object($reservation, 'debut'),0,5);
		$dur = substr(database_get_from_object($reservation, 'duree'),0,5);
		$dtstart = $dtstart.ereg_replace(":","",$deb)."00";
		$r = fwrite($fp,"$dtstart\n");

		$deb=substr(database_get_from_object($reservation, 'debut'),0,2);
		$mi=substr(database_get_from_object($reservation, 'debut'),3,2);
		if ($mi=="30") $deb = $deb + .5;
		$fin = $deb + substr(database_get_from_object($reservation, 'duree'),0,2);
		$mi = substr(database_get_from_object($reservation, 'duree'),3,2);
		if ($mi=="30") $fin = $fin + .5;
		$point = strpos($fin,".");
		if (!$point){
			$dtend=$fin;
			$dtend=fillWithZero($dtend)."00";
		}else{
			$dtend=substr($fin,0,$point);
			$dtend=fillWithZero($dtend)."30";
		}
		$dtend = "DTEND:".$dtstartJour."T".$dtend."00";
		$r = fwrite($fp,"$dtend\n");
		$r = fwrite($fp,"CLASS:PUBLIC\n");
		$text = utf8_encode(database_get_from_object($reservation, 'objet'))." : ".utf8_encode(database_get_from_object($reservation, 'titre'));
		$r = fwrite($fp,"SUMMARY:$text\n");
		$textorig = database_get_from_object($reservation, 'commentaire');
                $textorig = ereg_replace("\n", " ",$textorig);
                $text = utf8_encode($textorig)." ".utf8_encode(database_get_from_object($reservation, 'email'));
		$r = fwrite($fp,"DESCRIPTION:$text\n");
		$r = fwrite($fp,"END:VEVENT\n");
	}

	$r = fwrite($fp,"END:VCALENDAR\n");
	fclose($fp);
	return $r;
}
?>
