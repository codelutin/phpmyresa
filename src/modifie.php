<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File modifie.php
*
* This file is used to ask for confirmation and password to modify some fields of a reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	        http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
require_once('commun/commun_action.php');
if ($read_only)	exit($exit_message_authentification);

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ( (count($_POST) != 1)  || (count($_GET) != 0)) exitWrongSignature('modifie.php');
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('chercher.php');
} else exitWrongSignature('modifie.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$idmulti = getIdmultiFromId($id);
if ($idmulti == 0) $DB_request_condition = "R.id=$id"; else $DB_request_condition = "R.idmulti=$idmulti";
$continuous = reservationIsContinuous($idmulti);

$tab = GetClassProperties($DB_request_condition);
$classeForICal = $tab[0];
$classe = $tab[1];

$objetWithPriorityManagementExist = false;
$objetWithWifiManagementExist = false;
$DB_request = "SELECT DISTINCT O.nom, O.priority, O.wifi FROM reservation R, objet O WHERE R.idobjet = O.id AND R.state=0 AND ".$DB_request_condition;
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$inter = "";
while ($row = database_fetch_object($resultat)){
	if (database_get_from_object($row, 'priority') == 1) $objetWithPriorityManagementExist = true;
	if (database_get_from_object($row, 'wifi') == 1) $objetWithWifiManagementExist = true;
	$inter .= database_get_from_object($row, 'nom').",";
}

$objet = substr($inter, 0, -1);
$tab = createObjectList($objet);
$objets = $tab[0];
$objetDisplay = $tab[3];

if ($objetWithPriorityManagementExist) $priorityMessage = getPriorityMessage($objets);

$priority = false;
$wifi = false;
$DB_request = "SELECT DISTINCT priority, wifi FROM reservation R WHERE R.state=0 AND ".$DB_request_condition;
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if (database_get_from_object($row, 'priority') == 1) $priority = true;
	if (database_get_from_object($row, 'wifi') == 1) $wifi = true;
}

$minDate = "";
$maxDate = "";
$i = 0;
$DB_request = "SELECT DISTINCT jour, debut, duree, commentaire, titre, email FROM reservation R WHERE R.state=0 AND $DB_request_condition ORDER BY jour";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if ($i == 0){
	        $heureDebut = substr(database_get_from_object($row, 'debut'), 0, -3);
	        $dureeDB = substr(database_get_from_object($row, 'duree'), 0, -3);
	        $minDate = database_get_from_object($row, 'jour');
	} else{
	        $dureeDB = substr(database_get_from_object($row, 'duree'), 0, -3);
	        $maxDate = database_get_from_object($row, 'jour');
	}
	$email = database_get_from_object($row, 'email');
	$titre = database_get_from_object($row, 'titre');
	$commentaire = database_get_from_object($row, 'commentaire');
	$i++;
}
$nbJours = $i;

$temp = split('-', $minDate);
$aa1 = $temp[0];
$mm1 = $temp[1];
$jj1 = $temp[2];
if (substr($mm1, 0, 1) == 0) $mm1 = substr($mm1, 1);
if (substr($jj1, 0, 1) == 0) $jj1 = substr($jj1, 1);

if ($maxDate != ""){
	$temp = split('-', $maxDate);
	$aa2 = $temp[0];
	$mm2 = $temp[1];
	$jj2 = $temp[2];
	if (substr($mm2, 0, 1) == 0) $mm2 = substr($mm2, 1);
	if (substr($jj2, 0, 1) == 0) $jj2 = substr($jj2, 1);
}

$debut = substr($heureDebut, 0, 2);
if (substr($debut, 0, 1) == '0') $debut = substr($debut, 1, 2);
$midebut = substr($heureDebut, 3);
$duree = substr($dureeDB, 0, 2);
if (substr($duree, 0, 1) == '0') $duree = substr($duree, 1, 2);
$miduree = substr($dureeDB, 3);

if ($midebut == 0) $maximumDuree = 25 - $debut;
else $maximumDuree = 25 - $debut - 1;
if ( ! $continuous) if ($duree > $maximumDuree) $duree = $default_duration;

echo $entete;
echo "<body style='font-size:small'>\n";


$today = date("Y-m-d");
if ( (($maxDate == "") && ($minDate < $today)) || (($maxDate != "") && ($maxDate < $today)) ){
	exit($_SESSION['s_language']['past_reservation']."<div style='font-size:small'><br /><br /><br /><a href='vueMois.php'>".$_SESSION['s_language']['invitevalide_planning']."</a></div>");
}

$tab = computeTitleInAction($minDate, $maxDate, $continuous, $nbJours);
$titleTypeAndDate = $tab[0];
$periodicity = $tab[1];
$titleObjects = strtoupper($objetDisplay);
?>

<script type="text/javascript">
<!--
function sComplet(){
	titre = document.forms.resa.titre.value;
	if (titre == ""){
	        alert("<?php echo $_SESSION['s_language']['reservation_error_title_J'];?>");
	        return;
	}
	if (titre.length > 64) {
	   	alert("<?php echo $_SESSION['s_language']['reservation_error_title2_J'];?>");
	        return;
	}
	var email = document.forms.resa.email.value;
	if (email == "" || email == "<?php echo $email_domain;?>"){
	        alert("<?php echo $_SESSION['s_language']['reservation_error_email_1_J'];?>");
	        return;
	}
	if ( ! emailIsCorrect(email)){
	        alert("<?php echo $_SESSION['s_language']['reservation_error_email_2_J'];?>");
	        return;
	}
	<?php if ($periodicity != -1){?>
	if (document.forms.resa.duree.options[document.forms.resa.duree.selectedIndex].value=='0' && document.forms.resa.miduree.options[document.forms.resa.miduree.selectedIndex].value=='00'){
	        alert("<?php echo $_SESSION['s_language']['reservation_error_duration_J'];?>");
	        return;
	}
	<?php } ?>
	window.document.forms.resa.submit();
}

function calendarDateFin(nb){
	if (nb == 1) url = 'selectionDateFin.php'+"?annee="+document.forms.resa.aa1.value+"&mois="+document.forms.resa.mm1.value+"&type=1";
	else url = 'selectionDateFin.php'+"?annee="+document.forms.resa.aa2.value+"&mois="+document.forms.resa.mm2.value+"&type=2";

	<?php if ($ie) echo "width = 200; height = 240;"; else echo "width = 185; height = 200;";?>
	x = (640 - width)/2, y = (480 - height)/2;
	if (screen){
	        y = (screen.availHeight - height)/2;
	        x = (screen.availWidth - width)/2;
	        if (screen.availWidth > 1800) x = ((screen.availWidth/2) - width)/2;
	}
	window.open(url,'Date_fin','width='+width+',height='+height+',screenX='+x+',screenY='+y+',top='+y+',left='+x+',resizable=no,scrollbars=no');
}

//-->
</script>

<div>
	<img src='img/edit.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['modify_submit'];?>" />
	<span style='font-size:large'><?php echo $titleObjects;?> <?php echo $titleTypeAndDate;?></span>
	<br /><br />
	<hr />
	<br />
	<b><?php echo $_SESSION['s_language']['modify_title'];?></b>
	<br /><br />
</div>

<form id='resa' action='execmodifie.php' method='post'>
<table class='all' cellpadding='3' cellspacing='1'>
<?php

if ($continuous){
	// Cas des rÃ©servations continues

	$nbColumn = 2;
	?>

	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array13'];?> <span class='star'>*</span></th>
	    <th><?php echo $_SESSION['s_language']['title_array3'];?> <span class='star'>*</span></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td>
	        <table class='center'>
	            <tr>
	                <td>
	                    <input type='hidden' name='aa1' value='<?php echo $aa1;?>' />
	                    <input type='hidden' name='mm1' value='<?php echo $mm1;?>' />
	                    <input type='hidden' name='jj1' value='<?php echo $jj1;?>' />
<?php
	if ($minDate >= $today) {?>
	                    <a href='javascript:calendarDateFin(1)'>
	                    <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
<?php
	}?>
	                </td>
	                <td>
	                    <input name='dateDebut' disabled='disabled' value="<?php echo displayCurrentLanguageDate($minDate, 0);?>"
	                    onfocus="javascript:this.blur();" size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>
	    <td class='center'>
	        <select name='debut'>
<?php
	for ($i=0; $i<24; $i++){
	        if ($i == $debut) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	        } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	        }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;
	        <select name='midebut'>
<?php
	        if ($midebut == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	        } else {?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	        }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array14'];?> <span class='star'>*</span></th>
	    <th><?php echo $_SESSION['s_language']['title_array15'];?> <span class='star'>*</span></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td>
	        <table class='center'>
	            <tr>
	                <td>
	                    <input type='hidden' name='aa2' value='<?php echo $aa2;?>' />
	                    <input type='hidden' name='mm2' value='<?php echo $mm2;?>' />
	                    <input type='hidden' name='jj2' value='<?php echo $jj2;?>' />
<?php
	if ($maxDate >= $today) {?>
	                    <a href='javascript:calendarDateFin(2)'>
	                    <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
<?php
	}?>
	                </td>
	                <td>
	                    <input name='dateFin' disabled='disabled' value="<?php echo displayCurrentLanguageDate($maxDate, 0);?>"
	                    onfocus="javasript:this.blur();" size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>

	    <td class='center'>
	        <select name='duree'>
<?php
	for ($i=0; $i<24; $i++){
	        if ($i == $duree) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	        } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	        }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;
	        <select name='miduree'>
<?php
	if ($miduree == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else {?>
	        <option value='00'>0</option>
	        <option value='30' selected='selected'>30</option>
<?php
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <td colspan='2'><b><?php echo $_SESSION['s_language']['title_array7'];?></b> <span class='star'>*</span>
	    </td>
	</tr>

<?php
	$emailCrypte = PHPcrypt($email);
//	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<input name='email' size='20' value='\", \"$emailCrypte\", \"' />\")</script>";
//	echo "<tr bgcolor='#CCCCCC'><td colspan='2'>".$afficheEmailCrypte."</td></tr>";
?>
	<tr style='background-color:#cccccc'>
	    <td colspan='2'>
	        <script type='text/javascript'> <!--
	            decrypt("<input name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	        //-->
	        </script>
	    </td>
	</tr>

	<tr style='background-color#d3dce3'>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array5'];?> <span class='star'>*</span></th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array8'];?></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td style='vertical-align:top'><textarea name='titre' cols='20' rows='5'><?php echo $titre;?></textarea></td>
	    <td><textarea name='commentaire' rows='5' cols='20'><?php echo $commentaire;?></textarea></td>
	</tr>
<?php
} else if ($maxDate == "") {
	// Cas des rÃ©servations ponctuelles

	$nbColumn = 3;
?>
	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array3'];?> <span class='star'>*</span></th>
	    <th><?php echo $_SESSION['s_language']['title_array4'];?> <span class='star'>*</span></th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array7'];?> <span class='star'>*</span></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td class='center'>
	        <select name='debut'  onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	for ($i=0; $i<24; $i++){
	    if ($i == $debut) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;
	        <select name='midebut'  onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	if ($midebut == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else {?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>

	    <td class='center'>
	        <select name='duree'>
<?php
	for ($i=0; $i<$maximumDuree; $i++){
	    if ($i == $duree) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;
	        <select name='miduree'>
<?php
	if ($miduree == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else {?>
	            <option value='00'>0</option>
	            <option selected='selected' value='30'>30</option>
<?php
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	        <input name='aa1' type='hidden' value='<?php echo $aa1;?>' />
	        <input name='mm1' type='hidden' value='<?php echo $mm1;?>' />
	        <input name='jj1' type='hidden' value='<?php echo $jj1;?>' />
	    </td>

<?php
	$emailCrypte = PHPcrypt($email);
//	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<input name='email' size='20' value='\", \"$emailCrypte\", \"' />\")</script>";
//	echo "<td>".$afficheEmailCrypte."</td></tr>";
?>
	    <td>
	        <script type='text/javascript'> <!--
	            decrypt("<input name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	        //-->
	        </script>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th class='gauche' colspan='2'><?php echo $_SESSION['s_language']['title_array5'];?> <span class='star'>*</span></th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array8'];?></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td style='vertical-align:top' colspan='2'><textarea name='titre' cols='20' rows='5'><?php echo $titre;?></textarea></td>
	    <td><textarea name='commentaire' cols='20' rows='5'><?php echo $commentaire;?></textarea></td>
	</tr>


<?php
} else {
	// Cas des rÃ©servations rÃ©pÃ©titives

	$nbColumn = 3;
?>
	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array3'];?> <span class='star'>*</span></th>
	    <th><?php echo $_SESSION['s_language']['title_array4'];?> <span class='star'>*</span></th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array7'];?> <span class='star'>*</span></th>
	</tr>

	<tr style='background-color:#cccccc'>
<?php
	if ($periodicity == -1){
	    // Dans le cas de rÃ©servation de pÃ©riodicitÃ© non correctement dÃ©finie, on ne peut pas changer les horaires ni les dates
?>
	    <td class='center'>
	        <input name='debut' size='2' disabled='disabled' value='<?php echo $debut;?>' />
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>
	        <input name='midebut' size='2' disabled='disabled' value='<?php echo $midebut;?>' />
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>

	    <td class='center'>
	        <input name='duree' size='2' disabled='disabled' value='<?php echo $duree;?>' />
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>
	        <input name='miduree' size='2' disabled='disabled' value='<?php echo $miduree;?>' />
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
<?php
	} else {?>
	    <td class='center' style='white-space:nowrap'>
	        <select name='debut' onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	    for ($i=0; $i<24; $i++){
	        if ($i == $debut) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	        } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	        }
	    }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;
	        <select name='midebut' onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	    if ($midebut == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	    } else {?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	    }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>

	    <td class='center' style='white-space:nowrap'>
	        <select name='duree'>
<?php
	    for ($i=0; $i<$maximumDuree; $i++){
	        if ($i == $duree) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	        } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	        }
	    }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?> &nbsp;

	        <select name='miduree'>
<?php
	    if ($miduree == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	    } else {?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	    }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
<?php
	}
	$emailCrypte = PHPcrypt($email);
//	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<input name='email' size='20' value='\", \"$emailCrypte\", \"' />\")</script>";
//	echo "<td>".$afficheEmailCrypte."</td></tr>";
?>
	    <td>
	        <script type='text/javascript'> <!--
	            decrypt("<input name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	        //-->
	        </script>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th class='gauche' colspan='2'><?php echo $_SESSION['s_language']['title_array5'];?> <span class='star'>*</span></th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array8'];?></th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td style='vertical-align:top' colspan='2'><textarea name='titre' cols='20' rows='5'><?php echo $titre;?></textarea></td>
	    <td><textarea name='commentaire' rows='5' cols='20'><?php echo $commentaire;?></textarea></td>
	</tr>

	</tr>
	<tr><td colspan='3'>&nbsp;</td></tr>
	<tr>
	    <th class='gauche' colspan='3'><b>
	    <?php echo $_SESSION['s_language']['reservation_periodic_strict'];?>
<?php
	if ($periodicity == 1) {?>
	    <?php echo $_SESSION['s_language']['modify_1jour'];?>
<?php
	} else if ($periodicity == 7) {?>
	    <?php echo $_SESSION['s_language']['modify_1semaine'];?>
<?php
	} else if ($periodicity == 14) {?>
	    <?php echo $_SESSION['s_language']['modify_2semaines'];?>
<?php
	} else {?>
	    <?php echo " -  ".$_SESSION['s_language']['modify_periodicity_not_defined'];?>
<?php
	}?>
	    </th>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <td colspan='2'><b><?php echo $_SESSION['s_language']['execresa_from'];?></b> <span class='star'>*</span></th>
	    <td><b><?php echo $_SESSION['s_language']['execresa_to'];?></b> <span class='star'>*</span></td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <td colspan='2' style='vertical-align:top'>
	        <table>
	            <tr>
	                <td>
	                    <input type='hidden' name='aa1' value='<?php echo $aa1;?>' />
	                    <input type='hidden' name='mm1' value='<?php echo $mm1;?>' />
	                    <input type='hidden' name='jj1' value='<?php echo $jj1;?>' />
<?php
	if ($minDate >= $today) if ($periodicity != -1) {?>
	                    <a href='javascript:calendarDateFin(1)'>
	                        <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
<?php
	}?>
	                </td>
	                <td>
	                    <input name='dateDebut' disabled='disabled' value='<?php echo displayCurrentLanguageDate($minDate, 0);?>'
	                        onfocus='javascript:this.blur();' size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>
	    <td valign='top'>
	        <table>
	            <tr>
	                <td>
	                    <input type='hidden' name='aa2' value='<?php echo $aa2;?>' />
	                    <input type='hidden' name='mm2' value='<?php echo $mm2;?>' />
	                    <input type='hidden' name='jj2' value='<?php echo $jj2;?>' />
<?php
	if ($maxDate >= $today) if ($periodicity != -1) {?>
	                    <a href='javascript:calendarDateFin(2)'>
	                        <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
<?php
	}?>
	                </td>
	                <td>
	                    <input name='dateFin' disabled='disabled' value='<?php echo displayCurrentLanguageDate($maxDate, 0);?>'
	                        onfocus='javascript:this.blur();' size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>
	</tr>
<?php
	if ($periodicity == -1) $periodicity = 0;
}
?>
	<tr>
	    <td colspan='<?php echo $nbColumn;?>'>
	        <span class='star'>*</span> <span style='font-size:x-small'>= <?php echo $_SESSION['s_language']['reservation_mandatory'];?></span>
	    </td>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>

<?php
if ($objetWithPriorityManagementExist == 1){?>
	<tr>
	    <td style='background-color:#d3dce3' colspan='<?php echo $nbColumn;?>'>
	        <b><?php echo sprintf($_SESSION['s_language']['priority_2'], $priorityMessage);?></b>
	    </td>
	</tr>
	<tr>
	    <td style='background-color:#cccccc;vertical-align:middle' colspan='<?php echo $nbColumn;?>'>
<?php
	if ($priority == 1) {?>
	        <input type='radio' name='priority' value='1' checked='checked' /><?php echo $_SESSION['s_language']['yes'];?> &nbsp; &nbsp; <input type='radio' name='priority' value='0' />
<?php
	} else {?>
	        <input type='radio' name='priority' value='1' /><?php echo $_SESSION['s_language']['yes'];?> &nbsp; &nbsp; <input type='radio' name='priority' value='0' checked='checked' />
<?php
	}?>
	        <?php echo $_SESSION['s_language']['no'];?>
	    </td>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
<?php
}?>

<?php
if ($objetWithWifiManagementExist == 1){?>
	<tr>
	    <td style='background-color:#d3dce3' colspan='<?php echo $nbColumn;?>'>
	        <b><?php echo $_SESSION['s_language']['reservation_wifi'];?></b>
	    </td>
	</tr>
	<tr>
	    <td style='background-color:#cccccc;vertical-align:middle' colspan='<?php echo $nbColumn;?>'>
<?php
	if ($wifi == 1) {?>
	        <input type='radio' name='wifi' value='1' checked='checked' /><?php echo $_SESSION['s_language']['yes'];?> &nbsp; &nbsp; <input type='radio' name='wifi' value='0' />
<?php
	} else {?>
	        <input type='radio' name='wifi' value='1' /><?php echo $_SESSION['s_language']['yes'];?> &nbsp; &nbsp; <input type='radio' name='wifi' value='0' checked='checked' />
<?php
	}?>
	        <?php echo $_SESSION['s_language']['no'];?>
	    </td>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
<?php
}?>

</table>

<div>
<br />
<hr />

<?php
echo "<b>".$_SESSION['s_language']['modify_ask']."</b>\n";
echo "<ul>\n";
	echo "<li>".$_SESSION['s_language']['ask_user_password']."</li>\n";
	echo "<li>".$_SESSION['s_language']['or']." ";
	if (count($classe) == 1){
		$SPECIAL_classe_to_display = $classe[0];
		$text = $_SESSION['s_language']['ask_admin_password_for_one_class'];
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_one_class']."\";" );
		echo $text;
	} else{
		$SPECIAL_classe_to_display = "";
		for ($i = 0 ; $i < count($classe) ; $i++) $SPECIAL_classe_to_display .= "&#39;".$classe[$i]."&#39;, ";
		$SPECIAL_classe_to_display = replaceLastOccurenceOfComa(substr($SPECIAL_classe_to_display, 0, -2), 'or');
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_several_classes']."\";" );
		echo $text;
		echo "<br /><br />";
	} ?>
	</li>
</ul>
</div>
<p>

<?php
if($idmulti != 0){
	echo "<br />\n";

	if ($continuous) echo $_SESSION['s_language']['continuous_reservation'];
	else if ($nbJours == 1) echo $_SESSION['s_language']['reservation_muliple_objets'];
	else echo $_SESSION['s_language']['periodic_reservation'];

	echo $_SESSION['s_language']['modify_all_reservations'];
	echo "<br /><br /><br />\n";
}


if (isset($_SERVER["HTTP_REFERER"])){
	$temp = $_SERVER["HTTP_REFERER"];
	if (strpos($temp, 'invitevalide.php') === FALSE) $where = "'$page_accueil', '_self'";
	else $where = "'index.php', '_parent'";
}
else $where = "'index.php', '_parent'";
?>


<?php echo $_SESSION['s_language']['password'];?>
	<input name='motdepasse' type='password' /> &nbsp;
	<input type='button' name='soumettre' value="<?php echo $_SESSION['s_language']['modify_submit'];?>"
	    onclick='javascript:return sComplet();' /> &nbsp;
	<input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	    onclick="javascript:window.open(<?php echo $where;?>);" />

	<input type='hidden' name='id' value='<?php echo $id;?>' />
	<input type='hidden' name='idmulti' value='<?php echo $idmulti;?>' />
	<input type='hidden' name='classeForICal' value='<?php echo addslashes(urlencode(serialize($classeForICal)));?>' />
	<input type='hidden' name='classe' value='<?php echo addslashes(urlencode(serialize($classe)));?>' />
	<input type='hidden' name='objet' value='<?php echo $objet;?>' />
	<input type='hidden' name='periodicity' value='<?php echo $periodicity;?>' />
	<input type='hidden' name='minDate' value='<?php echo $minDate;?>' />
	<input type='hidden' name='maxDate' value='<?php echo $maxDate;?>' />
<?php
if ($objetWithPriorityManagementExist != 1){?>
	<input type='hidden' name='priority' value='0' />
<?php
}
if ($objetWithWifiManagementExist != 1){?>
	<input type='hidden' name='wifi' value='0' />
<?php
}?>

</p>
</form>

<?php echo $body_end;?>
