<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File execvalide.php
*
* This file is used to execute the validation a reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only)	exit($exit_message_authentification);

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 10) || (count($_GET) != 0)) exitWrongSignature('execvalide.php');
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['idmulti'])){
	$idmulti = $_POST['idmulti'];
	if ( ! ctype_digit($idmulti)|| ($idmulti == '') ) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['multi'])){
	$multi = $_POST['multi'];
	if ( ! in_array($multi, array('oui', 'non')) ) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	if ( ! in_array($objet, getAvailableObjects()) && ($objet != '-1') ) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['motdepasse'])){
	$motdepasse= $_POST['motdepasse'];
	if (strlen($motdepasse) > 20) exitWrongSignature('execvalide.php');
	$motdepasse = database_real_escape_string($motdepasse);
	$motdepasse = htmlspecialchars($motdepasse);
} else exitWrongSignature('execvalide.php');
if (isset($_POST['titleObjects'])){
	$titleObjects = $_POST['titleObjects'];			// que pour l'envoi de mail
	$titleObjects = htmlspecialchars($titleObjects);
} else exitWrongSignature('execvalide.php');
if (isset($_POST['titleTypeAndDate'])){
	$titleTypeAndDate = $_POST['titleTypeAndDate'];		// que pour l'envoi de mail
	$titleTypeAndDate = htmlspecialchars($titleTypeAndDate);
} else exitWrongSignature('execvalide.php');
if (isset($_POST['nbJours'])){
	$nbJours = $_POST['nbJours'];
	if ( ! ctype_digit($nbJours)|| ($nbJours == '') ) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['selectedDate'])){
	$selectedDate = $_POST['selectedDate'];
	if ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $selectedDate)) exitWrongSignature('execvalide.php');
} else exitWrongSignature('execvalide.php');
if (isset($_POST['classeToValidate'])){
	$classeToValidate = unserialize(urldecode(stripslashes($_POST['classeToValidate'])));
	$availableClass = getAvailableClass();
	for ($i = 0 ; $i < count($classeToValidate) ; $i++){
		if ( ! in_array($classeToValidate[$i], $availableClass) ) exitWrongSignature('execvalide.php');
	}
} else exitWrongSignature('execvalide.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

if ($idmulti == 0) $DB_request_condition = "R.id=$id"; else $DB_request_condition = "R.idmulti=$idmulti";

$password_OK = false;
$listClassId = "";
$listAdminEmail = "";
$classNameOK = array();
$i = 0;
$DB_request = "SELECT DISTINCT A.pass, C.id, C.nom, A.mail FROM administrateur A, reservation R, objet O, classe C ";
$DB_request .= "WHERE R.idobjet = O.id AND R.valide = 0 AND O.id_classe = C.id AND A.id_classe = C.id AND R.state=0 AND ".$DB_request_condition;
if ($objet != -1) $DB_request .= " AND O.nom = '$objet'";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if ($motdepasse == database_get_from_object($row, 'pass')){
	    $password_OK = true;
	    $className = database_get_from_object($row, 'nom');
	    if ( ! in_array($className, $classNameOK)){
	        $listClassId .= "'".database_get_from_object($row, 'id')."',";
	        $classNameOK[$i] = $className;
	        $email = database_get_from_object($row, 'mail');
	        if (strpos($listAdminEmail, $email) === FALSE) $listAdminEmail .= $email.",";
	    }
	    $i++;
	}
}
$listClassId = substr($listClassId, 0, -1);
$listAdminEmail = substr($listAdminEmail, 0, -1);

echo $entete;
?>

<body style='font-size:small'>

<?php
if ($password_OK){
	/******************************************************************
	****                Case of RIGHT password                     ****
	*******************************************************************/

	if ($idmulti == 0) 	// case of reservations of a single object for only one day
	    $DB_request = "UPDATE reservation SET valide='1' WHERE id=$id AND state=0";
	else {	        // case of reservations of multiple objects for only one day or continuous reservations or periodic reservations
	    $DB_request = "SELECT R.id FROM reservation R, objet O, classe C WHERE R.idmulti=$idmulti AND R.idobjet = O.id AND R.state=0 AND ";
	    if ($objet == -1) $DB_request .= "O.id_classe = C.id AND C.id IN ($listClassId)";
	    else $DB_request .= "O.nom = '$objet'";
	    if ($multi == 'non') $DB_request .= " AND R.jour = '$selectedDate'";
	    $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $inter = "";
	    while ($row = database_fetch_object($resultat)){
	        $inter .= database_get_from_object($row, 'id').", ";
	    }
	    $inter = substr($inter, 0, -2);

	    $DB_request = "UPDATE reservation SET valide='1' WHERE id IN ($inter) AND state=0";
	}
	database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	/***********************************************************************************
	****                    Part email send                                        *****
	************************************************************************************/
	$DB_request = "SELECT DISTINCT email, titre, debut, duree, commentaire FROM reservation R WHERE R.state=0 AND ".$DB_request_condition;
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat);
	$to = database_get_from_object($row, 'email');
	$title = database_get_from_object($row, 'titre');
	$commentaire = database_get_from_object($row, 'commentaire');
        $debut = database_get_from_object($row, 'debut');
        $duree = database_get_from_object($row, 'duree');

	$sujet = $_SESSION['s_language']['execvalide_mail_subject']." $titleObjects, '$title'";
	$contenu  = $_SESSION['s_language']['execvalide_mail_title']."\n\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_subject']." ".strtolower($titleObjects)."\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_title']." $title\n";
	$contenu .= $_SESSION['s_language']['reservation_kind']." $titleTypeAndDate\n";

	// Pour Patricia
	//$contenu .= $_SESSION['s_language']['execresa_mail_start_time']." ".substr($debut,0,5)." \n";
	//$contenu .= $_SESSION['s_language']['execresa_mail_duration']." ".substr($duree,0,5)." \n";
	//$contenu .= $_SESSION['s_language']['execresa_mail_user']." $to\n";

	if ($commentaire != '') $contenu .= $_SESSION['s_language']['title_array12']." $commentaire\n";

	if ($idmulti == 0) $contenu .= "\n".$_SESSION['s_language']['execvalide_mail_done']."\n\n";
	else {
	    $DB_request = "SELECT count(*) AS nb FROM reservation WHERE valide = 0 AND idmulti=$idmulti AND state=0";
	    $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $row = database_fetch_object($resultat);
	    $nb = database_get_from_object($row, 'nb');
	    if ($nb == 0) $contenu .= "\n".$_SESSION['s_language']['execvalide_mail_done']."\n\n";
	    else $contenu .= "\n".$_SESSION['s_language']['execvalide_mail_done2']."\n\n";
	    if ($nbJours != 1){
	        if ($multi == 'non') $contenu .= $_SESSION['s_language']['execvalide_only_one_date']." ".displayCurrentLanguageDate($selectedDate, 0)."\n\n";
	        else $contenu .= $_SESSION['s_language']['execvalide_all']." \n\n";
	    }
	    if ($objet != -1) $contenu .= $_SESSION['s_language']['execvalide_only_one_object']." $objet\n\n";
	}

	$contenu .= "\n".sprintf($_SESSION['s_language']['execefface_new_resa'], WEB)."\n";
	$contenu = ereg_replace("\\\'","'",$contenu);
	$contenu = ereg_replace("\\\\\"","\"",$contenu);
	$from = "From: $listAdminEmail";
	$destinataire = str_replace($web_server, $mail_server, $to);
	$sendUser = mail($destinataire,$sujet,$contenu,$from);

	// Pour Patricia
	/*
	if (strpos($listAdminEmail, "resavoiture@lpnhep.in2p3.fr") !== FALSE){
		$destinataire = "autorisevoiture@lpnhep.in2p3.fr";
		$contenu = "PAPIERS A ETABLIR pour la demande suivante :\n\n".$contenu;
		$sendUser = mail($destinataire,$sujet,$contenu,$from);
	}
	*/

	/*******************************************************************
	****                Display in PHPMyResa                        ****
	********************************************************************/

	echo "<div>\n";
	echo "<span class='lblue'>".$_SESSION['s_language']['execvalide_done']."</span>\n";
	echo "<br /><br />\n";

	if ($sendUser) {
	    echo $_SESSION['s_language']['execefface_notification']." ".$to;
	} else {
	    echo $_SESSION['s_language']['execefface_notification_false']." ".$to." ".$_SESSION['s_language']['execefface_notification_false_2'];
	}
	echo "<br /><br /><br />\n";
	echo "<a href='vueMois.php'>".$_SESSION['s_language']['invitevalide_planning']."</a>\n";
	echo "</div>\n";

} else {
	/*******************************************************************
	****                Case of WRONG password	                    ****
	********************************************************************/

	echo "<div>\n";
	echo "<span class='bred'>".$_SESSION['s_language']['incorrect_password']."</span>\n";
	echo "<p>\n";

	echo "<b>".$_SESSION['s_language']['validate_ask']."</b> ";

	if (count($classeToValidate) == 1){
		$SPECIAL_classe_to_display = $classeToValidate[0];
		$text = $_SESSION['s_language']['ask_admin_password_for_one_class'];
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_one_class']."\";" );
		echo $text;
	} else{
		$SPECIAL_classe_to_display = "";
		for ($i = 0 ; $i < count($classeToValidate) ; $i++) $SPECIAL_classe_to_display .= "&#39;".$classeToValidate[$i]."&#39;, ";
		$SPECIAL_classe_to_display = replaceLastOccurenceOfComa(substr($SPECIAL_classe_to_display, 0, -2), 'or');
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_several_classes']."\";" );
		echo $text;
		echo "<br />\n(".$_SESSION['s_language']['validation_multiple_objets'].")";
	}
?>
	    </p>
	    <br />

	    <form id='executeValide' action='execvalide.php' method='post'>
	    <p>
	        <?php echo $_SESSION['s_language']['password'];?> <input type='password' name='motdepasse' value='' /> &nbsp;
	        <input type='hidden'name='multi' value='<?php echo $multi;?>' />
	        <input type='hidden' name='id' value='<?php echo $id;?>' />
	        <input type='hidden' name='idmulti' value='<?php echo $idmulti;?>' />
	        <input type='hidden' name='objet' value='<?php echo $objet;?>' />
	        <input type='hidden' name='titleObjects' value='<?php echo $titleObjects;?>' />
	        <input type='hidden' name='titleTypeAndDate' value='<?php echo $titleTypeAndDate;?>' />
	        <input type='hidden' name='nbJours' value='<?php echo $nbJours;?>' />
	        <input type='hidden' name='selectedDate' value='<?php echo $selectedDate;?>' />
	        <input type='hidden' name='classeToValidate' value='<?php echo addslashes(urlencode(serialize($classeToValidate)));?>' />
	        <input type='submit' value="<?php echo $_SESSION['s_language']['valide_submit'];?>" /> &nbsp;
	        <input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	            onclick="javascript:window.open('index.php', '_parent')" />
	    </p>
	    </form>
	</div>

<?php
}

echo $body_end;
?>
