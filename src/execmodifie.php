<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File execmodifie.php
*
* This file is used to execute modification on some fields of a reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only) exit($exit_message_authentification);

if ($iCal) require_once('wiCal.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
/*****************************************************************************
	20 pour les rÃ©servations rÃ©pÃ©titives de pÃ©riodicitÃ© indÃ©finie
	21 pour les rÃ©servations ponctuelles
	24 pour les rÃ©servations continues ou rÃ©pÃ©titives de pÃ©riodicitÃ© dÃ©finie
*****************************************************************************/
if ( (($nbPost != 20) && ($nbPost != 21) && ($nbPost != 24)) || (count($_GET) != 0)) exitWrongSignature('execmodifie.php');
if ($nbPost == 21) $singleDay = true; else $singleDay = false;
$availableClass = getAvailableClass();
if (isset($_POST['email'])){
	$email = $_POST['email'];
	if ( ! validate_email($email)) exitWrongSignature('execmodifie.php');
	if (strlen($email) > 128) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['titre'])){
	$titre = $_POST['titre'];
	if (strlen($titre) > 64) exitWrongSignature('execmodifie.php');
	$titre = database_real_escape_string($titre);
	$titre = htmlspecialchars($titre);
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['commentaire'])){
	$commentaire = $_POST['commentaire'];
	if (strlen($commentaire) > 255) exitWrongSignature('execmodifie.php');
	$commentaire = database_real_escape_string($commentaire);
	$commentaire = htmlspecialchars($commentaire);
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['priority'])){
	$priority = $_POST['priority'];
	if ( ! in_array($priority, array('0', '1')) ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['wifi'])){
	$wifi = $_POST['wifi'];
	if ( ! in_array($wifi, array('0', '1')) ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['idmulti'])){
	$idmulti = $_POST['idmulti'];
	if ( ! ctype_digit($idmulti) || ($idmulti == '') ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['motdepasse'])){
	$motdepasse = $_POST['motdepasse'];
	if (strlen($motdepasse) > 20) exitWrongSignature('execmodifie.php');
	$motdepasse = database_real_escape_string($motdepasse);
	$motdepasse = htmlspecialchars($motdepasse);
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['classeForICal'])){
	$classeForICal = unserialize(urldecode(stripslashes($_POST['classeForICal'])));
	for ($i = 0 ; $i < count($classeForICal) ; $i++){
		if ( ! in_array($classeForICal[$i], $availableClass) ) exitWrongSignature('execmodifie.php');
	}
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['classe'])){
	$classe = unserialize(urldecode(stripslashes($_POST['classe'])));
	for ($i = 0 ; $i < count($classe) ; $i++){
		if ( ! in_array($classe[$i], $availableClass) ) exitWrongSignature('execmodifie.php');
	}
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['periodicity'])){
	$periodicity = $_POST['periodicity'];
	if ( ! in_array($periodicity, array('0', '1', '7', '14', '-1')) ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if ($periodicity != 0){
	if (isset($_POST['debut'])){
		$debut = $_POST['debut'];
		if (strlen($debut) > 2) exitWrongSignature('execmodifie.php');
		if ( ! ctype_digit($debut) || ($debut == '') ) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	if (isset($_POST['midebut'])){
		$midebut = $_POST['midebut'];
		if (strlen($midebut) > 2) exitWrongSignature('execmodifie.php');
		if ( ! ctype_digit($midebut) || ($midebut == '') ) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	if (isset($_POST['duree'])){
		$duree = $_POST['duree'];
		if (strlen($duree) > 2) exitWrongSignature('execmodifie.php');
		if ( ! ctype_digit($duree) || ($duree == '') ) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	if (isset($_POST['miduree'])){
		$miduree = $_POST['miduree'];
		if (strlen($miduree) > 2) exitWrongSignature('execmodifie.php');
		if ( ! ctype_digit($miduree) || ($miduree == '') ) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	$debut_ini = $debut;
	$midebut_ini = $midebut;
	$duree_ini = $duree;
	$miduree_ini = $miduree;
}
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	$tab = createObjectList($objet);
	$tmp_tab = $tab[0];
	$nbObjets = $tab[1];
	$objets_list = $tab[2];
	$objetDisplay = $tab[3];
	$availableObjects = getAvailableObjects();
	for ($i = 0 ; $i < count($tmp_tab) ; $i++){
		if ( ! in_array($tmp_tab[$i], $availableObjects) ) exitWrongSignature('calendrier.php');
	}
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['aa1'])){
	$aa1 = $_POST['aa1'];
	if (strlen($aa1) > 4) exitWrongSignature('execmodifie.php');
	if ( ! ctype_digit($aa1) || ($aa1 == '') ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['mm1'])){
	$mm1 = $_POST['mm1'];
	if ( ! ctype_digit($mm1) || ($mm1 == '') ) exitWrongSignature('execmodifie.php');
	if ($mm1 > 12) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['jj1'])){
	$jj1 = $_POST['jj1'];
	if ( ! ctype_digit($jj1) || ($jj1 == '') ) exitWrongSignature('execmodifie.php');
	if ($jj1 > 31) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['minDate'])){
	$minDate = $_POST['minDate'];
	if ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $minDate)) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if (isset($_POST['maxDate'])){
	$maxDate = $_POST['maxDate'];
	if ( ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $maxDate)) && ($maxDate != "") ) exitWrongSignature('execmodifie.php');
} else exitWrongSignature('execmodifie.php');
if ( ! $singleDay){
	if (isset($_POST['aa2'])){
		$aa2 = $_POST['aa2'];
		if ( ! ctype_digit($aa2)|| ($aa2 == '')  ) exitWrongSignature('execmodifie.php');
		if (strlen($aa2) > 4) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	if (isset($_POST['mm2'])){
		$mm2 = $_POST['mm2'];
		if ( ! ctype_digit($mm2) || ($mm2 == '') ) exitWrongSignature('execmodifie.php');
		if ($mm2 > 12) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
	if (isset($_POST['jj2'])){
		$jj2 = $_POST['jj2'];
		if ( ! ctype_digit($jj2) || ($jj2 == '') ) exitWrongSignature('execmodifie.php');
		if ($jj2 > 31) exitWrongSignature('execmodifie.php');
	} else exitWrongSignature('execmodifie.php');
}

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

echo $entete;
?>

<body style='font-size:small'>

<?php
if ($periodicity != 0){
	$idebut = $debut;
	if ($midebut == "30") $idebut = $idebut + .5;
	$debut = fillWithZero($debut).":".$midebut.":00";

	$iduree = $duree;
	if ($miduree == "30") $iduree = $iduree + .5;
	$duree = fillWithZero($duree).":".$miduree.":00";

	$ifin = $idebut + $iduree;
	if (($periodicity != -1) || $singleDay)	    // Cas des rÃ©servations non continues
	    if ($ifin>24) exit("<p class='red' style='font-size:large'>".$_SESSION['s_language']['execresa_error']."</p>");

	$date = makeDateWithZero($aa1, $mm1, $jj1);
}

if ($idmulti == 0) $DB_request_condition = "R.id=$id"; else $DB_request_condition = "R.idmulti=$idmulti";

$password_OK = false;
$DB_request = "SELECT DISTINCT A.pass AS adminpass, R.pass FROM administrateur A, reservation R, objet O, classe C WHERE R.idobjet = O.id AND O.id_classe = C.id AND A.id_classe = C.id AND ";
$DB_request .= $DB_request_condition;
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if (($motdepasse == database_get_from_object($row, 'pass')) || ($motdepasse == database_get_from_object($row, 'adminpass'))) $password_OK = true;
}

if ($password_OK){

	if ($singleDay){
	    // Cas des rÃ©servations sur 1 jour (un ou plusieurs objets)

	    /******************************************************************
	    ****                    Check if time available                ****
	    *******************************************************************/
	    $tab = check_time_available_on_single_reservation($date, $objets_list, $idebut, $ifin, " AND NOT ($DB_request_condition)");
	    $compteur = $tab[0];
	    if ($compteur != 0){
	        $message = $tab[1];
	        if ($compteur == 1) $messageToSend = $_SESSION['s_language']['execresa_conflict']."</span> $message";
	        else $messageToSend = $_SESSION['s_language']['execresa_conflicts']."</span> $message";
	        exit("<p class='red' style='font-size:large'>$messageToSend</p>");
	    }

	    /******************************************************************
	    ****                    UPDATE in table reservation            ****
	    *******************************************************************/
	    // this line is due to the impossibility to use alias for table name in UPDATE sql request in MySQL 3.xx
	    if ($idmulti == 0) $DB_request_condition = "id=$id"; else $DB_request_condition = "idmulti=$idmulti";
	    $DB_request = "UPDATE reservation ";
	    $DB_request .= "SET email='$email', titre='$titre', commentaire='$commentaire', debut='$debut', duree='$duree', priority='$priority', wifi='$wifi' ";
	    $DB_request .= " WHERE $DB_request_condition AND state=0";
	    database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    ?>
	<div><br />
	    <span class='lblue'><?php echo $_SESSION['s_language']['execmodifie_done'];?></span>
	    <br /><br /><br />
	    <a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
	</div>
<?php
	    if ($iCal){
	        for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
	    }

	} else if ($periodicity == 0) {
	    // Cas des rÃ©servations pour lesquelles par la pÃ©riodicitÃ© n'a pas pu Ãªtre trouvÃ©e

	    /******************************************************************
	    ***	        UPDATE in table reservation	    ***
	    *******************************************************************/
	    // this line is due to the impossibility to use alias for table name in UPDATE sql request in MySQL 3.xx
	    if ($idmulti == 0) $DB_request_condition = "id=$id"; else $DB_request_condition = "idmulti=$idmulti";
	    $DB_request = "UPDATE reservation ";
	    $DB_request .= "SET email='$email', titre='$titre', commentaire='$commentaire', priority='$priority', wifi='$wifi' ";
	    $DB_request .= " WHERE $DB_request_condition AND state=0";
	    database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    ?>
	<div><br />
	    <span class='lblue'><?php echo $_SESSION['s_language']['execmodifie_done'];?></span>
	    <br /><br /><br />
	    <a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
	</div>
<?php
	    if ($iCal){
	        for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
	    }

	} else {
	    // Cas des rÃ©servations rÃ©pÃ©titives et continues - ne sont mises Ã  jour que s'il n'y a aucun conflit

	    /*
	        $periodicity = 1, 7 or 14 ( or -1 for continuous reservations)
	        $date is the departure date in format YYYY-MM-DD
	        $datefintxt is the arrival date in format YYYY-MM-DD
	    */

	    $datefintxt = makeDateWithZero($aa2, $mm2, $jj2);

	    /******************************************************************
	    ****                Check if time available                    ****
	    *******************************************************************/
	    $tab = check_time_available_on_multiple_reservation($periodicity, $date, $datefintxt, $objets_list, $idebut, $ifin, " AND NOT ($DB_request_condition)");
	    $nbJoursOK = $tab[0];
	    $nbJoursConflit = $tab[1];
	    $nbConflit = $tab[2];
	    $message = $tab[3];

	    /******************************************************************
	    ****                Display in PHPMyResa	                   ****
	    *******************************************************************/
	    $departureDepart = displayCurrentLanguageDate($date, 0);
	    $arrivalDate = displayCurrentLanguageDate($datefintxt, 0);
	    ?>
	<div><br />
	    <span style='font-size:large'><?php echo $objetDisplay;?> <?php echo $_SESSION['s_language']['execresa_from'];?> <?php echo $departureDepart;?> <?php echo $_SESSION['s_language']['execresa_to'];?> <?php echo $arrivalDate;?></span>
	    <br />
	    <i><?php echo stripslashes($titre);?></i>
	    <br />
<?php
	    if ($periodicity == 1) {
	        echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_1day']."<br />";
	    } else if ($periodicity == 7) {
	        echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_1week']."<br />";
	    } else if ($periodicity == 14) {
	        echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_2week']."<br />";
	    } else if ($periodicity == -1) {
	        echo $_SESSION['s_language']['reservation_continue']."<br />";
	    }
	    if ($periodicity == -1){
//debug?	    periode=-1<br />
	        echo $_SESSION['s_language']['title_array3'].$_SESSION['s_language']['ponctuation']." ".(int)substr($debut,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($debut,3,2)." ".$_SESSION['s_language']['reservation_minute']." (".$departureDepart.")<br />";
	        echo $_SESSION['s_language']['title_array15'].$_SESSION['s_language']['ponctuation']." ".(int)substr($duree,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($duree,3,2)." ".$_SESSION['s_language']['reservation_minute']." (".$arrivalDate.")<br />";
	    } else {
//debug?	    periode >1<br />
	        echo $_SESSION['s_language']['execresa_mail_start_time']." ".(int)substr($debut,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($debut,3,2)." ".$_SESSION['s_language']['reservation_minute']." - ";
	        echo $_SESSION['s_language']['execresa_mail_duration']." ".(int)substr($duree,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($duree,3,2)." ".$_SESSION['s_language']['reservation_minute'];
	        echo "<br />";
	    } ?>

	    <br />
	    <table border=0 cellpadding=3>
<?php
	    $color = "#eeeeee";
	    for($i = 0; $i < $nbConflit; $i++) {?>
	        <tr style='background-color:<?php echo $color;?>'>
<?php
	        if ($nbObjets != 1) {?>
	            <td><?php echo $message[$i][0];?></td>
<?php
	        }?>
	            <td> <?php echo $message[$i][1];?></td>
	            <td> <?php echo $message[$i][2];?></td>
	            <td>
	                <span class='bred'> <?php echo $_SESSION['s_language']['execresa_not_available'];?></span>
	                <i>(<?php echo $message[$i][3];?>)</i>
	            </td>
	        </tr>
<?php
	        if ($color == "#eeeeee") $color="#d3dce3"; else $color="#eeeeee";
	    }?>
	    </table>
<?php
	    if ($nbConflit > 0) {
	        echo $_SESSION['s_language']['execresa_nb_possible']." :";
	        echo "<span class='bblue'>".$nbJoursOK."</span>,";
	        echo $_SESSION['s_language']['execresa_nb_not_possible']." :";
	        echo "<span class='bred'>".$nbJoursConflit."</span>";
	        if ($nbObjets != 1){
	            if ($nbConflit == 1) {
	                echo "(".$nbConflit." ".$_SESSION['s_language']['execresa_conflict2'].")";
	            } else {
	                echo "(".$nbConflit." ".$_SESSION['s_language']['execresa_conflict3'].")";
	            }
	        }
	    } else {
	        echo $_SESSION['s_language']['execresa_nb_OK']." : <span class='bblue'>".$nbJoursOK."</span>";
	        echo "<br /><br />";
	    }

	    if ($nbJoursConflit != 0){
	    echo "<br />".$_SESSION['s_language']['execmodifie_not_allowed']."<br />";
	    ?>
	    <input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	        onclick="javascript:window.open('<?php echo $page_accueil;?>', '_self')" />
<?php
	    } else {
	        /*******************************************************************
	        ****          Cas oÃ¹ la modification est possible	            ****
	        ********************************************************************/
	        if ( ($minDate <= $date) && ($maxDate >= $datefintxt) ){
	            /******************************************************************
	            ****          Cas sans modification des identifiants	       ****
	            *******************************************************************/
	            // On commence par supprimer les rÃ©servations 'externes' si nÃ©cessaire
	            $DB_request = "UPDATE reservation SET state = 1 WHERE idmulti=$idmulti AND (jour < '$date' OR jour > '$datefintxt')";
	            $result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	            // On effectue ensuite la mise Ã  jour
	            if ($periodicity == -1){
	                // Dans le cas des rÃ©servations continues, seules les rÃ©servations des jours de dÃ©but et de fin sont Ã  modifier pour les heures
	                $DB_request = "UPDATE reservation ";
	                $DB_request .= "SET email='$email', titre='$titre', commentaire='$commentaire', priority='$priority', wifi='$wifi' WHERE idmulti=$idmulti";
	                database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	                $dureeContinuous = 24-$idebut;
	                if ($midebut=="30"){
	                    $dureeContinuous = $dureeContinuous - 0.5;
	                    $dureeContinuous = fillWithZero($dureeContinuous);
	                    $dureeContinuous .= ":30:00";
	                } else{
	                    $dureeContinuous = fillWithZero($dureeContinuous);
	                    $dureeContinuous .= ":00:00";
	                }
	                $DB_request = "UPDATE reservation ";
	                $DB_request .= "SET debut='$debut', duree='$dureeContinuous' WHERE idmulti=$idmulti AND jour = '$date'";
	                database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	                $DB_request = "UPDATE reservation ";
	                $DB_request .= "SET debut='00:00:00', duree='$duree' WHERE idmulti=$idmulti AND jour = '$datefintxt'";
	                database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	            } else {
	                $DB_request = "UPDATE reservation ";
	                $DB_request .= "SET email='$email', titre='$titre', commentaire='$commentaire', debut='$debut', duree='$duree', priority='$priority', wifi='$wifi' ";
	                $DB_request .= " WHERE idmulti=$idmulti AND state=0";
	                database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	            }
	            ?>

	    <span class='lblue'><?php echo $_SESSION['s_language']['execmodifie_done'];?></span>
	    <br /><br /><br />
	    <a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>

<?php
	            if ($iCal){
	                for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
	            }
	        } else {
	            /******************************************************************
	            ****            Cas avec modification des identifiants         ****
	            *******************************************************************/
	            // On ne fait rien !
?>
	    <span class='lblue'><?php echo $_SESSION['s_language']['execmodifie_extension'];?></span>
	    <br /><br /><br />
	    <a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
<?php
	        }
	    }?>
	</div>
<?php
	}
} else {
	/***************************************************************************
	****                Case of WRONG password                              ****
	****************************************************************************/

	$emailCrypte = PHPcrypt($email);
//	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<input name='email' type='hidden' value='\", \"$emailCrypte\", \"' />\")</script>";
?>
<div>
	<span class='bred'> <?php echo $_SESSION['s_language']['incorrect_password'];?></span>
	<br /><br />
	<?php
	echo "<b>".$_SESSION['s_language']['modify_ask']."</b>";
	echo "<ul>\n";
		echo "<li>".$_SESSION['s_language']['ask_user_password']."</li>\n";
		echo "<li>".$_SESSION['s_language']['or']." ";
		if (count($classe) == 1){
			$SPECIAL_classe_to_display = $classe[0];
			$text = $_SESSION['s_language']['ask_admin_password_for_one_class'];
			eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_one_class']."\";" );
			echo $text;
		} else{
			$SPECIAL_classe_to_display = "";
			for ($i = 0 ; $i < count($classe) ; $i++) $SPECIAL_classe_to_display .= "&#39;".$classe[$i]."&#39;, ";
			$SPECIAL_classe_to_display = replaceLastOccurenceOfComa(substr($SPECIAL_classe_to_display, 0, -2), 'or');
			eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_several_classes']."\";" );
			echo $text;
			echo "<br /><br />";
		} ?>
		</li>
	</ul>
</div>


<form id='executeModifie' action='execmodifie.php' method='post'>
<div>
	<?php echo $_SESSION['s_language']['password'];?> <input name='motdepasse' type='password' /> &nbsp;

	<script type='text/javascript'>
	<!--
	    decrypt("<input type='hidden' name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	//-->
	</script>

	<input type='hidden' name='titre' value='<?php echo $titre;?>' />
	<input type='hidden' name='commentaire' value='<?php echo $commentaire;?>' />
	<input type='hidden' name='priority' value='<?php echo $priority;?>' />
	<input type='hidden' name='id' value='<?php echo $id;?>' />
	<input type='hidden' name='idmulti' value='<?php echo $idmulti;?>' />
	<input type='hidden' name='wifi' value='<?php echo $wifi;?>' />
	<input type='hidden' name='classeForICal' value='<?php echo addslashes(urlencode(serialize($classeForICal)));?>' />
	<input type='hidden' name='classe' value='<?php echo addslashes(urlencode(serialize($classe)));?>' />
<?php
	if ($periodicity != 0){?>
	<input type='hidden' name='debut' value='<?php echo $debut_ini;?>' />
	<input type='hidden' name='midebut' value='<?php echo $midebut_ini;?>' />
	<input type='hidden' name='duree' value='<?php echo $duree_ini;?>' />
	<input type='hidden' name='miduree' value='<?php echo $miduree_ini;?>' />
<?php
	}?>
	<input type='hidden' name='objet' value='<?php echo $objet;?>' />
	<input type='hidden' name='periodicity' value='<?php echo $periodicity;?>' />
	<input type='hidden' name='aa1' value='<?php echo $aa1;?>' />
	<input type='hidden' name='mm1' value='<?php echo $mm1;?>' />
	<input type='hidden' name='jj1' value='<?php echo $jj1;?>' />
	<input type='hidden' name='minDate' value='<?php echo $minDate;?>' />
	<input type='hidden' name='maxDate' value='<?php echo $maxDate;?>' />
<?php
	if ( ! $singleDay){?>
	<input type=hidden name='aa2' value='<?php echo $aa2;?>' />
	<input type=hidden name='mm2' value='<?php echo $mm2;?>' />
	<input type=hidden name='jj2' value='<?php echo $jj2;?>' />
<?php
	}?>
	<input type='submit' value="<?php echo $_SESSION['s_language']['modify_submit'];?>" /> &nbsp;
	<input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	    onclick="javascript:window.open('index.php', '_parent')" />
</div>
</form>

<?php
}


echo $body_end;
?>
