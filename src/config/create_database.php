<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/create_database.php
*
* This file enables to create the PHPMyResa database items according to the ../doc/INSTALL_creation_db_script.php file
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 0) || (count($_GET) != 0)) exitWrongSignature("config/create_database.php");

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

function display_if_fill_languages(){
	global $txt_nbRows_languages_empty, $txt_nbRows_languages_not_empty, $txt_fill_languages_button, $txt_titre_languages;

	$message = display(" &nbsp; ");

	$tab = array();
	$tab = calculate_number_of_rows_on_table_language();
	$temp = $tab[1];
	if ($temp == ""){
		$nbRows = $tab[0];
		if ($nbRows == 0) $message .= display("$txt_nbRows_languages_empty<input type='submit' value=\"$txt_fill_languages_button\" />");
		else $message .= display("$txt_nbRows_languages_not_empty<input type='submit' value=\"$txt_titre_languages\" />");
	} else $message .= $temp;

	return $message;
}

echo $entete;
echo sprintf($entete2, $txt_titre_create_database);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") exit($message_redirect_configure_database);
$connexionDB = $tab[1];

$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message != "") exit($error_message);

if ($tab2[0] == 0){
	$_SESSION['config'] = 1;
	require_once("../doc/INSTALL_creation_db_script.php");
	$temp = strtolower($database);
	$script = $$temp;
	$script = strip_tags(str_replace('&nbsp;','' , $script));
	$DB_request_tab = explode('CREATE', $script);
	$message = "";
	for ($i = 1 ; $i < count($DB_request_tab) ; $i++){
		$DB_request = trim("CREATE ".$DB_request_tab[$i]);
		if (substr($DB_request, -1) == ";") $DB_request = substr($DB_request, 0, -1);
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			$message .= display_error($DB_request);
			$message .= display(" &nbsp; ");
		}
	}
	if ($message == ""){
		echo "<form id='fill_languages' method='post' action='fill_languages.php'>\n";
		echo "<table class='center'>\n";
		echo display($txt_create_db_correct);
		echo display_if_fill_languages();
		echo "</table>\n";
		echo "</form>\n";
	} else{
		echo "<table class='center'>\n";
		echo display($txt_create_db_incorrect);
		echo display(" &nbsp; ");
		echo display(" &nbsp; ");
		echo $message;
		echo "</table>\n";
	}
} else {
	echo "<form id='fill_languages' method='post' action='fill_languages.php'>\n";
	echo "<table class='center'>\n";
	echo display($txt_no_need_to_create_db);
	echo display_if_fill_languages();
	echo "</table>\n";
	echo "</form>\n";
}

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>

