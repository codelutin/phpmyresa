<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File	config/fill_languages.php
*
* This file enables to fill in data into the languages database table according to the ../doc/INSTALL_data_db.sql file
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ( ( ($nbPost != 0) && ($nbPost != 1) ) || (count($_GET) != 0)) exitWrongSignature("config/fill_languages.php");
// $_POST n'est pas utilisé !

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

$dataString_file = "../doc/INSTALL_data_db.sql";

echo $entete;
if (isset($_SESSION['config'])) echo sprintf($entete2, $txt_titre_languages." - <i>$txt_installation</i>");
else if (isset($_SESSION['upgrade'])) echo sprintf($entete2, $txt_titre_languages." - <i>$txt_upgrade</i>");
else echo sprintf($entete2, $txt_titre_languages);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") exit($message_redirect_configure_database);
$connexionDB = $tab[1];

$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message != "") exit($error_message);
if ($tab2[0] == 0) exit($message_redirect_create_database);

$tab3 = verify_database_structure();
$error_message = $tab3[0];
if ($error_message != "") exit($error_message);
$error_message = $tab3[1];
if ($error_message != "") exit($error_message);

$tab = array();
$tab = calculate_number_of_rows_on_table_language();
$error_message = $tab[1];
if ($error_message != "") exit($error_message);
$nbRows = $tab[0];

if ( ($nbPost == 0) && ($nbRows != 0) && ( ! isset($_SESSION['upgrade'])) ){

	/*********************************************************************************/
	/****						 			      ****/
	/****				Affichage des erreurs 			      ****/
	/****						 			      ****/
	/*********************************************************************************/
	echo "<table class='center'>\n";
	echo "<tr><td>\n";
	echo "<table class='special' style='width:100%'>\n";
	echo "<tr><td class='titre'>$txt_summary2</td></tr>\n";
	echo "<tr><td>$txt_no_db_structure_pb</td></tr>\n";

	$encounred_problem = false;

	/* -------------------------------------------------------------------------------
		On récupère la liste des langues disponibles dans la base de données
	------------------------------------------------------------------------------- */
	$temp = getAvailableLanguagesFromDB();
	$pb = $temp[0];
	if ($pb != ""){
		echo $pb;
		$encounred_problem = true;
		//on donne des valeurs par défaut
		$tab_languages = array();
		$tab_languages[0] = "french";
		$tab_languages[1] = "english";
	} else $tab_languages = $temp[1];
	$nb_tab_languages = count($tab_languages);

	/* -------------------------------------------------------------------------------
		 Vérifier qu'il n'y a pas de traduction vide
	------------------------------------------------------------------------------- */
	for ($i = 0; $i < $nb_tab_languages; $i++){
		$DB_request = "SELECT count(*) AS nb FROM languages WHERE $tab_languages[$i] = '' OR $tab_languages[$i] IS NULL";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else{
			$row = database_fetch_object($resultat);
			$nb = database_get_from_object($row, 'nb');
			if ($nb != 0){
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_missing_translation, $nb, $tab_languages[$i]));
				$encounred_problem = true;
			}
		}
	}

	if ( ! $encounred_problem) echo display_left($txt_db_languages_content_ok);

	echo "</table>\n";
	echo "</td></tr>\n";
	echo "</table>\n\n";

	/*********************************************************************************/
	/****						 			      ****/
	/****				Demande de mise à jour	 		      ****/
	/****						 			      ****/
	/*********************************************************************************/

	echo "<form id='fill_languages' method='post' action='fill_languages.php'>\n";
	echo "<table class='center'>\n";
	echo display(" &nbsp; ");
	echo display(" &nbsp; ");
	echo display(sprintf($txt_fill_languages_question1, $nbRows));
	echo display($txt_fill_languages_question4);
	if ( ($tab = @file($dataString_file)) === FALSE ) echo display(sprintf($txt_file_pb, $dataString_file));
	echo display(sprintf($txt_fill_languages_question5, count($tab)));
	echo display("");
	echo "<tr><td>\n";
	echo "<table class='center'>\n";
	echo display("<input type='hidden' name='test' value='1' /><input type='button' value=\"$txt_no\" onclick=\"javascript:window.location='./index.php'\" /> &nbsp; &nbsp; <input type='submit' value=\"$txt_yes\" />");
	echo display(" &nbsp; ");
	echo display(" &nbsp; ");
	echo display("<i>$txt_notice_session_deletion</i>");
	echo "</table>\n";
	echo "</td></tr>\n";
	echo "</table>\n";
	echo "</form>\n";
} else{
	/*********************************************************************************/
	/****						 			      ****/
	/****				On effectue la mise à jour 		      ****/
	/****						 			      ****/
	/*********************************************************************************/
	$tab = array();
	echo "<table class='center'>\n";
	if ( ($tab = @file($dataString_file)) === FALSE ) echo display(sprintf($txt_file_pb, $dataString_file));
	else{
		// Suppression de l'ancien contenu de la table languages
		$DB_request = "delete from languages where name not like 'resa_mandatory_priority_%'";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result) echo display_error($DB_request);
		else{
			echo display($txt_suppress_data_table_languages);
			echo display(" &nbsp; ");

			// Insertion du contenu du fichier INSTALL_data_db.sql
			$pb = false;
			$nb = 0;
			for ($i = 0 ; $i < count($tab) ; $i++){
				$DB_request = trim($tab[$i]);
				if (substr($DB_request, -1) == ";") $DB_request = substr($DB_request, 0, -1);
				$result = database_query($DB_request, $connexionDB);
				if ( ! $result){
					echo display(sprintf($txt_insert_incorrect_data_table_languages, $nb, count($tab)));
					echo display_error($DB_request);
					$pb = true;
					break;
				}
				$nb++;
			}
			if ( ! $pb) echo display("$txt_insert_data_table_languages");
			echo display(" &nbsp; ");

			if ( isset($_SESSION['config']) || isset($_SESSION['upgrade']) ){
				echo "<tr><td class='centre'><input type='button' value='$txt_suiv' onclick=\"document.location.href='configure_main.php'\" /></td></tr>";
			} else {
				// Suppression des fichiers de session
				$session_path = session_save_path();
				if ($session_path == "") $session_path = "/tmp";
				$message = deletion_of_session_files($session_path, true);
				if ($message == "") echo display("$txt_titre_session$txt_ok");
				else{
					echo display("$txt_titre_session$txt_error");
					echo $message;
				}
			}
		}
	} echo "</table>\n";
}

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>
