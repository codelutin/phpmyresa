<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/ajax_verify_file.php
*
* This file verify the existence of a folder or a file, called by configure_annexe.php and configure_main.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2007,2008 Frédéric Melot
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
require_once('../commun/some_functions.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 2) || (count($_GET) != 0) ) exit('-1');

if (isset($_POST['file'])) $file = $_POST['file']; else exit('-1');
if (isset($_POST['method'])){
	$method = $_POST['method'];
	if ( ! in_array($method, array("locale", "distante", "locale2"))) exit('-1');
} else exit('-1');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

if ($method == 'locale'){
	if ( ! file_exists($file)) exit('1');
	else if ( ! is_readable($file)) exit('2');
	else exit('0');
} else if ($method == 'distante'){
	if ( ! does_file_exists($file)) exit('1');
	else exit('0');
} else  if ($method == 'locale2'){
	if ( ! file_exists($file)) exit('1');
	else if ( ! is_readable($file)) exit('2');
	else if ( ! is_writable($file)) exit('3');
	else exit('0');
}

?>
