<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/configure_database_access.php
*
* This file enables to configure the PHPMyResa database access
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);
if ( ! isset($database)) $database = "MySQL";
if ( ! defined("HOST")) define('HOST',"");
if ( ! defined("USER")) define('USER',"");
if ( ! defined("PWD")) define('PWD',"");
if ( ! defined("BD")) define('BD',"");
if ($database == '') $database = "MySQL";

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ( ( ($nbpar != 0) && ($nbpar != 6) && ($nbpar != 5) ) || (count($_GET) != 0)) exitWrongSignature("config/configure_database_access.php");
if (isset($_POST['database_type'])) $database_type = $_POST['database_type']; else $database_type = $database;
if (isset($_POST['hostname'])) $hostname = $_POST['hostname']; else $hostname = HOST;
if (isset($_POST['username'])) $username = $_POST['username']; else $username = USER;
if (isset($_POST['password'])) $password = $_POST['password']; else $password = PWD;
if (isset($_POST['database_name'])) $database_name = $_POST['database_name']; else $database_name = BD;
if (isset($_POST['verif'])) $verif = $_POST['verif']; else $verif = 'off';

if ( ! in_array($database_type, array('MySQL', 'PostgreSQL', 'Oracle'))) exitWrongSignature("config/configure_database_access.php");
if ( ! in_array($verif, array('on', 'off'))) exitWrongSignature("config/configure_database_access.php");
$hostname = htmlspecialchars($hostname);
$username = htmlspecialchars($username);
$password = htmlspecialchars($password);
$database_name = htmlspecialchars($database_name);

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$db_extension_exist = false;
if ( extension_loaded('mysql') || extension_loaded('pgsql') || extension_loaded('oci8')) $db_extension_exist = true;

$tr1ContentOracle = "<td class='droite2'>$txt_hostname</td><td><span class='red'>*</span>&nbsp;&nbsp;</td><td class='gauche'>";
$tr1ContentOracle .= "<textarea name='hostname' cols=33 rows=10>$hostname</textarea></td>";

$tr1ContentNotOracle = "<td class='droite2'>$txt_hostname</td><td><span class='red'>*</span>&nbsp;&nbsp;</td><td class='gauche'>";
$tr1ContentNotOracle .= "<input name='hostname' size='40' value='$hostname' tabindex='2' /></td>";

$tr2ContentOracle = "";
$tr2ContentNotOracle = "<td class='droite2'>$txt_database_name</td><td><span class='red'>*</span>&nbsp;&nbsp;</td><td class='gauche'><input name='database_name' size='40' value='$database_name' tabindex='5' /></td>"; // Do not fill for Oracle

if ($database_type == "Oracle") {
	$tr1Content = $tr1ContentOracle;
	$tr2Content = $tr2ContentOracle;
} else {
	$tr1Content = $tr1ContentNotOracle;
	$tr2Content = $tr2ContentNotOracle;
}

echo $entete;
echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";
?>

	<script type='text/javascript'><!--
	function sComplet(){
		if ((document.forms.database.hostname.value == '') || (document.forms.database.username.value == '') || (document.forms.database.password.value == '')){
			alert('<?php echo $txt_formulaire_incomplet;?>');
			return false;
		}
		<?php if ($database_type != 'Oracle'){?>
		if (document.forms.database.database_name.value == ''){
			alert('<?php echo $txt_formulaire_incomplet;?>');
			return false;
		}
		<?php } ?>
		return true;
	}

	function oracleOrNot(){
		if (document.forms.database.database_type.value == 'Oracle') {
			document.getElementById("hostname").innerHTML = "<?php echo $tr1ContentOracle;?>";
			document.getElementById("database_name").innerHTML = "<?php echo $tr2ContentOracle;?>";
		}else {
			var host = document.getElementById("hostname").innerHTML;
			var php = "<?php echo $tr1ContentNotOracle;?>";

			if ( host.length != (php.length-2) ) {
				document.getElementById("hostname").innerHTML = "<?php echo $tr1ContentNotOracle;?>";
				document.getElementById("database_name").innerHTML = "<?php echo $tr2ContentNotOracle;?>";
			}
		}
	}

	//--></script>

<?php
echo sprintf($entete2, $txt_titre_database);

if ( ! $db_extension_exist){
	echo "<table class='center'>\n";
	echo display_red($txt_no_db_extension_installed);
	echo "</table>\n";
} else if ($verif == 'on'){
	$tab = array();
	$tab = verify_database_connexion($database_type, $hostname, $username, $password, $database_name);
	$message = $tab[0];
	if ($message == ""){
		//*********************************************************************
		// Mise à jour du fichier - lecture du contenu puis écriture
		//*********************************************************************
		echo "<table class='center'>\n";
		$contenu = "";
		$handle = @fopen($config_database_file, "r");
		if ($handle) {
			$i = 0;
			$trouve = false;
			while (!feof($handle) && $i != 2) {
				$buffer = fgets($handle, 4096);
				$contenu .= $buffer;
				if (strlen($buffer) > 8){
					if (substr($buffer, 0, 8) == "//******"){
						$i++;
					}
				}
			}
			if ($i == 2) $trouve = true;
			fclose($handle);
		} else echo display(sprintf($txt_file_pb, $config_database_file));
		if ($trouve){
			$handle = @fopen($config_database_file, "w");
			if ($handle) {
				fwrite($handle, $contenu);
				fwrite($handle, "\n\n");
				fwrite($handle, "$"."database = \"$database_type\";\n\n");
				fwrite($handle, "define('HOST',\"$hostname\");\n");
				fwrite($handle, "define('USER',\"$username\");\n");
				fwrite($handle, "define('PWD',\"$password\");\n");
				fwrite($handle, "define('BD',\"$database_name\");\n");
				fwrite($handle, "?>");
				fclose($handle);

				echo display($txt_db_no_error1);
				echo display($txt_db_no_error2);
				echo display(" &nbsp; ");

				$connexionDB = $tab[1];

				// Mise à jour de la variable 'globale' $database
				$database = $database_type;
				$tab = array();
				$tab = calculate_number_of_tables($database_type);
				$error_message = $tab[1];
				if ($error_message != "") exit($error_message);
				$nbTable = $tab[0];
				if ($nbTable == 0)
					echo display("$txt_create_tables<form name='creation' method='post' action='create_database.php'><input type='submit' value=\"$txt_titre_create_database\" /></form>");
				else
					// on est dans le cas où les tables existent déjà
					if (isset($_SESSION['config'])){
						unset($_SESSION['config']);
						$_SESSION['upgrade'] = 1;
						echo display("$txt_verify_tables<form name='upgrade' method='post' action='verify_database_structure.php'><input type='submit' value=\"$txt_titre_db_structure\" /></form>");
					}
			} else echo display(sprintf($txt_file_pb, $config_database_file));
		} else echo display(sprintf($txt_db_file_error, $config_database_file));
		echo "</table>\n";
	} else{
		echo "<form id='sauvegarde' action='configure_database_access.php' method='post'>\n";
		echo "<table class='center'>\n";
		echo display($txt_db_error);
		echo display($message.".");
		echo display(" &nbsp; ");
		if ($database_type != "PostgreSQL"){ 		// pas de possibilité de retrouver l'erreur avec postgresql
			echo display($txt_pb_requete2);
			if ($database_type == "MySQL") $err = mysql_error();
			else if ($database_type == "Oracle"){
				$temp = oci_error();
				$err = $temp['message'];
			}
			echo display_red($err);
			echo display(" &nbsp; ");
		}
		echo display($txt_db_error_back);
		echo display(" &nbsp; ");
		echo "\n\n";
		$affiche =
			"<input type='hidden' name='verif' value='off' />\n".
			"<input type='hidden' name='database_type' value='$database_type' />\n".
			"<input type='hidden' name='hostname' size='40' value='$hostname' />\n".
			"<input type='hidden' name='username' size='40' value='$username' />\n".
			"<input type='hidden' name='password' size='40' value='$password' />\n".
			"<input type='hidden' name='database_name' size='40' value='$database_name' />\n";
		echo display($affiche);
		echo display("<input type='submit' value='$txt_back' />");
		echo "</table>\n";
		echo "</form>\n";
	}

} else {
	echo "<table class='special'>\n";
	echo "<tr><td class='titre'>$txt_summary</td></tr>\n";

	if ($database_type == "") echo display_left_red($txt_db_file_error4);
	else {
		if ( ! isset($_SESSION['config'])){
			$tab = array();
			$tab = verify_database_connexion($database_type, $hostname, $username, $password, $database_name);
			$message = $tab[0];
			if ($message == "") echo "<tr><td>$txt_db_no_error4</td></tr>\n";
			else {
				echo "<tr><td>$txt_db_error</td></tr>\n";
				echo "<tr><td>$message</td></tr>\n";
				echo "<tr><td>$txt_db_file_error4</td></tr>\n";
			}
		} else echo "<tr><td>$txt_db_init</td></tr>\n";
	}
	echo "</table>\n";

	echo "<p></p>";

	echo "<form id='database' action='configure_database_access.php' method='post' onsubmit='return sComplet();'>\n";

	echo "<table class='center'>\n";
	echo "<tr><td>";
			echo "<table class='presentation2'>\n";
				echo "  <tr class='top'>\n";
					echo "    <td class='left'></td>\n";
					echo "    <td class='center'></td>\n";
					echo "    <td class='right'></td>\n";
				echo "  </tr>\n";
				echo "  <tr class='middle'>\n";
					echo "    <td class='left'></td>\n";
					echo "    <td class='center'>\n";


						echo "<table class='center'>\n";
							echo "<tr><td class='droite2'>$txt_database_type</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<input type='hidden' name='verif' value='$verif' />\n";

							//Choix du type de SGBD
							$ki = 0;
							$options_db = "";
							$db_name = "";
							if (extension_loaded('mysql')){
								$ki++;
								if ($database_type == 'MySQL') $options_db .= "<option selected='selected' value='MySQL'>MySQL</option>\n"; else $options_db .= "<option value='MySQL'>MySQL</option>\n";
								$db_name = "MySQL";
							}
							if (extension_loaded('pgsql')){
								$ki++;
								if ($database_type == 'PostgreSQL') $options_db .= "<option selected='selected' value='PostgreSQL'>PostgreSQL</option>\n"; else $options_db .= "<option value='PostgreSQL'>PostgreSQL</option>\n";
								$db_name = "PostgreSQL";
							}
							if (extension_loaded('oci8')){
								$ki++;
								if ($database_type == 'Oracle') $options_db .= "<option selected='selected' value='Oracle'>Oracle</option>\n"; else $options_db .= "<option value='Oracle'>Oracle</option>\n";
								$db_name = "Oracle";
							}
							if ($ki < 2) echo "<input type='hidden' name='database_type' value='$db_name' />$db_name\n";
							else{
								echo "<select name='database_type' onchange='oracleOrNot();' tabindex='1'>\n";
								echo $options_db;
								echo "</select>";
							}
							echo "</td></tr>\n";


							echo "<tr id='hostname'>$tr1Content</tr>";

							echo "<tr><td class='droite2'>$txt_username</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input name='username' size='40' value='$username' tabindex='3'/></td></tr>\n";
							echo "<tr><td class='droite2'>$txt_password</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input type='password' name='password' size='40' value='$password' tabindex='4'/></td></tr>\n";

							echo "<tr id='database_name'>$tr2Content</tr>";

						echo "</table>\n";


					echo "    </td>\n";
					echo "    <td class='right'></td>\n";
				echo "  </tr>\n";
				echo "  <tr class='bottom'>\n";
					echo "    <td class='left'></td>\n";
					echo "    <td class='center'></td>\n";
					echo "    <td class='right'></td>\n";
				echo "  </tr>\n";
			echo "</table>\n";

	echo "</td></tr>";
	echo "<tr><td>";
		echo "<table>\n";
			echo "<tr><td class='gauche'><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";


	echo "<table class='center'>\n";
		echo "<tr><td class='centre' colspan='1'><input type='button' value='$txt_erase' onclick=\"document.location.href='configure_database_access.php'\" tabindex='6'/></td>";
		echo "<td>&nbsp; &nbsp;  &nbsp;  &nbsp;</td>";
		echo "<td class='centre'><input type='submit' value='$txt_validate' onclick=\"document.forms.database.verif.value='on'\" tabindex='7' /></td></tr>\n";
	echo "</table>\n";

	echo "</form>\n";
	echo "<script type='text/javascript'>Form.focusFirstElement('database');</script>";
}

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>
