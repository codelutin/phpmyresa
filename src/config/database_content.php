<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/database_content.php
*
* This file enables to fill in and modify data in the database tables classe, administrateur and objet
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ( ( ($nbPost != 0) && (($nbPost > 18)  || ($nbPost < 1))) || (count($_GET) != 0)) exitWrongSignature("config/database_content.php");

if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['action'])){
	$action = $_POST['action'];
	if ( ! in_array($action, array('new_class', 'new_object', 'supprimer_classe', 'supprimer_objet', 'supprimer_objet_reserv', 'modifier_classe', 'modifier_objet', 'new_translation'))) exitWrongSignature("config/database_content.php");
} else $action = "";

if (isset($_POST['classe_nom_classe'])){
	$classe_nom_classe = $_POST['classe_nom_classe'];
	if (strlen($classe_nom_classe) > 50) exitWrongSignature("config/database_content.php");
	$classe_nom_classe = htmlspecialchars($classe_nom_classe);
} else $classe_nom_classe = "";
if (isset($_POST['classe_jour_meme'])){
	$classe_jour_meme = $_POST['classe_jour_meme'];
	if ( ! in_array($classe_jour_meme, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['classe_nom_admin'])){
	$classe_nom_admin = $_POST['classe_nom_admin'];
	if (strlen($classe_nom_admin) > 40) exitWrongSignature("config/database_content.php");
	$classe_nom_admin = htmlspecialchars($classe_nom_admin);
} else $classe_nom_admin = "";
if (isset($_POST['classe_prenom_admin'])){
	$classe_prenom_admin = $_POST['classe_prenom_admin'];
	if (strlen($classe_prenom_admin) > 40) exitWrongSignature("config/database_content.php");
	$classe_prenom_admin = htmlspecialchars($classe_prenom_admin);
} else $classe_prenom_admin = "";
if (isset($_POST['classe_pass'])){
	$classe_pass = $_POST['classe_pass'];
	if (strlen($classe_pass) > 32) exitWrongSignature("config/database_content.php");
	$classe_pass = htmlspecialchars($classe_pass);
} else $classe_pass = "";
if (isset($_POST['classe_mail'])){
	$classe_mail = $_POST['classe_mail'];
	if ( ! validate_email($classe_mail)) exitWrongSignature("config/database_content.php");
} else $classe_mail = "";
if (isset($_POST['classe_telephone'])){
	$classe_telephone = $_POST['classe_telephone'];
	if (strlen($classe_telephone) > 20) exitWrongSignature("config/database_content.php");
	$classe_telephone = htmlspecialchars($classe_telephone);
} else $classe_telephone = "";
if (isset($_POST['classe_language'])){
	$classe_language = $_POST['classe_language'];
	if (strlen($classe_language) > 30) exitWrongSignature("config/database_content.php");
	$classe_language = htmlspecialchars($classe_language);
}

if (isset($_POST['nom_objet'])){
	$nom_objet = $_POST['nom_objet'];
	if (strlen($nom_objet) > 128) exitWrongSignature("config/database_content.php");
	$nom_objet = htmlspecialchars($nom_objet);
} else $nom_objet = "";
if (isset($_POST['capacite'])){
	$capacite = $_POST['capacite'];
	if (strlen($capacite) > 30) exitWrongSignature("config/database_content.php");
	$capacite = htmlspecialchars($capacite);
} else $capacite = "";
if (isset($_POST['status'])){
	$status = $_POST['status'];
	if ( ! in_array($status, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['libelle'])){
	$libelle = $_POST['libelle'];
	if (strlen($libelle) > 255) exitWrongSignature("config/database_content.php");
	$libelle = htmlspecialchars($libelle);
} else $libelle = "";
if (isset($_POST['priority'])){
	$priority = $_POST['priority'];
	if ( ! in_array($priority, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['pubical'])){
	$pubical = $_POST['pubical'];
	if ( ! in_array($pubical, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['available'])){
	$available = $_POST['available'];
	if ( ! in_array($available, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['visible'])){
	$visible = $_POST['visible'];
	if ( ! in_array($visible, array('0', '1'))) exitWrongSignature("config/database_content.php");
}
if (isset($_POST['wifi'])){
	$wifi = $_POST['wifi'];
	if ( ! in_array($wifi, array('0', '1'))) exitWrongSignature("config/database_content.php");
}

if (isset($_POST['paramName'])){
	$paramName = $_POST['paramName'];
	$paramName = htmlspecialchars($paramName);
}

if ( in_array($action, array('new_class', 'modifier_classe')) ){
	if ($classe_nom_classe == "") exitWrongSignature("config/database_content.php");
	if ($classe_pass == "") exitWrongSignature("config/database_content.php");
	if ($classe_mail == "") exitWrongSignature("config/database_content.php");
}

if ( in_array($action, array('new_object', 'modifier_objet')) ){
	if ($nom_objet == "") exitWrongSignature("config/database_content.php");
	if ($libelle == "") exitWrongSignature("config/database_content.php");
}

if ($action == 'new_translation'){
	if ($paramName == "") exitWrongSignature("config/database_content.php");
}

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

/**************************************************************************************************
***********										***********
***********				Functions					***********
***********										***********
**************************************************************************************************/
$nb_field_in_table_object = 9;
$nb_field_in_table_classe = 8;


function new_object($nom_classe, $id){
	global $txt_new_object, $nb_field_in_table_object, $txt_add, $txt_yes, $txt_no;

	$message = "<tr><td colspan='$nb_field_in_table_object'><hr/></td></tr>";
	$message .= "<tr><td colspan='$nb_field_in_table_object'>$txt_new_object '$nom_classe'</td></tr>";

	$message .= object_header();

	$message .= "<tr>\n";
	$message .= "   <td><input type='hidden' name='action' value='new_object' /><input type='hidden' name='id' value='$id'/><input id='nom_objet$id' name='nom_objet' value=''/></td>\n";
	$message .= "   <td><input name='capacite' value='' /></td>\n";
	$message .= "   <td><select name='status'><option selected='selected' value='0'>$txt_yes</option><option value='1'>$txt_no</option></select></td>\n";
	$message .= "   <td><input name='libelle' value='' /></td>\n";
	$message .= "   <td><select name='priority'><option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>\n";
	$message .= "   <td><select name='pubical'><option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>\n";
	$message .= "   <td><select name='available'><option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>\n";
	$message .= "   <td><select name='visible'><option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>\n";
	$message .= "   <td><select name='wifi'><option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>\n";
	$message .= "   <td><input type='submit' value='$txt_add' /></td>\n";
	$message .= "</tr>\n";
	$message .= "<tr><td> &nbsp; </td></tr>\n";

	return $message;
}

function display_object($id, $nom_objet, $capacite, $status, $libelle, $priority, $pubical, $available, $visible, $wifi, $color){
	global $txt_no, $txt_yes, $txt_modi, $txt_sup;

	$message = "<tr class='$color'>\n";
	$message .= "   <td>$nom_objet</td>\n";
	$message .= "   <td>$capacite</td>\n";
	if ($status == 0) $message .= "   <td>$txt_yes</td>\n";
	else $message .= "   <td>$txt_no</td>\n";
	$message .= "   <td>$libelle</td>\n";
	if ($priority == 0) $message .= "   <td>$txt_no</td>\n";
	else $message .= "   <td>$txt_yes</td>\n";
	if ($pubical == 0) $message .= "   <td>$txt_no</td>\n";
	else $message .= "   <td>$txt_yes</td>\n";
	if ($available == 0) $message .= "   <td>$txt_no</td>\n";
	else$message .= "   <td>$txt_yes</td>\n";
	if ($visible == 0) $message .= "   <td>$txt_no</td>\n";
	else $message .= "   <td>$txt_yes</td>\n";
	if ($wifi == 0) $message .= "   <td>$txt_no</td>\n";
	else $message .= "   <td>$txt_yes</td>\n";
	$message .= "   <td><button type='button' style='background:transparent;border:0' onclick=\"javascript: modifier_objet($id, '$nom_objet');\" ><img src='img/edit.gif' alt='$txt_modi' /></button></td>\n";
	$message .= "   <td><button type='button' style='background:transparent;border:0' onclick=\"javascript: supprimer_objet($id,'$nom_objet');\" ><img src='img/poub.gif' alt='$txt_sup' /></button></td>\n";
	$message .= "</tr>\n";

	return $message;
}


$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

echo $entete;
echo "<script type='text/javascript' src='".$URL_overlib."overlib.js'>\n</script><div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>\n";


/**************************************************************************************************
***********										***********
***********			Traitement Javascript					***********
***********										***********
**************************************************************************************************/
echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";

?>
	<script type='text/javascript' src='../commun/sprintf.js'>\n</script>

	<script type='text/javascript'><!--

	function sComplet_classe(){
		if (document.forms.new_class.classe_nom_classe.value == "") {
			alert('<?php echo $txt_new_class_nom_classe_incomplet;?>');
			return false;
		}
		if (document.forms.new_class.classe_pass.value == "") {
			alert('<?php echo $txt_new_class_pass_incomplet;?>');
			return false;
		}

		if (document.forms.new_class.classe_mail.value == "") {
			alert("<?php echo $txt_new_class_mail_incomplet;?>");
			return false;
		}

		if ( ! emailIsCorrect(document.forms.new_class.classe_mail.value)) {
				alert("<?php echo $txt_new_class_mail1_incomplet;?>");
				return false;
		}

		new Ajax.Request('ajax_database_unicity.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				ajout: $('classe_nom_classe').value,
				pass: $('classe_pass').value,
				class_object : 'classe'
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}

		});

		if(valide == "1") {
			alert("<?php echo $txt_new_class_nom_classe_invalide;?>");
			return false;
		} else if(valide == "2") {
			alert("<?php echo $txt_new_class_pass_invalide;?>");
			return false;
		} else if (valide == "-1") {
			alert("<?php echo $txt_error2;?>");
			return false;
		}
		return true;
	}

	function sComplet_object(j){
		obj = document.getElementById("new_object"+j);
		if (obj.nom_objet.value == "") {
			alert("<?php echo $txt_new_objet_nom_objet_incomplet;?>");
			return false;
		}

		if (obj.libelle.value == "") {
			alert("<?php echo $txt_new_objet_libelle_incomplet;?>");
			return false;
		}
		if (obj.nom_objet.value.indexOf(',') != -1) {
			alert("<?php echo $txt_database_content_J_13;?>");
			return false;
		}
		if (obj.nom_objet.value.indexOf("'") != -1) {
			alert("<?php echo $txt_database_content_J_14;?>");
			return false;
		}

		new Ajax.Request('ajax_database_unicity.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				ajout: obj.nom_objet.value,
				class_object : 'objet'
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}

		});

		if (valide == "1") {
			alert("<?php echo $txt_new_objet_nom_objet_invalide;?>");
			return false;
		} else if (valide == "-1") {
			alert("<?php echo $txt_error2;?>");
			return false;
		}
		return true;
	}

	function supprimer_classe(id, nom){
		new Ajax.Request('ajax_database_reser_classe.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				classe_id: id
			},
			onComplete: function(transport){
				valide = transport.responseText;
				if (valide != '-1'){
					response = transport.responseXML;
					nb_objet = response.getElementsByTagName('nb_objet')[0].firstChild.data;
					nb_reser = response.getElementsByTagName('nb_reser')[0].firstChild.data;
				}
			},
			onFailure: function(){
				valide = -1;
			}

		});
		if (valide == "-1") {
			alert("<?php echo $txt_error2;?>")
			return false;
		} else if (confirm(sprintf("<?php echo $txt_confirm_supprimer_classe;?>", nom, parseInt(nb_objet), parseInt(nb_reser)))) {
			document.forms.supprimer.action.value = 'supprimer_classe';
			document.forms.supprimer.id.value = id;
			document.forms.supprimer.submit();
		}
	}

	function supprimer_objet(id, nom){
		new Ajax.Request('ajax_database_reser_objet.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				object_id: id
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				alert('Something went wrong...');
				return false;
			}

		});

		if (valide == "-1") {
			alert("<?php echo $txt_error2;?>")
			return false;
		} else if (valide == "0") {
			if(confirm(sprintf("<?php echo $txt_confirm_supprimer_objet;?>", nom))) {
				document.forms.supprimer.action.value = 'supprimer_objet';
				document.forms.supprimer.id.value = id;
				document.forms.supprimer.submit();
			}
		} else {
			if(confirm("<?php echo $txt_confirm_supprimer_classe_reserv;?>")) {

				if(confirm("<?php echo $txt_confirm_supprimer_reserv;?>")) {
					document.forms.supprimer.action.value = 'supprimer_objet_reserv';
					document.forms.supprimer.id.value = id;
					document.forms.supprimer.submit();
					alert("<?php echo $txt_confirmation_objet_reserv;?>")
				} else {
					document.forms.supprimer.action.value = 'supprimer_objet';
					document.forms.supprimer.id.value = id;
					document.forms.supprimer.submit();
					alert("<?php echo $txt_confirmation_objet;?>")
				}
			}
		}
	}

	function modifier_classe(id, nom_classe){
		if(confirm(sprintf("<?php echo $txt_confirm_modifier_classe;?>", nom_classe))) {
			document.forms.modifier_classe.action.value = 'modifier_classe';
			document.forms.modifier_classe.id.value = id;
			document.forms.modifier_classe.submit();
		}
	}

	function modifier_objet(id, nom_objet){
		if(confirm(sprintf("<?php echo $txt_confirm_modifier_objet;?>", nom_objet))) {
			document.forms.modifier_objet.action.value = 'modifier_objet';
			document.forms.modifier_objet.id.value = id;
			document.forms.modifier_objet.submit();
		}
	}

	//--></script>

<?php
echo sprintf($entete2, $txt_titre_db_content);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") exit($message_redirect_configure_database);
$connexionDB = $tab[1];

$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message != "") exit($error_message);
if ($tab2[0] == 0) exit($message_redirect_create_database);

$tab3 = verify_database_structure();
$error_message = $tab3[0];
if ($error_message != "") exit($error_message);
$error_message = $tab3[1];
if ($error_message != "") exit($error_message);


/**************************************************************************************************
***********										***********
***********				Actions						***********
***********										***********
**************************************************************************************************/
if ($action != ''){
	if ( in_array($action, array('new_class', 'modifier_classe')) ){
		$classe_nom_classe = database_real_escape_string($classe_nom_classe);
		$classe_nom_admin = database_real_escape_string($classe_nom_admin);
		$classe_prenom_admin = database_real_escape_string($classe_prenom_admin);
		$classe_pass = database_real_escape_string($classe_pass);
		$classe_mail = database_real_escape_string($classe_mail);
		$classe_telephone = database_real_escape_string($classe_telephone);
		$classe_language = database_real_escape_string($classe_language);
	} else if ( in_array($action, array('new_object', 'modifier_objet')) ){
		$nom_objet = database_real_escape_string($nom_objet);
		$capacite = database_real_escape_string($capacite);
		$libelle = database_real_escape_string($libelle);
	}
}

if ($action == 'new_class'){
	$DB_request = "INSERT INTO classe(nom, jour_meme) VALUES ('$classe_nom_classe', $classe_jour_meme)";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);

	$DB_request = "SELECT id FROM classe WHERE nom='$classe_nom_classe'";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
	$result = database_fetch_object($result);
	$id_classe = database_get_from_object($result, 'id');

	$DB_request = "INSERT INTO administrateur(id_classe, nom, prenom, pass, mail, telephone, default_language) VALUES ($id_classe, '$classe_nom_admin', '$classe_prenom_admin', '$classe_pass', '$classe_mail', '$classe_telephone', '$classe_language')";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'new_object'){
	$DB_request = "INSERT INTO objet(id_classe, nom, capacite, status, libelle, priority, pubical, available, visible, wifi) VALUES ($id, '$nom_objet', '$capacite', $status, '$libelle', $priority, $pubical, $available, $visible, $wifi)";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'supprimer_classe'){
	$DB_request = "DELETE reservation FROM reservation, objet WHERE objet.id_classe=$id AND objet.id=reservation.idobjet";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);

	$DB_request = "DELETE FROM objet WHERE id_classe=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);

	$DB_request = "DELETE FROM administrateur WHERE id_classe=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);

	$DB_request = "DELETE FROM classe WHERE id=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'supprimer_objet'){
	$DB_request = "DELETE FROM objet WHERE id=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'supprimer_objet_reserv'){
	$DB_request = "DELETE FROM objet WHERE id=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
	$DB_request = "DELETE FROM reservation WHERE idobjet=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'modifier_classe'){
	$DB_request = "UPDATE classe SET nom='$classe_nom_classe', jour_meme=$classe_jour_meme WHERE id=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);

	$DB_request = "UPDATE administrateur SET nom='$classe_nom_admin', prenom='$classe_prenom_admin', pass='$classe_pass', mail='$classe_mail', telephone='$classe_telephone', default_language='$classe_language' WHERE id_classe=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'modifier_objet'){
	$DB_request = "UPDATE objet SET nom='$nom_objet', capacite='$capacite', status=$status, libelle='$libelle', priority=$priority, pubical=$pubical, available=$available, visible=$visible, wifi=$wifi WHERE id=$id";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
} else if ($action == 'new_translation'){
	$temp = getAvailableLanguagesFromDB();
	$tab_languages = $temp[1];
	$tmp1 = '';
	$tmp2 = '';
	for ($i = 0 ; $i < count($tab_languages) ; $i++){
		$int = $tab_languages[$i];
		$tmp1 .= ", $int";
		$tmp2 .= ", '".database_real_escape_string($_POST[$int])."'";
	}
	$DB_request = "INSERT INTO languages(name $tmp1) VALUES ('$paramName' $tmp2)";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
}




/**************************************************************************************************
***********										***********
***********				Affichage					***********
***********										***********
**************************************************************************************************/
// initialisations de variables afin de ne pas ré-utiliser de vieilles valeurs !
$nom_objet = "";
$classe_nom_classe = "";

echo "<table class='center'>\n";

$DB_request = "SELECT C.id AS id_classe, C.nom AS nom_classe, C.jour_meme, ";
$DB_request .= "A.nom AS nom_admin, A.prenom AS prenom_admin, A.pass, A.mail, A.telephone, A.default_language, ";
$DB_request .= "O.id AS id_objet, O.nom AS nom_objet, O.capacite, O.status, O.libelle, O.priority, O.pubical, O.available, O.visible, O.wifi ";
$DB_request .= "FROM classe C ";
$DB_request .= "LEFT JOIN administrateur A ON C.id = A.id_classe ";
$DB_request .= "LEFT JOIN objet O ON C.id = O.id_classe ";
$DB_request .= "ORDER BY C.id, O.nom";
$result_main_SQL = database_query($DB_request, $connexionDB);
if ($result_main_SQL){

	/*********************************************************************************/
	/****						 			      ****/
	/****				Affichage des erreurs 			      ****/
	/****						 			      ****/
	/*********************************************************************************/

	echo "<tr><td colspan='$nb_field_in_table_classe'>";
	echo "<table class='special'>\n";
	$mes = "<ol>";
	for ($i=0; $i<count($txt_database_content); $i++) {
		$mes .= "<li>$txt_database_content[$i]</li>";
	}
	$mes .= "</ol>";
	echo "<tr><td class='titre'>$txt_summary <a href='#' onmouseover=\"return overlib('".addslashes($mes)."', WIDTH, 700, CAPTION, '$txt_verification', CENTER, OFFSETY, 20, TEXTSIZE, '12px', FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);\" onmouseout='return nd();'><img src='img/help.gif' alt='$txt_help' /></a></td></tr>\n";
	echo "<tr><td>$txt_no_db_structure_pb</td></tr>\n";

	$encounred_problem = false;

	/* -------------------------------------------------------------------------------
		On vérifie qu'il existe au moins une classe dans la base de données
	------------------------------------------------------------------------------- */
	$DB_request2 = "SELECT count(*) AS nb FROM classe";
	$result2 = database_query($DB_request2, $connexionDB);
	if ( ! $result2){
		echo display_error($DB_request2);
		$encounred_problem = true;
	} else{
		$resultat2 = database_fetch_object($result2);
		$nb_classe = database_get_from_object($resultat2, 'nb');
		if ($nb_classe == 0){
			echo display_left_red($txt_nedd_class);
			$encounred_problem = true;
		}
	}

	/* -------------------------------------------------------------------------------
		On récupère la liste des langues disponibles dans la base de données
	------------------------------------------------------------------------------- */
	$temp = getAvailableLanguagesFromDB();
	$pb = $temp[0];
	if ($pb != ""){
		echo $pb;
		$encounred_problem = true;
		//on donne des valeurs par défaut
		$tab_languages = array();
		$tab_languages[0] = "french";
		$tab_languages[1] = "english";
	} else $tab_languages = $temp[1];
	$nb_tab_languages = count($tab_languages);

	if ( ! $encounred_problem){

		/* -------------------------------------------------------------------------------
			Gestion des priorités (ajout d'un message si non défini)
		------------------------------------------------------------------------------- */
		$compteur = '0';
		$needJavascriptForTranslation = false;
		$DB_request = "SELECT nom FROM objet WHERE priority = 1";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			$messageName = getPriorityMessageName($nom);
			$DB_request = "SELECT name FROM languages WHERE name = '".$messageName."'";
			$res = database_query($DB_request, $connexionDB);
			if ( ! $res){
				echo display_error($DB_request);
				$encounred_problem = true;
			} else if ( ! database_fetch_object($res)){
				$needJavascriptForTranslation = true;
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_priority, $nom));
				echo display_left($txt_priority_prompt1);
				echo "<tr><td><form id='addPriorityMsg".$compteur."' action='database_content.php' method='post' onsubmit=\"return testForm('".$compteur."');\">\n";
				echo "<table>\n";
				echo "<tr><td><input type='hidden' name='action' value='new_translation' /></td></tr>\n";
				echo display_left("$txt_priority_prompt2 '<i>$txt_priority_prompt5 (XXX)</i>'");
				echo display_left($txt_priority_prompt3);
				echo "<tr><td><table border='0' style='color: black'>\n";
				echo "<tr align='center'>\n";
				echo "  <td></td>\n";
				for ($i = 0; $i < $nb_tab_languages; $i++) echo "  <td><b>$tab_languages[$i]</b></td>\n";
				echo "  <td></td>\n";
				echo "</tr>\n";
				echo "<tr>\n";
				echo "  <td><input type='hidden' name='paramName' value='$messageName' /></td>";
				for ($i = 0; $i < $nb_tab_languages; $i++) echo "  <td><input type='text' size='40' name='$tab_languages[$i]' /></td>\n";
				echo "<td><input type='submit' value='go' /></td></tr>";
				echo "</table></td></tr>\n";
				echo "</table>\n";
				echo "</form></td></tr>\n";
				echo display_left($txt_priority_prompt4);
				$compteur++;
			}
		}

		if ($needJavascriptForTranslation){
		 	echo "\n<script type='text/javascript'><!--\n";
		 	echo "function testForm(f){\n";
			echo "	for (var i = 0; i < document.forms.length; i++) {\n";
			echo "		if (document.forms[i].id == 'addPriorityMsg'+f) break;\n";
			echo "	}\n";
		 	echo "	for (j=0 ; j<document.forms[i].length - 1 ; j++){\n";
		 	echo "		if (document.forms[i][j].value == ''){\n";
		 	echo "			alert('$txt_validation');\n";
		 	echo "			return false;\n";
		 	echo "		}\n";
		 	echo "	}\n";
		 	echo "	return true;\n";
		 	echo "}\n";
		 	echo "//--></script>\n\n";
		}

		// $list_languages is the list of available languages
		$list_languages = "";
		for ($i = 0; $i < $nb_tab_languages ; $i++) $list_languages .= "'$tab_languages[$i]', ";
		$list_languages = substr($list_languages, 0, -2);
		if ($database == "Oracle") if ($list_languages == "") $list_languages = "-1";

		/* -------------------------------------------------------------------------------
			table administrateur: has default_language correct values?
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT nom, prenom, default_language FROM administrateur ";
		$DB_request .= "WHERE default_language NOT IN ($list_languages)";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
			$erreur = database_get_from_object($row, 'default_language');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_admin_error2, $admin, $erreur));
			echo display_left(sprintf($txt_possible_values, $list_languages));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table administrateur: field 'pass' can not be empty
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT DISTINCT nom, prenom FROM administrateur WHERE pass = ''";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else 	while ($row = database_fetch_object($resultat)){
			$admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_admin_error3, $admin));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table administrateur: each administrator (distinction on the email)
			must have a distinct password
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT count(*) AS nb FROM administrateur A1, administrateur A2 WHERE A1.pass = A2.pass AND A1.mail <> A2.mail";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else{
			$row = database_fetch_object($resultat);
			$nb = database_get_from_object($row, 'nb');
			if ($nb != 0){
				echo display(" &nbsp; ");
				echo display_left_red($txt_admin_error4);
				$encounred_problem = true;
			}
		}

		if ($database == "MySQL"){ 	// Les autres bases de données empêchent ces inconsistences à l'aide des clés étrangères
						// $list_id_class is here the list of id from the classe table
			$DB_request = "SELECT distinct id FROM classe";
			$resultat = database_query($DB_request, $connexionDB);
			if ( ! $resultat){
				echo display_error($DB_request);
				$list_id_class = "";
				$encounred_problem = true;
			} else $list_id_class = constructionListe($resultat, 'id');

			/* -------------------------------------------------------------------------------
				table administrateur: has id_classe correct values? (only MySQL)
			------------------------------------------------------------------------------- */
			$DB_request = "SELECT nom, prenom, mail, id_classe FROM administrateur ";
			$DB_request .= "WHERE id_classe NOT IN ($list_id_class)";
			$resultat = database_query($DB_request, $connexionDB);
			if ( ! $resultat){
				echo display_error($DB_request);
				$encounred_problem = true;
			} else while ($row = database_fetch_object($resultat)){
				$admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
				$mail = database_get_from_object($row, 'mail');
				if ($mail != "") $admin .= " ($mail)";
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_admin_error5, $admin));
				$encounred_problem = true;
			}

			/* -------------------------------------------------------------------------------
				table objet: has id_classe correct values? (only MySQL)
			------------------------------------------------------------------------------- */
			$DB_request = "SELECT nom, id_classe FROM objet ";
			$DB_request .= "WHERE id_classe NOT IN ($list_id_class)";
			$resultat = database_query($DB_request, $connexionDB);
			if ( ! $resultat){
				echo display_error($DB_request);
				$encounred_problem = true;
			} else while ($row = database_fetch_object($resultat)){
				$nom = database_get_from_object($row, 'nom');
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_admin_error51, $nom));
				$encounred_problem = true;
			}

			// $list_id_object is here the list of id from the objet table
			$DB_request = "SELECT distinct id FROM objet";
			$resultat = database_query($DB_request, $connexionDB);
			if ( ! $resultat){
				echo display_error($DB_request);
				$list_id_object = "";
				$encounred_problem = true;
			} else $list_id_object = constructionListe($resultat, 'id');

			/* -------------------------------------------------------------------------------
				table reservation: has id_objet correct values? (only MySQL)
			------------------------------------------------------------------------------- */
			$DB_request = "SELECT titre, jour, idobjet FROM reservation ";
			$DB_request .= "WHERE idobjet NOT IN ($list_id_object)";
			$resultat = database_query($DB_request, $connexionDB);
			if ( ! $resultat){
				echo display_error($DB_request);
				$encounred_problem = true;
			} else while ($row = database_fetch_object($resultat)){
				$titre = database_get_from_object($row, 'titre')." - ".database_get_from_object($row, 'jour');
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_objet_error, $titre));
				$encounred_problem = true;
			}
		}

		// $list_id_classe_admin is here the list of id_classe from the administrateur table
		$DB_request = "SELECT distinct id_classe FROM administrateur";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$list_id_classe_admin = "";
			$encounred_problem = true;
		} else $list_id_classe_admin = constructionListe($resultat, 'id_classe');

		/* -------------------------------------------------------------------------------
			table classe: each classe must have an administrator
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT nom FROM classe WHERE id NOT IN ($list_id_classe_admin)";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_admin_error6, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table objet : cannot contain an object with the character ","
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT nom FROM objet WHERE nom LIKE '%,%'";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_objet_error_comma, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table objet : cannot contain an object with the character "'"
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT nom FROM objet WHERE nom LIKE '%''%'";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_objet_error_quote, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table classe: field jour_meme can be set only to 0 or 1
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT DISTINCT nom FROM classe WHERE jour_meme NOT IN (0,1)";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_classe_error_01, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			table objet: fields status, priority, pubiCal, available, visible, wifi
			can be set only to 0 or 1
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT DISTINCT nom FROM objet WHERE status NOT IN (0,1) OR priority NOT IN (0,1) OR pubiCal NOT IN (0,1) OR available NOT IN (0,1) OR visible NOT IN (0,1) OR  wifi NOT IN (0,1)";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_object_error_01, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			Each class contains at least one object ?
		------------------------------------------------------------------------------- */
		$list_id_classe_without_object = "";
		$DB_request = "SELECT O.id_classe, C.id, C.nom FROM classe C LEFT JOIN objet O ON C.id = O.id_classe GROUP BY O.id_classe, C.id, C.nom";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$id = database_get_from_object($row, 'id_classe');
			if ( ! is_numeric($id)){
				$nom = database_get_from_object($row, 'nom');
				echo display(" &nbsp; ");
				echo display_left_red(sprintf($txt_classe_error1, $nom));
				$list_id_classe_without_object .= database_get_from_object($row, 'id').", ";
				$encounred_problem = true;
			}
		}

		// Si une classe ne comporte aucun objet il n'est pas nécessaire de signaler qu'elle ne contient ni objet visible ni objet disponible
		if ($list_id_classe_without_object != ""){
			$list_id_classe_without_object = substr($list_id_classe_without_object, 0, -2);
			$SQLcondition = "C.id NOT IN ($list_id_classe_without_object) AND ";
		} else $SQLcondition = "";;

		/* -------------------------------------------------------------------------------
			Each class contains at least one visible object ?
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT DISTINCT C.nom FROM classe C LEFT JOIN objet O ON C.id = O.id_classe AND O.visible = 1 WHERE $SQLcondition O.visible IS NULL";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_classe_error2, $nom));
			$encounred_problem = true;
		}

		/* -------------------------------------------------------------------------------
			Each class contains at least one available object ?
		------------------------------------------------------------------------------- */
		$DB_request = "SELECT DISTINCT C.nom FROM classe C LEFT JOIN objet O ON C.id = O.id_classe AND O.available = 1 WHERE $SQLcondition O.available IS NULL";
		$resultat = database_query($DB_request, $connexionDB);
		if ( ! $resultat){
			echo display_error($DB_request);
			$encounred_problem = true;
		} else while ($row = database_fetch_object($resultat)){
			$nom = database_get_from_object($row, 'nom');
			echo display(" &nbsp; ");
			echo display_left_red(sprintf($txt_classe_error3, $nom));
			$encounred_problem = true;
		}
	}

	if ( ! $encounred_problem) echo display_left($txt_db_content_ok);

	echo "</table>\n";
	echo "</td></tr>";
	echo display(" &nbsp; ");
	echo display(" &nbsp; ");



	/*********************************************************************************/
	/****						 			      ****/
	/****			Affichage du contenu de la bd 			      ****/
	/****						 			      ****/
	/*********************************************************************************/
	$nom_classe_old = "";
	$id_classe_old = "";
	$j = 1;
	while ($row = database_fetch_object($result_main_SQL)){
		$id_classe = database_get_from_object($row, 'id_classe');
		$nom_classe = database_get_from_object($row, 'nom_classe');
		$jour_meme = database_get_from_object($row, 'jour_meme');

		$nom_admin = database_get_from_object($row, 'nom_admin');
		$prenom_admin = database_get_from_object($row, 'prenom_admin');
		$pass = database_get_from_object($row, 'pass');
		$mail = database_get_from_object($row, 'mail');
		$telephone = database_get_from_object($row, 'telephone');
		$default_language = database_get_from_object($row, 'default_language');

		$id_objet = database_get_from_object($row, 'id_objet');
		$nom_objet = database_get_from_object($row, 'nom_objet');
		$capacite = database_get_from_object($row, 'capacite');
		$status = database_get_from_object($row, 'status');
		$libelle = database_get_from_object($row, 'libelle');
		$priority = database_get_from_object($row, 'priority');
		$pubical = database_get_from_object($row, 'pubical');
		$available = database_get_from_object($row, 'available');
		$visible = database_get_from_object($row, 'visible');
		$wifi = database_get_from_object($row, 'wifi');

		if ($nom_classe == $nom_classe_old){
			if ($color == "color1") $color = "color2"; else $color = "color1";
			echo display_object($id_objet, $nom_objet, $capacite, $status, $libelle, $priority, $pubical, $available, $visible, $wifi, $color);
		} else{
			if ($nom_classe_old != ""){
				//Insertion d'un nouvel objet pour la classe
				echo new_object($nom_classe_old, $id_classe_old);
				echo "</table></form></td></tr>\n";
				echo "<tr><td> &nbsp; </td></tr>\n";
			}

			echo class_header();

			echo "<tr class='color1'>\n";
			echo "   <td>$nom_classe</td>\n";
			if ($jour_meme == 0) echo "   <td>$txt_no</td>\n";
			else echo "   <td>$txt_yes</td>\n";
			echo "   <td>$nom_admin</td>\n";
			echo "   <td>$prenom_admin</td>\n";
			echo "   <td>$pass</td>\n";
			echo "   <td>$mail</td>\n";
			echo "   <td>$telephone</td>\n";
			echo "   <td>$default_language</td>\n";
			echo "   <td><button type='button' style='background:transparent;border:0' onclick=\"javascript: modifier_classe($id_classe, '".addslashes($nom_classe)."');\" ><img src='img/edit.gif' alt='$txt_modi' /></button></td>\n";
			echo "   <td><button type='button' style='background:transparent;border:0' onclick=\"javascript: supprimer_classe($id_classe, '".addslashes($nom_classe)."');\" ><img src='img/poub.gif' alt='$txt_sup' /></button></td>\n";
			echo "</tr>\n";
			echo "<tr><td> &nbsp; </td></tr>\n";

			echo "<tr><td></td><td colspan='$nb_field_in_table_classe'><form id='new_object"."$j' action='database_content.php' method='post' onsubmit='return sComplet_object($j);'><table>\n";
			$j++;
			if ($nom_objet != NULL){
				echo object_header();
				$color = "color1";
				echo display_object($id_objet, $nom_objet, $capacite, $status, $libelle, $priority, $pubical, $available, $visible, $wifi, $color);
			}
		}
		$nom_classe_old = $nom_classe;
		$id_classe_old = $id_classe;
	}

	if (isset($nom_classe)){
		echo new_object($nom_classe, $id_classe);
		echo "</table></form></td></tr>\n";
		echo "<tr><td> &nbsp; </td></tr>\n";
		echo "<tr><td colspan='$nb_field_in_table_classe'><hr/></td></tr>\n";
	} else{
		// Cas où aucune classe n'a été trouvée !
	}
	echo "</table>\n";

	// Insertion d'une nouvelle classe
	echo "<table class='center'>\n";
	echo "<tr><td>\n";
	echo "<form id='new_class' action='database_content.php' method='post' onsubmit=\"return sComplet_classe()\">\n";
	echo "<table class='center'>\n";
	echo "<tr><td colspan='$nb_field_in_table_classe'>$txt_new_class</td></tr>\n";
	echo class_header();

	echo "<tr>\n";
	echo "   <td><input type='hidden' name='action' value='new_class' /><input id='classe_nom_classe' name='classe_nom_classe' value='$classe_nom_classe' /></td>\n";
	echo "   <td><select name='classe_jour_meme'><option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>\n";
	echo "   <td><input name='classe_nom_admin' value='' /></td>\n";
	echo "   <td><input name='classe_prenom_admin' value=''/></td>\n";
	echo "   <td><input type='password' id='classe_pass' name='classe_pass' value='' /></td>\n";
	echo "   <td><input name='classe_mail' value='' /></td>\n";
	echo "   <td><input name='classe_telephone' value='' /></td>\n";
	echo "   <td><select name='classe_language'>";
	for ($u = 0 ; $u < $nb_tab_languages ; $u ++){
		$current_language = $tab_languages[$u];
		if ($language == $current_language) echo "<option selected='selected' value='$current_language'>$current_language</option>";
		else echo "<option value='$current_language'>$current_language</option>";
	}
	echo "</select></td>\n";
	echo "   <td><input type='submit' value='$txt_add' /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";

	echo "<tr><td>\n";
	echo "<form id='supprimer' action='database_content.php' method='post'>\n";
	echo "<table class='center'>\n";
	echo "<tr>\n";
	echo "<td><input type='hidden' name='action' value='' /></td>\n";
	echo "<td><input type='hidden' name='id' value='' /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";

	echo "<tr><td>\n";
	echo "<form id='modifier_classe' action='database_content_modify.php' method='post'>\n";
	echo "<table class='center'>\n";
	echo "<tr>\n";
	echo "<td><input type='hidden' name='action' value='' /></td>\n";
	echo "<td><input type='hidden' name='id' value='' /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";

	echo "<tr><td>\n";
	echo "<form id='modifier_objet' action='database_content_modify.php' method='post'>\n";
	echo "<table class='center'>\n";
	echo "<tr>\n";
	echo "<td><input type='hidden' name='action' value='' /></td>\n";
	echo "<td><input type='hidden' name='id' value='' /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";

	echo "</table>\n";

	echo "<table>";
	echo "<tr><td><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
	echo "<tr><td> &nbsp; </td></tr>\n";
	echo "</table>\n";

} else echo display_error($DB_request)."</table>\n";

echo "</div>\n";
echo "</div>\n";

echo $div_footer;
echo $end;
?>
