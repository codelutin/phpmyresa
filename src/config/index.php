<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/index.php
*
* This file is the entry to configure PHPMyResa
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ((($nbPost != 0) && ($nbPost != 2) && ($nbPost != 3)) || (count($_GET) != 0)) exitWrongSignature("config/index.php");
// $_POST n'est pas utilisé !

/***********************************************************************************************
**************		 fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/



/***********************************************************************************************
*****    All cases of redirection are now studied to find installation or upgrade cases    *****
**********************************************************************************************/

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

$temp = verify_file($config_file);
if ($temp != "") exit($temp);
require_once($config_file);

function go_configure_database_access(){
	$_SESSION['config'] = 1;
	Header("Location:./configure_database_access.php");
	exit();
}

function go_configure_fill_languages(){
	Header("Location:./fill_languages.php");
	exit();
}

function go_configure_main(){
	Header("Location:./configure_main.php");
	exit();
}

function go_configure_database_content(){
	Header("Location:./database_content.php");
	exit();
}

if ( ! isset($database) || empty($database))  $database = "MySQL";
else if (($database != "MySQL") && ($database != "PostgreSQL") && ($database != "Oracle")) go_configure_database_access();

if ( ! defined('HOST') || ! constant('HOST')) go_configure_database_access();
if ( ! defined('USER') || ! constant('USER')) go_configure_database_access();
if ( ! defined('PWD')  || ! constant('PWD')) go_configure_database_access();
if ( ($database != "Oracle") && (! defined('BD')   || ! constant('BD'))) go_configure_database_access();

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") go_configure_database_access();
$connexionDB = $tab[1];

$error_message = "";
$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message == ""){
	if ($tab2[0] == 0){
		header("location:./create_database.php");
		exit();
	}
	$tab3 = verify_database_structure();
	$error_message = $tab3[0];
	if ($error_message == ""){
		$mes = $tab3[1];
		if ( ($mes != "") && (isset($_SESSION['upgrade'])) ){
			echo $entete;
			echo sprintf($entete2, $txt_bookmark);
			exit($mes);
		} else $error_message = $mes;
		if ($error_message == ""){
			if ($database == "Oracle") $DB_request = "SELECT count(*) AS nb FROM languages WHERE french is not null";
			else $DB_request = "SELECT count(*) AS nb FROM languages WHERE french != ''";
			$result = database_query($DB_request, $connexionDB);
			if ( ! $result) $error_message = "<table class='center'>".display_error($DB_request)."</table>$end_error";
			if ($error_message == ""){
				$resultat = database_fetch_object($result);
				$nb_french = database_get_from_object($resultat, 'nb');
				if ($nb_french == 0){
					$_SESSION['config'] = 1;
					go_configure_fill_languages();
				}
				if ($database == "Oracle") $DB_request = "SELECT count(*) AS nb FROM languages WHERE german is not null";
				else $DB_request = "SELECT count(*) AS nb FROM languages WHERE german != ''";
				$result = database_query($DB_request, $connexionDB);
				if ( ! $result) $error_message = "<table class='center'>".display_error($DB_request)."</table>$end_error";
				if ($error_message == ""){
					$resultat = database_fetch_object($result);
					$nb_german = database_get_from_object($resultat, 'nb');
					if ($nb_german == 0){
						$_SESSION['upgrade'] = 1;
						go_configure_fill_languages();
					}
					if ( ! isset($technical_contact) || empty($technical_contact) || ! isset($technical_email) || empty($technical_email) || ! isset($technical_tel) || empty($technical_tel)) go_configure_main();
				}
			}
		}
	}
}

if ( isset($_SESSION['config']) || isset($_SESSION['upgrade']) ) go_configure_fill_languages();

if ($error_message == ""){
	$DB_request = "SELECT count(*) AS nb FROM classe";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) $error_message = "<table class='center'>".display_error($DB_request)."</table>$end_error";
	if ($error_message == ""){
		$resultat = database_fetch_object($result);
		if (database_get_from_object($resultat, 'nb') == 0) go_configure_database_content();

		$DB_request = "SELECT count(*) AS nb FROM administrateur";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result) $error_message = "<table class='center'>".display_error($DB_request)."</table>$end_error";
		if ($error_message == ""){
			$resultat = database_fetch_object($result);
			if (database_get_from_object($resultat, 'nb') == 0) go_configure_database_content();

			$DB_request = "SELECT count(*) AS nb FROM objet";
			$result = database_query($DB_request, $connexionDB);
			if ( ! $result) $error_message = "<table class='center'>".display_error($DB_request)."</table>$end_error";
			if ($error_message == ""){
				$resultat = database_fetch_object($result);
				if (database_get_from_object($resultat, 'nb') == 0) go_configure_database_content();
			}
		}
	}
}

/***********************************************************************************************
*****        If no installation nor upgrade cases are detected the index is displayed      *****
**********************************************************************************************/

echo $entete;
echo sprintf($entete2, $txt_bookmark);

if ($error_message != "") exit($error_message);

echo "<table class='presentation'>\n";
	echo "  <tr class='top'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='middle'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'>\n";
			echo "      <table class='all'><tr><td style='text-align:left;'><a href='configure_password.php'>$txt_titre_password</a></td><td style='text-align:right;'><a href='logout.php'>$txt_deconnexion</a></td></tr></table>\n";
		echo "    </td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='bottom'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
echo "</table>\n";
echo "<table><tr><td> &nbsp; </td></tr></table>\n";
echo "<table class='presentation'>\n";
	echo "  <tr class='top'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='middle'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center' style='font-weight:bold;font-size:18pt;text-align:center;'>\n";
			echo "      $txt_titre_database_config\n";
		echo "    </td>\n";
		echo "    <td class='right'></td>\n";
	echo "</tr>\n";
	echo "<tr class='middle'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'>\n";
			echo "      <p><a href='configure_database_access.php'>$txt_titre_database</a></p>\n";
			echo "      <p><a href='verify_database_structure.php'>$txt_titre_db_structure</a></p>\n";
			echo "      <p><a href='fill_languages.php'>$txt_titre_languages</a></p>\n";
			echo "      <p><a href='database_content.php'>$txt_titre_db_content</a></p>\n";
			echo "      <p><a href='delete_session_files.php'>$txt_titre_session</a></p>\n";
		echo "    </td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='bottom'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
echo "</table>\n";
echo "<table><tr><td> &nbsp; </td></tr></table>\n";
echo "<table class='presentation'>\n";
	echo "  <tr class='top'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='middle'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center' style='font-weight:bold;font-size:18pt;text-align:center;'>\n";
			echo "      $txt_titre_application_config\n";
		echo "    </td>\n";
		echo "    <td class='right'></td>\n";
	echo "</tr>\n";
	echo "<tr class='middle'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'>\n";
			echo "      <p><a href='configure_main.php'>$txt_titre_config1</a></p>\n";
			echo "      <p><a href='configure_annexe.php'>$txt_titre_config2</a></p>\n";
		echo "    </td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
	echo "  <tr class='bottom'>\n";
		echo "    <td class='left'></td>\n";
		echo "    <td class='center'></td>\n";
		echo "    <td class='right'></td>\n";
	echo "  </tr>\n";
echo "</table>\n";

echo "<table><tr><td> &nbsp; </td></tr></table>\n";

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>
