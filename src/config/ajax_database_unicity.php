<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/ajax_database_unicity.php
*
* This file verify the unicity of the name of the object or the class and the unicity of
* the administrator password for a class, called by database_content.php and database_content_modify.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_database_file);
if ($temp != "") exit('-1');
require_once($config_database_file);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
$connexionDB = $tab[1];

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ( ($nbPost < 2 && $nbPost > 4) || (count($_GET) != 0) ) exit('-1');

if (isset($_POST['ajout'])){
	$ajout = $_POST['ajout'];
	if (strlen($ajout) > 128) exit('-1');
	$ajout = database_real_escape_string($ajout);
	$ajout = htmlspecialchars($ajout);
} else exit('-1');
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exit('-1');
} else $id = -1;
if (isset($_POST['class_object'])){
	$class_object = $_POST['class_object'];
	if ( ! in_array($class_object, array("classe", "objet"))) exit('-1');
} else exit('-1');
if (isset($_POST['pass'])){
	$pass = $_POST['pass'];
	if (strlen($pass) > 32) exit('-1');
	$pass = database_real_escape_string($pass);
	$pass = htmlspecialchars($pass);
}

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$DB_request = "SELECT count(*) as nb FROM $class_object WHERE nom='$ajout' AND id <> '$id'";
$result = database_query($DB_request, $connexionDB);
$result = database_fetch_object($result);
$nb_classe = database_get_from_object($result, 'nb');

if($class_object == "classe") {
	$nb_mail = 0;
	$DB_request = "SELECT count(*) as nb FROM administrateur WHERE pass='$pass AND id_classe <> '$id'";
	$result = database_query($DB_request, $connexionDB);
	$result = database_fetch_object($result);
	$nb_mail = database_get_from_object($result, 'nb');

	if ($nb_classe == 0 && $nb_mail == 0) exit('0');
	else if ($nb_classe == 0 && $nb_mail != 0) exit('2');
	else exit('1');
} else {
	if ($nb_classe == 0) exit('0');
	else exit('1');
}
?>
