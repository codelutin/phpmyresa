<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/login.php
*
* This file is the entry to login the configuration area of PHPMyResa
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


if (ini_get('session.auto_start') == "0") session_start();
$_SESSION = array();
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time()-42000, '/');
}
session_destroy();

require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ( (($nbPost != 0) && ($nbPost != 2)) || (count($_GET) != 0)) exitWrongSignature("config/login.php");
// $_POST n'est pas utilisé !

/***********************************************************************************************
**************		 fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/


$temp = verify_file($config_credential_file);
if ($temp != "") exit($temp);
require_once($config_credential_file);

echo $entete;

echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";
?>

<script type='text/javascript'><!--

	function sComplet_inscription(){
		txt1 = document.forms.inscription.login.value;
		txt2 = document.forms.inscription.passe.value;

		testlength = txt1.length * txt2.length;
		if(testlength == 0){
			alert ("<?php echo $txt_champs_obligatoire;?>");
			return false;
		} else if (txt2.length < 8){
			alert("<?php echo $txt_pass_contain;?>");
			document.forms.inscription.passe.value = "";
			return false;
		}

		new Ajax.Request('ajax_login_inscription.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				login: $('login').value,
				passe: $('passe').value
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}
		});
		if(valide != "1") {
			alert("<?php echo $txt_inscription_invalide;?>");
			return false;
		}
		return true;
	}

	function sComplet_connexion(){
		txt1 = document.forms.connexion.login.value;
		txt2 = document.forms.connexion.passe.value;

		testlength = txt1.length * txt2.length;
		if(testlength == 0){
			alert ("<?php echo $txt_champs_obligatoire;?>");
			return false;
		}

		new Ajax.Request('ajax_login_connexion.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				login: $('login').value,
				passe: $('passe').value
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}
		});
		if(valide != "1") {
			alert("<?php echo $txt_connexion_invalide;?>");
			return false;
		}
		return true;
	}

//--></script>


<?php
if ( isset($credential_login) && isset($credential_pass) && ($credential_login != '') && ($credential_pass != '') ){	// login authentification request

	echo sprintf($entete2, $txt_titre_connexion);

	echo "<form id='connexion' action='index.php' method='post' onsubmit=\"return sComplet_connexion()\">\n";
	echo "<table class='center'>\n";
	echo "<tr><td>";
		echo "<table class='presentation2'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";
					echo "<table class='center'>";
							echo "<tr><td class='gauche'>$txt_inscription_explication1</td></tr>\n";
							echo "<tr><td class='gauche'>$txt_inscription_explication2</td></tr>\n";
               				echo "</table>\n";
				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>\n";
	echo "<tr><td>";
	       echo "<table class='presentation2'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "      <table class='center'>\n";
						echo "        <tr>\n";
			      				echo "          <td class='droite2'>$txt_login</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>\n";
        		       				echo "          <td class='gauche'><input id='login' name='login' value='' /></td>\n";
               					echo "        </tr>\n";
               					echo "        <tr>\n";
        	       					echo "          <td class='droite2'>$txt_passe</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>\n";
        	       					echo "          <td class='gauche'><input type='password' id='passe' name='passe' value='' /></td>\n";
	               				echo "        </tr>\n";
	               			echo "      </table>\n";
				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "</td></tr>";
	echo "<tr><td>";
		echo "<table>\n";
			echo "<tr><td class='gauche'><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>";
		echo "<tr>";
		echo "<td class='centre'  colspan='3'>";
		echo "<input type='submit' value='$txt_validate' />";
		echo "</td>";
	       	echo "</tr>";
	echo "</table>";

	echo "</form>";
	echo "<script type='text/javascript'>Form.focusFirstElement('connexion');</script>";

} else{					// creation of the credentials
	echo sprintf($entete2, $txt_titre_inscription);

	echo "<form id='inscription' action='login.php' method='post' onsubmit=\"return sComplet_inscription()\">\n";

	echo "<table class='center'>\n";
	echo "<tr><td>";
		echo "<table class='presentation2'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";
					echo "<table class='center'>";
							echo "<tr><td class='gauche'>$txt_inscription_explication1</td></tr>\n";
							echo "<tr><td class='gauche'>$txt_inscription_explication3</td></tr>\n";
							echo "<tr><td class='gauche'> &nbsp; </td></tr>\n";
							echo "<tr><td class='gauche'><img src='./img/attention.gif'  width='25' alt='$txt_carefull' /> $txt_inscription_explication4</td></tr>\n";
	               			echo "</table>\n";
				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>\n";
	echo "<tr><td>";
		echo "<table class='presentation2'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";
					echo "<table class='center'>";
						echo "<tr>";
		      					echo "<td class='droite2'>$txt_login</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>";
	        	       				echo "<td class='gauche'><input id='login' name='login' value='' /></td>";
	               				echo "</tr>";
        	       				echo "<tr>";
        		       				echo "<td class='droite2'>$txt_passe</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>";
        	       					echo "<td class='gauche'><input type='password' id='passe' name='passe' value='' /></td>";
               					echo "</tr>";
	               			echo "</table>\n";
				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "</td></tr>";
	echo "<tr><td>";
		echo "<table>\n";
			echo "<tr><td class='gauche'><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>";
		echo "<tr>";
		echo "<td class='centre'  colspan='3'>";
		echo "<input type='submit' value='$txt_validate' />";
		echo "</td>";
	       	echo "</tr>";
	echo "</table>";

	echo "</form>";
	echo "<script type='text/javascript'>Form.focusFirstElement('inscription');</script>";
}

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>

