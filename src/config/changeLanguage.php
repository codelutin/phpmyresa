<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/changeLanguage.php
*
* This file is used to change the current language in the configuration tool
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');


/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
$nbGet = count($_GET);
if ( ($nbPost != 4) && ($nbPost != 2) && ($nbPost != 0) && ($nbGet != 0) && ($nbGet != 1) ) exit('-1');

if (isset($_POST['lang'])){
	$language = $_POST['lang'];
	if ( ! in_array($language, $lang)) exit('-1');
} else $language = 'english';
if (isset($_POST['from'])) $from = $_POST['from']; else if (isset($_GET['from'])) $from = $_GET['from']; else $from = 'index.php';
$from = htmlspecialchars($from);
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exit('-1');
}
if (isset($_POST['action'])){
	$action = $_POST['action'];
	if ( ! in_array($action, array('modifier_classe', 'modifier_objet'))) exit('-1');
}

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée	 	  **************
**********************************************************************************************/

if ( ! isset($_COOKIE['resa_lang'])) exit("YOU MUST ENABLE AT LEAST THE resa_lang COOKIE !");
setcookie("resa_lang", $language, time()+3600*24*30*12);


if (isset($id)){
	echo "<html>";
	echo "<body>";

	echo "<form id='change_language' action='database_content_modify.php' method='post'>\n";
	echo "<input type='hidden' name='id' value='$id' />\n";
	echo "<input type='hidden' name='action' value='$action' />\n";
	echo "</form>\n";

	echo "<script type='text/javascript'><!--
		document.forms.change_language.submit();
	//--></script>";

	echo "</body>";
	echo "</html>";

}
else {
	echo "<script type='text/javascript'><!--
		window.top.location.href='$from'
	//--></script>";
}

?>
