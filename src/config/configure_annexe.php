<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/configure_annexe.php
*
* This file enables to setup the configuration of used sofwate in PHPMyResa
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_file);
if ($temp != "") exit($temp);
require_once($config_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ( ( ($nbpar != 0) && (($nbpar > 7)  || ($nbpar < 1))) || (count($_GET) != 0)) exitWrongSignature("config/configure_annexe.php");
$bool = array('true', '', 'false');

if (isset($_POST['conf_iCal'])) $conf_iCal = $_POST['conf_iCal']; elseif (isset($iCal)) $conf_iCal = $iCal; else $conf_iCal = false;
if ( ! in_array($conf_iCal, $bool)) exitWrongSignature("config/configure_annexe.php");
if (isset($_POST['conf_URL_iCal'])) $conf_URL_iCal = $_POST['conf_URL_iCal']; elseif (isset($URL_iCal)) $conf_URL_iCal = $URL_iCal; else $conf_URL_iCal = "";
$conf_URL_iCal = htmlspecialchars($conf_URL_iCal);
if (isset($_POST['conf_CALENDARS_file_location'])) $conf_CALENDARS_file_location = $_POST['conf_CALENDARS_file_location']; elseif (isset($CALENDARS_file_location)) $conf_CALENDARS_file_location = $CALENDARS_file_location; else $conf_CALENDARS_file_location = "";
$conf_CALENDARS_file_location = htmlspecialchars($conf_CALENDARS_file_location);
if (isset($_POST['conf_CALENDARS_file_beginning_of_name'])) $conf_CALENDARS_file_beginning_of_name = $_POST['conf_CALENDARS_file_beginning_of_name']; elseif (isset($CALENDARS_file_beginning_of_name)) $conf_CALENDARS_file_beginning_of_name = $CALENDARS_file_beginning_of_name; else $conf_CALENDARS_file_beginning_of_name = "";
$conf_CALENDARS_file_beginning_of_name = htmlspecialchars($conf_CALENDARS_file_beginning_of_name);
if (isset($_POST['conf_CALENDARS_end_of_uid'])) $conf_CALENDARS_end_of_uid = $_POST['conf_CALENDARS_end_of_uid']; elseif (isset($CALENDARS_end_of_uid)) $conf_CALENDARS_end_of_uid = $CALENDARS_end_of_uid; else $conf_CALENDARS_end_of_uid = "";
$conf_CALENDARS_end_of_uid = htmlspecialchars($conf_CALENDARS_end_of_uid);
if (isset($_POST['conf_iCal_delay'])){
	$conf_iCal_delay = $_POST['conf_iCal_delay'];
	if ($conf_iCal_delay == '') $conf_iCal_delay = -1;
	else if ($conf_iCal_delay != -1){
		if ( ! ctype_digit($conf_iCal_delay) ) exitWrongSignature("config/configure_annexe.php");
	}
} elseif (isset($iCal_delay)) $conf_iCal_delay = $iCal_delay;
else $conf_iCal_delay = -1;
if (isset($_POST['conf_verif'])) $conf_verif = $_POST['conf_verif']; else $conf_verif = 'off';
if ( ! in_array($conf_verif, array('on', 'off'))) exitWrongSignature("config/configure_annexe.php");

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 	 	 **************
**********************************************************************************************/

$tr1ContentURL_iCal = "<td class='droite2'>$txt_config2_URL_iCal ".my_overlib($txt_overlib_URL_iCal, $txt_config2_URL_iCal)."</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_URL_iCal' size='30' value='$conf_URL_iCal' tabindex='2' /></td>";
$tr2ContentCALENDARS_file_location = "<td class='droite2'>$txt_config2_CALENDARS_file_location ".my_overlib($txt_overlib_CALENDARS_file_location, $txt_config2_CALENDARS_file_location)."</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_CALENDARS_file_location' size='30' value='$conf_CALENDARS_file_location' tabindex='3' /></td>";
$tr3ContentCALENDARS_file_beginning_of_name = "<td class='droite2'>$txt_config2_CALENDARS_file_beginning_of_name ".my_overlib($txt_overlib_CALENDARS_file_beginning_of_name, $txt_config2_CALENDARS_file_beginning_of_name)."</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_CALENDARS_file_beginning_of_name' size='30' value='$conf_CALENDARS_file_beginning_of_name' tabindex='4' /></td>";
$tr4ContentCALENDARS_end_of_uid = "<td class='droite2'>$txt_config2_CALENDARS_end_of_uid ".my_overlib($txt_overlib_CALENDARS_end_of_uid, $txt_config2_CALENDARS_end_of_uid)."</td><td><span class='red'>*</span> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_CALENDARS_end_of_uid' size='30' value='$conf_CALENDARS_end_of_uid' tabindex='5' /></td>";
$tr5ContentiCal_delay = "<td class='droite2'>$txt_config2_iCal_delay ".my_overlib($txt_overlib_iCal_delay, $txt_config2_iCal_delay)."</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_iCal_delay' size='30' value='$conf_iCal_delay' tabindex='6' /></td>";

$tr1ContentNotURL_iCal = "";
$tr2ContentNotCALENDARS_file_location = "";
$tr3ContentNotCALENDARS_file_beginning_of_name = "";
$tr4ContentNotCALENDARS_end_of_uid = "";
$tr5ContentNotiCal_delay = "";

if ($conf_iCal == true) {
	$tr1Content = $tr1ContentURL_iCal;
	$tr2Content = $tr2ContentCALENDARS_file_location;
	$tr3Content = $tr3ContentCALENDARS_file_beginning_of_name;
	$tr4Content = $tr4ContentCALENDARS_end_of_uid;
	$tr5Content = $tr5ContentiCal_delay;
}else {
	$tr1Content = $tr1ContentNotURL_iCal;
	$tr2Content = $tr2ContentNotCALENDARS_file_location;
	$tr3Content = $tr3ContentNotCALENDARS_file_beginning_of_name;
 	$tr4Content = $tr4ContentNotCALENDARS_end_of_uid;
	$tr5Content = $tr5ContentNotiCal_delay;
}

echo $entete;

echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";

echo "<script type='text/javascript' src='".$URL_overlib."overlib.js'></script><div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>";

?>

	<script type='text/javascript'><!--
	function sComplet(){
		if (document.forms.config_annexe.conf_iCal.value == 'true'){
			var loc1 = document.forms.config_annexe.conf_URL_iCal.value;
			if (loc1.substr(0,4) != 'http') loc1 = '../'+loc1;
			var loc2 = document.forms.config_annexe.conf_CALENDARS_file_location.value;
			if ((loc1 == '') || (loc2 == '') || (document.forms.config_annexe.conf_CALENDARS_file_beginning_of_name.value == '') || (document.forms.config_annexe.conf_CALENDARS_end_of_uid.value == '')){
				alert('<?php echo $txt_formulaire_incomplet;?>');
				return false;
			}

			if (loc1.substr(-1,1) != '/'){
				alert("<?php echo $txt_pb2_URL_iCal;?>");
				return false;
			}

			if (loc2.substr(-1,1) != '/'){
				alert("<?php echo $txt_pb2_CALENDARS_file_location;?>");
				return false;
			}

			new Ajax.Request('ajax_verify_file.php', {
				asynchronous:false,
				method:'post',
				parameters: {
					file: loc1,
					method: "distante",
				},
				onComplete: function(transport){
					valide = transport.responseText.replace(/(^\s*)|(\s*$)/g,'');
				},
				onFailure: function(){
					valide = -1;
				}
			});

			if (valide == -1){
				alert('Something went wrong...');
				return false;
			} else if (valide == 1){
				alert("<?php echo $txt_pb3_URL_iCal;?>");
				return false;
			}

			new Ajax.Request('ajax_verify_file.php', {
				asynchronous:false,
				method:'post',
				parameters: {
					file: loc2,
					method: "locale2",
				},
				onComplete: function(transport){
					valide = transport.responseText.replace(/(^\s*)|(\s*$)/g,'');
				},
				onFailure: function(){
					valide = -1;
				}
			});

			if (valide == -1){
				alert('Something went wrong...');
				return false;
			} else if (valide == 1){
				alert("<?php echo $txt_pb3_CALENDARS_file_location;?>");
				return false;
			} else if (valide == 2){
				alert("<?php echo $txt_pb4_CALENDARS_file_location;?>");
				return false;
			} else if (valide == 3){
				alert("<?php echo $txt_pb5_CALENDARS_file_location;?>");
				return false;
			}

		}
	}

	function icalOrNot(){
		if (document.forms.config_annexe.conf_iCal.value == 'true') {
			document.getElementById("conf_URL_iCal").innerHTML = " <?php echo addslashes($tr1ContentURL_iCal); ?> ";
			document.getElementById("conf_CALENDARS_file_location").innerHTML = " <?php echo addslashes($tr2ContentCALENDARS_file_location); ?> ";
			document.getElementById("conf_CALENDARS_file_beginning_of_name").innerHTML = " <?php echo addslashes($tr3ContentCALENDARS_file_beginning_of_name); ?> ";
			document.getElementById("conf_CALENDARS_end_of_uid").innerHTML = " <?php echo addslashes($tr4ContentCALENDARS_end_of_uid); ?> ";
			document.getElementById("conf_iCal_delay").innerHTML = " <?php echo addslashes($tr5ContentiCal_delay); ?> ";
		} else {
			document.getElementById("conf_URL_iCal").innerHTML = " <?php echo addslashes($tr1ContentNotURL_iCal); ?> ";
			document.getElementById("conf_CALENDARS_file_location").innerHTML = " <?php echo addslashes($tr2ContentNotCALENDARS_file_location); ?> ";
			document.getElementById("conf_CALENDARS_file_beginning_of_name").innerHTML = " <?php echo addslashes($tr3ContentNotCALENDARS_file_beginning_of_name); ?> ";
			document.getElementById("conf_CALENDARS_end_of_uid").innerHTML = " <?php echo addslashes($tr4ContentNotCALENDARS_end_of_uid); ?> ";
			document.getElementById("conf_iCal_delay").innerHTML = " <?php echo addslashes($tr5ContentNotiCal_delay); ?> ";
		}
	}

	//--></script>

<?php

if (isset($_SESSION['config'])) echo sprintf($entete2, $txt_titre_config2." - <i>$txt_installation</i>");
else if (isset($_SESSION['upgrade'])) echo sprintf($entete2, $txt_titre_config2." - <i>$txt_upgrade</i>");
else echo sprintf($entete2, $txt_titre_config2);

if ($conf_verif == 'on'){

	echo "<table class='center'>\n";
	$config_ok = true;

	if ($conf_iCal == "true") {
		if ($conf_URL_iCal == "") {
			echo display($txt_pb1_URL_iCal);
			$config_ok = false;
		} else if (substr($conf_URL_iCal,-1) != '/') {
			echo display($txt_pb2_URL_iCal);
			$config_ok = false;
		}

		if ($conf_CALENDARS_file_location == "") {
			echo display($txt_pb1_CALENDARS_file_location);
			$config_ok = false;
		} else if (substr($conf_CALENDARS_file_location,-1) != '/') {
			echo display($txt_pb2_CALENDARS_file_location);
			$config_ok = false;
		}

		if ($conf_CALENDARS_file_beginning_of_name == "") {
			echo display($txt_pb_CALENDARS_file_beginning_of_name);
			$config_ok = false;
		}

		if ($conf_CALENDARS_end_of_uid == "") {
			echo display($txt_pb_CALENDARS_end_of_uid);
			$config_ok = false;
		}
	}
	if ( ! is_numeric($conf_iCal_delay)) $conf_iCal_delay = 1;

	echo "</table>\n";

	if ($config_ok) {
		//*********************************************************************
	 	// Mise à jour du fichier - lecture du contenu puis écriture
	 	//*********************************************************************
		echo "<table class='center'>\n";
	 	$contenu = "";
	 	$handle = @fopen($config_file, "r");

	 	if ($handle) {
			$find = false;

			while (!feof($handle) && !$find) {
				$buffer = fgets($handle, 4096);
				$contenu .= $buffer;
				if (strlen($buffer) > 16){
					if (substr($buffer, 0, 16) == "$"."default_comment") {
						$find = true;
					}
				}
			}

			fclose($handle);
		} else echo display(sprintf($txt_file_pb, $config_file));

		if ($find){
			$handle = @fopen($config_file, "w");
			if ($handle) {
				fwrite($handle, $contenu);
				fwrite($handle, "\n");
				fwrite($handle, "$"."iCal = $conf_iCal;\n");
				fwrite($handle, "$"."URL_iCal = \"$conf_URL_iCal\";\n");
				fwrite($handle, "$"."CALENDARS_file_location = \"$conf_CALENDARS_file_location\";\n");
				fwrite($handle, "$"."CALENDARS_file_beginning_of_name = \"$conf_CALENDARS_file_beginning_of_name\";\n");
				fwrite($handle, "$"."CALENDARS_end_of_uid = \"$conf_CALENDARS_end_of_uid\";\n");
				fwrite($handle, "$"."iCal_delay = $conf_iCal_delay;\n");
				fwrite($handle, "?>\n");
				fclose($handle);

				echo display($txt_db_no_error3);

				if ( isset($_SESSION['config']) || isset($_SESSION['upgrade']) ){
					echo "<tr><td></td></tr>";
					echo "<tr><td class='centre'><input type='button' value='$txt_term' onclick=\"document.location.href='index.php'\" /></td></tr>";
					if ( isset($_SESSION['config']) ) unset($_SESSION['config']); else unset($_SESSION['upgrade']);
				} else{
					echo "<tr><td></td></tr>";
					echo "<tr><td class='centre'><input type='button' value='$txt_back_config' onclick=\"document.location.href='index.php'\" /></td></tr>";
				}
			} else echo display(sprintf($txt_file_pb, $config_file));
		} else echo display(sprintf($txt_db_file_error, $config_file));
		echo "</table>\n";
	}
} else {
	echo "<form id='config_annexe' action='configure_annexe.php' method='post' onsubmit='return sComplet()'>\n";
		echo "<table class='center'>\n";
		echo "<tr><td>";

		echo "<h3>$txt_config2_main_titre1</h3>\n";
		echo "<table class='presentation3'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";

						echo "<tr><td class='droite2'>";
						echo "<input type='hidden' name='conf_verif' value='$conf_verif' />\n";
						echo "$txt_config2_iCal</td><td></td><td class='gauche'>\n";
							echo "<select name='conf_iCal' onchange='icalOrNot();' tabindex='1'>\n";
							if ($conf_iCal == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
						echo "</select></td>\n";
						echo "</tr>\n";

						echo "<tr id='conf_URL_iCal'>$tr1Content</tr>\n";
						echo "<tr id='conf_CALENDARS_file_location'>$tr2Content</tr>\n";
						echo "<tr id='conf_CALENDARS_file_beginning_of_name'>$tr3Content</tr>\n";
						echo "<tr id='conf_CALENDARS_end_of_uid'>$tr4Content</tr>\n";
						echo "<tr id='conf_iCal_delay'>$tr5Content</tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "<tr><td  style='background-color:#F5F5F5;font-size:12px' colspan='3'>$txt_explanation_ical</td></tr>";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config2_main_titre2</h3>\n";
		echo "<table class='presentation3'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td>";
							if (@file('Spreadsheet/Excel/Writer.php', FILE_USE_INCLUDE_PATH) === FALSE) $generation_excel1 = false; else $generation_excel1 = true;
							$repertoire = getcwd()."/../stats";
							if ( ! is_writable($repertoire)) $generation_excel2 = false; else $generation_excel2 = true;

							if ( $generation_excel1 && $generation_excel2 ) echo $txt_excel_actif;
							else{
								echo $txt_explanation_excel1;
								echo "<ul>";
								if ( ! $generation_excel1) echo "<li>".$txt_explanation_excel2."</li>";
								if ( ! $generation_excel2) echo "<li>".$txt_explanation_excel3."</li>";
								echo "</ul>";
							}
						echo "</td></tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config2_main_titre3</h3>\n";
		echo "<table class='presentation3'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td>";

							$tab_extension = get_loaded_extensions();
							$trouve = false;
							for ($i = 0 ; $i < count($tab_extension) ; $i++)
								if (strtoupper($tab_extension[$i]) == 'SOAP'){
									$trouve = true;
									break;
								}
							if ($trouve) echo $txt_excel_actif; else echo $txt_soap_pb;
						echo "</td></tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "</td></tr>";
	echo "<tr><td>";
		echo "<table>\n";
			echo "<tr><td class='gauche'><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>\n";
		echo "<tr><td class='centre' colspan='2'><input type='button' value='$txt_erase' onclick=\"document.location.href='configure_annexe.php'\" tabindex='7' /></td>";
		echo "<td>&nbsp; &nbsp;  &nbsp;  &nbsp;</td>";
		echo "<td class='centre'><input type='submit' value='$txt_validate' onclick=\"document.forms.config_annexe.conf_verif.value='on'\" tabindex='8' /></td></tr>\n";
	echo "</table>\n";

	echo "</form>\n";
}

echo "</div>\n";
echo "</div>\n";

echo "<script type='text/javascript'>Form.focusFirstElement('config_annexe');</script>";
echo $div_footer;
echo $end;
?>
