<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/configure_password.php
*
* This file allows to change the credentials to enter the configuration area of PHPMyResa
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_credential_file);
if ($temp != "") exit($temp);
require_once($config_credential_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 0) || (count($_GET) != 0)) exitWrongSignature("config/configure_password.php");

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

echo $entete;

echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";

?>
	<script type='text/javascript'><!--

		var valide = "0";

		function sComplet_pass(){
			txt1 = document.forms.pass_change.old_password.value;
			txt2 = document.forms.pass_change.new_password.value;
			txt3 = document.forms.pass_change.new_password2.value;
			testlength = txt1.length * txt2.length * txt3.length;
			if(testlength == 0){
				alert ("<?php echo $txt_champs_obligatoire;?>");
				return false;
			} else if (txt2 != txt3){
				alert("<?php echo $txt_retaper_pass;?>");
				document.forms.pass_change.new_password2.value = "";
				return false;
			} else if (txt2.length < 8){
				alert("<?php echo $txt_pass_contain;?>");
				document.forms.pass_change.new_password.value = "";
				document.forms.pass_change.new_password2.value = "";
				return false;
			}

			new Ajax.Request('ajax_configure_password.php', {
				asynchronous:false,
				method:'post',
				parameters: {
					new_password: $('new_password').value,
					old_password: $('old_password').value
				},
				onComplete: function(transport){
					valide = transport.responseText;
				},
				onFailure: function(){
					valide = -1;
				}
			});

			if(valide == "1") {
				alert("<?php echo $txt_pass_change_valide;?>");
				return true;
			} else if(valide == "0") {
				alert("<?php echo $txt_pass_change_invalide;?>");
				document.forms.pass_change.old_password.value = "";
				return false;
			} else {
				alert("<?php echo $txt_error2;?>");
				return false;
			}
		}

	//--></script>

<?php
echo sprintf($entete2, $txt_titre_password);

echo "<form id='pass_change' action='logout.php' method='post' onsubmit=\"return sComplet_pass()\">\n";
	echo "<table class='presentation2'>\n";
		echo "  <tr class='top'>\n";
			echo "    <td class='left'></td>\n";
			echo "    <td class='center'></td>\n";
			echo "    <td class='right'></td>\n";
		echo "  </tr>\n";
		echo "  <tr class='middle'>\n";
			echo "    <td class='left'></td>\n";
			echo "    <td class='center'>\n";



				echo "<table class='center'>\n";
					echo "<tr>\n";
        					echo "<td class='droite2'>$txt_old_password</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>\n";
						echo "<td class='gauche'><input type='password' id='old_password' name='old_password' value='' /></td>\n";
					echo "</tr>\n";
					echo "<tr>\n";
						echo "<td class='droite2'>$txt_new_password</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>\n";
						echo "<td class='gauche'><input type='password' id='new_password' name='new_password' value='' /></td>\n";
					echo "</tr>\n";
					echo "<tr>\n";
						echo "<td class='droite2'>$txt_new_password</td><td><span class='red'>*</span> &nbsp;&nbsp;</td>\n";
						echo "<td class='gauche'><input type='password' id='new_password2' name='new_password2' value='' /></td>\n";
					echo "</tr>\n";
					echo "<tr><td><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
				echo "</table>\n";
			echo "    </td>\n";
			echo "    <td class='right'></td>\n";
		echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
		echo "  </tr>\n";
	echo "</table>\n";

	echo "<table class='center'>";
		echo "<tr>\n";
			echo "<td class='centre' colspan='3'>";
				echo "<input type='submit' value='$txt_validate' />";
			echo "</td>\n";
		echo "</tr>\n";
	echo "</table>\n";

echo "</form>\n";

echo "<table>\n";
echo "<tr><td>$txt_note</td></tr>\n";;
echo "</table>\n";

echo "</div>\n";
echo "</div>\n";

echo "<script type='text/javascript'>Form.focusFirstElement('pass_change');</script>";
echo $div_footer;
echo $end;
?>
