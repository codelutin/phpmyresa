<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/delete_session_files.php
*
* This file enables to delete all session files of the web server
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierres
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/
$nbPost = count($_POST);
if ( ( ($nbPost != 0) && ($nbPost != 1) ) || (count($_GET) != 0)) exitWrongSignature("config/delete_session_files.php");
// $_POST n'est pas utilisé

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 	 	 **************
**********************************************************************************************/
echo $entete;
echo sprintf($entete2, $txt_titre_session);

$session_path = session_save_path();
if ($session_path == "") $session_path = "/tmp";

$message = deletion_of_session_files($session_path, false);
if ($message == ""){
	if ($nbPost == 0){
		$nb = count(glob($session_path."/sess_*"));
		if ($nb == 0){
			echo "<table class='center'>\n";
			echo display(sprintf($txt_no_session_file, $session_path));
			echo "</table>\n";
		} else{
			echo "<form id='reload' method='post' action='delete_session_files.php'>\n";
			echo "<table class='center'>\n";
			echo display(sprintf($txt_propose_deletion_session_files1, $nb, $session_path));
			echo display("$txt_propose_deletion_session_files2");
			echo display(" &nbsp; ");
			echo display("$txt_confirm");
			echo display("<input type='hidden' name='test' value='1' />");
			echo display("<input type='button' value=\"$txt_no\" onclick=\"javascript:window.location='./index.php'\" />
			               &nbsp; &nbsp;
			              <input type='submit' value=\"$txt_yes\" />");
			echo display(" &nbsp; ");
			echo display(" &nbsp; ");
			echo display("<i>$txt_notice_session_deletion</i>");
			echo "</table>\n";
			echo "</form>\n";
		}
	} else{
		echo "<table class='center'>\n";
		$message = deletion_of_session_files($session_path, true);
		if ($message == "") echo display("$txt_titre_session$txt_ok");
		else{
			echo display("$txt_titre_session$txt_error");
			echo $message;
		}
		echo "</table>\n";
	}
} else{
	echo "<table class='center'>\n";
	echo display(sprintf($txt_pb_session, $session_path));
	echo $message;
	echo "</table>\n";
}

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>
