<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/verify_database_structure.php
*
* This file verify the database structure, according to the structure.dat file
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 0) || (count($_GET) != 0)) exitWrongSignature("config/verify_database_structure.php");

/***********************************************************************************************
**************		 fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

echo $entete;
echo sprintf($entete2, $txt_titre_db_structure);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") exit($message_redirect_configure_database);
$connexionDB = $tab[1];

$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message != "") exit($error_message);
if ($tab2[0] == 0) exit($message_redirect_create_database);

$tab3 = verify_database_structure();
$pb = $tab3[0];
if ($pb != "") exit($pb);
$mes = $tab3[1];
if ($mes != "") exit($mes);
else echo "<table class='center'>".display($txt_no_db_structure_pb)."</table>";

if (isset($_SESSION['upgrade'])) echo "<table class='center'><tr><td><input type='button' value='$txt_suiv' onclick=\"document.location.href='fill_languages.php'\" /></td></tr></table>";

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo $end;
?>
