<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/configure_main.php
*
* This file enables to setup the main configuration of PHPMyResa
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_file);
if ($temp != "") exit($temp);
require_once($config_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ( ( ($nbpar != 0) && ($nbpar != 2) && ($nbpar != 31)  && ($nbpar != 32) && ($nbpar > 34)) || (count($_GET) != 0)) exitWrongSignature("config/configure_main.php");

$bool = array('true', '', 'false');

if (isset($_POST['conf_titre'])) $conf_titre = $_POST['conf_titre']; elseif (isset($titre)) $conf_titre = $titre; else $conf_titre = "PHPMyResa";
$conf_titre = htmlspecialchars($conf_titre);
if (isset($_POST['conf_bookmark'])) $conf_bookmark = $_POST['conf_bookmark']; elseif (isset($bookmark)) $conf_bookmark = $bookmark; else $conf_bookmark = "PHPMyResa";
$conf_bookmark = htmlspecialchars($conf_bookmark);
if (isset($_POST['conf_image_titre'])) $conf_image_titre = $_POST['conf_image_titre']; elseif (isset($image_titre)) $conf_image_titre = $image_titre; else $conf_image_titre = "";
$conf_image_titre = htmlspecialchars($conf_image_titre);
if (isset($_POST['conf_image_pdf'])) $conf_image_pdf = $_POST['conf_image_pdf']; elseif (isset($image_pdf)) $conf_image_pdf = $image_pdf; else $conf_image_pdf = "";
$conf_image_pdf = htmlspecialchars($conf_image_pdf);
if (isset($_POST['conf_page_accueil'])){
	$conf_page_accueil = $_POST['conf_page_accueil'];
	if ( ! in_array($conf_page_accueil, array('vueMois.php', 'vide.php', 'today.php'))) exitWrongSignature("config/configure_main.php");
}elseif (isset($page_accueil)) $conf_page_accueil = $page_accueil; else $conf_page_accueil = "vueMois.php";
if (isset($_POST['conf_email_domain'])) $conf_email_domain = $_POST['conf_email_domain']; elseif (isset($email_domain)) $conf_email_domain = $email_domain; else $conf_email_domain = "";
$conf_email_domain = htmlspecialchars($conf_email_domain);
if (isset($_POST['conf_capacite'])){
	$conf_capacite = $_POST['conf_capacite'];
	if ( ! in_array($conf_capacite, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($capacite)) $conf_capacite = $capacite; else $conf_capacite = false;
if (isset($_POST['conf_modification_enable'])){
	$conf_modification_enable = $_POST['conf_modification_enable'];
	if ( ! in_array($conf_modification_enable, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($modification_enable)) $conf_modification_enable = $modification_enable; else $conf_modification_enable = false;
if (isset($_POST['conf_default_language'])) $conf_default_language = $_POST['conf_default_language']; elseif (isset($default_language)) $conf_default_language = $default_language; else $conf_default_language = "english";
$conf_default_language = htmlspecialchars($conf_default_language);
if (isset($_POST['conf_display_tomorrow'])){
	$conf_display_tomorrow = $_POST['conf_display_tomorrow'];
	if ( ! in_array($conf_display_tomorrow, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($display_tomorrow)) $conf_display_tomorrow = $display_tomorrow; else $conf_display_tomorrow = false;
if (isset($_POST['conf_several_object_possible'])){
	$conf_several_object_possible = $_POST['conf_several_object_possible'];
	if ( ! in_array($conf_several_object_possible, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($several_object_possible)) $conf_several_object_possible = $several_object_possible; else $conf_several_object_possible = false;
if (isset($_POST['conf_week_end_enabled'])){
	$conf_week_end_enabled = $_POST['conf_week_end_enabled'];
	if ( ! in_array($conf_week_end_enabled, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($week_end_enabled)) $conf_week_end_enabled = $week_end_enabled; else $conf_week_end_enabled = true;
if (isset($_POST['conf_tree'])){
	$conf_tree = $_POST['conf_tree'];
	if ( ! in_array($conf_tree, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($tree)) $conf_tree = $tree; else $conf_tree = false;
if (isset($_POST['conf_tree_scrollable'])){
	$conf_tree_scrollable = $_POST['conf_tree_scrollable'];
	if ( ! in_array($conf_tree_scrollable, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($tree_scrollable)) $conf_tree_scrollable = $tree_scrollable; else $conf_tree_scrollable = true;
if (isset($_POST['conf_mail_subject'])){
	$conf_mail_subject = $_POST['conf_mail_subject'];
	if ( ! in_array($conf_mail_subject, array('long', 'medium', 'short'))) exitWrongSignature("config/configure_main.php");
} elseif (isset($mail_subject)) $conf_mail_subject = $mail_subject; else $conf_mail_subject = "medium";
if (isset($_POST['conf_default_class_for_feed'])) $conf_default_class_for_feed = $_POST['conf_default_class_for_feed']; elseif (isset($default_class_for_feed)) $conf_default_class_for_feed = $default_class_for_feed; else $conf_default_class_for_feed = "";
$conf_default_class_for_feed = htmlspecialchars($conf_default_class_for_feed);
if (isset($_POST['conf_use_diffusion_flag'])){
	$conf_use_diffusion_flag = $_POST['conf_use_diffusion_flag'];
	if ( ! in_array($conf_use_diffusion_flag, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($use_diffusion_flag)) $conf_use_diffusion_flag = $use_diffusion_flag; else $conf_use_diffusion_flag = false;
if (isset($_POST['conf_technical_contact'])) $conf_technical_contact = $_POST['conf_technical_contact']; elseif (isset($technical_contact)) $conf_technical_contact = $technical_contact; else $conf_technical_contact = "";
$conf_technical_contact = htmlspecialchars($conf_technical_contact);
if (isset($_POST['conf_technical_email'])){
	$conf_technical_email = $_POST['conf_technical_email'];
	if ( ! validate_email($conf_technical_email)) exitWrongSignature("config/configure_main.php");
} elseif (isset($technical_email)) $conf_technical_email = $technical_email; else $conf_technical_email = "";
if (isset($_POST['conf_technical_tel'])) $conf_technical_tel = $_POST['conf_technical_tel']; elseif (isset($technical_tel)) $conf_technical_tel = $technical_tel; else $conf_technical_tel = "";
$conf_technical_tel = htmlspecialchars($conf_technical_tel);
if (isset($_POST['conf_read_only_enable'])){
	$conf_read_only_enable = $_POST['conf_read_only_enable'];
	if ( ! in_array($conf_read_only_enable, $bool)) exitWrongSignature("config/configure_main.php");
} elseif (isset($read_only_enable)) $conf_read_only_enable = $read_only_enable; else $conf_read_only_enable = false;
if (isset($_POST['conf_IP_regex'])) $conf_IP_regex = $_POST['conf_IP_regex']; elseif (isset($IP_regex)) $conf_IP_regex = $IP_regex; else $conf_IP_regex = "";
$conf_IP_regex = htmlspecialchars($conf_IP_regex);
if (isset($_POST['conf_enabled_domain'])){
	$conf_enabled_domain = $_POST['conf_enabled_domain'];
	$conf_enabled_domain = htmlspecialchars($conf_enabled_domain);
} elseif (isset($enabled_domain)) $conf_enabled_domain = $enabled_domain; else $conf_enabled_domain = "";
if (isset($_POST['conf_mail_server'])) $conf_mail_server = $_POST['conf_mail_server']; elseif (isset($mail_server)) $conf_mail_server = $mail_server; else $conf_mail_server = "";
$conf_mail_server = htmlspecialchars($conf_mail_server);
if (isset($_POST['conf_web_server'])) $conf_web_server = $_POST['conf_web_server']; elseif (isset($web_server)) $conf_web_server = $web_server; else $conf_web_server = "";
$conf_web_server = htmlspecialchars($conf_web_server);
if (isset($_POST['conf_default_beginning_hour'])){
	$conf_default_beginning_hour = $_POST['conf_default_beginning_hour'];
	if ( ! ctype_digit(strval($conf_default_beginning_hour)) || (strval($conf_default_beginning_hour) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_beginning_hour)) $conf_default_beginning_hour = $default_beginning_hour; else $conf_default_beginning_hour = 8;
if (isset($_POST['conf_default_beginning_mihour'])){
	$conf_default_beginning_mihour = $_POST['conf_default_beginning_mihour'];
	if ( ! ctype_digit(strval($conf_default_beginning_mihour)) || (strval($conf_default_beginning_mihour) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_beginning_mihour)) $conf_default_beginning_mihour = $default_beginning_mihour; else $conf_default_beginning_mihour = 0;
if (isset($_POST['conf_default_duration'])){
	$conf_default_duration = $_POST['conf_default_duration'];
	if ( ! ctype_digit(strval($conf_default_duration)) || (strval($conf_default_duration) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_duration)) $conf_default_duration = $default_duration; else $conf_default_duration = 1;
if (isset($_POST['conf_default_miduration'])){
	$conf_default_miduration = $_POST['conf_default_miduration'];
	if ( ! ctype_digit(strval($conf_default_miduration)) || (strval($conf_default_miduration) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_miduration)) $conf_default_miduration = $default_miduration; else $conf_default_miduration = 0;
if (isset($_POST['conf_default_end_hour'])){
	$conf_default_end_hour = $_POST['conf_default_end_hour'];
	if ( ! ctype_digit(strval($conf_default_end_hour)) || (strval($conf_default_end_hour) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_end_hour)) $conf_default_end_hour = $default_end_hour; else $conf_default_end_hour = 18;
if (isset($_POST['conf_default_end_mihour'])){
	$conf_default_end_mihour = $_POST['conf_default_end_mihour'];
	if ( ! ctype_digit(strval($conf_default_end_mihour)) || (strval($conf_default_end_mihour) == '') ) exitWrongSignature("config/configure_main.php");
} elseif (isset($default_end_mihour)) $conf_default_end_mihour = $default_end_mihour; else $conf_default_end_mihour = 0;
if (isset($_POST['conf_default_comment'])){
	$conf_default_comment = $_POST['conf_default_comment'];
	if (!get_magic_quotes_gpc()) $conf_default_comment = addslashes($conf_default_comment);
} elseif (isset($default_comment)) $conf_default_comment = $default_comment; else $conf_default_comment = "";
$conf_default_comment = htmlspecialchars($conf_default_comment);
if (isset($_POST['conf_verif'])) $conf_verif = $_POST['conf_verif']; else $conf_verif = 'off';
if ( ! in_array($conf_verif, array('on', 'off'))) exitWrongSignature("config/configure_main.php");

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);



if ( is_array($conf_enabled_domain) ) {
	$temp = "";
	for ($i=0; $i<count($conf_enabled_domain); $i++) {
		$temp .= $conf_enabled_domain[$i].", ";
	}
	$temp = substr($temp, 0, -2);
	$conf_enabled_domain = $temp;
}

$tr1ContentTreeScrol = "<td class='droite2'>$txt_config1_tree_scrollable</td><td> &nbsp; &nbsp;</td><td class='gauche'>";
$tr1ContentTreeScrol .= "<select name='conf_tree_scrollable' tabindex='14'>";
$tr1ContentTreeScrol .= "<option selected='selected' value='true'>$txt_yes</option>";
$tr1ContentTreeScrol .= "<option value='false'>$txt_no</option>";
$tr1ContentTreeScrol .= "</select></td>";

$tr1ContentTreeNotScrol = "<td class='droite2'>$txt_config1_tree_scrollable</td><td> &nbsp; &nbsp;</td><td class='gauche'>";
$tr1ContentTreeNotScrol .= "<select name='conf_tree_scrollable'>";
$tr1ContentTreeNotScrol .= "<option value='true'>$txt_yes</option>";
$tr1ContentTreeNotScrol .= "<option selected='selected' value='false'>$txt_no</option>";
$tr1ContentTreeNotScrol .= "</select></td>";

$tr1ContentNotTree = "";

if ($conf_tree == true){
	if ($conf_tree_scrollable == 'true') $tr1Content = $tr1ContentTreeScrol;
	else $tr1Content = $tr1ContentTreeNotScrol;
}else $tr1Content = $tr1ContentNotTree;


$tr2ContentIP_regex = "<td class='droite2'>$txt_config1_IP_regex ".my_overlib($txt_overlib_IP_regex, $txt_config1_IP_regex)."</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_IP_regex' size='30' value='$conf_IP_regex' tabindex='22' /></td>";
$tr3Contentenabled_domain = "<td class='droite2'>$txt_config1_enabled_domain ".my_overlib($txt_overlib_enabled_domain, $txt_config1_enabled_domain)."</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_enabled_domain' size='30' value='$conf_enabled_domain' tabindex='23' /></td>";

$tr2ContentNotReseau = "";
$tr3ContentNotReseau = "";

if ($conf_read_only_enable == true) {
	$tr2Content = $tr2ContentIP_regex;
	$tr3Content = $tr3Contentenabled_domain;
}else {
	$tr2Content = $tr2ContentNotReseau;
	$tr3Content = $tr3ContentNotReseau;
}






//*********************************************************************
//  Beginning of display
//*********************************************************************

echo $entete;
echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>";
echo "<script type='text/javascript' src='".$URL_overlib."overlib.js'></script><div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>";

?>

	<script type='text/javascript'><!--
	function sComplet(){
		var emailTC = document.forms.config_main.conf_technical_email.value;
		var img = document.forms.config_main.conf_image_titre.value;
		var img2 = document.forms.config_main.conf_image_pdf.value;

		if (img != ""){
			new Ajax.Request('ajax_verify_file.php', {
				asynchronous:false,
				method:'post',
				parameters: {
					file: "../img/"+img,
					method: "locale",
				},
				onComplete: function(transport){
					valide = transport.responseText.replace(/(^\s*)|(\s*$)/g,'');
				},
				onFailure: function(){
					valide = -1;
				}
			});

			if (valide == -1){
				alert('Something went wrong...');
				return false;
			} else if (valide == 1){
				alert("<?php echo $txt_pb1_image_titre;?>");
				return false;
			} else if (valide == 2){
				alert("<?php echo $txt_pb2_image_titre;?>");
				return false;
			}
		}

		if (img2 != ""){
			if ( (img2.substr(-4,4) != ".jpg") && (img2.substr(-5,5) != ".jpeg") ){
				alert("<?php echo $txt_pb2_image_pdf;?>");
				return false;
			}

			new Ajax.Request('ajax_verify_file.php', {
				asynchronous:false,
				method:'post',
				parameters: {
					file: "../img/"+img2,
					method: "locale",
				},
				onComplete: function(transport){
					valide = transport.responseText.replace(/(^\s*)|(\s*$)/g,'');
				},
				onFailure: function(){
					valide = -1;
				}
			});

			if (valide == -1){
				alert('Something went wrong...');
				return false;
			} else if (valide == 1){
				alert("<?php echo $txt_pb1_image_pdf;?>");
				return false;
			} else if (valide == 2){
				alert("<?php echo $txt_pb3_image_pdf;?>");
				return false;
			}
		}

		if ((document.forms.config_main.conf_technical_contact.value == '') || (emailTC == '') || (document.forms.config_main.conf_technical_tel.value == '')){
			alert('<?php echo $txt_formulaire_incomplet;?>');
			return false
		}

		if ( ! emailIsCorrect(emailTC)) {
			alert("<?php echo $txt_new_class_mail1_incomplet;?>");
			return false
		}

		if (document.forms.config_main.conf_read_only_enable.value == 'true') {
			var domain = document.forms.config_main.conf_enabled_domain.value;
			if (domain != ""){
				var RegExp1 = new RegExp("(^[a-z0-9._-]{2,}\.[a-z0-9 ,._-]+\.[a-z]{2,4}$)");
				if (!RegExp1.exec(domain)){
					alert('<?php echo $txt_pb_domain;?>');
					return false;
				}
			} else if (document.forms.config_main.conf_IP_regex.value == '') {
				alert("<?php echo $txt_pb_IP_regex;?>");
				return false;
			}
		}

		if ((document.forms.config_main.conf_mail_server.value == '') && (document.forms.config_main.conf_web_server.value != '')){
			alert('<?php echo $txt_pb_mail_server;?>');
			return false;
		}

		if ((document.forms.config_main.conf_mail_server.value != '') && (document.forms.config_main.conf_web_server.value == '')){
			alert('<?php echo $txt_pb_web_server;?>');
			return false;
		}

		var verif = /^[0-9]+$/
		if (verif.exec(document.forms.config_main.conf_default_beginning_hour.value) == null){
			alert("<?php echo $txt_config1_default_beginning_hour_error;?>");
			return false;
		}
		if (verif.exec(document.forms.config_main.conf_default_beginning_mihour.value) == null){
			alert("<?php echo $txt_config1_default_beginning_mihour_error;?>");
			return false;
		}


		if (verif.exec(document.forms.config_main.conf_default_duration.value) == null){
			alert("<?php echo $txt_config1_default_duration_error;?>");
			return false;
		}
		if (verif.exec(document.forms.config_main.conf_default_miduration.value) == null){
			alert("<?php echo $txt_config1_default_miduration_error;?>");
			return false;
		}
		if (verif.exec(document.forms.config_main.conf_default_end_hour.value) == null){
			alert("<?php echo $txt_config1_default_end_hour_error;?>");
			return false;
		}
		if (verif.exec(document.forms.config_main.conf_default_end_mihour.value) == null){
			alert("<?php echo $txt_config1_default_beginning_mihour_error;?>");
			return false;
		}

		return true;
	}

	function scrolornot(){
		if (document.forms.config_main.conf_tree.value == 'true') document.getElementById("conf_tree_scrollable").innerHTML = " <?php echo $tr1ContentTreeScrol; ?> ";
		else document.getElementById("conf_tree_scrollable").innerHTML = " <?php echo $tr1ContentNotTree; ?> ";
	}

	function ReseauOrNot(){
		if (document.forms.config_main.conf_read_only_enable.value == 'true') {
			document.getElementById("conf_IP_regex").innerHTML = " <?php echo addslashes($tr2ContentIP_regex); ?> ";
			document.getElementById("conf_enabled_domain").innerHTML = " <?php echo addslashes($tr3Contentenabled_domain); ?> ";
		} else {
			document.getElementById("conf_IP_regex").innerHTML = " <?php echo addslashes($tr2ContentNotReseau); ?> ";
			document.getElementById("conf_enabled_domain").innerHTML = " <?php echo addslashes($tr3ContentNotReseau); ?> ";
		}
	}
	//--></script>

<?php

if (isset($_SESSION['config'])) echo sprintf($entete2, $txt_titre_config1." - <i>$txt_installation</i>");
else if (isset($_SESSION['upgrade'])) echo sprintf($entete2, $txt_titre_config1." - <i>$txt_upgrade</i>");
else echo sprintf($entete2, $txt_titre_config1);

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
if ($tab[0] != "") exit($message_redirect_configure_database);
$connexionDB = $tab[1];

$tab2 = array();
$tab2 = calculate_number_of_tables($database);
$error_message = $tab2[1];
if ($error_message != "") exit($error_message);
if ($tab2[0] == 0) exit($message_redirect_create_database);

$tab3 = verify_database_structure();
$pb = $tab3[0];
if ($pb != "") exit($pb);
$mes = $tab3[1];
if ($mes != "") exit($mes);

$DB_request = "SELECT nom FROM classe";
$resultat = database_query($DB_request, $connexionDB);
if ( ! $resultat) exit("<table class='center'>".display_error($DB_request)."</table>$end_error");

$trContentClassForFeed = "";
while ($row = database_fetch_object($resultat)){
	$nom = database_get_from_object($row, 'nom');
	if ($nom == $conf_default_class_for_feed) $trContentClassForFeed .= "<option selected='selected' value='$nom'>$nom</option>\n";
	else $trContentClassForFeed .= "<option value='$nom'>$nom</option>\n";
}


if ($conf_verif == 'on'){

	echo "<table class='center'>\n";
	$config_ok = true;

	if ($conf_image_titre != ""){
		$location = '../img/'.$conf_image_titre;
		if ( ! file_exists($location)) {
			echo display($txt_pb1_image_titre);
			$config_ok = false;
		}
	}

	if ($conf_image_pdf != ""){
			$location = '../img/'.$conf_image_pdf;
			if ( ! file_exists($location)) {
				echo display($txt_pb1_image_pdf);
				$config_ok = false;
			}
			else if ( (substr($conf_image_pdf, -4) != '.jpg') && (substr($conf_image_pdf, -5) != '.jpeg') ) {
				echo display($txt_pb2_image_pdf);
				$config_ok = false;
			}
	}

	if ($conf_technical_contact == "") {
		echo display($txt_pb_technical_contact);
		$config_ok = false;
	}

	if ($conf_technical_email == "") {
		echo display($txt_pb_technical_email);
		$config_ok = false;
	}

	if ($conf_technical_tel == "") {
		echo display($txt_pb_technical_tel);
		$config_ok = false;
	}

	if ($conf_read_only_enable == "true"){
		if ($conf_enabled_domain != ""){
			$regex = '#^[a-z0-9._-]{2,}\.[a-z0-9 ,._-]+\.[a-z]{2,4}$#';
			if ( ! (preg_match($regex, $conf_enabled_domain)) ){
				$enabled_domain = false;
				echo display($txt_pb_domain);
				$config_ok = false;
			} else {
				$tmp = split(",",$conf_enabled_domain);
				$conf_enabled_domain = "array(";
				for ($i=0; $i<count($tmp); $i++)  $conf_enabled_domain .= "\"".trim($tmp[$i])."\", ";
				$conf_enabled_domain = substr($conf_enabled_domain, 0, -2).")";
			}
		} else if ($conf_IP_regex == "") {
			echo display($txt_pb_IP_regex);
			$config_ok = false;
		}
	} else {
		if ($conf_enabled_domain != ""){
			$regex = '#^[a-z0-9._-]{2,}\.[a-z0-9 ,._-]+\.[a-z]{2,4}$#';
			if ( ! (preg_match($regex, $conf_enabled_domain)) ) $conf_enabled_domain == "";
			else {
				$tmp = split(",",$conf_enabled_domain);
				$conf_enabled_domain = "array(";
				for ($i=0; $i<count($tmp); $i++)  $conf_enabled_domain .= "\"".trim($tmp[$i])."\", ";
				$conf_enabled_domain = substr($conf_enabled_domain, 0, -2).")";
			}
		}
	}
	if ($conf_enabled_domain == "") $conf_enabled_domain = "\"\"";

	if ( ($conf_mail_server == "") && ($conf_web_server != "") ) {
		echo display($txt_pb_mail_server);
		$config_ok = false;
	}
	else if ( ($conf_mail_server != "") && ($conf_web_server == "") ) {
		echo display($txt_pb_web_server);
		$config_ok = false;
	}
	echo "</table>\n";

	if ($config_ok) {
		//*********************************************************************
	 	// Mise à jour du fichier - lecture du contenu puis écriture
	 	//*********************************************************************
		echo "<table class='center'>\n";
	 	$contenu1 = "";
		$contenu2 = "";
	 	$handle = @fopen($config_file, "r");

	 	if ($handle) {
			$find_part1 = false;
			$find_part2 = false;
			$can_write = false;

			$buffer = fgets($handle, 4096);
			while (!feof($handle)) {
				while ( ! $find_part1) {
					if (strlen($buffer) > 6){
						if (substr($buffer, 0, 6) == "$"."titre"){
							$find_part1 = true;
							continue;
						}
					}
					$contenu1 .= $buffer;
					$buffer = fgets($handle, 4096);
				}
				if (strlen($buffer) > 5){
					if (substr($buffer, 0, 5) == "$"."iCal"){
						$contenu2 .= $buffer;
						while (!feof($handle)) {
							$buffer = fgets($handle, 4096);
							$contenu2 .= $buffer;
							if (strlen($buffer) > 2) {
								if (substr($buffer, 0, 2) == "?>") $find_part2 = true;
							}
						}
					}
				}
				$buffer = fgets($handle, 4096);
			}
			if (($find_part1 == true) && ($find_part2 == true)) $can_write = true;
			fclose($handle);
		} else echo display(sprintf($txt_file_pb,$config_file));

		if ($can_write){
			$handle = @fopen($config_file, "w");
			if ($handle) {
				if (  $conf_tree == 'false') $conf_tree_scrollable = 'true';
				fwrite($handle, $contenu1);
				fwrite($handle, "$"."titre = \"$conf_titre\";\n");
				fwrite($handle, "$"."bookmark = \"$conf_bookmark\";\n");
				fwrite($handle, "$"."image_titre = \"$conf_image_titre\";\n");
				fwrite($handle, "$"."image_pdf = \"$conf_image_pdf\";\n");
				fwrite($handle, "$"."page_accueil = \"$conf_page_accueil\";\n");
				fwrite($handle, "$"."email_domain = \"$conf_email_domain\";\n");
				fwrite($handle, "$"."capacite = $conf_capacite;\n");
				fwrite($handle, "$"."modification_enable = $conf_modification_enable;\n");
				fwrite($handle, "$"."default_language = \"$conf_default_language\";\n");
				fwrite($handle, "$"."display_tomorrow = $conf_display_tomorrow;\n");
				fwrite($handle, "$"."several_object_possible = $conf_several_object_possible;\n");
				fwrite($handle, "$"."week_end_enabled = $conf_week_end_enabled;\n");
				fwrite($handle, "$"."tree = $conf_tree;\n");
				fwrite($handle, "$"."tree_scrollable = $conf_tree_scrollable;\n");
				fwrite($handle, "$"."mail_subject = \"$conf_mail_subject\";\n");
				fwrite($handle, "$"."default_class_for_feed = \"$conf_default_class_for_feed\";\n");
				fwrite($handle, "$"."use_diffusion_flag = $conf_use_diffusion_flag;\n\n");
				fwrite($handle, "$"."technical_contact = \"$conf_technical_contact\";\n");
				fwrite($handle, "$"."technical_email = \"$conf_technical_email\";\n");
				fwrite($handle, "$"."technical_tel = \"$conf_technical_tel\";\n\n");
				fwrite($handle, "$"."read_only_enable = $conf_read_only_enable;\n");
				fwrite($handle, "$"."IP_regex = \"$conf_IP_regex\";\n");
				fwrite($handle, "$"."enabled_domain = $conf_enabled_domain;\n\n");
				fwrite($handle, "$"."mail_server = \"$conf_mail_server\";\n");
				fwrite($handle, "$"."web_server = \"$conf_web_server\";\n\n");
				fwrite($handle, "$"."default_beginning_hour = $conf_default_beginning_hour;\n");
				fwrite($handle, "$"."default_beginning_mihour = $conf_default_beginning_mihour;\n");
				fwrite($handle, "$"."default_duration = $conf_default_duration;\n");
				fwrite($handle, "$"."default_miduration = $conf_default_miduration;\n");
				fwrite($handle, "$"."default_end_hour = $conf_default_end_hour;\n");
				fwrite($handle, "$"."default_end_mihour = $conf_default_end_mihour;\n");
				fwrite($handle, "$"."default_comment = \"$conf_default_comment\";\n\n");
				fwrite($handle, $contenu2);
				fclose($handle);

				echo display($txt_db_no_error3);

				if ( isset($_SESSION['config']) || isset($_SESSION['upgrade']) ){
					echo "<tr><td></td></tr>";
					echo "<tr><td class='centre'><input type='button' value='$txt_suiv' onclick=\"document.location.href='configure_annexe.php'\" /></td></tr>";
				} else{
					echo "<tr><td></td></tr>";
					echo "<tr><td class='centre'><input type='button' value='$txt_back_config' onclick=\"document.location.href='index.php'\" /></td></tr>";
				}
			} else echo display(sprintf($txt_file_pb, $config_file));
		} else echo display(sprintf($txt_db_file_error, $config_file));
		echo "</table>\n";
	}
} else {

	/***********************************************************************************************
	**************		 Affichage de quelques données de configuration		  **************
	**********************************************************************************************/

	$version = phpversion();
	$register_global = ini_get('register_globals');
	if ($register_global == false) $register_global_txt = "";
	else $register_global_txt = "<tr><td class='centre'><img src='img/attention.gif' width='25' alt='$txt_carefull' /><span class='red'>$txt_register_global</span></td></tr>";

	echo "
	<table class='presentation3'>
	  <tr class='top'>
	    <td class='left'></td>
	    <td class='center'></td>
	    <td class='right'></td>
	  </tr>
	  <tr class='middle'>
	    <td class='left'></td>
	    <td class='center'>
		<table class='center'>
			<tr><td class='centre'>".sprintf($txt_info_php, $version)."</td></tr>
			$register_global_txt
		</table>
	    </td>
	    <td class='right'></td>
	  </tr>
	  <tr class='bottom'>
	    <td class='left'></td>
	    <td class='center'></td>
	    <td class='right'></td>
	  </tr>
	</table>";

	/***********************************************************************************************
	**************	    Fin d'affichage de quelques données de configuration	  **************
	**********************************************************************************************/


	echo "<form id='config_main' action='configure_main.php' method='post' onsubmit='return sComplet()'>\n";
	echo "<table class='center'>\n";
	echo "<tr><td>";
		echo "<h3>$txt_config1_main_titre1</h3>\n";
		echo "<table class='presentation4'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td class='droite2'>";
						echo "<input type='hidden' name='conf_verif' value='$conf_verif' />\n";
						echo "$txt_config1_titre</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_titre' size='30' value='$conf_titre' tabindex='1' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_bookmark </td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_bookmark' size='30' value='$conf_bookmark' tabindex='2' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_image_titre</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_image_titre' size='30' value='$conf_image_titre' tabindex='3' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_image_pdf</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_image_pdf' size='30' value='$conf_image_pdf' tabindex='4' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_page_accueil</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_page_accueil' tabindex='5'>\n";
							if ($conf_capacite == 'vueMois.php'){
								echo "<option selected='selected' value='vueMois.php'>$txt_config1_page_accueil1</option>\n";
								echo "<option value='vide.php'>$txt_config1_page_accueil2</option>\n";
								echo "<option value='today.php'>$txt_config1_page_accueil3</option>\n";
							} else if ($conf_capacite == 'vide.php'){
								echo "<option value='vueMois.php'>$txt_config1_page_accueil1</option>\n";
								echo "<option selected='selected' value='vide.php'>$txt_config1_page_accueil2</option>\n";
								echo "<option value='today.php'>$txt_config1_page_accueil3</option>\n";
							} else {
								echo "<option value='vueMois.php'>$txt_config1_page_accueil1</option>\n";
								echo "<option value='vide.php'>$txt_config1_page_accueil2</option>\n";
								echo "<option selected='selected' value='today.php'>$txt_config1_page_accueil3</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_email_domain</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_email_domain' size='30' value='$conf_email_domain' tabindex='6' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_capacite ".my_overlib($txt_overlib_capacite, $txt_config1_capacite)."</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_capacite' tabindex='7'>\n";
							if ($conf_capacite == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_modification_enable ".my_overlib($txt_overlib_modification_enable, $txt_config1_modification_enable)."</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_modification_enable' tabindex='8'>\n";
							if ($conf_modification_enable == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
						echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_language</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_default_language' tabindex='9'>\n";
							$lang2_tmp = getAvailableLanguagesFromDB();
							$lang2 = $lang2_tmp[1];
							for ($i = 0 ; $i < count($lang2) ; $i++){
								$inter = $lang2[$i];
								if ($conf_default_language == $inter) echo "<option selected='selected' value='$inter'>$inter</option>\n";
								else echo "<option value='$inter'>$inter</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_display_tomorrow</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_display_tomorrow' tabindex='10'>\n";
							if ($conf_display_tomorrow == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_several_object_possible</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_several_object_possible' tabindex='11'>\n";
							if ($conf_several_object_possible == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_week_end_enabled </td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_week_end_enabled' tabindex='12'>\n";
							if ($conf_week_end_enabled == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_tree</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_tree' onchange='scrolornot();'  tabindex='13'>\n";
							if ($conf_tree == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";

						echo "<tr id='conf_tree_scrollable'><td>$tr1Content</td></tr>\n";

						echo "<tr><td class='droite2'>$txt_config1_mail_subject</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_mail_subject' tabindex='15'>\n";
							if ($conf_mail_subject == 'short') echo "<option selected='selected' value='short'>$txt_short</option>\n"; else echo "<option value='short'>$txt_short</option>\n";
							if ($conf_mail_subject == 'medium') echo "<option selected='selected' value='medium'>$txt_medium</option>\n"; else echo "<option value='medium'>$txt_medium</option>\n";
							if ($conf_mail_subject == 'long') echo "<option selected='selected' value='long'>$txt_long</option>\n"; else echo "<option value='long'>$txt_long</option>\n";
						echo "</select></td></tr>\n";

						echo "<tr><td class='droite2'>$txt_config1_default_class_for_feed</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_default_class_for_feed' tabindex='16'>\n";
							echo $trContentClassForFeed;
							echo "</select></td></tr>\n";

						echo "<tr><td class='droite2'>$txt_config1_use_diffusion_flag ".my_overlib($txt_overlib_feed, $txt_config1_use_diffusion_flag)."</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
							echo "<select name='conf_use_diffusion_flag' tabindex='17'>\n";
							if ($conf_use_diffusion_flag == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";

					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td><span style='font-size:smaller'>&nbsp;$txt_recommandation1</span></td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config1_main_titre2</h3>\n";
		echo "<table class='presentation4'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td class='droite2'>$txt_config1_technical_contact</td><td><span class='red'>*</span> &nbsp;&nbsp;</td><td class='gauche'><input name='conf_technical_contact' size='30' value='$conf_technical_contact' tabindex='18' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_technical_email</td><td><span class='red'>*</span> &nbsp;&nbsp;</td><td class='gauche'><input name='conf_technical_email' size='30' value='$conf_technical_email' tabindex='19' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_technical_tel</td><td><span class='red'>*</span> &nbsp;&nbsp;</td><td class='gauche'><input name='conf_technical_tel' size='30' value='$conf_technical_tel' tabindex='20' /></td></tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config1_main_titre3</h3>\n";
		echo "<table class='presentation4'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td class='droite2'>$txt_config1_read_only_enable ".my_overlib($txt_overlib_read_only_enable, $txt_config1_read_only_enable)."</td><td> &nbsp; &nbsp;</td><td class='gauche'>\n";
						echo "<select name='conf_read_only_enable' onchange='ReseauOrNot()' tabindex='21'>\n";
							if ($conf_read_only_enable == 'true'){
								echo "<option selected='selected' value='true'>$txt_yes</option>\n";
								echo "<option value='false'>$txt_no</option>\n";
							} else{
								echo "<option value='true'>$txt_yes</option>\n";
								echo "<option selected='selected' value='false'>$txt_no</option>\n";
							}
							echo "</select></td></tr>\n";

						echo "<tr id='conf_IP_regex'>$tr2Content</tr>\n";
						echo "<tr id='conf_enabled_domain'>$tr3Content</tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config1_main_titre4</h3>\n";
		echo "<table class='presentation4'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td class='droite2'>$txt_config1_mail_server ".my_overlib($txt_overlib_mail_server, $txt_config1_mail_server)."</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_mail_server' size='30' value='$conf_mail_server' tabindex='24' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_web_server ".my_overlib($txt_overlib_web_server, $txt_config1_web_server)."</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_web_server' size='30' value='$conf_web_server' tabindex='25' /></td></tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "<table>\n";
		echo "<tr><td>&nbsp;</td></tr>";
	echo "</table>\n";

		echo "<h3>$txt_config1_main_titre5</h3>\n";
		echo "<table class='presentation4'>\n";
			echo "  <tr class='top'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='middle'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'>\n";

					echo "<table class='center'>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_beginning_hour</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_beginning_hour' size='2' value='$conf_default_beginning_hour' tabindex='26' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_beginning_mihour</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_beginning_mihour' size='2' value='$conf_default_beginning_mihour' tabindex='27' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_duration</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_duration' size='2' value='$conf_default_duration' tabindex='28' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_miduration</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_miduration' size='2' value='$conf_default_miduration' tabindex='29' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_end_hour</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_end_hour' size='2' value='$conf_default_end_hour' tabindex='30' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_end_mihour</td><td> &nbsp; &nbsp;</td><td class='gauche'><input name='conf_default_end_mihour' size='2' value='$conf_default_end_mihour' tabindex='31' /></td></tr>\n";
						echo "<tr><td class='droite2'>$txt_config1_default_comment</td><td> &nbsp;&nbsp;</td><td class='gauche'><input name='conf_default_comment' size='30' value='$conf_default_comment' tabindex='32' /></td></tr>\n";
					echo "</table>\n";

				echo "    </td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
			echo "  <tr class='bottom'>\n";
				echo "    <td class='left'></td>\n";
				echo "    <td class='center'></td>\n";
				echo "    <td class='right'></td>\n";
			echo "  </tr>\n";
		echo "</table>\n";

	echo "</td></tr>";
	echo "<tr><td>";
		echo "<table>\n";
			echo "<tr><td class='gauche'><span class='red'>*</span><i>$txt_mandatory</i></td></tr>\n";
		echo "</table>\n";
	echo "</td></tr>";
	echo "</table>\n";

	echo "<table class='center'>\n";
		echo "<tr><td class='centre'  colspan='2'><input type='button' value='$txt_erase' onclick=\"document.location.href='configure_main.php'\" tabindex='35' /></td>";
		echo "<td>&nbsp; &nbsp;  &nbsp;  &nbsp;</td>";
		echo "<td class='centre'><input type='submit' value='$txt_validate' onclick=\"document.forms.config_main.conf_verif.value='on'\" tabindex='36' /></td></tr>\n";
	echo "</table>\n";

	echo "</form>\n";
}

echo "</div>\n";
echo "</div>\n";

echo "<script type='text/javascript'>Form.focusFirstElement('config_main');</script>";
echo $div_footer;
echo $end;
?>
