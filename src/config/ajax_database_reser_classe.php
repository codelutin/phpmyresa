<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/ajax_database_reser_classe.php
*
* This file allows to know the number of objects and reservations depending on a specific class,
* called by database_content.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_database_file);
if ($temp != "") exit('-1');
require_once($config_database_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 1) || (count($_GET) != 0) ) exit('-1');
if (isset($_POST['classe_id'])){
	$classe_id = $_POST['classe_id'];
	if ( ! ctype_digit($classe_id) || ($classe_id == '') ) exit('-1');
} else exit('-1');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
$connexionDB = $tab[1];

$DB_request = "SELECT count(*) as nb FROM objet WHERE id_classe='$classe_id'";
$result = database_query($DB_request, $connexionDB);
$result = database_fetch_object($result);
$nb_objet = database_get_from_object($result, 'nb');


$DB_request2 = "SELECT count(*) as nb2 FROM reservation R, objet O WHERE O.id_classe='$classe_id' AND O.id=R.idobjet";
$result = database_query($DB_request2, $connexionDB);
$result = database_fetch_object($result);
$nb_reser = database_get_from_object($result, 'nb2');


$resultat  = '<?xml version="1.0"?>';
$resultat .= "<response>";
$resultat .= "<nb_objet>$nb_objet</nb_objet>";
$resultat .= "<nb_reser>$nb_reser</nb_reser>";
$resultat .= "</response>";

header('Content-Type: text/xml');
echo $resultat;

?>
