<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/ajax_login_connexion.php
*
* This file enables to verify user's credentials via ajax, called by login.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_credential_file);
if ($temp != "") exit('-1');
require_once($config_credential_file);


/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 2) || (count($_GET) != 0) ) exit('-1');

if (isset($_POST['login'])){
	$login = $_POST['login'];
	$login = htmlspecialchars($login);
	if ($login == '') exit('-1');
} else exit('-1');
if (isset($_POST['passe'])){
	$passe = $_POST['passe'];
	if (strlen($passe) < 8) exit('-1');
	$passe = htmlspecialchars($passe);
} else exit('-1');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 	 	 **************
**********************************************************************************************/

// The file $config_credential_file must already contain values for $credential_login and $credential_pass
if ( isset($credential_login) && isset($credential_pass) ){
	if ( ($credential_login == '') && ($credential_pass == '') ) exit('-1');
} else exit('-1');

if( ($credential_login == sha1($login)) && ($credential_pass == sha1($passe)) ){
	$_SESSION['connected']='true';
	exit('1');
} else exit('-1');

?>
