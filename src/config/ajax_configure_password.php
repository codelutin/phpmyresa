<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/ajax_configure_password.php
*
* This file enables a password modification via ajax, called by configure_password.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain	Pierre
*
* @copyright	2007,2008 Frédéric Melot
* @copyright	2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

$temp = verify_file($config_credential_file);
if ($temp != "") exit('-1');
require_once($config_credential_file);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ( (count($_POST) != 2) || (count($_GET) != 0) ) exit('-1');

if (isset($_POST['new_password'])){
	$new_password = $_POST['new_password'];
	if (strlen($new_password) < 8) exit('-1');
	$new_password = htmlspecialchars($new_password);
} else exit('-1');
if (isset($_POST['old_password'])){
	$old_password = $_POST['old_password'];
	if (strlen($old_password) < 8) exit('-1');
	$old_password = htmlspecialchars($old_password);
} else exit('-1');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/


// The file $config_credential_file must already contain values for $credential_login and $credential_pass
if ( isset($credential_login) && isset($credential_pass) ){
	if ( ($credential_login == '') && ($credential_pass == '') ) exit('-1');
} else exit('-1');

if( $credential_pass != sha1($old_password) ) exit('0');

$contenu = "";
$handle = @fopen($config_credential_file, "r");
if ($handle) {
	$i = 0;
	$trouve = false;
	while (!feof($handle) && $i != 1) {
		$buffer = fgets($handle, 4096);
		$contenu .= $buffer;
		if (strlen($buffer) > 8){
			if (substr($buffer, 0, 17) == "$"."credential_login"){
				$i++;
			}
		}
	}
	if ($i == 1) $trouve = true;
	fclose($handle);
} else exit('-1');
if ($trouve){
	$handle = @fopen($config_credential_file, "w");
	if ($handle) {
		fwrite($handle, $contenu);
		fwrite($handle, "$"."credential_pass = \"".sha1($new_password)."\";\n");
		fwrite($handle, "?>");
		fclose($handle);
	} else exit('-1');
} else exit('-1');

exit('1');
?>
