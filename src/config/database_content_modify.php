<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/database_content_modify.php
*
* This file enables to modify the content of the database tables
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

$nbPost = count($_POST);
if ( ($nbPost != 2) || (count($_GET) != 0)) exitWrongSignature("config/database_content_modify.php");

if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature("config/database_content_modify.php");
} else exitWrongSignature("config/database_content_modify.php");
if (isset($_POST['action'])){
	$action = $_POST['action'];
	if ( ! in_array($action, array('modifier_classe', 'modifier_objet'))) exitWrongSignature("config/database_content_modify.php");
} else exitWrongSignature("config/database_content_modify.php");

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$temp = verify_file($config_database_file);
if ($temp != "") exit($temp);
require_once($config_database_file);

echo $entete;
echo "<div class='princ'>\n";

echo "<script type='text/javascript' src='".$URL_overlib."overlib.js'>\n</script><div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>\n\n";
echo "<script type='text/javascript' src='".$URL_Ajax."'>\n</script>\n";


echo "<form id='changeLanguage' action='changeLanguage.php' method='post'>\n";
echo "<table class='all'>\n<tr><td class='droite'>\n";
echo "<p><input type='hidden' name='from' value='$from' /></p>\n";
echo "<p><input type='hidden' name='id' value='$id' /></p>\n";
echo "<p><input type='hidden' name='action' value='$action' /></p>\n";
echo "<p><input type='hidden' name='lang' value='' /></p>\n";
for ($i = 0 ; $i < count($lang) ; $i++){
	if ($lang[$i] != $language) echo "<img height='15pt' src='../img/drapeaux/".$lang[$i].".png' alt='".$lang[$i]."' onclick=\"goChangeLanguage('".$lang[$i]."')\" /> \n";
}
echo "</td></tr>\n</table>\n";
echo "</form>\n\n";


?>
	<script type='text/javascript'><!--

	function sComplet_classe(){
		if (document.forms.modifier_classe.classe_nom_classe.value == "") {
			alert('<?php echo $txt_new_class_nom_classe_incomplet;?>');
			return false;
		}
		if (document.forms.modifier_classe.classe_pass.value == "") {
			alert('<?php echo $txt_new_class_pass_incomplet;?>');
			return false;
		}
		if (document.forms.modifier_classe.classe_mail.value == "") {
			alert("<?php echo $txt_new_class_mail_incomplet;?>");
			return false;
		}
		if ( ! emailIsCorrect(document.forms.modifier_classe.classe_mail.value)) {
				alert("<?php echo $txt_new_class_mail1_incomplet;?>");
				return false;
		}

		new Ajax.Request('ajax_database_unicity.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				ajout: $('classe_nom_classe').value,
				id: $('id').value,
				pass: $('classe_pass').value,
				class_object : 'classe'
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}

		});

		if(valide == "1") {
			alert("<?php echo $txt_new_class_nom_classe_invalide;?>");
			return false;
		} else if(valide == "2") {
			alert("<?php echo $txt_new_class_pass_invalide;?>");
			return false;
		} else if (valide == "-1") {
			alert("<?php echo $txt_error2;?>");
			return false;
		}
		return true;
	}

	function sComplet_object(){
		if (document.forms.modifier_objet.nom_objet.value == "") {
			alert("<?php echo $txt_new_objet_nom_objet_incomplet;?>");
			return false;
		}
		if (document.forms.modifier_objet.libelle.value == "") {
			alert("<?php echo $txt_new_objet_libelle_incomplet;?>");
			return false;
		}
		if (document.forms.modifier_objet.nom_objet.value.indexOf(',') != -1) {
			alert("<?php echo $txt_database_content_J_13;?>");
			return false;
		}
		if (document.forms.modifier_objet.nom_objet.value.indexOf("'") != -1) {
			alert("<?php echo $txt_database_content_J_14;?>");
			return false;
		}

		new Ajax.Request('ajax_database_unicity.php', {
			asynchronous:false,
			method:'post',
			parameters: {
				ajout: $('nom_objet').value,
				id: $('id').value,
				class_object : 'objet'
			},
			onComplete: function(transport){
				valide = transport.responseText;
			},
			onFailure: function(){
				valide = -1;
			}

		});

		if(valide == "1") {
			alert("<?php echo $txt_new_objet_nom_objet_invalide;?>");
			return false;
		} else if (valide == "-1") {
			alert("<?php echo $txt_error2;?>");
			return false;
		}
		return true;
	}
	//--></script>
<?php


if ($action == 'modifier_classe') $var = $txt_titre_db_content_modify_classe;
else if ($action == 'modifier_objet') $var = $txt_titre_db_content_modify_object;



echo "<div class='d2'>\n";
echo "<h1>$var</h1>\n";
echo "</div>\n\n";
echo "<div class='d4'>\n";

echo "<table class='center'>\n";

$tab = array();
$tab = verify_database_connexion($database, HOST, USER, PWD, BD);
$connexionDB = $tab[1];

if ($action == 'modifier_classe') {
	$temp = getAvailableLanguagesFromDB();
	$pb = $temp[0];
	if ($pb != ""){
		echo $pb;
		$encounred_problem = true;
		//on donne des valeurs par défaut
		$tab_languages = array();
		$tab_languages[0] = "french";
		$tab_languages[1] = "english";
	} else $tab_languages = $temp[1];
	$nb_tab_languages = count($tab_languages);


	$DB_request = "SELECT nom, jour_meme FROM classe WHERE id='$id'";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
	$result = database_fetch_object($result);
	$classe_nom_classe = database_get_from_object($result, 'nom');
	$classe_jour_meme = database_get_from_object($result, 'jour_meme');

	$DB_request = "SELECT nom, prenom, pass, mail, telephone, default_language FROM administrateur WHERE id_classe='$id'";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
	$result = database_fetch_object($result);
	$classe_nom_admin = database_get_from_object($result, 'nom');
	$classe_prenom_admin = database_get_from_object($result, 'prenom');
	$classe_pass = database_get_from_object($result, 'pass');
	$classe_mail = database_get_from_object($result, 'mail');
	$classe_telephone = database_get_from_object($result, 'telephone');
	$classe_language = database_get_from_object($result, 'default_language');

	echo "<tr><td>\n";
	echo "<form id='modifier_classe' action='database_content.php' method='post' onsubmit='return sComplet_classe()'>\n";
	echo "<table class='center'>\n";
	echo class_header();
	echo "<tr class='color1'>\n";
	echo "   <td><input type='hidden' name='action' value='modifier_classe' />
	         <input type='hidden' id='id' name='id' value='$id' />\n";
	echo "   <input id='classe_nom_classe' name='classe_nom_classe' value='$classe_nom_classe' /></td>\n";
	echo "   <td><select name='classe_jour_meme'>";
	if ($classe_jour_meme == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><input name='classe_nom_admin' value='$classe_nom_admin' /></td>\n";
	echo "   <td><input name='classe_prenom_admin' value='$classe_prenom_admin' /></td>\n";
	echo "   <td><input name='classe_pass' id='classe_pass' value='$classe_pass' /></td>\n";
	echo "   <td><input name='classe_mail' value='$classe_mail' /></td>\n";
	echo "   <td><input name='classe_telephone' value='$classe_telephone' /></td>\n";
	echo "   <td><select name='classe_language'>";
	for ($u = 0 ; $u < $nb_tab_languages ; $u ++){
		$current_language = $tab_languages[$u];
		if ($classe_language == $current_language) echo "<option selected='selected' value='$current_language'>$current_language</option>";
		else echo "<option value='$current_language'>$current_language</option>";
	}
	echo "   <td><input type='submit' value='$txt_validate' /></td>
	         <td><input type='button' value='$txt_cancel' onclick=\"window.open('database_content.php', '_self')\" /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";
} else if ($action == 'modifier_objet') {
	$DB_request = "SELECT nom, capacite, status, libelle, priority, pubical, available, visible, wifi FROM objet WHERE id='$id'";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result) echo display_error($DB_request);
	$result = database_fetch_object($result);
	$nom_objet = database_get_from_object($result, 'nom');
	$capacite = database_get_from_object($result, 'capacite');
	$status = database_get_from_object($result, 'status');
	$libelle = database_get_from_object($result, 'libelle');
	$priority = database_get_from_object($result, 'priority');
	$pubical = database_get_from_object($result, 'pubical');
	$available = database_get_from_object($result, 'available');
	$visible = database_get_from_object($result, 'visible');
	$wifi = database_get_from_object($result, 'wifi');

	echo "<tr><td>\n";
	echo "<form id='modifier_objet' action='database_content.php' method='post' onsubmit='return sComplet_object()'>\n";
	echo "<table class='center'>\n";
	echo object_header();
	echo "<tr class='color1'>\n";
	echo "   <td><input type='hidden' name='action' value='modifier_objet' />
	             <input type='hidden' id='id' name='id' value='$id' />\n";
	echo "   <input id='nom_objet' name='nom_objet' value='$nom_objet' /></td>\n";
	echo "   <td><input name='capacite' value='$capacite' /></td>\n";
	echo "   <td><select name='status'>";
	if ($status  == '1') echo "   <option value='0'>$txt_yes</option><option selected='selected' value='1'>$txt_no</option></select></td>";
	else echo "   <option selected='selected' value='0'>$txt_yes</option><option value='1'>$txt_no</option></select></td>";
	echo "   <td><input name='libelle' value='$libelle' /></td>\n";
	echo "   <td><select name='priority'>";
	if ($priority == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><select name='pubical'>";
	if ($pubical == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><select name='available'>";
	if ($available == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><select name='visible'>";
	if ($visible == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><select name='wifi'>";
	if ($wifi == '1') echo "   <option selected='selected' value='1'>$txt_yes</option><option value='0'>$txt_no</option></select></td>";
	else echo "   <option value='1'>$txt_yes</option><option selected='selected' value='0'>$txt_no</option></select></td>";
	echo "   <td><input type='submit' value='$txt_validate' /></td>
	         <td><input type='button' value='$txt_cancel' onclick=\"window.open('database_content.php', '_self')\" /></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</form>\n";
	echo "</td></tr>\n";
}

echo "</table>\n";

echo "</div>\n";
echo "</div>\n";
echo $div_footer;
echo "<script type='text/javascript'>Form.focusFirstElement('$action');</script>";
echo $end;
?>
