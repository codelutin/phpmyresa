
//**********************************************************************************************
/**
* Project PHPMyResa / File /config/commun/function.js
*
* This file contains some javascript functions
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Fr�d�ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2003,2004,2005,2006,2007,2008 Fr�d�ric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


function emailIsCorrect(email){
	if (email.search(/^[+_a-zA-Z0-9-]+(\.[+_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$/) == -1) return false;
	else return true;
}

function goChangeLanguage(lang){
	document.forms.changeLanguage.lang.value=lang;
	document.forms.changeLanguage.submit();
}
