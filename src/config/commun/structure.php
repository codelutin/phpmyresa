<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/structure.php
*
* This file describe the current database structure
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2006,2007,2008 Frédéric Melot
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


$table[0] = "classe";
$table[1] = "administrateur";
$table[2] = "objet";
$table[3] = "reservation";
$table[4] = "languages";

$column[0][0] = "id";
$column[0][1] = "nom";
$column[0][2] = "jour_meme";

$column[1][0] = "id_classe";
$column[1][1] = "nom";
$column[1][2] = "prenom";
$column[1][3] = "pass";
$column[1][4] = "mail";
$column[1][5] = "telephone";
$column[1][6] = "default_language";

$column[2][0] = "id";
$column[2][1] = "id_classe";
$column[2][2] = "nom";
$column[2][3] = "capacite";
$column[2][4] = "status";
$column[2][5] = "libelle";
$column[2][6] = "priority";
$column[2][7] = "pubical";
$column[2][8] = "available";
$column[2][9] = "visible";
$column[2][10] = "wifi";

$column[3][0] = "id";
$column[3][1] = "idmulti";
$column[3][2] = "idobjet";
$column[3][3] = "titre";
$column[3][4] = "jour";
$column[3][5] = "debut";
$column[3][6] = "duree";
$column[3][7] = "email";
$column[3][8] = "commentaire";
$column[3][9] = "valide";
$column[3][10] = "pass";
$column[3][11] = "priority";
$column[3][12] = "wifi";
$column[3][13] = "state";
$column[3][14] = "diffusion";

$column[4][0] = "name";
$column[4][1] = "english";
$column[4][2] = "french";
$column[4][3] = "spanish";
$column[4][4] = "german";
?>
