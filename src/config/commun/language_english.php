<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/english.php
*
* This file is the english texts for the configuration part
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2007,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


/***********************************************************************************************
****************				General				****************
************************************************************************************************/

$txt_yes = "Yes";
$txt_no = "No";
$txt_bookmark = "Configuration of PHPMyResa";
$txt_exit_nb_parameter = "Non supported method or bad number of parameters";
$txt_titre_inscription = "Inscription";
$txt_titre_connexion = "Connection";
$txt_titre_password = "Password modification";
$txt_titre_database = "Database connection";
$txt_titre_create_database = "Database structure creation";
$txt_titre_languages = "Verification of the languages table content and update of its data";
$txt_titre_session = "Deletion of session files";
$txt_titre_db_structure = "Verification of the database structure";
$txt_titre_db_content = "Database content (classes, administrators and objects)";
$txt_titre_db_content_modify_classe = "Class modification";
$txt_titre_db_content_modify_object = "Object modification";
$txt_titre_config1 = "Main configuration";
$txt_titre_config2 = "Configuration of third-party softwares";
$txt_titre_database_config = "Database content configuration";
$txt_titre_application_config = "Application configuration";
$txt_back_resa = "Back to the booking system";
$txt_back_config = "Back to the main configuration menu";
$txt_mandatory = ": mandatory";
$txt_erase = "Re initialize";
$txt_validate = "Validate";
$txt_cancel = "Cancel";
$txt_modi = "To modify";
$txt_sup = "To delete";
$txt_suiv = "Next";
$txt_term = "To finish";
$txt_formulaire_incomplet = "Please fill in all mandatory fields";
$txt_help = "Help";
$txt_installation = "installation";
$txt_upgrade = "upgrade";


/***********************************************************************************************
****************	Partie Configuration de la base de donnÃ©es		****************
***********************************************************************************************/

$txt_no_db_extension_installed = "No database module loaded by PHP! You must install MySQL, PostgreSQL or Oracle.";
$txt_database_type = "Database type:";
$txt_hostname = "Name of the database server:";
$txt_username = "Database username:";
$txt_password = "Database password:";
$txt_database_name = "Database name:";
$txt_db_no_error1 = "A database connection test succeeded.";
$txt_db_no_error2 = "Your configuration has been saved in the database.php file.";
$txt_db_no_error3 = "Your configuration has been saved in the config.php file.";
$txt_db_no_error4 = "Your database connection parameters are already known.";
$txt_db_error = "A database connection test failed.";
$txt_db_init = "Please fill in all parameters";
$txt_back = "Try again";
$txt_db_error_back = "Click on the '$txt_back' button below to change your connection parameters.";
$txt_db_error1 = "There is a database connection error on '%s' with the login/password '%s'";
$txt_db_error2 = "There is a database connection error on '%s' on the database server '%s' with the login/password '%s'";
$txt_db_file_error1 = "The configuration file '%s' does not exist";
$txt_db_file_error2 = "The configuration file '%s' is read-only access";
$txt_db_file_error3 = "The folder '%s' is not readable";
$txt_db_file_error4 = "Some database connection parameters have incorrect values, please fill in the following fields.";
$txt_db_file_error5 = "The folder '%s' is read-only access";
$txt_db_file_error = "Impossible to update your file '%s'";
$txt_file_pb = "Failed to open the file '%s'";
$txt_redirect_connexion = "Please begin by configuring your database connection.";
$txt_config = "to configure";

$txt_nbTable_error = "The number of database tables can not be found.";
$txt_create_tables = "You must now create the database structure. To do so, click here: ";
$txt_verify_tables = "You are upgrading PHPMyResa. To continue, click here: ";
$txt_no_need_to_create_db = "The database structure is already created.";
$txt_need_to_create_db = "The database structure is not created.";
$txt_redirect_structure = "Please begin by creating it. To do so, click here: ";
$txt_pb_requete1 = "A problem occurs during the execution of the SQL request: ";
$txt_pb_requete2 = "The error message is: ";
$txt_create_db_incorrect = "Impossible to automatically create the database structure. Please do it manually.";
$txt_create_db_correct = "The database structure creation succeded.";

$txt_nbRows_languages_error = "Impossible to obtain the database languages table content.";
$txt_nbRows_languages_empty = "The database languages table is empty. To fill it, click here: ";
$txt_nbRows_languages_not_empty = "The database languages table is already filled. If you want to verify the coherence of its content or to update it, click here: ";
$txt_fill_languages_button = "Filling of the database languages table";
$txt_fill_languages_question1 = "The database languges tables is already filled. It contains %d lines. Are you sure you want to update it?";
$txt_fill_languages_question4 = "(deletion of old data, insertion of data from the file doc/INSTALL_data_db.sql then deletion of session files)";
$txt_fill_languages_question5 = "(%d translations are located in your file doc/INSTALL_data_db.sql)";
$txt_suppress_data_table_languages = "Deletion of old data ... OK";
$txt_insert_data_table_languages = "Insertion of new data ... OK";
$txt_insert_incorrect_data_table_languages = "All data have been inserted (%d out of %d) ... ERROR";
$txt_notice_session_deletion = "N.B. If you delete the session files your connection to the config area will be lost ==> you will need to log in again";

$txt_summary2 = "Database languages table content verification";
$txt_structure_file_pb = "The file that contains the database structure is corrupted";
$txt_no_db_structure_pb = "Your database structure is correct.";
$txt_db_structure_update = "Your database structure has been updated (database table(s) creation / Field(s) creation for existing database tables).";
$txt_db_structure_pb_table = "Some mandatory tables are missing:";
$txt_db_structure_pb_column = "Some mandatory columns are missing:";
$txt_db_structure_pb1 = "Please fix that manually";
$txt_db_structure_pb2 = "Your database structure is not correct.";
$txt_table = "table";
$txt_column = "column";

$txt_pass_change_valide = "Your password has been successfully saved";
$txt_pass_change_invalide = "Please re enter your password";
$txt_old_password = "Old password:";
$txt_new_password = "New password:";
$txt_note = "Notice: a password must contain at least 8 characters.";
$txt_champs_obligatoire = "All fields must be filled in";
$txt_retaper_pass = "Please fill in again your password";
$txt_pass_contain = "Your password must contain at least 8 characters";

$txt_login = "Login: ";
$txt_passe = "Password: ";
$txt_inscription_valide = "Your login/password have been successfully saved";
$txt_inscription_invalide = "a problem occured during your registration";
$txt_connexion_invalide = "Incorrect login/password";
$txt_deconnexion = "To logout";
$txt_inscription_explication1 = "You are in the configuration area of PHPMyResa.";
$txt_inscription_explication2 = "Please log in to access it.";
$txt_inscription_explication3 = "In order to secure this area, please register a login / password to access it.";
$txt_inscription_explication4 = "No link will be created to this area, so bookmark it";

$txt_error = " ... ERROR";
$txt_error2 = "An error occured";
$txt_ok = " ... OK";
$txt_pb_creation = "The folder '%s' cannot be created";
$txt_pb_deletion = "Impossible to delete the file '%s'";
$txt_pb_deletion2 = "Please finish manually the session file destruction (%s/sess_*)";
$txt_pb_session = "A problem occured at the readout of the folder containing the session files: %s";
$txt_propose_deletion_session_files1 = "You are going to delete all session files of your web server (%d files 'sess_*' in the folder '%s')";
$txt_propose_deletion_session_files2 = "It is mandatory if you modified the database languages table content.";
$txt_confirm = "Are you sure you want to continue?";
$txt_no_session_file = "No session file exists in the folder %s";


/***********************************************************************************************
****************		Partie Contenu de la base de donnÃ©es		****************
***********************************************************************************************/

$txt_verification = "Tests done :";
$txt_database_content[0] = "Verification of existence and write access on the file '$config_database_file'";
$txt_database_content[1] = "Database connection test";
$txt_database_content[2] = "Verification that the number of database tables is not null";
$txt_database_content[3] = "Verification of the database structure";
$txt_database_content[4] = "Verification that at least one class exists in the database";
$txt_database_content[5] = "For each ressource with a priority level, a message containing the kind of priority is mandatory in the languages database table";
$txt_database_content[6] = "The administrator default language must be correctly set";
$txt_database_content[7] = "Empty administrator passwords are not allowed";
$txt_database_content[8] = "Administrator passwords must be all different";
$txt_database_content[9] = "All administrators must be link to an existing class (only MySQL) ?";
$txt_database_content[10] = "All objects must be part of an existing class (only MySQL) ?";
$txt_database_content[11] = "All reservations must be link to existing objects (only MySQL) ?";
$txt_database_content[12] = "An administrator must be declared for each class";
$txt_database_content[13] = "The name of an object can not contain the character &quot;,&quot;";
$txt_database_content_J_13 = "The name of an object can not contain the character ','.";
$txt_database_content[14] = "The name of an object can not contain the character &quot;'&quot;";
$txt_database_content_J_14 = "The name of an object can not contain the character \\\"'\\\"";
$txt_database_content[15] = "The 'jour_meme' field of database table classe must be set to 0 or 1";
$txt_database_content[16] = "Fields status, priority, pubiCal, available, visible, wifi of the database table objet must be set to 0 or 1";
$txt_database_content[17] = "Each class must contain at least one object";
$txt_database_content[18] = "Each class must contain at least one visible object";
$txt_database_content[19] = "Each class must contain at least one available object";

$txt_title_db_content_nom_classe = "Class name";
$txt_title_db_content_jour_meme = "Can be booked the current day";
$txt_title_db_content_nom_admin = "Administrator lastname";
$txt_title_db_content_prenom_admin = "Administrateur fistname";
$txt_title_db_content_pass = "Password";
$txt_title_db_content_email = "Email address";
$txt_title_db_content_telephone = "Phone number";
$txt_title_db_content_default_language = "Default Language";
$txt_title_db_content_nom_objet = "Object name";
$txt_title_db_content_capacite = "Capacity";
$txt_title_db_content_valide = "Self confirmed";
$txt_title_db_content_libelle = "Labelled";
$txt_title_db_content_priority = "With priority management";
$txt_title_db_content_ical = "Publish in Ical";
$txt_title_db_content_dispo = "Available";
$txt_title_db_content_visible = "Visible";
$txt_title_db_content_wifi = "Wifi";

$txt_new_class = "Insertion of a new class:";
$txt_new_object = "Insertion of a new object in the class ";
$txt_summary = "Summary";
$txt_add = "To add";
$txt_nedd_class = "You have not yet defined any class!";
$txt_db_content_ok = "No problem detected. Your database content is correct.";
$txt_db_languages_content_ok = "No problem detected. The content of your database table languages is correct.";
$txt_priority = "The object '%s' has been defined in the database as an object with several priority levels.";
$txt_priority_prompt1 = "You must define a message for this type of priority.";
$txt_priority_prompt2 = "If 'XXX' is the priority message, this will be displayed:";
$txt_priority_prompt3 = "Please set a value for the type of priority in the following fields:";
$txt_priority_prompt4 = "Do not forget after entering your text, in order to take it into account, to <a href='delete_session_files.php'><b>delete all session files</b></a>";
$txt_priority_prompt5 = "In <span style='color: red'>red</span>, high priority reservations";
$txt_validation = "Please fill all fields before validating!";
$txt_missing_translation = "There are %d missing translations in the column %s of the database table languages";
$txt_carefull = "be careful";
$txt_admin_error2 = "The administrator '%s' has an incorrect value for the field <i>default_language</i>: '%s'";
$txt_possible_values = "Possible values are: %s.";
$txt_admin_error3 = "The administrator '%s' must have a non empty password.";
$txt_admin_error4 = "Several administrators with different email adresses share the same password. Please fix this.";
$txt_admin_error5 = "The administrator %s is linked to a non-existing class.";
$txt_admin_error51 = "The object '%s' is linked to a non-existing class.";
$txt_admin_error6 = "There is no administrator for the class '%s'";
$txt_objet_error = "The object '%s' in linked to a non-existing object.";
$txt_objet_error_quote = "An object name can not contain an apostrophe: \"%s\"";
$txt_objet_error_comma = "An object name can not contain a comma: \"%s\"";
$txt_classe_error_01 = "The field 'jour_meme' of the class '%s' can only be set to 0 or 1";
$txt_object_error_01 = "Each characteristic status, priority, pubiCal, available, visible and wifi of the object '%s' can only be set to 0 or 1";
$txt_classe_error1 = "The class '%s' does not contain any object.";
$txt_classe_error2 = "The class '%s' does not contain any visible object.";
$txt_classe_error3 = "The class '%s' does not contain any available object.";

$txt_new_class_nom_classe_incomplet = "The class name is mandatory";
$txt_new_class_nom_classe_invalide = "The class name already exists";
$txt_new_class_pass_incomplet = "A password is mandatory";
$txt_new_class_mail_incomplet = "The email address is mandatory";
$txt_new_class_mail1_incomplet = "The email address is not correct";
$txt_new_class_pass_invalide = "This password already exists";
$txt_new_objet_nom_objet_incomplet = "The name of the object is mandatory";
$txt_new_objet_nom_objet_invalide = "The name of the object already exists";
$txt_new_objet_libelle_incomplet = "The label of the object is mandatory";

$txt_overlib_jour_meme = "<ul><li>If yes, nothing special</li><li>If no, for a reservation on the current day, a warning indicating that it will not be garanteed will be displayed.</li></ul>";
$txt_overlib_lang = "It is the language in which validation email will be sent to administrator's class";
$txt_overlib_capa = "Number of available seats for rooms, whatever you want otherwise";
$txt_overlib_valide = "<ul><li>If yes, reservations of this object will not need validation from the administrator</li><li>If no, reservations of this object will need a validation from the administrator</li></ul>";
$txt_overlib_libelle = "The full label of the object, to be displayed in PDF screens";
$txt_overlib_priority = "Is a priority level defined for reservations of this object?<br />Priority levels is just for information purpose";
$txt_overlib_ical = "Do reservations of this object need to be published in Ical (if Ical is used and installed of course)";
$txt_overlib_dispo = "Is this object available? This is used to temporarly disable an object";
$txt_overlib_visible = "Is this object visible? This is used to suppress an object from the web interface but without cancelling its history";
$txt_overlib_wifi = "Is a wifi access associated to reservations of this object? Useful to communicate with the PHPMyWifi software";

$txt_confirm_modifier_classe = "Are you sure you want to modify this class: %s?";
$txt_confirm_modifier_objet = "Are you sure you want to modify this object: %s?";
$txt_confirm_supprimer_classe = "Deleting a class deletes also all its objects. \\nThe class '%s' contains %d object(s). \\nIt is linked with %d reservation(s). \\nDo you want to detele it?";
$txt_confirm_supprimer_classe5 = "Do you want to detele it?";
$txt_confirm_supprimer_objet = "Are you sure you want to delete the object '%s'?";
$txt_confirm_supprimer_classe_reserv = "This object contains reservations, are you sure you want to delete it?";
$txt_confirm_supprimer_reserv = "Do you want to delete its reservations?";
$txt_confirmation_objet_reserv = "The objects and its reservations have been successfully deleted";
$txt_confirmation_objet = "The object has been successfully deleted";


/***********************************************************************************************
****************	Partie Configuration de l'application			****************
***********************************************************************************************/

// Partie GÃ©nÃ©rale

$txt_config1_main_titre1 = "General";
$txt_config1_main_titre2 = "Technical contact";
$txt_config1_main_titre3 = "Network";
$txt_config1_main_titre4 = "Server";
$txt_config1_main_titre5 = "Default values";

$txt_config1_titre = "The application title, displayed in the top left corner";
$txt_config1_bookmark = "The bookmark name for this web page";
$txt_config1_image_titre = "The name of the image, displayed in the top left corner of the web page *";
$txt_config1_image_pdf = "The name of the image for the PDF generation (jpg format only!) *";
$txt_config1_page_accueil = "Welcome page";
$txt_config1_page_accueil1 = "Planning of the month";
$txt_config1_page_accueil2 = "Help page";
$txt_config1_page_accueil3 = "Today's reservations";
$txt_config1_email_domain = "Email address domain, for autocompletion purpose (ex: @lpsc.in2p3.fr)";
$txt_config1_capacite = "Are capacities displayed?";
$txt_config1_modification_enable = "Are users able to modify some characteristics of reservations?";
$txt_config1_default_language = "Default language for users without language cookie";
$txt_config1_display_tomorrow = "Is the following day's planning displayed?";
$txt_config1_several_object_possible = "Is it possible to book simultaneously several objects?";
$txt_config1_week_end_enabled = "Are week-end days included in the system?";
$txt_config1_tree = "Is the objects' list displayed as a tree?";
$txt_config1_tree_scrollable = "Is this objects' tree scrollable?";
$txt_config1_mail_subject = "Size for emails' subject sent for reservations' creation";
$txt_config1_default_class_for_feed= "Default class for the display of rss feed";
$txt_config1_use_diffusion_flag = "Has the user the choice to disable or not the display of it reservations?";

$txt_config1_technical_contact = "Name of the local technical contact";
$txt_config1_technical_email = "Email address of the local technical contact";
$txt_config1_technical_tel = "Phone number of the local technical contact";
$txt_config1_read_only_enable = "Do you want to restrict the web site to read-only access?";
$txt_config1_IP_regex = "Regular expression for autorized IP addresses";
$txt_config1_enabled_domain = "List of autorized domains";
$txt_config1_mail_server = "Name of your email server";
$txt_config1_web_server = "Name of your web server";
$txt_config1_default_beginning_hour = "Default value for reservation begining time (h)";
$txt_config1_default_beginning_mihour = "Default value for reservation beginig time (min)";
$txt_config1_default_duration = "Default value for reservation duration (h)";
$txt_config1_default_miduration = "Default value for reservation duration (min)";
$txt_config1_default_end_hour = "Default value for reservation end time (h)";
$txt_config1_default_end_mihour = "Default value for reservation end time (min)";
$txt_config1_default_comment = "Default value for reservation comment";
$txt_config1_default_beginning_hour_error = "Incorrect default value for reservation begining time (h)";
$txt_config1_default_beginning_mihour_error = "Incorrect default value for reservation beginig time (min)";
$txt_config1_default_duration_error = "Incorrect default value for reservation duration (h)";
$txt_config1_default_miduration_error = "Incorrect default value for reservation duration (min)";
$txt_config1_default_end_hour_error = "Incorrect default value for reservation end time (h)";
$txt_config1_default_end_mihour_error = "Incorrect default value for reservation end time (min)";

$txt_recommandation1 = " * Put the image in the folder img/ and only specify the filename";

$txt_overlib_capacite = "Capacity is the seat number for a room or whatever you want if it is not a number";
$txt_overlib_modification_enable = "Otherwise to modify a reservation, it will be mandatory to delete it and to create it again";
$txt_overlib_read_only_enable = "This allows the enabling of full access to the web site to only predifined IP addresses ot to predifined domains and to restrict others to a read-only access.<br />This is particularly useful for web sites that are not inside intranet.<br /><br />You cannot use this function if your provider does not allow you to use the gethostbyaddr() PHP function";
$txt_overlib_IP_regex = "Ex: 111.222.3[4-5].\d{1,3}";
$txt_overlib_enabled_domain = "Domains are separated with commas";
$txt_overlib_mail_server = "To fill in only if the domain of your email addresses (ex: @lpsc.in2p3.fr) is the name of your web server (ex: http://lpsc.in2p3.fr), because in this case emails should not be correctly sent.<br />If you fill in this section, all occurence of the name of the web server in an email address will be automatically replaced by the name of the email server<br /><br />Ex: lpscmail.in2p3.fr";
$txt_overlib_web_server = "To fill in only if the domain of your email addresses (ex: @lpsc.in2p3.fr) is the name of your web server (ex: http://lpsc.in2p3.fr), because in this case emails should not be correctly sent.<br />If you fill in this section, all occurence of the name of the web server in an email address will be automatically replaced by the name of the email server<br /><br />Ex: lpsc.in2p3.fr";
$txt_overlib_feed = "Otherwise the display in the rss feed and in iCal (if used of course) is activated by default";

$txt_pb1_image_titre = "The location of the file of the image displayed in the top left corner is incorrect";
$txt_pb2_image_titre = "The file of the image displayed in the top left corner is not readable";
$txt_pb1_image_pdf = "The location of the file for the PDF generation is incorrect";
$txt_pb2_image_pdf = "The extension of the image file must be .jgp or .jpeg";
$txt_pb3_image_pdf = "The file of the image for the PDF generation is not readable";
$txt_pb_technical_contact = "The name of the technical contact is mandatory";
$txt_pb_technical_email = "The email address of the technical contact is mandatory";
$txt_pb_technical_tel = "The phone number of the technical contact is mandatory";
$txt_pb_IP_regex = "The regular expression or the array of IP addresses is mandatory";
$txt_pb_domain = "The specified domain names are not correct";
$txt_pb_mail_server = "The name of your email server is mandatory";
$txt_pb_web_server = "The name of your web server is mandatory";

$txt_short = "short";
$txt_medium = "medium";
$txt_long = "long";

$txt_info_php = "You are using PHP %s";
$txt_register_global = "It is not recommanded to set in your php.ini file the register_globals variable to on";


// Partie Annexe

$txt_config2_main_titre1 = "PHPICalendar";
$txt_config2_main_titre2 = "Excel";
$txt_config2_main_titre3 = "SOAP (for future versions)";

$txt_config2_iCal = "Du you want to use the format iCal";
$txt_config2_URL_iCal = "URL of the PHP iCalendar software";
$txt_config2_CALENDARS_file_location = "Location of the ical generated files";
$txt_config2_CALENDARS_file_beginning_of_name = "Begining of name of the generated files";
$txt_config2_CALENDARS_end_of_uid = "End of UID (identifier) in the generated files";
$txt_config2_iCal_delay = "Number of weeks to store in the history of the icalendar files";

$txt_overlib_URL_iCal = "The location must end by '/'<br />Ex: http://xxx/phpicalendar-1.0/ or ../phpicalendar-1.0/";
$txt_overlib_CALENDARS_file_location = "The location must end by '/'<br />Ex: /www/htdocs/calendars/ or D:/www/calendars/";
$txt_overlib_CALENDARS_file_beginning_of_name = "Ex: LPSCResa-";
$txt_overlib_CALENDARS_end_of_uid = "Ex: .LPSCResa.in2p3.fr";
$txt_overlib_iCal_delay = "-1 if unlimited";

$txt_pb1_URL_iCal = "The URL of the PHP iCalendar software is mandatory";
$txt_pb2_URL_iCal = "The URL of the PHP iCalendar software must end by the character '/'";
$txt_pb3_URL_iCal = "The URL of the PHP iCalendar software is incorrect";
$txt_pb1_CALENDARS_file_location = "The location of the ical generated files is mandatory";
$txt_pb2_CALENDARS_file_location = "The location of the ical generated files must end by the character '/'";
$txt_pb3_CALENDARS_file_location = "The location of the ical generated files is incorrect";
$txt_pb4_CALENDARS_file_location = "The location of the ical generated files is not readable";
$txt_pb5_CALENDARS_file_location = "The location of the ical generated files  is not writable";
$txt_pb_CALENDARS_file_beginning_of_name = "The begining of the name of the ical generated files is mandatory";
$txt_pb_CALENDARS_end_of_uid = "The end of UID (identifier) in the ical generated files is mandatory";
$txt_explanation_ical = "If you want to use iCal, you must download the <a href='http://phpicalendar.sourceforge.net/nuke/' target='_blank'>PHP iCalendar</a> software, then install it (tar xvzf xxx) and configure it, by editing the config.inc.php file. <br />You will have to set at least the $"."calendar_path variable. Its value must be set to the location of the ical generated files.";
$txt_explanation_excel1 = "If you want to use the Excel generation (in order to export the result of a search):";
$txt_explanation_excel2 = "you must install the pear OLE and Spreadsheet_Excel_Writer packages ('<i>pear install http://pear.php.net/get/OLE</i>' et '<i>pear install http://pear.php.net/get/Spreadsheet_Excel_Writer</i>')";
$txt_explanation_excel3 = "You will also have to tune the access rights on the 'stats' folder in order to allow your web server to create file into";
$txt_excel_actif = "This fonctionnality is activated";
$txt_soap_pb = "In order to use web services you must install PHP with the SOAP extension (compilation option --enable-soap)";
?>
