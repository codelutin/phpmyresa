<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/commun.php
*
* This file is included in almost all files in the config folder
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


error_reporting(E_ALL);


//*********************************************************************
//  Verification of authentication passed, login otherwise
//*********************************************************************

if (ini_get('session.auto_start') == "0") session_start();

if (isset($_SERVER['SCRIPT_FILENAME'])){
	$from = $_SERVER['SCRIPT_FILENAME'];
	if (strrpos($from, '/') !== false){
		$from = strrchr($from, '/');
		$from = substr($from, 1);
	}
} else $from = 'index.php';

if ( ($from != 'login.php') && ($from != 'ajax_login_connexion.php') && ($from != 'ajax_login_inscription.php') && ($from != 'changeLanguage.php')){
	if ( ! isset($_SESSION['connected'])){
		header("Location:./login.php");
		exit();
	}
}


//*********************************************************************
//  Languages management
//*********************************************************************

$lang = array();
$lang = getAvailableLanguages();

if (isset($_COOKIE['resa_lang'])) $language = $_COOKIE['resa_lang'];
else{
	$language = 'english';
	setcookie("resa_lang", $language, time()+3600*24*30*12);
	Header("Location:changeLanguage.php");
	exit();
}

$trouve = false;
for ($i = 0; $i < count($lang); $i++){
	if ($lang[$i] == $language){
		$trouve = true;
		break;
	}
}


//*********************************************************************
//  Files include and variables define
//*********************************************************************

$config_database_file = "../commun/database.php";
$config_file = "../commun/config.php";
$config_credential_file = "./commun/credentials.php";
$structure_file = "./commun/structure.php";

if ($trouve) require_once("language_".$language.".php");

@require_once('../commun/commun_all.php');

$URL_overlib = "../external_softwares/overlib/";
$URL_Ajax = "../external_softwares/prototype.js";

$entete =
"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf8' />
	<meta name='Author' content='Daniel Charnay, Frederic Melot, Patricia Warin-Charpentier' />
	<meta name='Keywords' content='reservation configuration' />
	<meta name='Description' content='reservation' />
	<link rel='SHORTCUT ICON' href='../img/favicon.ico' />
	<link rel='stylesheet' type='text/css' href='commun/config.css' />
	<title>$txt_bookmark</title>
	<script src='commun/function.js' type='text/javascript'></script>
</head>

<body>
";

$end = "\n\n
</body>
</html>";

$end_error = "\n
</div>
</div>
$end";

if ( ($from == 'index.php') || ($from == 'login.php') ) $liens = "<td class='gauche'><a href='../'>$txt_back_resa</a></td>";
else $liens = "<td class='gauche'><a href='../'>$txt_back_resa</a> &nbsp; &nbsp; - &nbsp; &nbsp; <a href='index.php'>$txt_back_config</a></td>";
if ( isset($_SESSION['config']) || isset($_SESSION['upgrade']) ) $div_footer = "";
else $div_footer =
"<div id='footer'>
<table class='all' >
<tr>
$liens
<td class='droite'><a href='http://validator.w3.org/check?uri=referer'><img src='img/valid-xhtml10.png' alt='Valid XHTML 1.0' style='border:0;width:60px;' /></a><a href='http://jigsaw.w3.org/css-validator/check/referer'><img style='border:0;width:60px;' src='img/vcss.png' alt='Valid CSS!' /></a></td>
</tr>
</table>
</div>
";

$change_language = "\n<form id='changeLanguage' action='changeLanguage.php' method='post'>\n<table class='all'>\n<tr><td class='droite'>\n";
$change_language .= "<p><input type='hidden' name='from' value='$from' /></p>\n";
$change_language .= "<p><input type='hidden' name='lang' value='english' /></p>\n";
for ($i = 0 ; $i < count($lang) ; $i++){
	if ($lang[$i] != $language) $change_language .= "<img height='15pt' src='../img/drapeaux/".$lang[$i].".png' alt='".$lang[$i]."' onclick=\"goChangeLanguage('".$lang[$i]."')\" /> \n";
}
$change_language .= "</td></tr>\n</table>\n</form>\n\n";

$entete2 =
"<div class='princ'>
$change_language
<div class='d2'>
<h1>%s</h1>
</div>
<div class='d4'>
";

$message_redirect_configure_database = "<form method='post' action='configure_database_access.php'>";
$message_redirect_configure_database .= "<table class='center'>\n";
$message_redirect_configure_database .= display_red($txt_db_error);
$message_redirect_configure_database .= display("$txt_redirect_connexion");
$message_redirect_configure_database .= display(" &nbsp; ");
$message_redirect_configure_database .= display("<input type='submit' value='$txt_config' />");
$message_redirect_configure_database .= "</table>\n";
$message_redirect_configure_database .= "</form>";
$message_redirect_configure_database .= $end_error;

$message_redirect_create_database = "<form method='post' action='create_database.php'>";
$message_redirect_create_database .= "<table class='center'>\n";
$message_redirect_create_database .= display_red($txt_need_to_create_db);
$message_redirect_create_database .= display("$txt_redirect_structure");
$message_redirect_create_database .= display(" &nbsp; ");
$message_redirect_create_database .= display("<input type='submit' value='$txt_titre_create_database' />");
$message_redirect_create_database .= "</table>\n";
$message_redirect_create_database .= "</form>";
$message_redirect_create_database .= $end_error;


//*********************************************************************
//  Functions
//*********************************************************************

function exitWrongSignature($file){
	global $txt_exit_nb_parameter, $entete, $end;
	exit("$entete<p style='color: red'>$txt_exit_nb_parameter<span style='color: black'> ($file)</span></p>\n\n$end");
}

function getAvailableLanguages(){

	$tab = array();
	$i = 0;
	if ($handle = opendir('commun')){
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && strlen($file) > 8) {
				if(substr($file, 0, 9) == 'language_'){
					$filename = substr($file, 9, -4);
					$tab[$i] = $filename;
					$i++;
				}
			}
		}
	}
	closedir($handle);
	return $tab;
}

function display($text){
	return "<tr><td class='centre'>$text</td></tr>\n";
}

function display_left($text){
	return "<tr><td class='gauche'>$text</td></tr>\n";
}

function display_red($text){
	global $txt_carefull;
	return "<tr><td class='centre'><img src='img/attention.gif' width='25' alt='$txt_carefull' /> <span class='red'>$text</span></td></tr>\n";
}

function display_left_red($text){
	global $txt_carefull;
	return "<tr><td class='gauche'><img src='img/attention.gif' width='25' alt='$txt_carefull' /> <span class='red'>$text</span></td></tr>\n";
}

function errorDB($DB_request){
	global $database, $connexionDB;

	if ($database == "MySQL") $database_error = mysql_error();
	else if ($database == "PostgreSQL") $database_error = pg_last_error();
	else if ($database == "Oracle"){
		$DB_parse = oci_parse($connexionDB, $DB_request);
		$test = @oci_execute($DB_parse, OCI_COMMIT_ON_SUCCESS);
		$e = oci_error($DB_parse);
		$database_error = $e['message'];
	}

	return $database_error;
}

function display_error($DB_request){
global $txt_pb_requete1, $txt_pb_requete2;

	$message = display("$txt_pb_requete1<span class='red'>'$DB_request'</span>.");
	$message .= display("$txt_pb_requete2<span class='red'>'".errorDB($DB_request)."'</span>.");
	return $message;
}

function verify_file($file){
	global $entete, $change_language, $end, $txt_db_file_error1, $txt_db_file_error2, $structure_file, $end_error;

	$message = "";
	if ($file == $structure_file){
		if ( ! file_exists($file)) $message = "<table class='center'><tr><td class='centre'>".sprintf($txt_db_file_error1, $file)."</td></tr></table>$end_error";
	} else{
		if ( ! file_exists($file)) $message = sprintf($txt_db_file_error1, $file);
		else if ( ! is_writable($file)) $message = sprintf($txt_db_file_error2, $file);
		if ($message != "") $message = "$entete $change_language <table class='center'><tr><td class='centre'> $message </td></tr></table> $end";
	}
	return $message;
}

function verify_database_connexion($database_type, $hostname, $username, $password, $database_name){
	global $txt_db_error1, $txt_db_error2;

	$message = "";
	$connexionDB = FALSE;
	if ($database_type == "MySQL"){
		$connexionDB = @mysql_connect($hostname, $username, $password);
		if ( ! $connexionDB) $message = sprintf($txt_db_error1, $hostname, "$username/$password");
		else if ( ! mysql_select_db($database_name,$connexionDB)) $message = sprintf($txt_db_error2, $database_name, $hostname, "$username/$password");
	} else if ($database_type == "PostgreSQL"){
		$connexionDB = @pg_Connect("host=".$hostname." dbname=".$database_name." user=".$username." password=".$password);
		if ( ! $connexionDB) $message = sprintf($txt_db_error2, $database_name, $hostname, "$username/$password");
	} else if ($database_type == "Oracle"){
		$connexionDB = @oci_connect($username, $password, $hostname);
		if ( ! $connexionDB) $message = sprintf($txt_db_error1, $hostname, "$username/$password");
	}else $message = "Wrong database type!";

	$tab = array();
	$tab[0] = $message;
	$tab[1] = $connexionDB;
	return $tab;
}

function calculate_number_of_tables($database){
	global $connexionDB, $txt_nbTable_error, $end_error;

	$nbTable = 0;
	$message = "";
	if ($database == "MySQL")
		$DB_request = "SHOW TABLES";
	else if ($database == "PostgreSQL")
		$DB_request = "select table_name from information_schema.tables where table_type='BASE TABLE' and table_name not like 'pg_%' and table_name not like 'sql_%'";
	else if ($database == "Oracle")
		$DB_request = "SELECT table_name FROM user_tables";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result){
		$message = "<table class='center'>\n";
		$message .= display($txt_nbTable_error);
		$message .= display_error($DB_request);
		$message .= "</table>\n";
		$message .= $end_error;
	} else{
		if ($database == "MySQL") $nbTable = mysql_num_rows($result);
		else if ($database == "PostgreSQL") $nbTable = pg_num_rows($result);
		else if ($database == "Oracle") $nbTable = oci_fetch_all($result, $re);
	}

	$tab = array();
	$tab[0] = $nbTable;
	$tab[1] = $message;
	return $tab;
}

function verify_database_structure(){
	global $end_error, $structure_file,  $database, $connexionDB, $txt_column, $txt_table, $txt_structure_file_pb, $txt_no_db_structure_pb, $txt_db_structure_pb_table, $txt_db_structure_pb_column, $txt_db_structure_pb1, $txt_db_structure_pb2, $txt_db_structure_update, $txt_suiv;

	$mes_pb = "";
	$mes = "";
	$tab = array();
	$tab[0] = "";
	$tab[1] = "";
	$separator = "/";

	$temp = verify_file($structure_file);
	if ($temp != ""){
		$tab[0] = $temp;
		return $tab;
	}
	require_once($structure_file);

	if ($database == "MySQL")
		$DB_request = "SHOW TABLES";
	else if ($database == "PostgreSQL")
		$DB_request = "select table_name from information_schema.tables where table_type='BASE TABLE' and table_name not like 'pg_%' and table_name not like 'sql_%'";
	else if ($database == "Oracle")
		$DB_request = "SELECT table_name FROM user_tables";
	$result = database_query($DB_request, $connexionDB);

	if ( ( ! isset($table)) || (! isset($column)) ) $mes_pb = display_red("$txt_structure_file_pb ($structure_file)");
	else if ( ! $result) $mes_pb = display_error($DB_request);
	else{
		$tableFound = array();
		$columnFound = array();

		// *******************************************************
		// Traitement des tables
		// *******************************************************
		if ($database == "MySQL"){
			for ($i = 0; $i < mysql_num_rows($result); $i++)
				$tableFound[$i] = mysql_tablename($result, $i);
		} else if ($database == "PostgreSQL"){
			$i = 0;
			while ($field = pg_fetch_object($result)){
				$tableFound[$i] = $field->table_name;
				$i++;
			}
		} else if ($database == "Oracle"){
			$i = 0;
			while ($field = oci_fetch_object($result)){
				$tableFound[$i] = strtolower($field->TABLE_NAME);
				$i++;
			}
		}

		$missingTable = array();
		$missingColumn = array();
		$j = 0;
		$z = 0;
		for ($i = 0 ; $i < count($table) ; $i++){
			if (in_array($table[$i], $tableFound)){
				// *******************************************************
				// Traitement des colonnes
				// *******************************************************
				$k = 0;
				if ($database == "MySQL"){
					$DB_request = "DESCRIBE $table[$i]";
					$result = mysql_query($DB_request, $connexionDB);
					if (! $result){
						$mes_pb = display_error($DB_request);
						break;
					}
					while ($field = mysql_fetch_object($result)){
						$columnFound[$k] = $field->Field;
						$k++;
					}
				} else if ($database == "PostgreSQL"){
					$DB_request = "select column_name from information_schema.columns where table_name = '$table[$i]'";
					$result = @pg_query($connexionDB, $DB_request );
					if (! $result){
						$mes_pb = display_error($DB_request);
						break;
					}
					while ($field = pg_fetch_object($result)){
						$columnFound[$k] = $field->column_name;
						$k++;
					}
				} else if ($database == "Oracle"){
					$DB_request = "SELECT COLUMN_NAME FROM user_tab_columns where table_name='".strtoupper($table[$i])."'";
					$result = oci_parse($connexionDB, $DB_request);
					$test = @oci_execute($result, OCI_COMMIT_ON_SUCCESS);
					if ( ! $test){
						$mes_pb = display_error($DB_request);
						break;
					}
					while ($field = oci_fetch_object($result)){
						$columnFound[$k] = strtolower($field->COLUMN_NAME);
						$k++;
					}
				}

				for ($a = 0 ; $a < count($column[$i]) ; $a++){
					if ( ! in_array($column[$i][$a], $columnFound)){
						$missingColumn[$z] = $table[$i].$separator.$column[$i][$a];
						$z++;
					}
				}
			} else{
				$missingTable[$j] = $table[$i];
				$j++;
			}
		}
		if ($mes_pb != ""){
			$tab[0] = "<table class='center'>$mes_pb</table>$end_error";
			return $tab;
		}

		$nbPbTable = count($missingTable);
		$nbPbColumn = count($missingColumn);
		$fromVersion3xTo4x = false;

		if ( (count($missingTable) == 0) &&  (count($missingColumn) == 3) ){

			if ( ($missingColumn[0] == 'reservation/diffusion') && ($missingColumn[1] == 'languages/spanish') && ($missingColumn[2] == 'languages/german') ){
				$fromVersion3xTo4x = true;
				$fromVersion3xTo4xError = executeDatabaseMigrationFromVersion3xTo4x();
			}
		}

		if ($fromVersion3xTo4x && ! $fromVersion3xTo4xError){
			$mes .= display("$txt_db_structure_update");
			$mes .= display("<form id='go' method='post' action='fill_languages.php'><table class='center'><tr><td><input type='submit' value=\"$txt_suiv\" /></td></tr></table></form>");
		} else {
			if ( ! (  ($nbPbTable == 0) && ($nbPbColumn == 0) ) ){
				$mes = display_red($txt_db_structure_pb2);
				if ($nbPbTable != 0){
					$mes .= display(" &nbsp;");
					$mes .= display_red($txt_db_structure_pb_table);
					foreach ($missingTable as $index => $val) $mes .= display("$val");
				}
				if ($nbPbColumn != 0){
					$mes .= display(" &nbsp;");
					$mes .= display_red($txt_db_structure_pb_column);
					foreach ($missingColumn as $index => $val){
						$mes .= display(str_replace($separator, " $separator $txt_column ", "$txt_table $val"));
					}
				}
				$mes .= display(" &nbsp;");
				$mes .= display($txt_db_structure_pb1);
			}
		}
	}
	if ($mes_pb != "") $mes_pb = "<table class='center'>$mes_pb</table>$end_error";
	if ($mes != "") $mes = "<table class='center'>$mes</table>$end_error";
	$tab[0] = $mes_pb;		// used when no result can be found
	$tab[1] = $mes;			// result of the verification: correct or wrong database structure (including update cases, which can be found with the $_SESSION['upgrade'] variable)
	return $tab;
}

function calculate_number_of_rows_on_table_language(){
	global $connexionDB, $txt_nbRows_languages_error, $end_error;

	$nbRows = 0;
	$message = "";
	$DB_request = "SELECT count(*) AS nb FROM languages";
	$result = database_query($DB_request, $connexionDB);
	if ( ! $result){
		$message = "<table class='center'>\n";
		$message .= display($txt_nbRows_languages_error);
		$message .= display_error($DB_request);
		$message .= "</table>$end_error\n";
	} else{
		$row = database_fetch_object($result);
		$nbRows = database_get_from_object($row, 'nb');
	}

	$tab = array();
	$tab[0] = $nbRows;
	$tab[1] = $message;
	return $tab;
}

function deletion_of_session_files($session_path, $todo){
	global $txt_pb_creation, $txt_db_file_error3, $txt_pb_deletion, $txt_pb_deletion2, $txt_db_file_error5;

	$message = "";
	if ($session_path != ""){
		if ( ! file_exists($session_path)){
			$test = @mkdir($session_path, 0700);
			if ( ! $test) $message .= display(sprintf($txt_pb_creation, $session_path));
		} else if ( ! is_readable($session_path)) $message .= display(sprintf($txt_db_file_error3, $session_path));
		else if ( ! is_writable($session_path)) $message .= display(sprintf($txt_db_file_error5, $session_path));
		else{
			if ($todo){
				foreach (glob($session_path."/sess_*") as $filename){
					if ( ! @unlink($filename)){
						$message .= display(sprintf($txt_pb_deletion, $filename));
						$message .= display(sprintf($txt_pb_deletion2 , $session_path));
						break;
					}
				}
			}
		}
	}
	return $message;
}

function getPriorityMessageName($name){
	return 'resa_mandatory_priority_'.strtolower(str_replace(' ','_',$name));
}

function constructionListe($resultat, $column){
	global $database;

	$list = "";
	while ($row = database_fetch_object($resultat)){
		$list .= "'".database_get_from_object($row, $column)."', ";
	}
	$list = substr($list, 0, -2);
	if (empty($list)){
		if ($database == "Oracle") $list = "-1";
		else $list = "''";
	}
	return $list;
}

function getAvailableLanguagesFromDB(){
	global $connexionDB, $database;

	$i = 0;
	$res = array();
	$pb = "";
	if ($database == "MySQL"){
		$DB_request = "DESCRIBE languages";
		$resultat = mysql_query($DB_request, $connexionDB);
		if ( ! $resultat) $pb = display_error($DB_request);
		else while ($row = database_fetch_object($resultat)){
			$champ1 =$row->Field;
			if ($champ1 != "name"){
				$res[$i] = $champ1;
				$i++;
			}
		}
	} else if ($database == "PostgreSQL"){
		$DB_request = "select column_name from information_schema.columns where table_name = 'languages'";
		$resultat = @pg_query($connexionDB, $DB_request);
		if ( ! $resultat) $pb = display_error($DB_request);
		else while ($row = pg_fetch_object($resultat)){
			$champ2 = $row->column_name;
			if ($champ2 != "name"){
				$res[$i] = $champ2;
				$i++;
			}
		}
	} else if ($database == "Oracle"){
		$DB_request = "SELECT COLUMN_NAME FROM user_tab_columns where table_name='LANGUAGES'";
		$resultat = oci_parse($connexionDB, $DB_request);
		$test = @oci_execute($resultat, OCI_COMMIT_ON_SUCCESS);
		if ( ! $test) $pb = display_error($DB_request);
		else while ($row = oci_fetch_object($resultat)){
			$champ2 = $row->COLUMN_NAME;
			if ($champ2 != "NAME"){
				$res[$i] = strtolower($champ2);
				$i++;
			}
		}
	}
	$tab = array();
	$tab[0] = $pb;
	$tab[1] = $res;
	return $tab;
}

function my_overlib($texte, $titre){
	global $txt_help;
	return "<a href='#' onmouseover=\"return overlib('".addslashes($texte)."', WIDTH, 500, CAPTION, '".addslashes($titre)."', CENTER, OFFSETY, 20, TEXTSIZE, '12px', FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);\" onmouseout='return nd();'><img src='img/help.gif' alt='$txt_help' /></a>";
}

function class_header(){
	global $txt_title_db_content_nom_classe, $txt_title_db_content_jour_meme, $txt_title_db_content_nom_admin, $txt_title_db_content_prenom_admin, $txt_title_db_content_pass, $txt_title_db_content_email, $txt_title_db_content_telephone, $txt_title_db_content_default_language, $txt_overlib_jour_meme, $txt_help, $txt_overlib_lang;

	$message = "<tr class='gray'>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_nom_classe <span class='red'>*</span></td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_jour_meme ".my_overlib($txt_overlib_jour_meme, $txt_title_db_content_jour_meme)."</td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_nom_admin</td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_prenom_admin</td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_pass <span class='red'>*</span></td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_email <span class='red'>*</span></td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_telephone</td>\n";
	$message .= "   <td class='header_black'>$txt_title_db_content_default_language ".my_overlib($txt_overlib_lang, $txt_title_db_content_default_language)."</td>\n";
	$message .= "   <td  class='void'></td>\n";
	$message .= "</tr>\n";

	return $message;
}

function object_header(){
	global $txt_title_db_content_nom_objet, $txt_title_db_content_capacite, $txt_title_db_content_valide, $txt_title_db_content_libelle, $txt_title_db_content_priority, $txt_title_db_content_ical, $txt_title_db_content_dispo, $txt_title_db_content_visible, $txt_title_db_content_wifi, $txt_overlib_capa, $txt_overlib_valide, $txt_overlib_libelle, $txt_overlib_priority, $txt_overlib_ical, $txt_overlib_dispo, $txt_overlib_visible, $txt_overlib_wifi, $txt_help;

	$message = "<tr class='gray'>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_nom_objet <span class='red'>*</span></td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_capacite ".my_overlib($txt_overlib_capa, $txt_title_db_content_capacite)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_valide ".my_overlib($txt_overlib_valide, $txt_title_db_content_valide)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_libelle ".my_overlib($txt_overlib_libelle, $txt_title_db_content_libelle)."</a><span class='red'>*</span></td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_priority ".my_overlib($txt_overlib_priority, $txt_title_db_content_priority)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_ical ".my_overlib($txt_overlib_ical, $txt_title_db_content_ical)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_dispo ".my_overlib($txt_overlib_dispo, $txt_title_db_content_dispo)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_visible ".my_overlib($txt_overlib_visible, $txt_title_db_content_visible)."</td>\n";
	$message .= "   <td class='header_white'>$txt_title_db_content_wifi ".my_overlib($txt_overlib_wifi, $txt_title_db_content_wifi)."</td>\n";
	$message .= "</tr>\n";

	return $message;
}

function executeDatabaseMigrationFromVersion3xTo4x(){

	global $database, $connexionDB;
	$fromVersion3xTo4xError = false;

	if ($database == "MySQL"){
		$DB_request = "ALTER TABLE `languages` ADD `spanish` TEXT ";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}

		$DB_request = "ALTER TABLE `languages` ADD `german` TEXT ";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}

		$DB_request = "ALTER TABLE `reservation` ADD `diffusion` INT( 1 ) NOT NULL DEFAULT '1'";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}
	} else if ($database == "PostgreSQL"){
		$DB_request = "ALTER TABLE languages ADD spanish TEXT";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}

		$DB_request = "ALTER TABLE languages ADD german TEXT";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}

		$DB_request = "ALTER TABLE reservation ADD COLUMN diffusion numeric(1)";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}
	} else{
		$DB_request = "ALTER TABLE languages ADD (spanish VARCHAR2(2000) NOT NULL, german VARCHAR2(2000) NOT NULL)";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}

		$DB_request = "ALTER TABLE reservation ADD (diffusion NUMBER(1) DEFAULT 1 NOT NULL)";
		$result = database_query($DB_request, $connexionDB);
		if ( ! $result){
			echo errorDB($DB_request);
			$fromVersion3xTo4xError = true;
		}
	}
	$_SESSION['upgrade'] = 1;
	return $fromVersion3xTo4xError;
}
?>
