<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File config/french.php
*
* This file is the french texts for the configuration part
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Sylvain Pierre
*
* @copyright	2006,2007,2008 Frédéric Melot
* @copyright	2006,2007 Sylvain Pierre
*
* @package	PHPMyResa
* @subpackage	config
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


/***********************************************************************************************
****************				General				****************
************************************************************************************************/

$txt_yes = "Oui";
$txt_no = "Non";
$txt_bookmark = "Configuration de PHPMyResa";
$txt_exit_nb_parameter = "Méthode non supportée ou nombre de paramètres insuffisant";
$txt_titre_inscription = "Inscription";
$txt_titre_connexion = "Connection";
$txt_titre_password = "Changement de mot de passe";
$txt_titre_database = "Connection à la base de données";
$txt_titre_create_database = "Création de la structure de la base de données";
$txt_titre_languages = "Vérification du contenu de la table languages et mise à jour de ses données";
$txt_titre_session = "Destruction des fichiers de session";
$txt_titre_db_structure = "Vérification de la structure de la base de données";
$txt_titre_db_content = "Contenu de la base de données (classes, administrateurs et objets)";
$txt_titre_db_content_modify_classe = "Modification d'une classe";
$txt_titre_db_content_modify_object = "Modification d'un objet";
$txt_titre_config1 = "Configuration générale";
$txt_titre_config2 = "Configuration des logiciels annexes";
$txt_titre_database_config = "Configuration de la base de données";
$txt_titre_application_config = "Configuration de l'application";
$txt_back_resa = "Retour au système de réservation";
$txt_back_config = "Retour au menu principal de configuration";
$txt_mandatory = " : obligatoire";
$txt_erase = "Ré initialiser";
$txt_validate = "Valider";
$txt_cancel = "Annuler";
$txt_modi = "Modifier";
$txt_sup = "Supprimer";
$txt_suiv = "Suivant";
$txt_term = "Terminer";
$txt_formulaire_incomplet = "Merci de compléter tous les champs obligatoires";
$txt_help = "Aide";
$txt_installation = "installation";
$txt_upgrade = "mise à jour";


/***********************************************************************************************
****************	Partie Configuration de la base de données		****************
***********************************************************************************************/

$txt_no_db_extension_installed = "Aucun module de base de données compilé et chargé par PHP ! Vous devez installer MySQL, PostgreSQL ou Oracle.";
$txt_database_type = "Type de base de données :";
$txt_hostname = "Nom du serveur de base de données :";
$txt_username = "Nom de l'utilisateur de la base de données :";
$txt_password = "Mot de passe de cet utilisateur :";
$txt_database_name = "Nom de la base de données :";
$txt_db_no_error1 = "Un test de connection à la base de donnée a fonctionné.";
$txt_db_no_error2 = "Votre configuration a été sauvegardée dans le fichier database.php.";
$txt_db_no_error3 = "Votre configuration a été sauvegardée dans le fichier config.php.";
$txt_db_no_error4 = "Les paramètres de connection à votre base de données sont déjà renseignés.";
$txt_db_error = "Un test de connection à la base de donnée a échoué.";
$txt_back = "Ré essayer";
$txt_db_error_back = "Cliquez sur le bouton '$txt_back' ci-dessous pour changer vos paramètres de connection.";
$txt_db_init = "Veuillez saisir tous les paramètres";
$txt_db_error1 = "Il y a une erreur de connection sur le serveur de base de données '%s' avec le login/mot de passe '%s'";
$txt_db_error2 = "Il y a une erreur de connection sur la base données '%s' sur le serveur de base de données '%s' avec le login/mot de passe '%s'";
$txt_db_file_error1 = "Le fichier de configuration '%s' n'existe pas";
$txt_db_file_error2 = "Le fichier de configuration '%s' n'est pas accessible en écriture";
$txt_db_file_error3 = "Le répertoire '%s' n'est pas accessible en lecture";
$txt_db_file_error4 = "Les paramètres de connection à votre base de données ne sont pas correctement renseignés, merci de remplir les champs suivant.";
$txt_db_file_error5 = "Le répertoire '%s' n'est pas accessible en écriture";
$txt_db_file_error = "Impossible de mettre à jour votre fichier '%s'";
$txt_file_pb = "Problème à l'ouverture du fichier '%s'";
$txt_redirect_connexion = "Veuillez commencer par configurer votre connection à la base de données.";
$txt_config = "configurer";

$txt_nbTable_error = "Le nombre de tables présentes dans la base de données n'a pas pu être trouvé.";
$txt_create_tables = "Vous devez maintenant créer la structure de base de données. Pour cela cliquez ici : ";
$txt_verify_tables = "Vous êtes actuellement en train d'effectuer une mise à jour de PHPMyResa. Pour continuer cliquez ici : ";
$txt_no_need_to_create_db = "La structure de base de données est déjà créée.";
$txt_need_to_create_db = "La structure de base de données n'est pas créee.";
$txt_redirect_structure = "Veuillez commencer par la créer. Pour cela cliquez ici : ";
$txt_pb_requete1 = "Un problème est survenu lors de l'exécution de la requête SQL : ";
$txt_pb_requete2 = "Le message d'erreur est : ";
$txt_create_db_incorrect = "Impossible de créer automatique la structure de la base de données. Merci de le faire manuellement.";
$txt_create_db_correct = "La structure de la base de données a été correctement crée.";

$txt_nbRows_languages_error = "Impossible de connaître le contenu de la table languages.";
$txt_nbRows_languages_empty = "La table languages est vide. Pour la remplir, cliquez ici : ";
$txt_nbRows_languages_not_empty = "La table languages est déjà remplie. Si vous désirez vérifier la consistence de son contenu ou la mettre à jour, cliquez ici : ";
$txt_fill_languages_button = "Remplissage de la table languages";
$txt_fill_languages_question1 = "La table languages est déjà remplie. Elle contient %d lignes. Etes-vous sûr de vouloir la mettre à jour ?";
$txt_fill_languages_question4 = "(suppression des anciennes données, insertion de celles contenues dans le fichier doc/INSTALL_data_db.sql puis suppression des fichiers de session)";
$txt_fill_languages_question5 = "(%d traductions sont présentes dans votre fichier doc/INSTALL_data_db.sql)";
$txt_suppress_data_table_languages = "Suppression des anciennes données ... OK";
$txt_insert_data_table_languages = "Insertion des nouvelles données ... OK";
$txt_insert_incorrect_data_table_languages = "Toutes les données n'ont pas été insérées (%d sur %d) ... ERREUR";
$txt_notice_session_deletion = "Note : supprimer les fichiers de session vous fera perdre votre connection à la zone 'config' => vous devrez vous reconnecter";

$txt_summary2 = "Vérification du Contenu de la table languages";
$txt_structure_file_pb = "Le fichier contenant la structure attendue de la base de données est corrompu";
$txt_no_db_structure_pb = "La structure de votre base de données est correcte.";
$txt_db_structure_update = "La structure de votre base de données a été mise à jour (création de table(s) / création de champ(s) pour les tables existantes).";
$txt_db_structure_pb_table = "Des tables obligatoires manquent :";
$txt_db_structure_pb_column = "Des colonnes obligatoires manquent :";
$txt_db_structure_pb1 = "Veuillez corriger cela manuellement";
$txt_db_structure_pb2 = "La structure de votre base de données est incorrecte.";
$txt_table = "table";
$txt_column = "colonne";

$txt_pass_change_valide = "Votre nouveau mot de passe a bien été enregistré";
$txt_pass_change_invalide = "Veuillez resaisir votre ancien mot de passe";
$txt_old_password = "Ancien mot de passe :";
$txt_new_password = "Nouveau mot de passe :";
$txt_note = "Note : un mot de passe doit contenir au moins 8 caractères.";
$txt_champs_obligatoire = "Tous les champs doivent être compléter";
$txt_retaper_pass = "Veuillez retaper votre nouveau mot de passe";
$txt_pass_contain = "Votre mot de passe doit contenir au moins 8 caratères";

$txt_login = "Login : ";
$txt_passe = "Mot de passe : ";
$txt_inscription_valide = "Votre Login/password ont bien été enregistrés";
$txt_inscription_invalide = "Un problème est survenu lors de votre inscription";
$txt_connexion_invalide = "Mauvais Login/password";
$txt_deconnexion = "Se déconnecter";
$txt_inscription_explication1 = "Vous êtes dans la partie configuration de PHPMyResa.";
$txt_inscription_explication2 = "Veuillez vous authentifier pour y accéder.";
$txt_inscription_explication3 = "Afin de sécuriser cette zone, merci d'enregistrer un login / mot de passe pour y accèder.";
$txt_inscription_explication4 = "Aucun lien ne sera créé vers cette zone, veuillez donc l'ajouter à vos favoris";

$txt_error = " ... ERREUR";
$txt_error2 = "Une erreur est survenue";
$txt_ok = " ... OK";
$txt_pb_creation = "Le répertoire '%s' n'a pas pu être créé";
$txt_pb_deletion = "Impossible de détruire le fichier '%s'";
$txt_pb_deletion2 = "Merci de finir manuellement la destruction des fichiers de session (%s/sess_*)";
$txt_pb_session = "Un problème est survenu à l'ouverture du répertoire contenant les fichiers de session : %s";
$txt_propose_deletion_session_files1 = "Vous êtes sur le point de supprimer tous les fichiers de session de votre serveur web  (%d fichiers 'sess_*' dans le répertoire '%s').";
$txt_propose_deletion_session_files2 = "Cela est nécessaire si vous avez modifié le contenu de la table languages.";
$txt_confirm = "Voulez-vous continuer ?";
$txt_no_session_file = "Aucun fichier de session n'existe dans le répertoire %s";


/***********************************************************************************************
****************		Partie Contenu de la base de données		****************
***********************************************************************************************/

$txt_verification = "Tests effectués :";
$txt_database_content[0] = "Vérification de l'existence et du droit d'écriture sur le fichier '$config_database_file'";
$txt_database_content[1] = "Test de connection à la base de données";
$txt_database_content[2] = "Vérification que le nombre de tables dans la base de données est non nul";
$txt_database_content[3] = "Vérification de la structure de la base de données";
$txt_database_content[4] = "Vérification qu'il existe au moins une classe dans la base de données";
$txt_database_content[5] = "Pour chaque ressource déclarée avec une gestion de priorité, un message donnant le type de priorité est obligatoire dans la table languages";
$txt_database_content[6] = "La langue par défaut des administrateurs doit être correctement remplie";
$txt_database_content[7] = "Les mots de passe vides ne sont pas permis pour les administrateurs";
$txt_database_content[8] = "Les mots de passe pour les administrateurs doivent être tous différents";
$txt_database_content[9] = "Les administrateurs déclarés se réfèrent-ils à des classes existantes (MySQL seulement) ?";
$txt_database_content[10] = "Les objets déclarés se réfèrent-ils à des classes existantes (MySQL seulement) ?";
$txt_database_content[11] = "Les réservations effectuées se réfèrent-elles à des objets existants (MySQL seulement) ?";
$txt_database_content[12] = "Un administrateur doit être déclaré pour chaque classe";
$txt_database_content[13] = "Le nom d'un objet ne peut pas contenir le caractère &quot;,&quot;";
$txt_database_content_J_13 = "Le nom d'un objet ne peut pas contenir le caractère ','.";
$txt_database_content[14] = "Le nom d'un objet ne peut pas contenir le caractère &quot;'&quot;";
$txt_database_content_J_14 = "Le nom d'un objet ne peut pas contenir le caractère \\\"'\\\"";
$txt_database_content[15] = "Le champ jour_meme de la table classe ne peut avoir pour valeur que 0 ou 1";
$txt_database_content[16] = "Les champs status, priority, pubiCal, available, visible, wifi de la table objet ne peuvent avoir pour valeur que 0 ou 1";
$txt_database_content[17] = "Chaque classe doit contenir au moins un objet";
$txt_database_content[18] = "Chaque classe doit contenir au moins un objet visible";
$txt_database_content[19] = "Chaque classe doit contenir au moins un objet disponible";

$txt_title_db_content_nom_classe = "Nom de la classe";
$txt_title_db_content_jour_meme = "Réservable le jour même";
$txt_title_db_content_nom_admin = "Nom de l'administrateur";
$txt_title_db_content_prenom_admin = "Prénom de l'administrateur";
$txt_title_db_content_pass = "Mot de passe";
$txt_title_db_content_email = "Adresse email";
$txt_title_db_content_telephone = "Numéro de téléphone";
$txt_title_db_content_default_language = "Langue par défaut";
$txt_title_db_content_nom_objet = "Nom de l'objet";
$txt_title_db_content_capacite = "Capacité";
$txt_title_db_content_valide = "Auto confirmé";
$txt_title_db_content_libelle = "Libellé";
$txt_title_db_content_priority = "Prioritaire";
$txt_title_db_content_ical = "Publié dans Ical";
$txt_title_db_content_dispo = "Disponible";
$txt_title_db_content_visible = "Visible";
$txt_title_db_content_wifi = "Wifi";

$txt_new_class = "Insertion d'une nouvelle classe :";
$txt_new_object = "Insertion d'un nouvel objet dans la classe ";
$txt_summary = "Etat des lieux";
$txt_add = "Ajouter";
$txt_nedd_class = "Vous n'avez pas encore défini de classe !";
$txt_db_content_ok = "Pas de problème détecté. Le contenu de votre base de données est correct.";
$txt_db_languages_content_ok = "Pas de problème détecté. Le contenu de votre table languages est correct.";
$txt_priority = "L'objet '%s' a été défini dans la base de données comme un objet pouvant possèder plusieurs niveaux de priorité.";
$txt_priority_prompt1 = "Vous devez définir le message correspondant à ce type de priorité.";
$txt_priority_prompt2 = "Si 'XXX' est le message définissant le type de priorité concerné, un message affiché sera :";
$txt_priority_prompt3 = "Veuillez rentrer une valeur pour votre type de priorité dans les champs suivants :";
$txt_priority_prompt4 = "N'oubliez pas après avoir saisi votre texte, pour que celui-ci soit pris en compte, de <a href='delete_session_files.php'><b>supprimer tous les fichiers de session</b></a>";
$txt_priority_prompt5 = "En <span style='color: red'>rouge</span>, les réservations prioritaires";
$txt_validation = "Merci de renseigner tous les champs avant de valider !";
$txt_missing_translation = "Il y a %d traductions manquantes dans la colonne %s de la table languages";
$txt_carefull = "attention";
$txt_admin_error2 = "L'administrateur '%s' a une valeur incorrecte pour le champ <i>default_language</i> : '%s'";
$txt_possible_values = "Les valeurs possibles sont : %s.";
$txt_admin_error3 = "L'administrateur '%s' doit avoir un mot de passe non vide.";
$txt_admin_error4 = "Plusieurs administrateurs avec des adresses mail différentes partagent le même mot de passe. Veuillez corriger cela.";
$txt_admin_error5 = "L'administrateur %s est lié à une classe n'existant pas.";
$txt_admin_error51 = "L'objet '%s' est lié à une classe n'existant pas.";
$txt_admin_error6 = "Il n'existe pas d'administrateur pour la classe '%s'";
$txt_objet_error = "L'objet '%s' est liée à un objet n'existant pas.";
$txt_objet_error_quote = "Un nom d'objet ne peut pas contenir d'apostrophe : \"%s\"";
$txt_objet_error_comma = "Un nom d'objet ne peut pas contenir de virgule : \"%s\"";
$txt_classe_error_01 = "La caratéristique 'jour_meme' de la classe '%s' ne peut avoir pour valeur que 0 ou 1";
$txt_object_error_01 = "Chacune des caractéristiques status, priority, pubiCal, available, visible et wifi de l'objet '%s' ne peut avoir pour valeur que 0 ou 1";
$txt_classe_error1 = "La classe '%s' ne contient aucun objet.";
$txt_classe_error2 = "La classe '%s' ne contient aucun objet visible.";
$txt_classe_error3 = "La classe '%s' ne contient aucun objet disponible.";

$txt_new_class_nom_classe_incomplet = "Le nom de la classe est obligatoire";
$txt_new_class_nom_classe_invalide = "Le nom de la classe existe deja";
$txt_new_class_pass_incomplet = "Un mot de passe est obligatoire";
$txt_new_class_mail_incomplet = "L'adresse email est obligatoire";
$txt_new_class_mail1_incomplet = "L'adresse email est incorecte";
$txt_new_class_pass_invalide = "Ce mot de passe existe deja";
$txt_new_objet_nom_objet_incomplet = "Le nom de l'objet est obligatoire";
$txt_new_objet_nom_objet_invalide = "Le nom de l'objet existe deja";
$txt_new_objet_libelle_incomplet = "Le libellé de l'objet est obligatoire";

$txt_overlib_jour_meme = "<ul><li>Si oui, rien de spécial</li><li>Si non, lors d'une réservation pour le jour même, un message indiquera que celle-ci n'est pas garantie.</li></ul>";
$txt_overlib_lang = "Il s'agit de la langue dans laquelle seront envoyé les emails de validation à l'administrateur de la classe";
$txt_overlib_capa = "Nombre de places disponibles pour les salles, commentaire libre sinon";
$txt_overlib_valide = "<ul><li>Si oui, les réservations de cet objet ne nécessiteront pas de validation de la part de l'administrateur</li><li>Si non, les réservations de cet objet nécessiteront une validation de la part de l'administrateur</li></ul>";
$txt_overlib_libelle = "Le libellé complet de l'objet, qui sera affiché dans les affiches PDF";
$txt_overlib_priority = "Une notion de priorité est-elle attaché aux réservations de cet objet ?<br />La notion de priorité a uniquement une vacation d'information";
$txt_overlib_ical = "Les réservations de cet objet doivent-elles être publiées dans Ical (si celui-ci est utilisé bien sûr)";
$txt_overlib_dispo = "Cet objet est-il disponible ? Cela sert à rendre un objet indisponible momentanément";
$txt_overlib_visible = "Cet objet est-il visible ? Cela sert à supprimer un objet de l'interface tout en conservant l'historique de ces réservations";
$txt_overlib_wifi = "Un accès wifi est-il associé aux réservations de cet objet ? Utile pour communiquer avec le logiciel PHPMyWifi";

$txt_confirm_modifier_classe = "Etes-vous sûr de vouloir modifier la classe : %s ?";
$txt_confirm_modifier_objet = "Etes-vous sûr de vouloir modifier l'objet : %s ?";
$txt_confirm_supprimer_classe = "Supprimer une classe revient à supprimer également tous les objets contenus dans cette classe. \\nLa classe '%s' contient %d objet(s). \\nElle est raccordée à %d réservation(s). \\nSouhaitez-vous la supprimer ?";
$txt_confirm_supprimer_objet = "Etes-vous sûr de vouloir supprimer l'objet '%s' ?";
$txt_confirm_supprimer_classe_reserv = "Cette objet contient des réservations, êtes-vous sûr de vouloir le supprimer ?";
$txt_confirm_supprimer_reserv = "Voulez-vous supprimer ses réservations ?";
$txt_confirmation_objet_reserv = "L'objet ainsi que ses réservations ont bien été supprimés";
$txt_confirmation_objet = "L'objet a bien été supprimé";


/***********************************************************************************************
****************	Partie Configuration de l'application			****************
***********************************************************************************************/

// Partie Générale

$txt_config1_main_titre1 = "Généralités";
$txt_config1_main_titre2 = "Contact technique";
$txt_config1_main_titre3 = "Réseau";
$txt_config1_main_titre4 = "Serveur";
$txt_config1_main_titre5 = "Valeurs par défaut";

$txt_config1_titre = "Le titre de l'application, affiché en haut à gauche de la page";
$txt_config1_bookmark = "Le nom du signet pour la page";
$txt_config1_image_titre = "Nom de l'image en haut à gauche de la page *";
$txt_config1_image_pdf = "Nom de l'image pour la génération du PDF (format jpg uniquement !)*";
$txt_config1_page_accueil = "Page d'accueil";
$txt_config1_page_accueil1 = "Planning du mois";
$txt_config1_page_accueil2 = "Page d'aide";
$txt_config1_page_accueil3 = "Réservations du jour";
$txt_config1_email_domain = "Domaine d'adresse email, dans un but d'autocomplétion (ex : @lpsc.in2p3.fr)";
$txt_config1_capacite = "Affiche t-on une capacité pour certains objets réservables";
$txt_config1_modification_enable = "Les utilisateurs peuvent-ils modifier certains champs d'une réservation";
$txt_config1_default_language = "Langue de l'application par défaut si l'utilisateur n'a pas de cookie la spécifiant";
$txt_config1_display_tomorrow = "Affiche t-on le planning du lendemain";
$txt_config1_several_object_possible = "Plusieurs objets peuvent-il être simultanément réservés";
$txt_config1_week_end_enabled = "Les jours du week-end sont-ils à intégrer dans le système";
$txt_config1_tree = "La présentation de la liste des objets est-elle sous forme d'arbre";
$txt_config1_tree_scrollable = "Cet arbre de présentation des objets est-il scrollable";
$txt_config1_mail_subject = "Taille du sujet des mails de création de réservations";
$txt_config1_default_class_for_feed= "Classe par défaut pour l'affichage du flux rss";
$txt_config1_use_diffusion_flag = "L'utilisateur peut-il choisir de diffuser ou non ses réservations ?";

$txt_config1_technical_contact = "Nom du contact technique local";
$txt_config1_technical_email = "Adresse email du contact technique local";
$txt_config1_technical_tel = "Numéro de téléphone du contact technique local";
$txt_config1_read_only_enable = "Voulez-vous restreindre l'accès au site en lecture seule";
$txt_config1_IP_regex = "Expression régulière des adresses IP autorisées";
$txt_config1_enabled_domain = "Liste des domaines autorisés";
$txt_config1_mail_server = "Le nom de votre serveur de messagerie";
$txt_config1_web_server = "Le nom de votre serveur web";
$txt_config1_default_beginning_hour = "Valeur par défaut de l'heure de début des réservations (h)";
$txt_config1_default_beginning_mihour = "Valeur par défaut de l'heure de début des réservations (min)";
$txt_config1_default_duration = "Valeur par défaut de la durée des réservations (h)";
$txt_config1_default_miduration = "Valeur par défaut de la durée des réservations (min)";
$txt_config1_default_end_hour = "Valeur par défaut de l'heure de fin des réservations (h)";
$txt_config1_default_end_mihour = "Valeur par défaut de l'heure de fin des réservations (min)";
$txt_config1_default_comment = "Valeur par défaut du commentaire des réservations ";
$txt_config1_default_beginning_hour_error = "Valeur par défaut de l'heure de début des réservations (h) incorrecte";
$txt_config1_default_beginning_mihour_error = "Valeur par défaut de l'heure de début des réservations (min) incorrecte";
$txt_config1_default_duration_error = "Valeur par défaut de la durée des réservations (h) incorrecte";
$txt_config1_default_miduration_error = "Valeur par défaut de la durée des réservations (min) incorrecte";
$txt_config1_default_end_hour_error = "Valeur par défaut de l'heure de fin des réservations (h) incorrecte";
$txt_config1_default_end_mihour_error = "Valeur par défaut de l'heure de fin des réservations (min) incorrecte";

$txt_recommandation1 = " * Placer l'image dans le répertoire img/ et ne spécifier que le nom du fichier";

$txt_overlib_capacite = "La capacité est le nombre de places dans une salle ou un commentaire libre si ce n'est pas un nombre";
$txt_overlib_modification_enable = "Sinon pour modifier une réservation il sera nécessaire de la supprimer puis de la recréer";
$txt_overlib_read_only_enable = "Cela permet de restreindre l'accès au site en écriture à certaines adresses IP ou à certains domaines et de le rendre accessible en lecture seule aux autres.<br />Cela est particulièrement utile pour les sites qui ne sont pas hébergés dans un intranet.<br /><br />Vous ne pouvez utiliser cette fonctionnalité que si votre hébergeur vous permet l'utilisation de la fonction php gethostbyaddr()";
$txt_overlib_IP_regex = "Ex : 111.222.3[4-5].\d{1,3}";
$txt_overlib_enabled_domain = "Les domaines sont séparés par des virgules";
$txt_overlib_mail_server = "A remplir uniquement si le nom de domaine de vos adresses mail (ex : @lpsc.in2p3.fr) correspond au nom de votre serveur web (ex : http://lpsc.in2p3.fr), car dans ce cas les mails ne seraient pas correctement envoyés.<br />Si vous remplissait cette section, toute occurence du nom du serveur web dans une adresse mail sera automatiquement replacée par le nom du serveur de mail<br /><br />Ex : lpscmail.in2p3.fr";
$txt_overlib_web_server = "A remplir uniquement si le nom de domaine de vos adresses mail (ex : @lpsc.in2p3.fr) correspond au nom de votre serveur web (ex : http://lpsc.in2p3.fr), car dans ce cas les mails ne seraient pas correctement envoyés.<br />Si vous remplissait cette section, toute occurence du nom du serveur web dans une adresse mail sera automatiquement replacée par le nom du serveur de mail<br /><br />Ex : lpsc.in2p3.fr";
$txt_overlib_feed = "Sinon l'affichage dans le flux rss et dans ical (si celui-ci est utilisé bien sûr) est activé par défaut";

$txt_pb1_image_titre = "L'emplacement du fichier de l'image en haut à gauche est erroné";
$txt_pb2_image_titre = "Le fichier de l'image en haut à gauche n'est pas accessible en lecture";
$txt_pb1_image_pdf = "L'emplacement du fichier pour la génération du PDF est erroné";
$txt_pb2_image_pdf = "L'extension du fichier image doit être .jgp ou .jpeg";
$txt_pb3_image_pdf = "Le fichier de l'image pour la génération du pdf n'est pas accessible en lecture";
$txt_pb_technical_contact = "Le nom du contact technique local est obligatoire";
$txt_pb_technical_email = "L'adresse email du contact technique local est obligatoire";
$txt_pb_technical_tel = "Le numéro de téléphone du contact technique local est obligatoire";
$txt_pb_IP_regex = "L'expression régulière ou le tableau correspondant aux adresses IP autorisées est obligatoire";
$txt_pb_domain = "Les noms de domaines spécifiés sont incorrects";
$txt_pb_mail_server = "Le nom de votre serveur de messagerie est obligatoire";
$txt_pb_web_server = "Le nom de votre serveur web est obligatoire";

$txt_short = "petit";
$txt_medium = "moyen";
$txt_long = "long";

$txt_info_php = "Vous utilisez PHP %s";
$txt_register_global = "Il n'est pas recommendé de positionner dans votre fichier php.ini la variable register_globals à on";


// Partie Annexe

$txt_config2_main_titre1 = "PHPICalendar";
$txt_config2_main_titre2 = "Excel";
$txt_config2_main_titre3 = "SOAP (pas encore utilisé)";

$txt_config2_iCal = "Désirez-vous utiliser le format iCal";
$txt_config2_URL_iCal = "URL du logiciel PHP iCalendar";
$txt_config2_CALENDARS_file_location = "Emplacement des fichiers ical générés";
$txt_config2_CALENDARS_file_beginning_of_name = "Début du nom des fichiers générés";
$txt_config2_CALENDARS_end_of_uid = "Fin des UID (identifiant) dans les fichiers générés";
$txt_config2_iCal_delay = "Nombre en semaines passées à garder dans l'historique des fichiers icalendar";

$txt_overlib_URL_iCal = "L'emplacement doit terminer par '/'<br />Ex : http://xxx/phpicalendar-1.0/ ou ../phpicalendar-1.0/";
$txt_overlib_CALENDARS_file_location = "L'emplacement doit terminer par '/'<br />Ex : /www/htdocs/calendars/ ou D:/www/calendars/";
$txt_overlib_CALENDARS_file_beginning_of_name = "Ex : LPSCResa-";
$txt_overlib_CALENDARS_end_of_uid = "Ex : .LPSCResa.in2p3.fr";
$txt_overlib_iCal_delay = "-1 si illimité";

$txt_pb1_URL_iCal = "L'URL du logiciel PHP iCalendar est obligatoire";
$txt_pb2_URL_iCal = "L'URL du logiciel PHP iCalendar doit finir par le caractère '/'";
$txt_pb3_URL_iCal = "L'URL du logiciel PHP iCalendar est erronée";
$txt_pb1_CALENDARS_file_location = "L'emplacement des fichiers ical générés est obligatoire";
$txt_pb2_CALENDARS_file_location = "L'emplacement des fichiers ical générés doit finir par le caractère '/'";
$txt_pb3_CALENDARS_file_location = "L'emplacement des fichiers ical générés est erroné";
$txt_pb4_CALENDARS_file_location = "L'emplacement des fichiers ical générés n'est pas accessible en lecture";
$txt_pb5_CALENDARS_file_location = "L'emplacement des fichiers ical générés n'est pas accessible en écriture";
$txt_pb_CALENDARS_file_beginning_of_name = "Le début du nom des fichiers générés est obligatoire";
$txt_pb_CALENDARS_end_of_uid = "La fin des UID (identifiant) dans les fichiers générés est obligatoire";
$txt_explanation_ical = "Si vous désirez utiliser iCal, vous devez télécharger le logiciel <a href='http://phpicalendar.sourceforge.net/nuke/' target='_blank'>PHP iCalendar</a>, l'installer (tar xvzf xxx) et enfin le configurer, en éditant le fichier config.inc.php. <br />La seule variable à modifier est $"."calendar_path. Donnez lui pour valeur l'emplacement des fichiers ical générés.";
$txt_explanation_excel1 = "Si vous désirez utiliser la fonction de création de fichier Excel (pour exporter le résultat d'une recherche) :";
$txt_explanation_excel2 = "vous devez installer les packages pear OLE et Spreadsheet_Excel_Writer ('<i>pear install http://pear.php.net/get/OLE</i>' et '<i>pear install http://pear.php.net/get/Spreadsheet_Excel_Writer</i>')";
$txt_explanation_excel3 = "vous devez ajuster les droits sur le répertoire 'stats' de façon à ce que votre serveur web puisse y écrire";
$txt_excel_actif = "Cette fonctionnalité est activée";
$txt_soap_pb = "Afin d'utiliser les web services vous devez installer PHP avec l'extension SOAP (option de compilation --enable-soap)";
?>
