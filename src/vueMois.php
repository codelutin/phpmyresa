<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File vueMois.php
*
* This file is used for the main display view
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ((($nbpar != 3) && ($nbpar != 0)) || (count($_GET) != 0)) exitWrongSignature('vueMois.php');
if (isset($_POST['moisSelect'])){
	$mois = $_POST['moisSelect'];
	if ( ! ctype_digit($mois) || ($mois == '') ) exitWrongSignature('vueMois.php');
	if ($mois > 12) exitWrongSignature('vueMois.php');
}else $mois = $moisCourant;

if (isset($_POST['anneeSelect'])){
	$annee = $_POST['anneeSelect'];
	if (strlen($annee) > 4) exitWrongSignature('vueMois.php');
	if ( ! ctype_digit($annee) || ($annee == '') ) exitWrongSignature('vueMois.php');
} else $annee = $anneeCourante;

if (isset($_POST['selectionObjet'])){
	$objet = $_POST['selectionObjet'];
	if ( ! in_array($objet, getAvailableObjects()) && ($objet != 'tout') && ! in_array($objet, getAvailableClass()) ) exitWrongSignature('vueMois.php');
} else $objet = "tout";
$objet = stripslashes($objet);

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

echo $entete;

function vueMois($mois,$annee,$objet) {
	global $connexionDB, $dateCourante, $week_end_enabled;

	$qdate = $annee."-".fillWithZero($mois);

	if ($objet=='tout'){
	    $DB_request = "SELECT R.id, R.jour, O.nom AS objet, R.debut, R.duree, R.titre, R.email, R.valide, R.commentaire FROM reservation R, objet O, classe C ";
	    $DB_request .= "WHERE R.idobjet = O.id AND R.jour LIKE '"."$qdate"."%' AND O.id_classe = C.id AND R.state=0";
	} else {
	    $DB_request = "SELECT count(*) AS nb FROM classe WHERE nom = '$objet'";
	    $result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $rowNb = database_fetch_object($result);
	    $DB_request = "SELECT R.id, R.jour, O.nom AS objet, R.debut, R.duree, R.titre, R.email, R.valide, R.commentaire FROM reservation R, objet O, classe C ";
	    $DB_request .= "WHERE R.idobjet = O.id AND R.jour LIKE '"."$qdate"."%' ";
	    if (database_get_from_object($rowNb, 'nb') == 1)
	        // Cas de la sÃ©lection d'une classe
	        $DB_request .= " AND O.id_classe = C.id AND C.nom='$objet'";
	    else
	        // Cas de la sÃ©lection d'un objet
	        $DB_request .= " AND O.nom LIKE '$objet' AND O.id_classe = C.id";
	    $DB_request .= " AND R.state=0 ";
	}
	$DB_request .= " ORDER BY R.jour, C.id, O.nom, R.debut";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$DBresa = array();
	$id = 0;
	while (($reservation = database_fetch_object($resultat)) != NULL){
	    $DBresa['id'][$id] = database_get_from_object($reservation, 'id');
	    $DBresa['jour'][$id] = database_get_from_object($reservation, 'jour');
	    $DBresa['objet'][$id] = database_get_from_object($reservation, 'objet');
	    $DBresa['debut'][$id] = database_get_from_object($reservation, 'debut');
	    $DBresa['duree'][$id] = database_get_from_object($reservation, 'duree');
	    $DBresa['titre'][$id] = database_get_from_object($reservation, 'titre');
	    $DBresa['email'][$id] = database_get_from_object($reservation, 'email');
	    $DBresa['valide'][$id] = database_get_from_object($reservation, 'valide');
	    $DBresa['commentaire'][$id] = database_get_from_object($reservation, 'commentaire');
	    $id++;
	}
	$nbResa = $id;

	$pm=getdate(mktime(0,0,0,$mois,1,$annee));	//On recupere le premier du mois
	$nj=$pm["wday"];	//Puis le numero du premier jour dans la semaine exprimee de 0 a 6
	if ($nj == 0){
	    if ($_SESSION['s_jour_offset'] == -1) $nj=7;
	}

	$width = 110;
	$totalWidth = 7 * $width;
	?>
	<div>
	    <table style='width:<?php echo $totalWidth;?>px;border:thin gray solid;' border='1' cellspacing='0' cellpadding='1'>
	        <tr style='background-color:#d3dce3'>
<?php
	for ($i=0; $i<7; $i++){
	    if ($_SESSION['s_jour_travail'][$i]) {
	    ?>
	            <td class='center'><b><?php echo $_SESSION['s_les_jours'][$i];?></b></td>
<?php
	    } else if ($week_end_enabled) {
	    ?>
	            <td class='center' style='background-color:#b3bcc3'><b><?php echo $_SESSION['s_les_jours'][$i];?></b></td>
<?php
	    }
	}
	?>
	        </tr>
<?php
	$sem_ini = 0;
	$test = 0;	// indique le nombre de premier de mois trouvÃ©
	//***********************************************************************************
	//	Test pour savoir si on affiche la premiÃšre semaine
	//	dans le cas d'un affichage hors we car alors le premier jour du mois
	//	peut Ãªtre un samedi ou un dimanche, donc Ã  ne pas afficher
	if ( ! $week_end_enabled){
	    for ($j=1; $j<=7; $j++){
	        $jourDuMois = $j-$_SESSION['s_jour_offset']-$nj;
	        $jj = getdate(mktime(0,0,0,$mois,$jourDuMois,$annee));
	        $leJour = $jj["mday"];
	        if ( ($leJour == 1) && ( ! $_SESSION['s_jour_travail'][$j - 1]) ){
	            $sem_ini = 1;
	            $test = 1;
	        }
	    }
	}
	//***********************************************************************************

	$index = 0;
	$maxSem=5;	    //Un mois peut s'afficher sur 6 semaines
	for ($sem = $sem_ini ; $sem <= $maxSem ; $sem++){
	?>
	        <tr>
<?php
	    for ($j=1 ; $j<=7 ; $j++){

	        $jourDuMois = $sem*7+$j-$_SESSION['s_jour_offset']-$nj;
	        $jj = getdate(mktime(0,0,0,$mois,$jourDuMois,$annee));
	        $leJour = $jj["mday"];	    // $lejour est diffÃ©rent de $jourDuMois Ã  la fin du mois
	        if ($leJour == 1) {
	            $test++;
	            if ($test>=2) $maxSem--;
	        }

	        $jourDuMoisSuivant = $jourDuMois + 1;
	        $jjSuivant=getdate(mktime(0,0,0,$mois,$jourDuMoisSuivant,$annee));
	        $leJourSuivant=$jjSuivant["mday"];
	        if ( ($leJourSuivant == 1) && ($test == 1) && ($j == 7) ) $maxSem--;

	        if ( ! $week_end_enabled){
	            if ( ! $_SESSION['s_jour_travail'][$j - 1]) continue;
	        }

	        if ($test == 1){
	            if ($jj["yday"] == $dateCourante["yday"] && $annee == $dateCourante["year"])
	                $couleur="#ff0000"; else $couleur="#c0c0c0";
	                ?>
	            <td class='gauche' style='vertical-align:top;color:<?php echo $couleur;?>;border-color:gray'>
	                <b><?php echo $leJour;?></b>
<?php
	            $resa = 0;
	            $qdateArray = $qdate."-".fillWithZero($jourDuMois);
	            for ($i = $index ; $i < $nbResa ; $i++){
	                if ($DBresa['jour'][$index] < $qdateArray){
	                    // Cas des rÃ©servations le we alors que $week_end_enabled = false
	                    $index++;
	                    continue;
	                }
	                if ($DBresa['jour'][$index] > $qdateArray) break;
	                if ($resa == 0){
	                ?>
	                <img src='img/agt.gif' style='vertical-align:top' alt='' /><br />
<?php
	                    $resa = 1;
	                }

	                $inter_deb = $_SESSION['s_language']['ponctuation']." ".substr($DBresa['debut'][$index],0,5);
	                $inter_dur = $_SESSION['s_language']['ponctuation']." ".substr($DBresa['duree'][$index],0,5);
	                $messageTime = $_SESSION['s_language']['title_array3'].$inter_deb." - ".$_SESSION['s_language']['title_array4'].$inter_dur;
	                $messageEmail = $_SESSION['s_language']['title_array11']." ".$DBresa['email'][$index];
	                $objetCourant = $DBresa['objet'][$index];
	                $comment = $DBresa['commentaire'][$index];
	                $messageTitle = $DBresa['titre'][$index];

	                $messageTitle = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$messageTitle);
	                $messageTitle = ereg_replace("'","\&#39;",$messageTitle);
	                $messageTitle = ereg_replace("\"","&quot;",$messageTitle);

	                if ($objet == 'tout'){
	                    if ($DBresa['valide'][$index]=='1'){
	                        $color = "#0000ff";
	                        $class = "es1";
	                    } else {
	                        $color = "#ff00ff";
	                        $class = "es2";
	                    }

	                    if ($comment != ""){
	                        $comment = "<p>".$_SESSION['s_language']['title_array12']."<br /><i><b>".$comment."</b></i>";
	                        $comment = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$comment);
	                        $comment = ereg_replace("'","\&#39;",$comment);
	                        $comment = ereg_replace("\"","\&#34;",$comment);
	                    }
	                    $messageCrypte = PHPcrypt($messageEmail);
	                    $afficheMessageCrypte = "decryptJ('$messageTime<br />', '$messageCrypte', '$comment')";
	                    ?>
	                <a href='#'
	                    onmouseover="return overlib(<?php echo $afficheMessageCrypte;?>, CAPTION, '<?php echo $messageTitle;?>', CENTER, OFFSETY, 20, TEXTSIZE, '12px', FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);"
	                    onmouseout="return nd();" class='<?php echo $class;?>'><?php echo $objetCourant;?></a>
	                <br />
<?php
	                } else {
	                    $messageCrypte = PHPcrypt($messageEmail);
	                    $afficheMessageCrypte = "<script type='text/javascript'>decrypt(\"$messageTime<br />\\\"$messageTitle\\\"<br />\", \"$messageCrypte\", \"\")</script>";
	                    if ($DBresa['valide'][$index]=='1'){
	                    ?>
	                <span style='color:#0000ff'><b><a href="javascript:genAffiche('<?php echo $DBresa['id'][$index];?>');")><?php echo $objetCourant;?></a></b>
	                <br /><?php echo $afficheMessageCrypte;?></span><br />
<?php
	                    } else {
	                    ?>
	                <span style='color:#ff00ff'><b><?php echo $objetCourant;?></b><br /><?php echo $afficheMessageCrypte;?></span><br />
<?php
	                    }
	                }
	                $index++;
	            }
	        } else {
	        ?>
	            <td class='center'>&nbsp;
<?php
	        }
	        ?>
	            </td>
<?php
	    }
	    ?>
	        </tr>
<?php
	}
	?>
	    </table>
	</div>
<?php
}
?>

<body style='font-size:small'>

<script type='text/javascript' src='<?php echo $URL_overlib;?>overlib.js'></script>
<script type="text/javascript">
<!--

	function genAffiche(id){
	    var formB = document.getElementById('startprint');
	    formB.id.value = id;
	    formB.submit();
	}

//-->
</script>

<div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>

<form id='genererCalendrier' action='vueMois.php' method='post'>
<table class='noborder' style='width:595px'>
	<tr>
	    <td class='center'>
	        <select name="moisSelect" onchange="javascript:document.getElementById('genererCalendrier').submit();">
<?php
	for ($i=1; $i<13; $i++){
	    if ($i == $mois){
	    ?>
	        <option value='<?php echo $i;?>' selected='selected'><?php echo $_SESSION['s_les_mois'][$i-1];?></option>
<?php
	    } else {
	    ?>
	        <option value='<?php echo $i;?>'><?php echo $_SESSION['s_les_mois'][$i-1];?></option>
<?php
	    }
	}
?>
	        </select>

	        <select name="anneeSelect" onchange="javascript:document.getElementById('genererCalendrier').submit();">
<?php
	for ($i=$anneeCourante-1; $i<$anneeCourante+4; $i++){
	    if ($i == $annee){
	    ?>
	        <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else {
	    ?>
	        <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}
?>
	        </select>

	        <select name="selectionObjet" onchange="javascript:document.getElementById('genererCalendrier').submit();">
	            <option value='tout'><?php echo $_SESSION['s_language']['vueMois_all'];?></option>
<?php
$DB_request = "SELECT C.nom AS nomclasse, O.nom AS nomobjet FROM classe C, objet O WHERE C.id = O.id_classe ORDER BY C.id, O.id";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, true);
$oldClasse='';
while ($row = database_fetch_object($resultat)){
	$classe = database_get_from_object($row, 'nomclasse');
	if ($oldClasse == $classe){
		$inter = database_get_from_object($row, 'nomobjet');
		if ($objet == $inter) echo "<option value='$inter' selected='selected'>$inter</option>\n";
		else echo "<option value='$inter'>$inter</option>\n";
	} else {
		if ($objet == $classe) echo "<option value='$classe' selected='selected' style='background:gray;color:white;'>$classe</option>\n";
		else echo "<option value='$classe' style='background:gray;color:white;'>$classe</option>\n";
		$oldClasse = $classe;
		$inter = database_get_from_object($row, 'nomobjet');
		if ($objet == $inter) echo "<option value='$inter' selected='selected'>$inter</option>\n";
		else echo "<option value='$inter'>$inter</option>\n";
	}
}
?>
	        </select>
	    </td>
	</tr>
</table>
</form>

<div><br /><br /></div>

<?php
if ($objet != 'tout') echo "<div style='font-size:smaller'>".$_SESSION['s_language']['vueMois_affiche']."</div>\n";

vueMois($mois,$annee,$objet);

if (get_objectValidationExists()){
?>
<div>
	<br />
	<span style='color:#ff00ff;'><?php echo $_SESSION['s_language']['vueMois_object'];?></span> = <?php echo $_SESSION['s_language']['vueMois_non_valide'];?>
	<br />
	<span style='color:#0000ff;'><?php echo $_SESSION['s_language']['vueMois_object'];?></span> = <?php echo $_SESSION['s_language']['vueMois_valide'];?>
	<br />
</div>
<?php
}
?>

<form id='startprint' action='affichePDF.php' method='get' target='affichePDF'>
	<p><input type='hidden' name='id' id='id' /></p>
</form>

<?php echo $body_end;?>
