<?php require_once('INSTALL_creation_db_script.php');?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border=1 width='100%'><tr bgcolor='#dddddd'><td><h1>Installation de la version V4.0 - CrÃ©ation de la structure de la base de donnÃ©es</h1></td></tr></table>
<br><br>

<h3>ModÃšle physique de la base de donnÃ©es (MySQL)</h3>
<img src='MPD.gif' width='100%' alt='modÃšle physique'>

<br><br><br><br><br>
<h3>Script de crÃ©ation des tables de la base de donnÃ©es</h3>

<table border=2 frame=void rules=all>
<tr align='center'><td><b>MySQL</b></td><td><b>PostgreSQL</b></td><td><b>Oracle</b></td></tr>
<tr valign='top'>
<td width='33%'>
<?php echo $mysql;?>
</td>
<td width='33%'>
<?php echo $postgresql;?>
</td>
<td width='33%'>
<?php echo $oracle;?>
</td></tr>
</table>

<br><br><br>
<h3>Script d'ajout des donnÃ©es dans la base de donnÃ©es</h3>

Pour remplir la table languages, copiez collez le contenu du fichier doc/INSTALL_data_db.sql dans votre base de donnÃ©es.<br>
Si vous utilisez Oracle, n'oubliez pas le <i>commit</i> aprÃšs.
<br><br>

</body>
</html>
