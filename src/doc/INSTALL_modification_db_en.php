<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<script src='../commun/function.js' type='text/javascript'></script>
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border=1 width='100%'><tr bgcolor='#dddddd'><td><h1>Installation of version V4.0 - Update of the database structure</h1></td></tr></table>
<br><br>

The database structure update depends on the PHPMyResa version you are currently running.
<br><br>
<b>Be careful</b>: the following scripts given below will work only if your MySQL version is greater than 4. Otherwise the best solution is to update your MySQL server.
 If it is not possible and if you do not know how to do the database structure modifications yourself, contact
<script type='text/javascript'>decrypt("<a href='", "UIQT1W:XPXU6ZM0IFKKGQVxXyGNZ?02JRMK1=kZWJTMU 1W 2XLI1M 1PM LI1IJI0M 01Z2K12ZM 4Q1P  h6nlg y WZ TM00", "'>")</script>
us</a>, for help.
<br><br>

<table border=0>



<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>If your current PHPMyResa version is V3.x</b></li></ul></td></tr>

<tr><td width=40></td><td>Here are the different steps to do in order to update your database structure to version V4.0:</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>1 - Addition of new fields</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `languages` ADD `spanish` TEXT NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `languages` ADD `german` TEXT NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `diffusion` int(1) NOT NULL default '1' ;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>2 - Update of data of the "languages" table</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td width=40></td><td>In order to update the data of the <i>languages</i> table, empty its content
	(with the command: <i>"delete from languages where name not like 'resa_mandatory_priority_%';"</i>) then copy and paste the content of the INSTALL_data_db.sql file in mysql.</td></tr>
<tr><td width=40></td><td><font color='red'>Do not forget to delete the session files afterwards.</font> They are to be found in the directory specified by the php variable session.save_path (see your php.ini file).
</td></tr>
<tr><td>&nbsp;</td><td></td></tr>








<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>If your current PHPMyResa version is V2.x</b></li></ul></td></tr>

<tr><td width=40></td><td>Here are the different steps to do in order to update your database structure to version V3.0:</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>1 - Changes in the structure of existing fields</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `date` `jour` DATE DEFAULT '0000-00-00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `jour` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `titre` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `email` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `jour` `jour` VARCHAR( 10 ) DEFAULT '0000-00-00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `debut` `debut` VARCHAR( 8 ) DEFAULT '00:00:00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `duree` `duree` VARCHAR( 8 ) DEFAULT '00:00:00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `pubIcal` `pubical` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `capacite` `capacite` VARCHAR( 30 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>2 - Addition of new fields</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `visible` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `wifi` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `wifi` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `state` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX `state` ( `state` );</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>3 - Update of data of the "languages" table</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td width=40></td><td>In order to update the data of the <i>languages</i> table, empty its content
	(with the command: <i>"delete from languages where name not like 'resa_mandatory_priority_%';"</i>) then copy and paste the content of the INSTALL_data_db.sql file in mysql.</td></tr>
<tr><td width=40></td><td><font color='red'>Do not forget to delete the session files afterwards.</font> They are to be found in the directory specified by the php variable session.save_path (see your php.ini file).
</td></tr>
<tr><td>&nbsp;</td><td></td></tr>







<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>If your current PHPMyResa version is V1.0 or V1.1</b></li></ul></td></tr>

<tr><td width=40></td><td>Here are the different steps to do in order to update your database structure to version V2.0:</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>1 - Creation of the table classe and use of the field id_classe in other tables</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `classe` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `id` int(2) NOT NULL auto_increment,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `nom` varchar(50) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `jour_meme` int(1) NOT NULL default '1',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  PRIMARY KEY  (`id`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  UNIQUE KEY `nom` (`nom`)</i></td></tr>
<tr><td></td><td><i>);</i></td></tr>
<tr><td></td><td> => fill in this table with correct values</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `administrateur` ADD `id_classe` INT( 2 ) FIRST;</i></td></tr>
<tr><td></td><td><i>UPDATE administrateur A, classe C SET A.id_classe = C.id WHERE A.classe = C.nom;</i></td></tr>
<tr><td></td><td>=> check the correct result of this command!</td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` CHANGE `id_classe` `id_classe` INT( 2 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` DROP `classe`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` ADD PRIMARY KEY ( `id_classe` );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `salles` ADD `id_classe` INT( 2 );</i></td></tr>
<tr><td></td><td><i>UPDATE salles S, classe C SET S.id_classe = C.id WHERE S.classe = C.nom;</i></td></tr>
<tr><td></td><td>=> check the correct result of this command!</td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` DROP `classe`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` ADD INDEX ( `id_classe` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` CHANGE `id_classe` `id_classe` INT( 2 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `reservation` DROP INDEX `idsalle`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `idsalle` `idobjet` INT( 2 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `idobjet` );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>2 - Creation of the table objet instead of the table salle</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `objet` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `id` int(2) unsigned NOT NULL auto_increment,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `id_classe` int(2) NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `nom` varchar(128) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `capacite` int(3) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `status` int(1) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `libelle` varchar(255) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `videoConf` int(1) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; PRIMARY KEY  (`id`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  UNIQUE KEY `nom` (`nom`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  KEY `id_classe` (`id_classe`)</i></td></tr>
<tr><td></td><td><i>);</i></td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>INSERT INTO `objet` ( `id` , `id_classe` , `nom` , `capacite` , `status` , `libelle` , `videoConf` ) SELECT `id` , `id_classe` , `nom` , `capacite` , `status` , `libelle` , `videoConf` FROM `salles`;</i></td></tr>
<tr><td></td><td><i>DROP TABLE `salles`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `pubIcal` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `available` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `videoConf` `priority` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>3 - Other</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>ALTER TABLE `administrateur` CHANGE `telephone` `telephone` VARCHAR( 20 ) NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` ADD `default_language` VARCHAR( 30 ) DEFAULT 'english' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `videoConference` `priority` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `languages` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `name` varchar(50) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `french` text NOT NULL,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `english` text NOT NULL,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; PRIMARY KEY  (`name`)</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>4 - Filling of the table languages</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td>In order to fill this table, copy and paste the content of the INSTALL_data_db.sql file in mysql.</td></tr>
<tr><td>&nbsp;</td><td></td></tr>








<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>If you have a version previous to PHPMyResa V1.0</b></li></ul></td></tr>

<tr><td width=40></td><td>Here is the sql script which will allow you to update the database structure to version V1.0:</td></tr>

<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td>ALTER TABLE salles DROP PRIMARY KEY;</td></tr>

<tr><td></td><td>ALTER TABLE salles ADD id INT(2) UNSIGNED NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (id);</td></tr>

<tr><td></td><td>ALTER TABLE reservation DROP classe;</td></tr>

<tr><td></td><td>ALTER TABLE reservation ADD idsalle INT(2) UNSIGNED NOT NULL;</td></tr>

<tr><td></td><td>UPDATE reservation R, salles S SET R.idsalle = S.id WHERE S.nom = R.salle;</td></tr>

<tr><td></td><td>ALTER TABLE reservation ADD index (idsalle);</td></tr>

<tr><td></td><td>ALTER TABLE reservation DROP salle;</td></tr>
<tr><td>&nbsp;</td><td></tr>

</table>

</body>
</html>
