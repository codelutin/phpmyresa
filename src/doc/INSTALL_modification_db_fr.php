<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<script src='../commun/function.js' type='text/javascript'></script>
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border=1 width='100%'><tr bgcolor='#dddddd'><td><h1>Installation de la version V4.0 - Mise Ã  jour de la structure de la base de donnÃ©es</h1></td></tr></table>
<br><br>

La mise Ã  jour de la structure de votre base de donnÃ©es dÃ©pend de la version de PHPMyResa que vous utilisez actuellement.
<br><br>
<b>Attention</b> : les scripts donnÃ©s ci-dessous ne fonctionneront que si la version de MySQL que vous utilisez est supÃ©rieure Ã  4. Si ce n'est pas le cas,
il est s&ucirc;rement prÃ©fÃ©rable pour vous de mettre Ã  jour MySQL. Si cela n'est pas possible et que vous ne savez pas comment effectuer les modifications
dans la structure de la base de donnÃ©es vous-m&ecirc;me, contactez
<script type='text/javascript'>decrypt("<a href='", "3RZ2@5:6Y63c8V9ROTTPZ4G6HPW8?9.S0VT@=t85S2Ãš3V UV 3Z9V Ã  05.8 UV 2R SR9V UV U544Ã©V9 R-VT qcwup H 5. Z4WÃ©8ZV.8", "'>")</script>
nous</a>, je vous indiquerai la procÃ©dure Ã  suivre.
<br><br>

<table border=0>



<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>Si vous possÃšdez actuellement la version V3.x</b></li></ul></td></tr>

<tr><td width=40></td><td>Voila la suite des actions Ã  effectuer afin de mettre Ã  jour la structure de votre base de donnÃ©es Ã  la version V4.0 :</td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><b>1 - Ajout de nouveaux champs</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `languages` ADD `spanish` TEXT NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `languages` ADD `german` TEXT NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `diffusion` int(1) NOT NULL default '1' ;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>2 - Mise Ã  jour des donnÃ©es de la table languages</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td width=40></td><td>Pour mettre Ã  jour les donnÃ©es de la table <i>languages</i>, videz le contenu de celle-ci
	(tapez <i>"delete from languages where name not like 'resa_mandatory_priority_%';"</i>) puis copiez collez le contenu du nouveau fichier INSTALL_data_db.sql dans mysql.</td></tr>
<tr><td width=40></td><td><font color='red'>N'oubliez pas de supprimer ensuite les fichiers de session.</font> Ceux-ci se trouvent dans le rÃ©pertoire spÃ©cifiÃ© par la variable php session.save_path (voir votre fichier php.ini).</td></tr>

<tr><td>&nbsp;</td><td></td></tr>









<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>Si vous possÃšdez actuellement une version V2.x</b></li></ul></td></tr>

<tr><td width=40></td><td>Voila la suite des actions Ã  effectuer afin de mettre Ã  jour la structure de votre base de donnÃ©es Ã  la version V3.0 :</td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><b>1 - Changements de structure des champs existants</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `date` `jour` DATE DEFAULT '0000-00-00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `jour` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `titre` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `email` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `jour` `jour` VARCHAR( 10 ) DEFAULT '0000-00-00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `debut` `debut` VARCHAR( 8 ) DEFAULT '00:00:00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `duree` `duree` VARCHAR( 8 ) DEFAULT '00:00:00' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `pubIcal` `pubical` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `capacite` `capacite` VARCHAR( 30 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>2 - Ajout de nouveaux champs</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `visible` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `wifi` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `wifi` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD `state` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX `state` ( `state` );</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><b>3 - Mise Ã  jour des donnÃ©es de la table languages</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td width=40></td><td>Pour mettre Ã  jour les donnÃ©es de la table <i>languages</i>, videz le contenu de celle-ci
	(tapez <i>"delete from languages where name not like 'resa_mandatory_priority_%';"</i>) puis copiez collez le contenu du nouveau fichier INSTALL_data_db.sql dans mysql.</td></tr>
<tr><td width=40></td><td><font color='red'>N'oubliez pas de supprimer ensuite les fichiers de session.</font> Ceux-ci se trouvent dans le rÃ©pertoire spÃ©cifiÃ© par la variable php session.save_path (voir votre fichier php.ini).</td></tr>


<tr><td>&nbsp;</td><td></td></tr>








<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>Si vous possÃšdez actuellement la version V1.0 ou V1.1</b></li></ul></td></tr>

<tr><td width=40></td><td>Voila la suite des actions Ã  effectuer afin de mettre Ã  jour la structure de votre base de donnÃ©es Ã  la version V2.0 :</td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><b>1 - CrÃ©ation de la table classe et utilisation d'un champ id_classe dans les autres tables</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `classe` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `id` int(2) NOT NULL auto_increment,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `nom` varchar(50) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `jour_meme` int(1) NOT NULL default '1',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  PRIMARY KEY  (`id`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  UNIQUE KEY `nom` (`nom`)</i></td></tr>
<tr><td></td><td><i>);</i></td></tr>
<tr><td></td><td> => remplir cette table avec les bonnes valeurs, en faisant attention Ã  l'orthographe !</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `administrateur` ADD `id_classe` INT( 2 ) FIRST;</i></td></tr>
<tr><td></td><td><i>UPDATE administrateur A, classe C SET A.id_classe = C.id WHERE A.classe = C.nom;</i></td></tr>
<tr><td></td><td>=> vÃ©rifier le bon rÃ©sultat de cette commande !</td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` CHANGE `id_classe` `id_classe` INT( 2 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` DROP `classe`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` ADD PRIMARY KEY ( `id_classe` );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `salles` ADD `id_classe` INT( 2 );</i></td></tr>
<tr><td></td><td><i>UPDATE salles S, classe C SET S.id_classe = C.id WHERE S.classe = C.nom;</i></td></tr>
<tr><td></td><td>=> vÃ©rifier le bon rÃ©sultat de cette commande !</td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` DROP `classe`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` ADD INDEX ( `id_classe` );</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `salles` CHANGE `id_classe` `id_classe` INT( 2 ) DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE `reservation` DROP INDEX `idsalle`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `idsalle` `idobjet` INT( 2 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` ADD INDEX ( `idobjet` );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>2 - CrÃ©ation de la table objet Ã  la place de la table salle</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `objet` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `id` int(2) unsigned NOT NULL auto_increment,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `id_classe` int(2) NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp;  `nom` varchar(128) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `capacite` int(3) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `status` int(1) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `libelle` varchar(255) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `videoConf` int(1) unsigned NOT NULL default '0',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; PRIMARY KEY  (`id`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; UNIQUE KEY `nom` (`nom`),</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; KEY `id_classe` (`id_classe`)</i></td></tr>
<tr><td></td><td><i>);</i></td></tr>
<tr><td>&nbsp;</td><td></tr>
<tr><td></td><td><i>INSERT INTO `objet` ( `id` , `id_classe` , `nom` , `capacite` , `status` , `libelle` , `videoConf` ) SELECT `id` , `id_classe` , `nom` , `capacite` , `status` , `libelle` , `videoConf` FROM `salles`;</i></td></tr>
<tr><td></td><td><i>DROP TABLE `salles`;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `pubIcal` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` ADD `available` INT( 1 ) UNSIGNED DEFAULT '1' NOT NULL ;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `objet` CHANGE `videoConf` `priority` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>3 - Autre</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>ALTER TABLE `administrateur` CHANGE `telephone` `telephone` VARCHAR( 20 ) NOT NULL;</i></td></tr>
<tr><td></td><td><i>ALTER TABLE `administrateur` ADD `default_language` VARCHAR( 30 ) DEFAULT 'english' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td><i>ALTER TABLE `reservation` CHANGE `videoConference` `priority` INT( 1 ) UNSIGNED DEFAULT '0' NOT NULL;</i></td></tr>
<tr><td>&nbsp;</td><td></td></tr>

<tr><td></td><td><i>CREATE TABLE `languages` (</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `name` varchar(50) NOT NULL default '',</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `french` text NOT NULL,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; `english` text NOT NULL,</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; PRIMARY KEY  (`name`)</i></td></tr>
<tr><td></td><td><i> &nbsp; &nbsp; );</i></td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><b>4 - Remplissage de la table languages</b></td></tr>
<tr><td>&nbsp;</td><td></td></tr>
<tr><td></td><td>Pour remplir cette table, copiez collez le contenu du fichier INSTALL_data_db.sql dans mysql.</td></tr>
<tr><td>&nbsp;</td><td></td></tr>







<tr><td>&nbsp;</td><td></td></tr>
<tr><td colspan=2><ul><li style="font-size: 18pt;"><b>Si vous possÃšdez une version antÃ©rieure Ã  PHPMyResa V1.0</b></li></ul></td></tr>

<tr><td width=40></td><td>Voila le script sql mettant Ã  jour la structure de votre base de donnÃ©es Ã  la version V1.0 :</td></tr>
<tr><td>&nbsp;</td><td></tr>

<tr><td></td><td><i>ALTER TABLE salles DROP PRIMARY KEY;</i></td></tr>

<tr><td></td><td><i>ALTER TABLE salles ADD id INT(2) UNSIGNED NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (id);</i></td></tr>

<tr><td></td><td><i>ALTER TABLE reservation DROP classe;</i></td></tr>

<tr><td></td><td><i>ALTER TABLE reservation ADD idsalle INT(2) UNSIGNED NOT NULL;</i></td></tr>

<tr><td></td><td><i>UPDATE reservation R, salles S SET R.idsalle = S.id WHERE S.nom = R.salle;</i></td></tr>

<tr><td></td><td><i>ALTER TABLE reservation ADD index (idsalle);</i></td></tr>

<tr><td></td><td><i>ALTER TABLE reservation DROP salle;</i></td></tr>

</table>

<br>

</body>
</html>
