<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border=1 width='100%'><tr bgcolor='#dddddd'><td><h1>Installation de la version V4.0</h1></td></tr></table>
<br><br>

<ul>
<li><a href='#prerequis'>PrÃ© requis</a>
<li><a href='#bd'>Partie base de donnÃ©es</a>
	<ul>
	<li><a href='INSTALL_creation_db_fr.php'>PremiÃšre installation</a>
	<li><a href='INSTALL_modification_db_fr.php'>Mise Ã  jour</a>
	</ul>
<li><a href='#file'>Partie fichiers de configuration</a>
	<ul>
	<li><a href='#bdfile'>Configuration du fichier commun/database.php</a>
	<li><a href='#configfile'>Configuration du fichier commun/config.php</a>
	</ul>
</ul>
<br><br>

<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='prerequis'>PrÃ© requis</a></h1>
</td></tr>
</table>
<br>
<table><tr><td><h3>SystÃšmes d'exploitation supportÃ©s : </h3></td><td> Linux, Windows</td></tr></table>
<h3>Logiciels Ã  installer au prÃ©alable :</h3>
<ul>
<li>Un serveur web, tous les essais ont Ã©tÃ© effectuÃ©s avec <a href='http://httpd.apache.org' target='_blanck'>apache</a>
<li><a href='http://www.php.net' target='_blanck'>PHP</a> version 4.3 minimum (version 5 si vous utilisez Oracle)
	<br>Remarque : PHPMyResa a Ã©tÃ© Ã©crit de telle faÃ§on Ã  pouvoir laisser la variable PHP register_global Ã  <i>off</i> (vivement conseillÃ©)
<li><a href='http://www.mysql.com' target='_blanck'>MySQL</a>, <a href='http://www.postgresql.org' target='_blanck'>PostgreSQL</a> ou <a href='http://oracle.com' target='_blanck'>Oracle</a>
<li><a href='http://www.zend.com/store/products/zend-optimizer.php' target='_blanck'>Zend Optimizer</a> (recommendÃ©)
</ul>

<h3>Logiciel Ã  installer en fonction de la configuration que vous allez choisir :</h3>
<ul>
<li><a href='http://phpicalendar.sourceforge.net/nuke/' target='_blanck'>iCal</a> - tous les tests ont Ã©tÃ© effectuÃ© avec phpicalendar 1.0
</ul>

<h3>Sur le navigateur des utilisateurs :</h3>
<ul>
<li>Javascript doit &ecirc;tre activÃ©
<li>Les cookies doivent &ecirc;tre permis
</ul>




<br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='bd'>INSTALLATION</a></h1>
</td></tr>
</table>
<br>

L'application PHPMyResa possÃšde une interface d'installation.<br>
Celle-ci devrait se lancer toute seule. Si ce n'est pas le cas, allez dans le rÃ©pertoire resa/config avec un navigateur et laisser vous guider.<br>
La suite de cette page dÃ©crit une installation manuelle de PHPMyResa. Vous devez Ã©galement la suivre en cas de mise Ã  jour si votre version actuelle n'est pas une version 3.x.<br>





<br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='bd'>Partie base de donnÃ©es</a></h1>
</td></tr>
</table>
<br>

Pour commencer vous devez crÃ©er une nouvelle base de donnÃ©es (nommÃ©e resa par exemple). Vous devez ensuite exÃ©cuter le <a href='INSTALL_creation_db_fr.php'>script de crÃ©ation des tables</a> dans votre base de donnÃ©es.<br><br>

<table border=0>
<tr><td><img src='attention.gif' width='25' alt='attention'></td>
<td> &nbsp; </td>
<td>La partie dÃ©crite ci-dessus correspond Ã  une installation initiale. Si vous possÃ©dez dÃ©jÃ  une version du logiciel de rÃ©servation,
cliquez <a href='INSTALL_modification_db_fr.php'>ici</a> pour savoir si vous devez effectuer des modifications de la structure de la base de donnÃ©es.</td></tr>
</table>

<br><br>
Ensuite vous devez saisir votre configuration dans les tables suivantes :
<ul>
<li><b>table classe :</b>
	<ul>
	<li>champ id : l'identifiant de la classe
	<li>champ nom : le nom de la classe d'objets rÃ©servable (ex : <i>'salle'</i>, <i>'vehicule'</i>, <i>'materiel informatique'</i>, <i>'salles de cours'</i>, ...)
	<li>champ jour_meme : ce champ sert Ã  indiquer si une rÃ©servation d'un des objets de cette classe pour le jour mÃªme est garanti ou non. Ce champ est Ã  0 s'il n'y a pas de garantie pour une rÃ©servation le jour mÃªme. Il est Ã  1 par dÃ©faut. Le fait de le mettre Ã  0 n'implique que l'ajout d'un message de prÃ©vention.
	</ul>
<li><b>table objet :</b>
	<ul>
	<li>champ id : l'identifiant de l'objet
	<li>champ id_classe : l'identifiant de la classe Ã  laquelle appartient l'objet
	<li>champ nom : nom de l'objet
	<li>champ capacite : capacitÃ© de l'objet - Ã  ne renseigner que si cela a un sens, par exemple le nombre de places pour une salle ou une voiture
	<li>champ status : a pour valeur 1 si une validation de l'administrateur pour une rÃ©servation de l'objet est nÃ©cessaire, 0 sinon
	<li>champ libelle : nom de l'objet dans l'affiche PDF
	<li>champ priority : a pour valeur 1 si une gestion de prioritÃ© existe pour cet objet (utilisation ou non de vidÃ©o-confÃ©rence par exemple pour une salle)
	<li>champ pubIcal : a pour valeur 1 si les rÃ©servation de cet objet doivent Ãªtre publiÃ©es dans iCal, 0 sinon
	<li>champ available : a pour valeur 0 si l'objet est momentanÃ©ment indisponible, 1 sinon
	<li>champ visible : a pour valeur 0 si l'objet ne doit plus &ecirc;tre visible, 1 sinon
	<li>champ wifi : a pour valeur 1 si un access wifi peut &ecirc;tre associÃ© Ã  une rÃ©servation de cet objet (l'accÃšs wifi ne sera pas implÃ©mentÃ© dans PHPMyResa)
	</ul>
<li><b>table administrateur :</b>
	<ul>
	<li>champ id_classe : identifiant de la classe gÃ©rÃ©e par l'admistrateur
	<li>champ nom : nom de l'administrateur
	<li>champ prenom : prenom de l'administrateur
	<li>champ pass : mot de passe de l'administrateur
	<li>champ mail : adresse mail de l'administrateur
	<li>champ telephone : tÃ©lÃ©phone de l'administrateur
	<li>champ default_language : langue dans laquelle l'administrateur souhaite recevoir les mails lui demandant de valider une rÃ©servation
	</ul>
</ul>

<b>Remarques :</b>
<ul>
<li>Si vous dÃ©sirez modifier certains textes contenus dans l'application, aucun de ceux-ci ne se trouve dans les fichiers php mais dans la table languages de la base de donnÃ©es.
<li><b><font color='red'>TrÃšs important :</font></b> Les donnÃ©es contenues dans la table languages sont chargÃ©es pour chaque utilisateur dans des variables de session, stockÃ©es sur le
serveur web dans des fichiers, un par session. Si vous changez des donnÃ©es dans cette table, afin de rendre vos modifications disponibles, vous devez donc supprimer
manuellement ces fichiers. Ceux-ci se trouvent dans le rÃ©pertoire spÃ©cifiÃ© par la variable php session.save_path (voir votre fichier php.ini).
</ul>




<br><br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='file'>Partie fichiers de configuration</a></h1>
</td></tr>
</table>
<br>

Pour commencer, faites appartenir tous les fichiers Ã  l'utilisateur qui a lancÃ© votre serveur web (commande <i>chown</i> sous Linux). Cette solution est prÃ©fÃ©rable Ã  celle
consistant Ã  changer les droits sur les fichiers (commande <i>chmod</i> sous Linux), car il est judicieux de ne pas donner l'accÃšs en lecture au fichier database.php,
puisque celui-ci contient la cha&icirc;ne de connection Ã  votre base de donnÃ©es.
<br><br>

Deux fichiers doivent ensuite &ecirc;tre configurÃ©s :
<ul>
<li>commun/database.php, qui contient les paramÃštres de connexion Ã  la base de donnÃ©es
<li>commun/config.php, qui contient les paramÃštres de configuration de votre installation, en particulier les fonctionnalitÃ©s que vous souhaitez utiliser ou non
</ul>


<h3><a name='bdfile'>Configuration du fichier commun/database.php :</a></h3>
Toutes les variables prÃ©sentes doivent &ecirc;tre dÃ©finies sinon une erreur surviendra sur le site web (le mot de passe de la base de donnÃ©es, en particulier, ne peut pas
&ecirc;tre vide) :
<ul>
<li>"$database", votre moteur de base de donnÃ©es ('MySQL', PostgreSQL' ou 'Oracle')
<li>"HOST", le nom de la machine hÃ©bergeant la base de donnÃ©es
<li>"USER", le compte accÃ©dant Ã  la base de donnÃ©es
<li>"PWD", le mot de passe de la base de donnÃ©es
<li>"BD", le nom de la base de donnÃ©es
</ul>

<h3><a name='configfile'>Configuration du fichier commun/config.php :</a></h3>

Dans ce fichier, si une valeur n'est pas dÃ©finie, la fonctionnalitÃ© correspondante est automatiquement dÃ©sactivÃ©e.<br><br>

<table border=1>
<tr bgcolor='#cccccc'><td align='center'><b>VARIABLE</b></td><td align='center'><b>OBLIGATOIRE</b></td><td align='center'><b>DEFAUT</b></td></tr>
<tr bgcolor='#dddddd'><td colspan=3><b> &nbsp; &nbsp; Configuration gÃ©nÃ©rale</b></td></tr>
<tr><td align='left'><b>$titre</b>, le titre de l'application, affichÃ© en haut Ã  gauche de la page</td><td align='center'>non</td><td align='center'>PHPMyResa</td></tr>
<tr><td align='left'><b>$bookmark</b>, le titre dans la partie &lt;head&gt; du code html, soit le nom du signet pour la page</td><td align='center'>non</td><td align='center'>PHPMyResa</td></tr>
<tr><td align='left'><b>$image_titre</b>, nom de l'image en haut Ã  gauche de la page <font color='red'>**</font></td><td align='center'>non</td><td align='center'>pas d'image affichÃ©e</td></tr>
<tr><td align='left'><b>$image_pdf</b>, nom de l'image pour la gÃ©nÃ©ration du PDF (format jpg uniquement !) <font color='red'>**</font></td><td align='center'>non</td><td align='center'>pas d'image affichÃ©e</td></tr>
<tr><td align='left'><b>$page_accueil</b>, URL de la page d'accueil (<i>vueMois.php</i>, <i>vide.php</i> ou <i>today.php</i>)</td><td align='center'>non</td><td align='center'>vueMois.php (planning)</td></tr>
<tr><td align='left'><b>$email_domain</b>, domaine de l'adresse email, dans un but d'autocomplÃ©tion (ex : <i>@lpsc.in2p3.fr</i>)</td><td align='center'>non</td><td align='center'>""</td></tr>
<tr><td align='left'><b>$capacite</b>, possibilitÃ© d'afficher une capacitÃ© pour certains objets rÃ©servables (ex : <i>salle</i>, ...)</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$modification_enable</b>, possibilitÃ© pour l'utilisateur de modifier certains champs d'une rÃ©servation</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$default_language</b>, langue de l'application par dÃ©faut si l'utilisateur n'a pas de cookie la spÃ©cifiant</td><td align='center'>non</td><td align='center'>english</td></tr>
<tr><td align='left'><b>$display_tomorrow</b>, possibilitÃ© d'afficher le planning du lendemain</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$several_object_possible</b>, dÃ©finit si plusieurs objets peuvent &ecirc;tre simultanÃ©ment rÃ©servÃ©s</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$week_end_enabled</b>, dÃ©finit si les jours du week-end sont Ã  intÃ©grer dans le systÃšme</td><td align='center'>non</td><td align='center'>true</td></tr>
<tr><td align='left'><b>$tree</b>, dÃ©finit si la prÃ©sentation de la liste des objets est sous forme d'arbre ou non</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$tree_scrollable</b>, si la prÃ©sentation de la liste des objets est sous forme d'arbre, cet arbre est-il scrollable</td><td align='center'>non</td><td align='center'>true</td></tr>
<tr><td align='left'><b>$mail_subject</b>, taille du sujet des mails de crÃ©ation de rÃ©servations (<i>short</i>, <i>medium</i> ou <i>long</i>)</td><td align='center'>non</td><td align='center'>medium</td></tr>
<tr><td align='left'><b>$default_class_for_feed</b>, nom d'une classe par dÃ©faut pour l'affichage du flux rss (pour afficher le flux d'une autre classe, utilisez l'url : http://.../resa/rss.php?class=autre_classe et pour changer de date utilisez le paramÃštre offset)</td><td align='center'>non</td><td align='center'>""</td></tr>
<tr><td align='left'><b>$use_diffusion_flag</b>, laisse t-on Ã  l'utilisateur le choix de diffuser ou non ses rÃ©servations dans le flux rss et dans iCal (si celui-ci est utilisÃ© bien sÃ»r) ?</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$technical_contact</b>, nom du contact technique local</td><td align='center'>oui</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$technical_email</b>, adresse email du contact technique local</td><td align='center'>oui</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$technical_tel</b>, numÃ©ro de tÃ©lÃ©phone du contact technique local</td><td align='center'>oui</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$read_only_enable</b>, possibilitÃ© de restreindre l'accÃšs au site en lecture seule (utile si hors de l'intranet) <font color='red'>***</font></td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$IP_regex</b>, expression rÃ©guliÃšre correspondant aux adresses IP autorisÃ©es (ex : <i>111.222.3[4-5].\d{1,3}</i>)</td><td align='center'>non</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$enabled_domain</b>, tableau contenant la liste des domaines autorisÃ©s (ex : <i>array('in2p3.fr', 'cern.ch')</i>)</td><td align='center'>non</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$mail_server</b>, le nom de votre serveur de messagerie (ex : lpscmail.in2p3.fr) <font color='red'>****</font></td><td align='center'>non</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$web_server</b>, le nom de votre serveur web (ex : lpsc.in2p3.fr) <font color='red'>****</font></td><td align='center'>non</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$default_beginning_hour</b>, valeur par dÃ©faut de l'heure de dÃ©but (h)</td><td align='center'>non</td><td align='center'>8</td></tr>
<tr><td align='left'><b>$default_beginning_mihour</b>, valeur par dÃ©faut de l'heure de dÃ©but (min)</td><td align='center'>non</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_duration</b>, valeur par dÃ©faut de la durÃ©e (h)</td><td align='center'>non</td><td align='center'>1</td></tr>
<tr><td align='left'><b>$default_miduration</b>, valeur par dÃ©faut de la durÃ©e (min)</td><td align='center'>non</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_end_hour</b>, valeur par dÃ©faut de l'heure de fin (h)</td><td align='center'>non</td><td align='center'>18</td></tr>
<tr><td align='left'><b>$default_end_mihour</b>, valeur par dÃ©faut de l'heure de fin (min)</td><td align='center'>non</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_comment</b>, valeur par dÃ©faut du commentaire de la rÃ©servation</td><td align='center'>non</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr bgcolor='#dddddd'><td colspan=3><b> &nbsp; &nbsp; Configuration de logiciels annexes - si nÃ©cessaire</b></td></tr>
<tr><td align='left'><b>$iCal</b>, possibilitÃ© d'utiliser le format iCal</td><td align='center'>non</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$URL_iCal</b>, URL du logiciel PHP iCalendar (ex : <i>http://xxx/phpicalendar-1.0/</i> ou <i>../phpicalendar-1.0/</i>) <font color='red'>*</font></td><td align='center'>oui, si $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_file_location</b>, emplacement des fichiers ical gÃ©nÃ©rÃ©s (ex : <i>/www/htdocs/calendars/</i> ou <i>D:/www/calendars/</i>)	<font color='red'>*</font></td><td align='center'>oui, si $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_file_beginning_of_name</b>, dÃ©but du nom des fichiers gÃ©nÃ©rÃ©s (ex : <i>LPSCResa-</i>)</td><td align='center'>oui, si $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_end_of_uid</b>, fin des UID (identifiant) dans les fichiers gÃ©nÃ©rÃ©s (ex : <i>.LPSCResa.in2p3.fr</i>)</td><td align='center'>oui, si $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$iCal_delay</b>, nombre en semaines passÃ©es Ã  garder dans l'historique des fichiers icalendar</td><td align='center'>non</td><td align='center'>-1</td></tr>
</table>

<table>
<tr><td align='right'><font color='red'>*</font></td><td>NE PAS OUBLIER LE '/' FINAL !</td></tr>
<tr><td align='right'><font color='red'>**</font></td><td>Placer l'image dans le rÃ©pertoire img/ et ne spÃ©cifier que le nom du fichier</td></tr>
<tr><td align='right'><font color='red'>***</font></td><td>Vous ne pouvez utiliser cette fonctionnalitÃ© que si votre hÃ©bergeur vous permet l'utilisation de la fonction php gethostbyaddr()</td></tr>
<tr><td align='right' valign='top'><font color='red'>****</font></td><td>Ã  remplir uniquement si le nom de domaine de vos adresses mail est le nom de votre serveur web, car dans ce cas les mails ne seraient pas correctement envoyÃ©s
	<br>(toute occurence du nom du serveur web dans une adresse mail sera automatiquement replacÃ©e par le nom du serveur de mail)</td></tr>
</table>

<h3>Autre :</h3>
<ul>
<li>Si vous dÃ©sirez utiliser iCal, vous devez tÃ©lÃ©charger le logiciel <a href='http://phpicalendar.sourceforge.net/nuke/' target='_blank'>PHP iCalendar</a>,
l'installer (tar xvzf xxx) et enfin le configurer, en Ã©ditant le fichier config.inc.php. <br>La seule variable Ã  modifier est $calendar_path.
Donnez lui pour valeur celle que vous avez donnÃ© Ã  la variable $CALENDARS_file_location dans le fichier config.php.
<br><br>
<li>Si vous dÃ©sirez utiliser la fonction de crÃ©ation de fichier excel (pour exporter le rÃ©sultat d'une recherche), vous devez installer les packages pear OLE et Spreadsheet_Excel_Writer ('<i>pear install http://pear.php.net/get/OLE</i>' et
'<i>pear install http://pear.php.net/get/Spreadsheet_Excel_Writer</i>')
<br><br>
<li>N'oubliez pas de remplacer l'ic&ocirc;ne favicon.ico du rÃ©pertoire img par l'ic&ocirc;ne de votre choix.
</ul>

</body>
</html>
