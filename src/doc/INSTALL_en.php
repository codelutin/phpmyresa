<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border=1 width='100%'><tr bgcolor='#dddddd'><td><h1>Installation of version V4.0</h1></td></tr></table>
<br><br>

<ul>
<li><a href='#prerequis'>Pre required</a>
<li><a href='#bd'>Database part</a>
	<ul>
	<li><a href='INSTALL_creation_db_en.php'>First installation</a>
	<li><a href='INSTALL_modification_db_en.php'>Update</a>
	</ul>
<li><a href='#file'>Configuration file part</a>
	<ul>
	<li><a href='#bdfile'>Configuration of the file commun/database.php</a>
	<li><a href='#configfile'>Configuration of the file commun/config.php</a>
	</ul>
</ul>
<br><br>

<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='prerequis'>Pre required</a></h1>
</td></tr>
</table>
<br>
<table><tr><td><h3>Supported OS: </h3></td><td> Linux, Windows</td></tr></table>
<h3>Mandatory pre-installed software:</h3>
<ul>
<li>An http server, all tests have been performed with <a href='http://httpd.apache.org' target='_blanck'>apache</a>
<li><a href='http://www.php.net' target='_blanck'>PHP</a> at least version 4.3 (version 5 if you use Oracle)
	<br>Remark: PHPMyResa has been written in such a way that the PHP variable register_global can be set to <i>off</i> (highly recommended)
<li><a href='http://www.mysql.com' target='_blanck'>MySQL</a>, <a href='http://www.postgresql.org' target='_blanck'>PostgreSQL</a> or <a href='http://oracle.com' target='_blanck'>Oracle</a>
<li><a href='http://www.zend.com/store/products/zend-optimizer.php' target='_blanck'>Zend Optimizer</a> (recommended)
</ul>

<h3>Optional software to install, depending on your configuration:</h3>
<ul>
<li><a href='http://phpicalendar.sourceforge.net/nuke/' target='_blanck'>iCal</a> - all tests have been done with phpicalendar 1.0
</ul>

<h3>On users browser:</h3>
<ul>
<li>Javascript must be activated
<li>Cookies must be enabled
</ul>




<br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='bd'>INSTALLATION</a></h1>
</td></tr>
</table>
<br>

PHPMyResa is provided with an installation interface.<br>
If it is not automatically launched, browse the resa/config folder and follow the instructions.<br>
The rest of this web page describes a manual installation of PHPMyResa. You must follow it also if you upgrade from a version prior to 3.x.






<br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='bd'>Database part</a></h1>
</td></tr>
</table>
<br>

To begin, you have to create a new database (called resa for instance). Then you have to execute the <a href='INSTALL_creation_db_en.php'>table creation script</a> in your database.<br><br>

<table border=0>
<tr><td><img src='attention.gif' width='25' alt='be careful'></td>
<td> &nbsp; </td>
<td>The above part describes an initial installation. If you already have a previous version of the reservation software, go <a href='INSTALL_modification_db_en.php'>here</a>
 to know if you have some database structure modifications to make.</td></tr>
</table>

<br><br>
Then you must fill in your configuration in the following tables:
<ul>
<li><b>table classe:</b>
	<ul>
	<li>field id: the class identifier
	<li>field nom: the class name of the reservable objects (ex: <i>'room'</i>, <i>'vehicule'</i>,  ...)
	<li>field jour_meme: this field indicates if a reservation of an object of this class the current date is guaranteed or not. This field is set to 0 if there is no guarantee for a reservation on the current date. It is set to 1 by default. To set it to 0 only involves a warning.
	</ul>
<li><b>table objet:</b>
	<ul>
	<li>field id: the object's identifier
	<li>field id_classe: the class' identifier the object beyong to
	<li>field nom: the object's name
	<li>field capacite: the capacity of the object - to fill in only if applicable, the number of places for a room or a vehicule for example
	<li>field status: set to 1 if an administrator's validation is needed for the object, 0 otherwise
	<li>field libelle: the object's name in the PDF generated document
	<li>field priority: set to 1 if a priority management exists for the object (use of a video conference for a room for example)
	<li>field pubIcal: set to 1 if the reservation of the object must be published in iCal, 0 otherwise
	<li>field available: set to 0 if the object is currently unavailable, 1 otherwise
	<li>field visible: set to 0 if the object does not need to be visible anymore, 1 otherwise
	<li>field wifi: set to 1 if a wifi access can be associated with a reservation of this object (the wifi access is not implemented in PHPMyResa)
	</ul>
<li><b>table administrateur:</b>
	<ul>
	<li>field id_classe: the identifier of the administered class
	<li>field nom: administrator's lastname
	<li>field prenom: administrator's firstname
	<li>field pass: administrator's password
	<li>field mail: administrator's email address
	<li>field telephone: administrator's telephone number
	<li>field default_language: language used in the email for the administrator when requesting him to validate a reservation
	</ul>
</ul>

<b>Remarks:</b>
<ul>
<li>If you want to modify any of the text messages contained in the application, you will not find them in the php files, but in the languages table of the database.
<li><b><font color='red'>Very important:</font></b> Data contained in the table languages are loaded for each user in the session variables and stored in files on the web server, one file per session.
  If the data is changed in this table, you must delete these files manually. They are to be found in the directory specified by the php variable session.save_path (see your php.ini file).
</ul>



<br><br><br><br><br>
<table border=1 width='40%'>
<tr><td align='left'>
<h1>&nbsp; &nbsp;<a name='file'>Configuration file part</a></h1>
</td></tr>
</table>
<br>

To begin, change the ownership of all the files to the user who launched your web server (command <i>chown</i> on Linux). This solution is better than
changing the files rights (command <i>chmod</i> on Linux), because it is judicious to forbid read access on the database.php file, as it contains your database connexion string.
<br><br>

Two files need then to be changed:
<ul>
<li>commun/database.php, which configures the database connexion
<li>commun/config.php, which configures the application features
</ul>


<h3><a name='bdfile'>Configuration of the file commun/database.php:</a></h3>
All data must be defined otherwise an error message will be displayed (the database password, in particular, cannot be empty):
<ul>
<li>"$database", your database engine (must be set to 'MySQL', 'PostgreSQL' or 'Oracle')
<li>"HOST", the database machine name
<li>"USER", the database user name
<li>"PWD", the database password
<li>"BD", the database name
</ul>

<h3><a name='configfile'>Configuration of the file commun/config.php:</a></h3>

In this file, if a value is not set, the feature is disabled.<br><br>

<table border=1>
<tr bgcolor='#cccccc'><td align='center'><b>VARIABLE</b></td><td align='center'><b>MANDATORY</b></td><td align='center'><b>DEFAULT</b></td></tr>
<tr bgcolor='#dddddd'><td colspan=3><b> &nbsp; &nbsp; General configuration</b></td></tr>
<tr><td align='left'><b>$titre</b>, the application title, displayed at the top of the left frame</td><td align='center'>no</td><td align='center'>PHPMyResa</td></tr>
<tr><td align='left'><b>$bookmark</b>, the title in the &lt;head&gt; part of the html code, so the name of the associated bookmark</td><td align='center'>no</td><td align='center'>PHPMyResa</td></tr>
<tr><td align='left'><b>$image_titre</b>, name of the image in the top of the left frame <font color='red'>**</font></td><td align='center'>no</td><td align='center'>no image displayed</td></tr>
<tr><td align='left'><b>$image_pdf</b>, name of the image for the PDF generation (jpg format only!) <font color='red'>**</font></td><td align='center'>no</td><td align='center'>no image displayed</td></tr>
<tr><td align='left'><b>$page_accueil</b>, URL of the welcome page (<i>vueMois.php</i>, <i>vide.php</i> or <i>today.php</i>)</td><td align='center'>no</td><td align='center'>vueMois.php (planning)</td></tr>
<tr><td align='left'><b>$email_domain</b>, domain of the email address, for autocompletion (ex: <i>@lpsc.in2p3.fr</i>)</td><td align='center'>no</td><td align='center'>""</td></tr>
<tr><td align='left'><b>$capacite</b>, possibility to display the capacity of a room or a car</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$modification_enable</b>, possibility to change some fields of an existing reservation</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$default_language</b>, default application language if the user has no cookie for it</td><td align='center'>no</td><td align='center'>english</td></tr>
<tr><td align='left'><b>$display_tomorrow</b>, possibility to display the tomorrow planning</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$several_object_possible</b>, defines if several objects can be booked together</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$week_end_enabled</b>, defines if weekend days are to be taken in account by the system</td><td align='center'>no</td><td align='center'>true</td></tr>
<tr><td align='left'><b>$tree</b>, defines if the presentation of the list of objets is displayed as a tree or not</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$tree_scrollable</b>, if the presentation of the list of objets is displayed as a tree, is it scrollable</td><td align='center'>no</td><td align='center'>true</td></tr>
<tr><td align='left'><b>$mail_subject</b>, length of the subject of the mail send during the creation of reservations (<i>short</i>, <i>medium</i> or <i>long</i>)</td><td align='center'>no</td><td align='center'>medium</td></tr>
<tr><td align='left'><b>$default_class_for_feed</b>, default class name for the display of the rss feed (to display the feed for another class, use the url: http://.../resa/rss.php?class=another_class and to change the date use the offset parameter)</td><td align='center'>no</td><td align='center'>""</td></tr>
<tr><td align='left'><b>$use_diffusion_flag</b>, has the user the choice to disable or not its reservation from rss feed and iCal (if used of course)?</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$technical_contact</b>, name of the local technical contact</td><td align='center'>yes</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$technical_email</b>, email address of the local technical contact</td><td align='center'>yes</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$technical_tel</b>, phone number of the local technical contact</td><td align='center'>yes</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$read_only_enable</b>, possibility to restric the site access with read only right (useful out of intranet) <font color='red'>***</font></td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$IP_regex</b>, regular expression for authorized IP address (ex: <i>111.222.3[4-5].\d{1,3}</i>)</td><td align='center'>no</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$enabled_domain</b>, array which lists all authorized domains (ex: <i>array('in2p3.fr', 'cern.ch')</i>)</td><td align='center'>no</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$mail_server</b>, the name of your mail server (ex: <i>lpscmail.in2p3.fr</i>) <font color='red'>****</font></td><td align='center'>no</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$web_server</b>, the name of your web server (ex: <i>lpsc.in2p3.fr</i>) <font color='red'>****</font></td><td align='center'>no</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr><td align='left'><b>$default_beginning_hour</b>, default value for the begin time (h)</td><td align='center'>no</td><td align='center'>8</td></tr>
<tr><td align='left'><b>$default_beginning_mihour</b>, default value for the begin time (min)</td><td align='center'>no</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_duration</b>, default value for the duration (h)</td><td align='center'>no</td><td align='center'>1</td></tr>
<tr><td align='left'><b>$default_miduration</b>, default value for the duration (min)</td><td align='center'>no</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_end_hour</b>, default value for the end time (h)</td><td align='center'>no</td><td align='center'>18</td></tr>
<tr><td align='left'><b>$default_end_mihour</b>, default value for the end time (min)</td><td align='center'>no</td><td align='center'>0</td></tr>
<tr><td align='left'><b>$default_comment</b>, default value for the reservation comment</td><td align='center'>no</td><td align='center'>-</td></tr>
<tr><td colspan=3>&nbsp;</td></tr>
<tr bgcolor='#dddddd'><td colspan=3><b> &nbsp; &nbsp; Configuration of other softwares - if needed</b></td></tr>
<tr><td align='left'><b>$iCal</b>, possibility to use the iCal format</td><td align='center'>no</td><td align='center'>false</td></tr>
<tr><td align='left'><b>$URL_iCal</b>, URL of the PHP iCalendar software (ex: <i>http://xxx/phpicalendar-1.0/</i> or <i>../phpicalendar-1.0/</i>) <font color='red'>*</font></td><td align='center'>yes, if $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_file_location</b>, location of the generated files (ex: <i>/www/htdocs/calendars/</i> or <i>D:/www/calendars/</i>) <font color='red'>*</font></td><td align='center'>yes, if $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_file_beginning_of_name</b>, beginnig of the name of the generated files (ex: <i>LPSCResa-</i>)</td><td align='center'>yes, if $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$CALENDARS_end_of_uid</b>, end of UID in the generated files (ex: <i>.LPSCResa.in2p3.fr</i>)</td><td align='center'>yes, if $iCal=true</td><td align='center'>-</td></tr>
<tr><td align='left'><b>$iCal_delay</b>, the number of past weeks to keep in the icalendar hustory</td><td align='center'>no</td><td align='center'>-1</td></tr>
</table>

<table>
<tr><td align='right'><font color='red'>*</font></td><td>DO NOT FORGET THE FINAL '/'!</td></tr>
<tr><td align='right'><font color='red'>**</font></td><td>PUT THE IMAGE IN THE img DIRECTORY AND SET THE FILE NAME ONLY</td></tr>
<tr><td align='right'><font color='red'>***</font></td><td>You can use this functionnality only if your provider let you use the php function gethostbyaddr()</td></tr>
<tr><td align='right' valign='top'><font color='red'>****</font></td><td>to fill in only if your email domain address is the name of your web server, because in this case emails would never be sent
    <br>(every occurence of the name of the web server in an email address will be replaced by the name of the mail server)</td></tr>
</table>

<h3>Other:</h3>
<ul>
<li>If you want to use iCal, you must download the <a href='http://phpicalendar.sourceforge.net/nuke/' target='_blank'>PHP iCalendar</a> software,
then install it (tar xvzf xxx) and configure it, by editing the config.inc.php file. <br>You will have to set at least the $calendar_path variable.
Its value must be set to the value of the $CALENDARS_file_location variable from the config.php file.
<br><br>
<li>If you want to use the Excel generation (in order to export the result of a search), you must install the OLE and Spreadsheet_Excel_Writer pear package ('<i>pear install http://pear.php.net/get/OLE</i>' and
'<i>pear install http://pear.php.net/get/Spreadsheet_Excel_Writer</i>').
<br><br>
<li>Do not forget to replace the img/favicon.ico icon by your own icon.
</ul>

</body>
</html>
