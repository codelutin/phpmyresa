<?php

//-------------------------------------------------------------------------------------------------
//-----					Mysql							---
//-------------------------------------------------------------------------------------------------
$mysql = "
CREATE TABLE classe (<br>
 &nbsp; &nbsp;  id int(2) unsigned NOT NULL auto_increment,<br>
 &nbsp; &nbsp;  nom varchar(50) NOT NULL,<br>
 &nbsp; &nbsp;  jour_meme int(1) unsigned NOT NULL default '1',<br>
 &nbsp; &nbsp;  PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  UNIQUE KEY nom (nom)<br>
);<br>
<br><br>

CREATE TABLE administrateur (<br>
 &nbsp; &nbsp;  id_classe int(2) unsigned NOT NULL,<br>
 &nbsp; &nbsp;  nom varchar(40) NOT NULL,<br>
 &nbsp; &nbsp;  prenom varchar(40) NOT NULL,<br>
 &nbsp; &nbsp;  pass varchar(32) NOT NULL,<br>
 &nbsp; &nbsp;  mail varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  telephone varchar(20) NOT NULL,<br>
 &nbsp; &nbsp;  default_language varchar(30) NOT NULL default 'english',<br>
 &nbsp; &nbsp;  PRIMARY KEY (id_classe)<br>
);<br>
<br><br>

CREATE TABLE objet (<br>
 &nbsp; &nbsp;  id int(2) unsigned NOT NULL auto_increment,<br>
 &nbsp; &nbsp;  id_classe int(2) unsigned NOT NULL,<br>
 &nbsp; &nbsp;  nom varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  capacite varchar(30) NOT NULL default '0',<br>
 &nbsp; &nbsp;  status int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  libelle varchar(255) NOT NULL,<br>
 &nbsp; &nbsp;  priority int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  pubical int(1) unsigned NOT NULL default '1',<br>
 &nbsp; &nbsp;  available int(1) unsigned NOT NULL default '1',<br>
 &nbsp; &nbsp;  visible int(1) unsigned NOT NULL default '1',<br>
 &nbsp; &nbsp;  wifi int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  UNIQUE KEY nom (nom),<br>
 &nbsp; &nbsp;  KEY id_classe (id_classe)<br>
);<br>
<br><br>

CREATE TABLE reservation (<br>
 &nbsp; &nbsp;  id int(16) unsigned NOT NULL auto_increment,<br>
 &nbsp; &nbsp;  idmulti int(16) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  idobjet int(2) unsigned NOT NULL,<br>
 &nbsp; &nbsp;  titre varchar(64) NOT NULL,<br>
 &nbsp; &nbsp;  jour varchar(10) NOT NULL,<br>
 &nbsp; &nbsp;  debut varchar(8) NOT NULL,<br>
 &nbsp; &nbsp;  duree varchar(8) NOT NULL,<br>
 &nbsp; &nbsp;  email varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  commentaire varchar(255) NOT NULL,<br>
 &nbsp; &nbsp;  pass varchar(20) NOT NULL,<br>
 &nbsp; &nbsp;  valide int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  priority int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  wifi int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  state int(1) unsigned NOT NULL default '0',<br>
 &nbsp; &nbsp;  diffusion int(1) unsigned NOT NULL default '1',<br>
 &nbsp; &nbsp;  PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  KEY idmulti (idmulti),<br>
 &nbsp; &nbsp;  KEY idobjet (idobjet),<br>
 &nbsp; &nbsp;  KEY jour (jour),<br>
 &nbsp; &nbsp;  KEY titre (titre),<br>
 &nbsp; &nbsp;  KEY email (email),<br>
 &nbsp; &nbsp;  KEY state (state)<br>
);<br>
<br><br>

CREATE TABLE languages (<br>
 &nbsp; &nbsp;  name varchar(50) NOT NULL,<br>
 &nbsp; &nbsp;  french text NOT NULL,<br>
 &nbsp; &nbsp;  english text NOT NULL,<br>
 &nbsp; &nbsp;  spanish text NOT NULL,<br>
 &nbsp; &nbsp;  german text NOT NULL,<br>
 &nbsp; &nbsp;  PRIMARY KEY (name)<br>
);<br>
<br><br>

";

//-------------------------------------------------------------------------------------------------
//-----					Postgresql						---
//-------------------------------------------------------------------------------------------------
$postgresql = "
CREATE TABLE classe (<br>
 &nbsp; &nbsp;  id serial NOT NULL,<br>
 &nbsp; &nbsp;  nom varchar(50) NOT NULL,<br>
 &nbsp; &nbsp;  jour_meme numeric(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_classe_id PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  CONSTRAINT un_classe_nom UNIQUE (nom)<br>
);<br>
<br><br>

CREATE TABLE administrateur (<br>
 &nbsp; &nbsp;  id_classe numeric(2) NOT NULL,<br>
 &nbsp; &nbsp;  nom varchar(40) NOT NULL,<br>
 &nbsp; &nbsp;  prenom varchar(40) NOT NULL,<br>
 &nbsp; &nbsp;  pass varchar(32) NOT NULL,<br>
 &nbsp; &nbsp;  mail varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  telephone varchar(20) NOT NULL,<br>
 &nbsp; &nbsp;  default_language varchar(30) DEFAULT 'english' NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_administrateur_id_classe PRIMARY KEY (id_classe),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_administrateur_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)<br>
);<br>
<br><br>

CREATE TABLE objet (<br>
 &nbsp; &nbsp;  id serial NOT NULL,<br>
 &nbsp; &nbsp;  id_classe numeric(2) NOT NULL,<br>
 &nbsp; &nbsp;  nom varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  capacite varchar(30) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  status numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  libelle varchar(255) NOT NULL,<br>
 &nbsp; &nbsp;  priority numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  pubical numeric(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  available numeric(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  visible numeric(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  wifi numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_objet_id PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  CONSTRAINT un_objet_nom UNIQUE (nom),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_objet_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)<br>
);<br>
CREATE INDEX in_objet_id_classe ON objet (id_classe);<br>
<br><br>

CREATE TABLE reservation (<br>
 &nbsp; &nbsp;  id serial NOT NULL,<br>
 &nbsp; &nbsp;  idmulti numeric(16) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  idobjet numeric(2) NOT NULL,<br>
 &nbsp; &nbsp;  titre varchar(64) NOT NULL,<br>
 &nbsp; &nbsp;  jour varchar(10) NOT NULL,<br>
 &nbsp; &nbsp;  debut varchar(8) NOT NULL,<br>
 &nbsp; &nbsp;  duree varchar(8) NOT NULL,<br>
 &nbsp; &nbsp;  email varchar(128) NOT NULL,<br>
 &nbsp; &nbsp;  commentaire varchar(255) NOT NULL,<br>
 &nbsp; &nbsp;  pass varchar(20) NOT NULL,<br>
 &nbsp; &nbsp;  valide numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  priority numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  wifi numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  state numeric(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  diffusion numeric(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_reservation_id PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_reservation_idobjet FOREIGN KEY (idobjet) REFERENCES objet(id)<br>
);<br>
CREATE INDEX in_reservation_idobjet ON reservation (idobjet);<br>
CREATE INDEX in_reservation_idmulti ON reservation (idmulti);<br>
CREATE INDEX in_reservation_jour ON reservation (jour);<br>
CREATE INDEX in_reservation_titre ON reservation (titre);<br>
CREATE INDEX in_reservation_email ON reservation (email);<br>
CREATE INDEX in_reservation_state ON reservation (state);<br>
<br><br>

CREATE TABLE languages (<br>
 &nbsp; &nbsp;  name varchar(50) NOT NULL,<br>
 &nbsp; &nbsp;  french text NOT NULL,<br>
 &nbsp; &nbsp;  english text NOT NULL,<br>
 &nbsp; &nbsp;  spanish text NOT NULL,<br>
 &nbsp; &nbsp;  german text NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_languages_name PRIMARY KEY (name)<br>
);<br>
<br><br>

";

//-------------------------------------------------------------------------------------------------
//-----					Oracle							---
//-------------------------------------------------------------------------------------------------
$oracle = "
CREATE TABLE classe(<br>
 &nbsp; &nbsp;  id NUMBER(2) NOT NULL,<br>
 &nbsp; &nbsp;  nom VARCHAR2(50) NOT NULL,<br>
 &nbsp; &nbsp;  jour_meme NUMBER(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_classe_id PRIMARY KEY(id),<br>
 &nbsp; &nbsp;  CONSTRAINT un_classe_nom UNIQUE(nom)<br>
);<br>
<br><br>

CREATE TABLE administrateur(<br>
 &nbsp; &nbsp;  id_classe NUMBER(2) NOT NULL,<br>
 &nbsp; &nbsp;  nom VARCHAR2(40) NOT NULL,<br>
 &nbsp; &nbsp;  prenom VARCHAR2(40) NOT NULL,<br>
 &nbsp; &nbsp;  pass VARCHAR2(32) NOT NULL,<br>
 &nbsp; &nbsp;  mail VARCHAR2(128) NOT NULL,<br>
 &nbsp; &nbsp;  telephone VARCHAR2(20) NOT NULL,<br>
 &nbsp; &nbsp;  default_language VARCHAR2(30) DEFAULT 'english' NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_administrateur_id_classe PRIMARY KEY (id_classe),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_administrateur_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)<br>
);<br>
<br><br>

CREATE TABLE objet(<br>
 &nbsp; &nbsp;  id NUMBER(2) NOT NULL,<br>
 &nbsp; &nbsp;  id_classe  NUMBER(2) NOT NULL,<br>
 &nbsp; &nbsp;  nom VARCHAR2(128) NOT NULL,<br>
 &nbsp; &nbsp;  capacite VARCHAR2(30) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  status NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  libelle VARCHAR2(255) NOT NULL,<br>
 &nbsp; &nbsp;  priority NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  pubical NUMBER(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  available NUMBER(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  visible NUMBER(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  wifi NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_objet_id PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  CONSTRAINT un_objet_nom UNIQUE (nom),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_objet_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)<br>
);<br>
CREATE INDEX in_objet_id_classe ON objet (id_classe);<br>
<br><br>

CREATE TABLE reservation(<br>
 &nbsp; &nbsp;  id NUMBER(16) NOT NULL,<br>
 &nbsp; &nbsp;  idmulti NUMBER(16) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  idobjet NUMBER(2) NOT NULL,<br>
 &nbsp; &nbsp;  titre VARCHAR2(64) NOT NULL,<br>
 &nbsp; &nbsp;  jour VARCHAR2(10) NOT NULL,<br>
 &nbsp; &nbsp;  debut VARCHAR2(8) NOT NULL,<br>
 &nbsp; &nbsp;  duree VARCHAR2(8) NOT NULL,<br>
 &nbsp; &nbsp;  email VARCHAR2(128) NOT NULL,<br>
 &nbsp; &nbsp;  commentaire VARCHAR2(255),<br>
 &nbsp; &nbsp;  pass VARCHAR2(20) NOT NULL,<br>
 &nbsp; &nbsp;  valide NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  priority NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  wifi NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  state NUMBER(1) DEFAULT 0 NOT NULL,<br>
 &nbsp; &nbsp;  diffusion NUMBER(1) DEFAULT 1 NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_reservation_id PRIMARY KEY (id),<br>
 &nbsp; &nbsp;  CONSTRAINT fk_reservation_idobjet FOREIGN KEY (idobjet) REFERENCES objet(id)<br>
);<br>
CREATE INDEX in_reservation_idobjet ON reservation (idobjet);<br>
CREATE INDEX in_reservation_idmulti ON reservation (idmulti);<br>
CREATE INDEX in_reservation_jour ON reservation (jour);<br>
CREATE INDEX in_reservation_titre ON reservation (titre);<br>
CREATE INDEX in_reservation_email ON reservation (email);<br>
CREATE INDEX in_reservation_state ON reservation (state);<br>
<br><br>

CREATE TABLE languages(<br>
 &nbsp; &nbsp;  name VARCHAR2(50) NOT NULL,<br>
 &nbsp; &nbsp;  french VARCHAR2(2000) NOT NULL,<br>
 &nbsp; &nbsp;  english VARCHAR2(2000) NOT NULL,<br>
 &nbsp; &nbsp;  spanish VARCHAR2(2000) NOT NULL,<br>
 &nbsp; &nbsp;  german VARCHAR2(2000) NOT NULL,<br>
 &nbsp; &nbsp;  CONSTRAINT pk_languages_name PRIMARY KEY (name)<br>
);<br>
<br><br>

CREATE SEQUENCE reservation_id_seq<br>
 &nbsp; &nbsp;  start with 1<br>
 &nbsp; &nbsp;  increment by 1<br>
 &nbsp; &nbsp;  nomaxvalue;<br>
<br><br>

CREATE OR REPLACE TRIGGER reservation_id_trigger<br>
 &nbsp; &nbsp;  before insert on reservation<br>
 &nbsp; &nbsp;  for each row<br>
 &nbsp; &nbsp;  begin<br>
 &nbsp; &nbsp;  select reservation_id_seq.nextval into :new.id from dual;<br>
 &nbsp; &nbsp;  end;<br>
<br><br>

CREATE SEQUENCE classe_id_seq<br>
 &nbsp; &nbsp;  start with 1<br>
 &nbsp; &nbsp;  increment by 1<br>
 &nbsp; &nbsp;  nomaxvalue;<br>
<br><br>

CREATE OR REPLACE TRIGGER classe_id_trigger<br>
 &nbsp; &nbsp;  before insert on classe<br>
 &nbsp; &nbsp;  for each row<br>
 &nbsp; &nbsp;  begin<br>
 &nbsp; &nbsp;  select classe_id_seq.nextval into :new.id from dual;<br>
 &nbsp; &nbsp;  end;<br>
<br><br>

CREATE SEQUENCE object_id_seq<br>
 &nbsp; &nbsp;  start with 1<br>
 &nbsp; &nbsp;  increment by 1<br>
 &nbsp; &nbsp;  nomaxvalue;<br>
<br><br>

CREATE OR REPLACE TRIGGER object_id_trigger<br>
 &nbsp; &nbsp;  before insert on objet<br>
 &nbsp; &nbsp;  for each row<br>
 &nbsp; &nbsp;  begin<br>
 &nbsp; &nbsp;  select object_id_seq.nextval into :new.id from dual;<br>
 &nbsp; &nbsp;  end;<br>

";
?>
