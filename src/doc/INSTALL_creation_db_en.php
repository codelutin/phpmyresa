<?php require_once('INSTALL_creation_db_script.php');?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>phpMyResa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="copyright" content="CNRS-IN2P3">
<meta name="author" content="Charnay, Melot, Warin @in2p3.fr">
<meta name="keywords" content="reservation, calendrier, agenda, ical, ressources, reunion, 'salles de reunion', vehicule, palm">
<LINK rel=stylesheet type='text/css' href='style.css'>
<link REL="SHORTCUT ICON" href="/favicon.ico">
<style type="text/css">
all.menu {text-decoration: none; color: #FFFFFF;}
*.menu {text-decoration: none; color: #FFFFFF;}
all.sites {text-decoration: none; color: #444444;}
*.sites {text-decoration: none; color: #444444;}
all.liens {text-decoration: none; color: #777777;}
*.liens{text-decoration: none; color: #777777;}
</style>
</head>

<body text="#000000" bgcolor="#f6f6f5" link="#777777" vlink="#777777" alink="#999944">

<table border='1' width='100%'><tr bgcolor='#dddddd'><td><h1>Installation of version V4.0 - Creation of the database structure</h1></td></tr></table>
<br><br>

<h3>Database physical model (MySQL)</h3>
<img src='MPD.gif' width='100%' alt='physical model'>

<br><br><br><br><br>
<h3>Database table creation script</h3>

<table border=2 frame=void rules=all>
<tr align='center'><td><b>MySQL</b></td><td><b>PostgreSQL</b></td><td><b>Oracle</b></td></tr>
<tr valign='top'><td width='33%'>
<?php echo $mysql;?>
</td>
<td width=33%>
<?php echo $postgresql;?>
</td>
<td width=33%>
<?php echo $oracle;?>
</td></tr>
</table>

<br><br><br>
<h3>Database table data insert script</h3>

In order to fill the languages table, copy and paste the content of the doc/INSTALL_data_db.sql file in your database.<br>
If you use Oracle, do not forget <i>commit</i> afterward.
<br><br>

</body>
</html>
