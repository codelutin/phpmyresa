<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/database_checks.php
*
* This file performs all possible checks on the database content
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************



/************************************************************************************************************************
*															*
*															*
*				1st part: test on existence of database	table and a minimum of data			*
*															*
*															*
*************************************************************************************************************************/

/* -------------------------------------------------------------------------------
		Tables classe, administrateur, objet and reservation exist?
------------------------------------------------------------------------------- */

$trouveClasse = false;
$trouveAdministrateur = false;
$trouveObjet = false;
$trouveReservation = false;
if ($database == "MySQL"){
	$DB_request = "SHOW TABLES";
	$result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	for ($i = 0; $i < mysql_num_rows($result); $i++){
	    if (mysql_tablename($result, $i) == "classe") $trouveClasse = true;
	    if (mysql_tablename($result, $i) == "administrateur") $trouveAdministrateur = true;
	    if (mysql_tablename($result, $i) == "objet") $trouveObjet = true;
	    if (mysql_tablename($result, $i) == "reservation") $trouveReservation = true;
	}
} else if ($database == "PostgreSQL"){
	$DB_request = "select table_name from information_schema.tables where table_type='BASE TABLE' and table_name not like 'pg_%' and table_name not like 'sql_%'";
	$result = @pg_query($connexionDB, $DB_request) or errorDB($DB_request, false);
	while ($field = pg_fetch_object($result)){
	    $fieldName = $field->table_name;
	    if ($fieldName == "classe") $trouveClasse = true;
	    if ($fieldName == "administrateur") $trouveAdministrateur = true;
	    if ($fieldName == "objet") $trouveObjet = true;
	    if ($fieldName == "reservation") $trouveReservation = true;
	}
} else if ($database == "Oracle"){
	$DB_request = "SELECT table_name FROM user_tables";
	$result = oci_parse($connexionDB, $DB_request) or errorDB($DB_request, false);
	@oci_execute($result, OCI_COMMIT_ON_SUCCESS) or errorDB($DB_request, false);
	while ($field = oci_fetch_object($result)){
	    $fieldName = $field->TABLE_NAME;
	    if ($fieldName == "CLASSE") $trouveClasse = true;
	    if ($fieldName == "ADMINISTRATEUR") $trouveAdministrateur = true;
	    if ($fieldName == "OBJET") $trouveObjet = true;
	    if ($fieldName == "RESERVATION") $trouveReservation = true;
	}
}
if ( ! $trouveClasse) exitMsg(sprintf($_SESSION['s_language']['init_config_table_not_exists_1'], 'classe')." ".$_SESSION['s_language']['init_config_table_not_exists_2']);
if ( !$trouveAdministrateur) exitMsg(sprintf($_SESSION['s_language']['init_config_table_not_exists_1'], 'administrateur')." ".$_SESSION['s_language']['init_config_table_not_exists_2']);
if ( ! $trouveObjet) exitMsg(sprintf($_SESSION['s_language']['init_config_table_not_exists_1'], 'objet')." ".$_SESSION['s_language']['init_config_table_not_exists_2']);
if ( ! $trouveReservation) exitMsg(sprintf($_SESSION['s_language']['init_config_table_not_exists_1'], 'reservation')." ".$_SESSION['s_language']['init_config_table_not_exists_2']);

/* -------------------------------------------------------------------------------
		Tables classe, admnistrateur and objet have value?
------------------------------------------------------------------------------- */
$DB_request = "SELECT count(*) AS nb FROM classe";
$result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$resultat = database_fetch_object($result);
if (database_get_from_object($resultat, 'nb') == 0){
	Header("Location:./config/");
	exit();
}

$DB_request = "SELECT count(*) AS nb FROM administrateur";
$result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$resultat = database_fetch_object($result);
if (database_get_from_object($resultat, 'nb') == 0){
	Header("Location:./config/");
	exit();
}

$DB_request = "SELECT count(*) AS nb FROM objet";
$result = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$resultat = database_fetch_object($result);
if (database_get_from_object($resultat, 'nb') == 0){
	Header("Location:./config/");
	exit();
}


/************************************************************************************************************************
*															*
*															*
*				2nd part: test on consistency of the database tables data				*
*															*
*															*
*************************************************************************************************************************/

$tabLanguage = getAvailableLanguages();
$message = "";

/* -------------------------------------------------------------------------------
	Add some consistency tests on the database
		table administrateur: has default_language correct values?
		table administrateur: field 'pass' can not be empty
		table administrateur: each administrator (distinction on the email) must have a distinct password
		table administrateur: has id_classe correct values? (only MySQL)
		table objet: has id_classe correct values? (only MySQL)
		table reservation: has id_objet correct values? (only MySQL)
		table classe: each classe must have an administrator
		table objet : cannot contain an object with the character ","
		table objet : cannot contain an object with the character "'"
		table classe: field jour_meme can be set only to 0 or 1
		table objet: fields status, priority, pubiCal, available, visible, wifi can be set only to 0 or 1
------------------------------------------------------------------------------- */

// $list is here the list of available languages
$list = "";
for ($i = 0; $i < count($tabLanguage); $i++) $list .= "'$tabLanguage[$i]', ";
$list = substr($list, 0, -2);
if (empty($list)) $list = "''";

$DB_request = "SELECT nom, prenom, default_language FROM administrateur ";
$DB_request .= "WHERE default_language NOT IN ($list)";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
	$message .= "<li>".sprintf($_SESSION['s_language']['init_config_administrator_wrong_value'], $admin, $default_language, database_get_from_object($row, 'default_language'));
	 $message .= "<br />".sprintf($_SESSION['s_language']['init_config_possible_values'], $list)."</li>\n";
}

$DB_request = "SELECT DISTINCT nom, prenom FROM administrateur WHERE pass = ''";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
	$message .= "<li>".sprintf($_SESSION['s_language']['init_config_administrator_error_password'], $admin)."</li>\n";
}

$DB_request = "SELECT count(*) AS nb FROM administrateur A1, administrateur A2 WHERE A1.pass = A2.pass AND A1.mail <> A2.mail";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$row = database_fetch_object($resultat);
$nb = database_get_from_object($row, 'nb');
if ($nb != 0) $message .= "<li>".$_SESSION['s_language']['init_config_administrator_error_email']."</li>"."\n";

if ($database == "MySQL"){ 	// Les autres bases de donnÃ©es empÃªchent ces inconsistences Ã  l'aide des clÃ©s Ã©trangÃšres

	// $list is here the list of id from the classe table
	$DB_request = "SELECT distinct id FROM classe";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$list = constructionListe($resultat, 'id');

	$DB_request = "SELECT nom, prenom, id_classe FROM administrateur ";
	$DB_request .= "WHERE id_classe NOT IN ($list)";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)){
	    $admin = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
	    $message .= "<li>".sprintf($_SESSION['s_language']['init_config_administrator_wrong_value'], $admin, 'id_classe', database_get_from_object($row, 'id_classe'));
	    $message .= "<br />".sprintf($_SESSION['s_language']['init_config_possible_values'], $list)."</li>\n";
	}

	$DB_request = "SELECT nom, id_classe FROM objet ";
	$DB_request .= "WHERE id_classe NOT IN ($list)";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)){
	    $message .= "<li>".sprintf($_SESSION['s_language']['init_config_object_wrong_value'], database_get_from_object($row, 'nom'), database_get_from_object($row, 'id_classe'));
	    $message .= "<br />".sprintf($_SESSION['s_language']['init_config_possible_values'], $list)."</li>\n";
	}

	// $list is here the list of id from the objet table
	$DB_request = "SELECT distinct id FROM objet";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$list = constructionListe($resultat, 'id');

	$DB_request = "SELECT titre, idobjet FROM reservation ";
	$DB_request .= "WHERE idobjet NOT IN ($list)";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)){
	    $message .= "<li>".sprintf($_SESSION['s_language']['init_config_reservation_wrong_value'], database_get_from_object($row, 'titre'), database_get_from_object($row, 'idobjet'));
	    $message .= "<br />".sprintf($_SESSION['s_language']['init_config_possible_values'], $list)."</li>\n";
	}
}

// $list is here the list of id_classe from the administrateur table
$DB_request = "SELECT distinct id_classe FROM administrateur";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$list = constructionListe($resultat, 'id_classe');

$DB_request = "SELECT nom FROM classe WHERE id NOT IN ($list)";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$message .= "<li>".sprintf($_SESSION['s_language']['init_config_classe_admin'], database_get_from_object($row, 'nom'))."</li>\n";
}

$DB_request = "SELECT nom FROM objet WHERE nom LIKE '%,%'";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$message .= "<li>".sprintf($_SESSION['s_language']['init_config_objet_comma'], database_get_from_object($row, 'nom'))."</li>\n";
}

$DB_request = "SELECT nom FROM objet WHERE nom LIKE '%''%'";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$message .= "<li>".sprintf($_SESSION['s_language']['init_config_objet_quote'], database_get_from_object($row, 'nom'))."</li>\n";
}

$DB_request = "SELECT DISTINCT nom FROM classe WHERE jour_meme NOT IN (0,1)";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$list = replaceLastOccurenceOfComa(constructionListe($resultat, 'nom'), 'and');
if ($list != "''") $message .= "<li>".sprintf($_SESSION['s_language']['init_config_jour_meme'], $list)."</li>\n";

$DB_request = "SELECT DISTINCT nom FROM objet WHERE status NOT IN (0,1) OR priority NOT IN (0,1) OR pubiCal NOT IN (0,1) OR available NOT IN (0,1) OR visible NOT IN (0,1) OR  wifi NOT IN (0,1)";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$list = replaceLastOccurenceOfComa(constructionListe($resultat, 'nom'), 'and');
if ($list != "''") $message .= "<li>".sprintf($_SESSION['s_language']['init_config_fields_objet'], $list)."</li>\n";

displayErrorsIfAny($message, "", sprintf($_SESSION['s_language']['init_config_general_msg3'], './config/database_content.php'));


/* -------------------------------------------------------------------------------
	Gestion des prioritÃ©s (ajout d'un message si non dÃ©fini)
------------------------------------------------------------------------------- */
$DB_request = "SELECT nom FROM objet WHERE priority = 1";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	$nom = database_get_from_object($row, 'nom');
	$messageName = getPriorityMessageName($nom);
	$DB_request = "SELECT name FROM languages WHERE name = '".$messageName."'";
	$res = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	if ( ! database_fetch_object($res)){
		Header("Location:./config/");
		exit();
	}
}
?>
