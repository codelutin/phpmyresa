<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/commun.php
*
* This file is included in almost all other files. It contains the main
* behaviour of PHPMyResa, checks on the correct values you put in the
* config.php file, and the variables and functions used in lots of php files.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


error_reporting(E_ALL);

$temp_array = getWEBurl();
define('WEB', $temp_array[0]);
$isFromSOAP = $temp_array[1];


//*******************************************************************************************************************************
// This part set message header and footer for error messages

$error_message_begin = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n";
$error_message_begin .= "<html>\n";
$error_message_begin .= "<head>\n";
$error_message_begin .= "<meta http-equiv='Content-Type' content='text/html; charset=utf8'>\n";
$error_message_begin .= "<meta name='Author' content='Daniel Charnay, Frederic Melot, Patricia Warin-Charpentier'>\n";
$error_message_begin .= "<meta name='Keywords' content='reservation'>\n";
$error_message_begin .= "<meta name='Description' content='reservation'>\n";
$error_message_begin .= "<title>PHPMyResa Error</title>\n";
$error_message_begin .= "<link rel='SHORTCUT ICON' href='".WEB."img/favicon.ico'>\n";
$error_message_begin .= "<link rel='stylesheet' type='text/css' href='".WEB."commun/resa.css'>\n";
$error_message_begin .= "</head>\n\n";
$error_message_begin .= "<body>\n";
$error_message_begin .= "<p class='red'>";

$body_end = "\n</body>\n\n</html>\n";
$error_message_end = "</p>\n\n".$body_end;
$error_message_db1 = $error_message_begin."Error during an operation on the database. The command is:<br />\"<b>";
$error_message_db2 = "</b>\"</p><p>Please contact your technical contact about this problem.".$error_message_end;
//*******************************************************************************************************************************

require_once('init_database.php');
require_once('commun_all.php');
require_once('init_language.php');	// Inside init_language.php a cookie should be set!

if ( ! isset($_COOKIE[ini_get('session.name')])){
	if (isset($_SERVER['SCRIPT_FILENAME'])) $from = $_SERVER['SCRIPT_FILENAME']; else $from = "index.php";

	$skip_cookie_test = false;
	if (strlen($from) > 7){
		if (substr($from,-7) == 'rss.php') $skip_cookie_test = true;			// the rss.php web page does not verify the use of cookie
	}
	if (strlen($from) > 18){
		if (substr($from,-18) == 'redirectCookie.php') $skip_cookie_test = true;	// the redirectCookie.php web page does not verify the use of cookie
	}
	if (strlen($from) > 20){
		if (substr($from,-20) == 'SOAP/resa_server.php') $skip_cookie_test = true;	// the SOAP/resa_server.php web page does not verify the use of cookie
	}

	if ( ! $skip_cookie_test){
		if (isset($_GET['id'])){
			header("Location:".WEB."redirectCookie.php?from=$from&id=".$_GET['id']);
			exit();
		} else if (isset($_GET['idmulti'])){
			header("Location:".WEB."redirectCookie.php?from=$from&idmulti=".$_GET['idmulti']);
			exit();
		} else{
			header("Location:".WEB."redirectCookie.php?from=$from");
			exit();
		}
	}
}

require_once('init_config.php');

$version = '4.0';

$ie = false;
if (strpos($_SERVER["HTTP_USER_AGENT"], "MSIE")) $ie = true;

$entete_inter = "<html xmlns='http://www.w3.org/1999/xhtml'>\n";
$entete_inter .= "<head>\n";
$entete_inter .= "<meta http-equiv='Content-Type' content='text/html; charset=utf8' />\n";
$entete_inter .= "<link rel='alternate' type='application/rss+xml' title='Canal RSS' href='rss.php' />";
$entete_inter .= "<meta name='Author' content='Daniel Charnay, Frederic Melot, Patricia Warin-Charpentier' />\n";
$entete_inter .= "<meta name='Keywords' content='reservation' />\n";
$entete_inter .= "<meta name='Description' content='reservation' />\n";
$entete_inter .= "<link rel='SHORTCUT ICON' href='".WEB."img/favicon.ico' />\n";
$entete_inter .= "<link rel='stylesheet' type='text/css' href='".WEB."commun/resa.css' />\n";
$entete_inter .= "<title>$bookmark</title>\n";
$entete_inter .= "<script src='".WEB."commun/function.js' type='text/javascript'></script>\n";
$entete_inter .= "</head>\n\n";
$entete_error = $entete_inter."<body>\n<p class='red'>";

$doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";

$entete =  $doctype."\n".$entete_inter;
$entete_frame = $doctype."\n".$entete_inter;

$dateCourante=getdate();
$moisCourant = $dateCourante["mon"];
$anneeCourante = $dateCourante["year"];
$jourCourant = $dateCourante["mday"];

$emailCrypte = PHPcrypt("mailto:".$technical_email."?subject=".$_SESSION['s_language']['help_25']);
$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<a href='\", \"$emailCrypte\", \"'>\")</script>";
$exit_message_authentification = $entete_error.$_SESSION['s_language']['not_allowed1'].' '.sprintf($_SESSION['s_language']['not_allowed2'], $afficheEmailCrypte.$technical_contact."</a>", $technical_tel)."</p>".$body_end;
$exit_message_javascript_authentification = $_SESSION['s_language']['not_allowed1'].' '.sprintf($_SESSION['s_language']['not_allowed2'], $technical_contact, $technical_tel);

function displayDate($date, $langue, $mode){

	list($annee, $mois, $jour) = split ("-", $date);
	if ($mode == 1) $Y = ""; else $Y = " Y";   		// si $mode=1 l'annee n'est pas affichee
	if ($langue == 'french') $newDate = date("D j M".$Y, mktime (0,0,0,$mois,$jour,$annee));
	else if ($langue == 'spanish') $newDate = date("D j \d\e M \d\e\l".$Y, mktime (0,0,0,$mois,$jour,$annee));
	else if ($langue == 'german') $newDate = date("D, j\. M ".$Y, mktime (0,0,0,$mois,$jour,$annee)); 	//Donnerstag, 8. November 2007
	else $newDate = date("D jS \of M".$Y, mktime (0,0,0,$mois,$jour,$annee));

	$avant = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	$apres = array();
	for ($i = 1 ; $i < 13 ; $i++) $apres[$i-1] = $_SESSION['s_language']['mois'.$i];
	$newDate = str_replace($avant, $apres,$newDate);
	$avant = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
	$apres = array();
	for ($i = 1 ; $i < 8 ; $i++) $apres[$i-1] = $_SESSION['s_language']['day'.$i];
	$newDate = str_replace($avant, $apres,$newDate);
	return $newDate;
}

function displayCurrentLanguageDate($date, $mode){
	return displayDate($date, $_SESSION['s_current_language'], $mode);
}

function get_objectValidationExists(){
	global $connexionDB;

	$DB_request = "SELECT count(*) AS res FROM objet WHERE status=1";
	$resultat_objet = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat_objet);
	if (database_get_from_object($row, 'res') == 0) return false; else return true;
}

function get_priorityExists(){
	global $connexionDB;

	$DB_request = "SELECT count(*) AS res FROM objet WHERE priority=1";
	$resultat_objet = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat_objet);
	if (database_get_from_object($row, 'res') == 0) return false; else return true;
}

function calendrier($mois,$annee) {
	global $dateCourante, $week_end_enabled;

	$pm = getdate(mktime(0,0,0,$mois,1,$annee)); 	//On recupere le premier du mois
	$nj = $pm["wday"]; 	//Puis le numero du premier jour dans la semaine exprimee de 0 a 6
	if ($nj == 0) $nj -= 7*$_SESSION['s_jour_offset'];
	$maxSem = 5;	//Un mois peut s'afficher sur 6 semaines

	echo "<table border='0' cellspacing='4' cellpadding='1'>\n";
	echo "<tr>\n";
	for ($i=0; $i<7; $i++)
	{
	    echo "<th>".strtoupper(substr($_SESSION['s_les_jours'][$i],0,1))."</th>\n";
	}
	echo "</tr>\n";

	$test=0;
	for($sem=0; $sem<=$maxSem; $sem++){
	    echo "<tr>\n";
	    for($j=1; $j<=7; $j++){
	    $jourDuMois = $sem*7+$j-$_SESSION['s_jour_offset']-$nj;
	    $jj = getdate(mktime(0,0,0,$mois,$jourDuMois,$annee));
	    $leJour = $jj["mday"];
	    if($leJour == 1) {
	        $test++;
	        if ($test>=2) $maxSem--;
	    }
	    $jourDuMoisSuivant = $jourDuMois + 1;
	    $jjSuivant=getdate(mktime(0,0,0,$mois,$jourDuMoisSuivant,$annee));
	    $leJourSuivant=$jjSuivant["mday"];
	    if ( ($leJourSuivant == 1) && ($test == 1) && ($j == 7) ) $maxSem--;
	        if ($test == 1) {
	            if($jj["yday"] == $dateCourante["yday"] && $annee==$dateCourante["year"])
	                echo "<td class='center jaune'>";
	            else
	                echo "<td class='center'>";
	            if ($week_end_enabled)
	                echo "<a class='liens' href='javascript:startResa($leJour);'>".$leJour."</a>";
	            else
	            {
	                if ($_SESSION['s_jour_travail'][$j-1])
	                    echo "<a class='liens' href='javascript:startResa($leJour);'>".$leJour."</a>";
	                else
	                    echo "<span style='background-color:gray'>".$leJour."</span>";
	            }
	        } else
	            echo "<td class='center'>&nbsp;";
	        echo "</td>\n";
	    }
	    echo "</tr>\n";
	}
	echo "</table>\n\n";
}

function getResaContent($resultat, $priorityManagement, $date, $objet){

	global $modification_enable;

	$content = "";
	$compteur = 0;
	$objets = array();
	$couleur = "#dddddd";

	$content .= "\n\n<table class='noborder' cellpadding='3' cellspacing='1'>\n";
	$content .= "<tr class='center' style='background-color:#d3dce3'>\n";
	$content .= "	<td style='background-color:whitesmoke'>&nbsp;</td>\n";
	if ($modification_enable) $content .= "	<td style='background-color:whitesmoke'>&nbsp;</td>\n";
	if ($objet) $content .= "	<td><b>".$_SESSION['s_language']['title_array1']."</b></td>\n";
	if ($date)  $content .= "	<td><b>".$_SESSION['s_language']['title_array2']."</b></td>\n";
	$content .= "	<td><b>".$_SESSION['s_language']['title_array3']."</b></td>\n";
	$content .= "	<td><b>".$_SESSION['s_language']['title_array4']."</b></td>\n";
	$content .= "	<td><b>".$_SESSION['s_language']['title_array5']."</b></td>\n";
	$content .= "	<td><b>".$_SESSION['s_language']['title_array6']."</b></td>\n";
	$content .= "</tr>\n";

	while ($reservation = database_fetch_object($resultat)){

	    if ($couleur == "#dddddd") $couleur = "#cccccc"; else $couleur = "#dddddd";

	    $leTitre = ereg_replace("'","&#39;", database_get_from_object($reservation, 'titre'));
	    $leTitre = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$leTitre);
	    $leTitre_HTML = $leTitre;
	    $leTitre = ereg_replace("\"","\\\"",$leTitre);
	    $leComment = ereg_replace("'","\'", database_get_from_object($reservation, 'commentaire'));
	    $leComment = ereg_replace("\"","\\\"",$leComment);
	    $leComment = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$leComment);
	    $email = database_get_from_object($reservation, 'email');
	    $id = database_get_from_object($reservation, 'id');
	    $reservationPriority = database_get_from_object($reservation, 'priority');

	    $content .= "<tr class='center' style='background-color:$couleur'>\n";
	    $content .= "	<td><img src='img/poub.gif' class='noborder' alt='".$_SESSION['s_language']['admin_or_user_J']."' onclick='return efface(\"$id\",\"$leTitre\");' /></td>\n";

	    if ($modification_enable)
	        $content .= "	<td><img src='img/edit.gif' class='noborder' alt='".$_SESSION['s_language']['admin_or_user_J']."' onclick='return modifie(\"$id\",\"$leTitre\");' /></td>\n";

	    if ($priorityManagement && ($reservationPriority == '1'))
	        $style = "style='color:red;'"; else $style = "";
	    $nom_objet = database_get_from_object($reservation, 'objet');
	    if ($objet)
	        $content .= "	<td $style>$nom_objet</td>\n";
	    if ($date)
	        $content .= "	<td $style>".displayCurrentLanguageDate(database_get_from_object($reservation, 'jour'), 0)."</td>\n";
	    $content .= "	<td $style>".substr(database_get_from_object($reservation, 'debut'),0,5)."</td>\n";
	    $content .= "	<td $style>".substr(database_get_from_object($reservation, 'duree'),0,5)."</td>\n";
	    if ($leComment == "") $leComment = $_SESSION['s_language']['no_comment'];
	    $content .= "	<td>".displayMessage($leComment, $leTitre_HTML)."</td>\n";

	    $emailCrypte = PHPcrypt("<a href='mailto:$email'>$email</a>");
	    $afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"\", \"$emailCrypte\", \"\")</script>";
	    $content .= "	<td>".$afficheEmailCrypte."</td>\n";


//	    $emailCrypte = PHPcrypt("mailto:$email");
//	    $afficheEmailCrypte1 = "<script type='text/javascript'>decrypt(\"\", \"$emailCrypte\", \"\")</script>";
//	    $emailCrypte = PHPcrypt($email);
//	    $afficheEmailCrypte2 = "<script type='text/javascript'>decrypt(\"\", \"$emailCrypte\", \"\")</script>";
//	    $content .= "	<td>".$afficheEmailCrypte1.$afficheEmailCrypte2."</td>\n";

	    if (database_get_from_object($reservation, 'valide') == 1)
	    {
	        $content .= "	<td><img src='img/cadenasFr.gif' class='noborder'alt='".$_SESSION['s_language']['commun_validated']."' /></td>\n";
	    } else {
	        $content .= "	<td><img src='img/cadenasO.gif' class='noborder' alt='".$_SESSION['s_language']['admin_only_J']."' onclick='return valide(\"$id\",\"$leTitre\");' /></td>\n";
	    }
	    $content .= "</tr>\n\n";
	    $objets[$compteur] = $nom_objet;
	    $compteur++;
	}
	$content .= "</table>\n\n";

	$tab = array();
	$tab[0] = $content;
	$tab[1] = $compteur;
	$temp = array_unique($objets);
	$tab[2] = array_values($temp);
	return $tab;
}

function getResaJavascriptContent(){
	global $exit_message_javascript_authentification, $read_only, $technical_email;

	$question_valide = $_SESSION['s_language']['confirm_valide_J']."\" + \" '\" + titre + \"' ? ".$_SESSION['s_language']['admin_only_J'];
	$question_modif  = $_SESSION['s_language']['confirm_modif_J']. "\" + \" '\" + titre + \"' ? ".$_SESSION['s_language']['admin_or_user_J'];
	$question_delete = $_SESSION['s_language']['confirm_delete_J']."\" + \" '\" + titre + \"' ? ".$_SESSION['s_language']['admin_or_user_J'];

	$content = "   ";
	$content .= getOverlibManagement();
	$content .= "<script type='text/javascript'><!--\n";

	if ($read_only)
	{
	    $emailCrypte = PHPcrypt($technical_email);
	    $content .= "function msg(){\n";
	    $content .= "	alert(\"$exit_message_javascript_authentification (\" + decryptJ(\"\", \"$emailCrypte\", \"\")  + \")\");\n";
	    $content .= "}\n\n";
	}

	$content .= "function efface (idr,titre){\n";
	$content .= "	document.forms.effacer.id.value=idr;\n";
	$content .= "	".confirm_overlib($question_delete, createAction('window.document.forms.effacer'), ';', 'OFFSETX, 150')."\n";
	$content .= "}\n\n";

	$content .= "function modifie(idr,titre){\n";
	$content .= "	document.forms.modifier.id.value=idr;\n";
	$content .= "	".confirm_overlib($question_modif, createAction('window.document.forms.modifier'), ';', 'OFFSETX, 150')."\n";
	$content .= "}\n\n";

	$content .= "function valide(idr,titre){\n";
	$content .= "	document.forms.valider.id.value=idr;\n";
	$content .= "	".confirm_overlib($question_valide, createAction('window.document.forms.valider'), ';', 'OFFSETX, -150')."\n";
	$content .= "}\n";

	$content .= "//--></script>\n\n";

	return $content;
}

function getResaFormContent(){

	$content  = "<form id='valider' action='valide.php' method='post'>\n";
	$content .= "	<p><input type='hidden' name='id' value='' /></p>\n";
	$content .= "</form>\n";

	$content .= "<form id='modifier' action='modifie.php' method='post'>\n";
	$content .= "	<p><input type='hidden' name='id' value='' /></p>\n";
	$content .= "</form>\n";

	$content .= "<form id='effacer' action='efface.php' method='post'>\n";
	$content .= "	<p><input type='hidden' name='id' value='' /></p>\n";
	$content .= "</form>\n";

	return $content;
}

function displayMessage($msg, $URL){

	$msg = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$msg);
	$msg = ereg_replace("\"","&quot;",$msg);
	return "<a href='#' onmouseover=\"return overlib('$msg', CAPTION, '', CENTER, OFFSETY, 20, TEXTSIZE, '12px', FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);\" onmouseout='return nd();'>$URL</a>";
}

function createSearchDBrequest($critere, $type, $valeurcritere, $toutVoir){
	global $moisCourant, $anneeCourante, $jourCourant;

	$datecourantetxt = makeDateWithZero($anneeCourante, $moisCourant, $jourCourant);

	if ($valeurcritere == "")
	    $DB_request = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority FROM reservation R, objet O WHERE R.idobjet = O.id ";
	else {
	    if ($type == 1)      $value = "'%$valeurcritere%'";
	    else if ($type == 2) $value = "'$valeurcritere%'";
	    else if ($type == 3) $value = "'%$valeurcritere'";
	    else if ($type == 4) $value = "'$valeurcritere'";
	    if ($critere == "classe")
	        $DB_request = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority FROM reservation R, objet O, classe C WHERE R.idobjet = O.id AND O.id_classe = C.id AND C.nom LIKE $value ";
	    else {
	        $DB_request = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority FROM reservation R, objet O WHERE R.idobjet = O.id ";
	        if ($critere == "objet")  $DB_request .= "AND O.nom LIKE ".$value;
	        else if ($critere == "date") $DB_request .= "AND R.jour LIKE ".$value;
	        else $DB_request .= "AND ".$critere." LIKE ".$value;
	    }
	}
	$DB_request .= " AND R.state=0";
	if ($toutVoir == false) $DB_request .= " AND jour >= '".$datecourantetxt."'";
	$DB_request .= " ORDER BY jour ASC, objet";
	return $DB_request;
}

function fillWithZero($nb){
	if ($nb < 10) return "0".$nb;
	else return $nb;;
}

function makeDateWithZero($year, $month, $day){
	return $year."-".fillWithZero($month)."-".fillWithZero($day);
}

function getPriorityMessage($objets){

	$message = "";
	for($i = 0 ; $i < count($objets) ; $i++){
		$inter = getPriorityMessageName($objets[$i]);
		if (isset($_SESSION['s_language'][$inter])) $message .= ", ".$_SESSION['s_language'][$inter];
	}
	$message = substr($message,2);
	$message = replaceLastOccurenceOfComa($message, 'or');
	return $message;
}

function createObjectList($objet){

	$objets = explode(',', $objet);
	$nbObjets = count($objets);
	$objets_list = "";
	for ($i = 0 ; $i < $nbObjets ; $i++) $objets_list .= "'".$objets[$i]."', ";
	$objets_list = substr($objets_list, 0, -2);
	$objets_to_display = str_replace(',', ', ', $objet);
	$objets_to_display = replaceLastOccurenceOfComa($objets_to_display, 'and');
	$tab = array();
	$tab[0] = $objets;  			// liste des objets sous forme de tableau
	$tab[1] = $nbObjets;    		// nombre d'objets
	$tab[2] = $objets_list; 	 	// liste des objets sous forme d'une chaÃ®ne de caractÃšres sÃ©parÃ©s par des virgules
	$tab[3] = $objets_to_display;	// liste des objets sous forme d'une chaÃ®ne de caractÃšres sÃ©parÃ©s par des virgules sauf le dernier par 'et'
	return $tab;
}

function replaceLastOccurenceOfComa($text, $conjonction){

	if ( ($pos = strrpos($text,",")) !== FALSE ) return substr($text, 0, $pos)." ".$_SESSION['s_language'][$conjonction]." ".substr($text, $pos+2);
	else return $text;
}

function mktimeFromTXTdate($ladatetxt){
	return mktime(0,0,0,substr($ladatetxt, 5 , 2), substr($ladatetxt, -2), substr($ladatetxt, 0 , 4));
}

function check_time_available_on_single_reservation($date, $objets_list, $idebut, $ifin, $exception){
	global $connexionDB;

	/*
	    $date au format YYYY-MM-DD
	    $objets_list au format "'A1', 'A2', 'A3'"
	    $idebut au format 8.5 pour 08h30
	    $ifin au format 9.5 pour 09h30
	*/

	$DB_request = "SELECT DISTINCT R.debut, R.duree, R.titre FROM reservation R, objet O ";
	$DB_request .= "WHERE R.idobjet = O.id AND R.jour = '$date' AND R.state=0 AND O.nom IN ($objets_list) $exception ORDER BY R.debut";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	$message = "";
	$compteur = 0;
	while ($reservation = database_fetch_object($resultat))
	{
	    $deb=substr(database_get_from_object($reservation, 'debut'),0,2);
	    $mi=substr(database_get_from_object($reservation, 'debut'),3,2);
	    if ($mi=="30") $deb=$deb+.5;
	    $end=$deb+substr(database_get_from_object($reservation, 'duree'),0,2);
	    $mi=substr(database_get_from_object($reservation, 'duree'),3,2);
	    if ($mi=="30") $end=$end+.5;
	    if ( (($idebut>=$deb)&&($idebut<$end)) || (($idebut<$deb)&&($ifin>$deb )) )
	    {
	        $message .= database_get_from_object($reservation, 'titre').", ";
	        $compteur++;
	    }
	}

	if ($compteur > 0) $message = substr($message, 0 , -2);
	if ($compteur > 1) $message = replaceLastOccurenceOfComa($message, "and");
	$tab = array();
	$tab[0] = $compteur;
	$tab[1] = $message;
	return $tab;
}

function check_time_available_on_multiple_reservation($multi, $datebegintxt, $datefintxt, $objets_list, $idebut, $ifin, $exception){
	global $connexionDB, $week_end_enabled;

	/*
	    $multi = 1, 7 or 14 ( or -1 for continuous reservations)
	    $datebegintxt is the departure date in format YYYY-MM-DD
	    $datefintxt is the arrival date in format YYYY-MM-DD
	    $ladatetxt is the current date in format YYYY-MM-DD
	    $ladate is the current date in array format
	*/

	$nbJoursOK = 0;
	$nbJoursConflit = 0;
	$nbConflit = 0;
	$message = array();
	$ladatetxt = $datebegintxt;
	$ladate = getdate(mktimeFromTXTdate($ladatetxt));

	while($ladatetxt <= $datefintxt )
	{
	    if ( ! $week_end_enabled){
	        if ( ! $_SESSION['s_jour_travail'][getDayInWeek($ladatetxt)])
	        {
	            $tmp = computeNextDayToReserve($multi, $ladate);
	            $ladate = $tmp[0];
	            $ladatetxt = $tmp[1];
	            continue;
	        }
	    }

	    $DB_request = "SELECT DISTINCT R.debut, R.duree, R.titre, O.nom, R.email FROM reservation R, objet O ";
	    $DB_request .= "WHERE R.idobjet = O.id AND R.jour = '$ladatetxt' AND R.state=0 AND O.nom IN ($objets_list) $exception ORDER BY R.debut, O.nom";
	    $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	    $valableJour = 1;
	    while ($reservation = database_fetch_object($resultat))
	    {
	        $valable = 1;
	        $deb = substr(database_get_from_object($reservation, 'debut'),0,2);
	        $mi = substr(database_get_from_object($reservation, 'debut'),3,2);
	        $objetName = database_get_from_object($reservation, 'nom');
	        if ($mi == "30") $deb = $deb + .5;
	        $end = $deb + substr(database_get_from_object($reservation, 'duree'),0,2);
	        $mi = substr(database_get_from_object($reservation, 'duree'),3,2);
	        if ($mi == "30") $end = $end + .5;
	        if ($multi == -1)
	        {
	            if ($ladatetxt == $datebegintxt)
	            {
	                if ($idebut < $end) $valable = 0;
	            } else if ($ladatetxt == $datefintxt){
	                if ($ifin > $deb) $valable = 0;
	            } else {
	                $valable=0;
	            }
	        } else {
	            if ( (($idebut>=$deb)&&($idebut<$end)) || (($idebut<$deb)&&($ifin>$deb)) ) $valable = 0;
	        }

	        if ($valable == 0)
	        {
	            $valableJour = 0;
	            $message[$nbConflit][0] = $objetName;
	            $message[$nbConflit][1] = displayCurrentLanguageDate($ladatetxt, 0);
	            $message[$nbConflit][2] = database_get_from_object($reservation, 'email');
	            $message[$nbConflit][3] = database_get_from_object($reservation, 'titre');
	            $nbConflit++;
	        }
	    }

	    if ($valableJour == 0) $nbJoursConflit++;
	    else $nbJoursOK++;

	    $tmp = computeNextDayToReserve($multi, $ladate);
	    $ladate = $tmp[0];
	    $ladatetxt = $tmp[1];
	}

	$tab = array();
	$tab[0] = $nbJoursOK;
	$tab[1] = $nbJoursConflit;
	$tab[2] = $nbConflit;
	$tab[3] = $message;
	return $tab;
}

function check_time_available_on_multiple_reservation_for_one_day($ladatetxt, $objets_list, $idebut, $ifin, $multi, $ladatebegintxt, $datefintxt){
	global $connexionDB;

	$DB_request = "SELECT DISTINCT R.debut, R.duree FROM reservation R, objet O ";
	$DB_request .= "WHERE R.idobjet = O.id AND R.jour = '$ladatetxt' AND R.state=0 AND O.nom IN ($objets_list) ORDER BY R.debut";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	$valable=1;
	while ($reservation = database_fetch_object($resultat)) {
	    $deb=substr(database_get_from_object($reservation, 'debut'),0,2);
	    $mi=substr(database_get_from_object($reservation, 'debut'),3,2);
	    if ($mi=="30") $deb=$deb+.5;
	    $end=$deb+substr(database_get_from_object($reservation, 'duree'),0,2);
	    $mi=substr(database_get_from_object($reservation, 'duree'),3,2);
	    if ($mi=="30") $end=$end+.5;
	    if ($multi == -1){
	        if ($ladatetxt == $ladatebegintxt){
	            if ($idebut < $end) $valable=0;
	        } else if ($ladatetxt == $datefintxt){
	            if ($ifin > $deb) $valable=0;
	        } else {
	            $valable=0;
	        }
	    } else {
	        if ( (($idebut>=$deb)&&($idebut<$end)) || (($idebut<$deb)&&($ifin>$deb )) ) $valable=0;
	    }
	}

	return $valable;
}

function getAvailableObjects(){
	global $connexionDB;

	$DB_request = "SELECT DISTINCT O.nom FROM objet O ";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	$object = array();
	$i = 0;
	while ($reservation = database_fetch_object($resultat)) {
		$object[$i] = database_get_from_object($reservation, 'nom');
		$i++;
	}

	return $object;
}

function getAvailableClass(){
	global $connexionDB;

	$DB_request = "SELECT DISTINCT C.nom FROM classe C ";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	$class = array();
	$i = 0;
	while ($reservation = database_fetch_object($resultat)) {
		$class[$i] = database_get_from_object($reservation, 'nom');
		$i++;
	}

	return $class;
}


function computeNextDayToReserve($multi, $ladate){
	if ($multi == -1) $ladate = getdate(mktime(0,0,0,$ladate['mon'], $ladate['mday'] + 1, $ladate['year']));
	else $ladate = getdate(mktime(0,0,0,$ladate['mon'], $ladate['mday'] + $multi, $ladate['year']));
	$ladatetxt = makeDateWithZero($ladate['year'], $ladate['mon'], $ladate['mday']);
	$tmp = array();
	$tmp[0] = $ladate;
	$tmp[1] = $ladatetxt;
	return $tmp;
}

function sans_accent($text) {
    return ( strtr( $text, 'ÃÃÃÃÃÃÃ Ã¡Ã¢Ã£Ã€Ã¥ÃÃÃÃÃÃÃ²Ã³ÃŽÃµÃ¶ÃžÃÃÃÃÃšÃ©ÃªÃ«ÃÃ§ÃÃÃÃÃ¬Ã­Ã®Ã¯ÃÃÃÃÃ¹ÃºÃ»ÃŒÃ¿ÃÃ±', 'AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn' ) );
}

function errorDBinternal($DB_request, $select, $database_error){
	global $technical_contact, $technical_tel, $body_end, $error_message_begin;

	$problem_msg = "<p>".$_SESSION['s_language']['commun_error_mysql_contact1']." ".$technical_contact." (".$technical_tel.") ".$_SESSION['s_language']['commun_error_mysql_contact2']."</p>";
	$mes = "";

	$ob_active = ob_get_length() !== FALSE;
	if ($ob_active){
	    if (ob_get_length() == 0) $head_created = false;
	    else $head_created = true;
	} else {
	    if (headers_sent()) $head_created = true;
	    else  $head_created = false;
	}

	if ($select) $mes .= "\n\n</select></form><br /><br />\n<p class='red'>";
	else if ( ! $head_created) $mes .= $error_message_begin;
	else $mes .= "\n\n<p class='red'>\n";
	$mes .= $_SESSION['s_language']['commun_error_mysql']."</p>\n<p class='red'>\"<b>$DB_request</b>\"</p>\n";
	$mes .= "<p class='red'>".$_SESSION['s_language']['inc_commun_error']." : \"$database_error\"</p>";
	$mes .= $problem_msg.$body_end;
	exit($mes);
}

function errorDB($DB_request, $select){
	global $error_message_db1, $error_message_db2, $database, $connexionDB;

	if ($database == "MySQL") $database_error = mysql_error();
	else if ($database == "PostgreSQL") $database_error = pg_last_error();
	else if ($database == "Oracle"){
	    $DB_parse = oci_parse($connexionDB, $DB_request);
	    $test = @oci_execute($DB_parse, OCI_COMMIT_ON_SUCCESS);
	    $e = oci_error($DB_parse);
	    $database_error = $e['message'];
	}

	if (isset($_SESSION['s_language'])) errorDBinternal($DB_request, $select, $database_error);
	else die($error_message_db1.$DB_request."</b>\"</p>\n<p>".$database_error.$error_message_db2);
}

function displayErrorForGetAvailableLanguages($DB_request){
	if (substr($_SERVER['SCRIPT_FILENAME'], -14) == 'calendrier.php') errorDB($DB_request, true);
	else errorDB($DB_request, false);
}

function exitWrongSignature($file){
	global $entete_error, $body_end, $error_message_begin;

	if (isset($entete_error)) $msg = $entete_error;
	else $msg = $error_message_begin;
	$msg .= $_SESSION['s_language']['general_exit_message']."<span style='color: black'> ($file)</span></p>".$body_end;
	exit($msg);
}

function getAvailableLanguages(){
	global $connexionDB, $database, $technical_contact;

	$i = 0;
	$res = array();
	if ($database == "MySQL"){
	    $DB_request = "DESCRIBE languages";
	    $resultat = mysql_query($DB_request, $connexionDB) or displayErrorForGetAvailableLanguages($DB_request, false);
	    while ($row = database_fetch_object($resultat)){
	        $champ1 =$row->Field;
	        if ($champ1 != "name"){
	            $res[$i] = $champ1;
	            $i++;
	        }
	    }
	} else if ($database == "PostgreSQL"){
	    $DB_request = "select column_name from information_schema.columns where table_name = 'languages'";
	    $resultat = @pg_query($connexionDB, $DB_request) or displayErrorForGetAvailableLanguages($DB_request, false);
	    while ($row = pg_fetch_object($resultat)){
	        $champ2 = $row->column_name;
	        if ($champ2 != "name"){
	            $res[$i] = $champ2;
	            $i++;
	        }
	    }
	} else if ($database == "Oracle"){
	    $DB_request = "SELECT COLUMN_NAME FROM user_tab_columns where table_name='LANGUAGES'";
	    $resultat = oci_parse($connexionDB, $DB_request) or displayErrorForGetAvailableLanguages($DB_request, false);
	    @oci_execute($resultat, OCI_COMMIT_ON_SUCCESS) or displayErrorForGetAvailableLanguages($DB_request, false);
	    while ($row = oci_fetch_object($resultat)){
	        $champ2 = $row->COLUMN_NAME;
	        if ($champ2 != "NAME"){
	            $res[$i] = strtolower($champ2);
	            $i++;
	        }
	    }
	}
	return $res;
}

function getPriorityMessageName($name){
	return 'resa_mandatory_priority_'.strtolower(str_replace(' ','_',$name));
}

function PHPcrypt($string){
	global $known_letters, $key_known_letters, $count_known_letters;

	$known_letters = array("a","b","c","d","e","f","g","h","i","j","k","l","m",
	                       "n","o","p","q","r","s","t","u","v","w","x","y","z",
	                       "A","B","C","D","E","F","G","H","I","J","K","L","M",
	                       "N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
	                       "0","1","2","3","4","5","6","7","8","9","@",".","-");
	$key_known_letters = array_flip($known_letters);
	$count_known_letters = count($known_letters);

	$key = iconv_strlen($string) % $count_known_letters;
	$out = "";
	for ($i=0 ; $i<strlen($string) ; $i++){
	    if ( in_array($string[$i], $known_letters) ) $out .= $known_letters[(($key_known_letters[$string[$i]]+$key)%$count_known_letters)];
	    else $out .= $string[$i];
	}
	return($out);
}



/**********************************************************************************
***		Functions for internal use only (only for commun.php)		***
**********************************************************************************/

function getWEBurl(){

	$temp = $_SERVER['SCRIPT_NAME'];
	$temp = substr($temp, 0, strrpos($temp, '/') + 1);
	$url = "http://".$_SERVER['SERVER_NAME'].$temp;

	$isFromSOAP = false;
	if (substr($url, -5) == "SOAP/"){
		$url = substr($url, 0, -5);
		$isFromSOAP = true;
	}

	return array($url, $isFromSOAP);
}

function date_diff_without_we($dat1, $dat2){
	/* Dat1 and Dat2 passed as "YYYY-MM-DD" */
	/* Dat1 is min date and Dat2 is max date */

	$cpt = 0;
	$diff = 0;
	$month = substr($dat1,5,2);
	$day = substr($dat1,8,2);
	$year = substr($dat1,0,4);
	$tmp_dat =mktimeFromTXTdate($dat1);
	$tmp_dat_END = mktimeFromTXTdate($dat2);
	while ($tmp_dat <= $tmp_dat_END){
	    $date_txt = getdate($tmp_dat);
	    $nb_day = $date_txt["wday"] + $_SESSION['s_jour_offset'];
	    if ($nb_day == -1) $nb_day = 6;
	    if ($_SESSION['s_jour_travail'][$nb_day]) $diff++;
	    $cpt++;
	    $tmp_dat = mktime(0,0,0,$month,$day+$cpt,$year);
	}
	return $diff;
}

function createAction ($form)
{
	global $read_only;

	if ($read_only) return "msg();";
	else return $form.".submit();";
}

function getOverlibManagement(){
	return "
	<script type='text/javascript'><!--\n
	    window.document.onkeyup = keyhandler;

	    function keyhandler(e) {
	        var key;
	        if (window.event) key = window.event.keyCode;
	        else if (e) key = e.which;

	        if ( (key == 13) || (key == 27) ){
	            for (i=0;i<window.document.forms.length;i++){
	                if (window.document.forms[i].id == 'overlib_form'){
	                    if (window.document.forms[i].open.value == 'true'){
	                        if (key == 13) window.document.forms[i].yes.click();
	                        else  window.document.forms[i].no.click();
	                    }
	                }
	            }
	        }
	    }

	    function setOpenToFalse(){
	        document.forms.overlib_form.open.value='false';
	    }\n
	--></script>\n\n";
}

function confirm_overlib ($question, $action_yes, $action_no, $positionX)
{
	$fermeture = "setOpenToFalse();";
	return " return overlib(\"".$question."<div><br /></div><form id='overlib_form'><p><input type='hidden' name='open' value='true' /></p><table class='center'><tr><td class='center'><input type='button' name='yes' value='".$_SESSION['s_language']['yes']."' onclick='javascript:".$fermeture."cClick();$action_yes' /></td><td>&nbsp; &nbsp; &nbsp; &nbsp;</td><td class='center'><input type='button' name='no' value='".$_SESSION['s_language']['no']."' onclick='javascript:".$fermeture."cClick();$action_no' /></td></tr></table></form>\",
	TEXTSIZE, '12px',
	STICKY, CLOSECLICK, CLOSETEXT, '".$_SESSION['s_language']['close']."',
	WIDTH, 300, $positionX,
	CAPTION, ' ', CENTER, FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);";
}

?>
