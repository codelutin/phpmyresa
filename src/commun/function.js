//**********************************************************************************************
/**
* Project PHPMyResa / File commun/function.js
*
* This file contains some javascript functions
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Fr�d�ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
*
* @copyright	2003,2004,2005,2006,2008 Fr�d�ric Melot
* @copyright	2003 Gilbert Federico
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


function compareDate(annee1, annee2, mois1, mois2, jour1, jour2, stricte){
	var retour = 'true';

	annee1 = parseInt(annee1);
	annee2 = parseInt(annee2);
	mois1 = parseInt(mois1);
	mois2 = parseInt(mois2);
	jour1 = parseInt(jour1);
	jour2 = parseInt(jour2);
	if (annee1<annee2){
	    retour = 'false';
	} else if (annee1 == annee2){
	    if (mois1<mois2){
	        retour = 'false';
	    } else if (mois1 == mois2){
	        if (jour1<jour2){
	            retour = 'false';
	        }
	        else if (stricte == 'true'){
	            if (jour1 == jour2)    retour = 'false';
	        }
	    }
	}
	return retour;
}

function decrypt(debut, chaine, fin) {
	document.write(decryptJ(debut, chaine, fin));
}

function emailIsCorrect(email){
	if (email.search(/^[+_a-zA-Z0-9-]+(\.[+_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)+$/) == -1) return false;
	else return true;
}

function decryptJ(debut, chaine, fin) {
	var known_letters = new Array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p",
	                              "q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F",
	                              "G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V",
	                              "W","X","Y","Z","0","1","2","3","4","5","6","7","8","9","@",".","-");
	var count_known_letters = known_letters.length;
	var key = (chaine.length)%count_known_letters;
	var out = "";
	for ( var i=0; i<chaine.length; i++ ) {
	  current = chaine.charAt(i);
	  if ( isInArray(current, known_letters) ) {
	     for ( var j=0; j<count_known_letters; j++ ) {
	        if ( known_letters[j] == current ) break;
	     }
	     currentIndiceLetter = j;
	     newIndiceLetter = (currentIndiceLetter-key)%count_known_letters;
	     if ( newIndiceLetter < 0 ) {
	        newIndiceLetter += count_known_letters;
	     }
	     out += known_letters[newIndiceLetter];
	  } else {
	     out += current;
	  }
	}
	return debut+out+fin;
}

function isInArray(needle, arrayHaystack){
	for (x=0; x < arrayHaystack.length; x++) if (arrayHaystack[x] == needle) return true;
	return false;
} 

function fixeDuree(default_duration) {
	begin1 = document.forms.resa.debut;
	begin2 = document.forms.resa.midebut;
	fullhour = begin1.options[begin1.selectedIndex].value;
	halfhour = begin2.options[begin2.selectedIndex].value;
	duration = document.forms.resa.duree;
	number = duration.length;

	while (number > 0) {
	    duration.remove(number-1);
	    number--;
	}

	if (halfhour == 0) {
	    max_hour = 25 - fullhour;
	} else {
	    max_hour = 25 - fullhour - 1;
	}

	for (h=0; h<max_hour; h++) {
	    ajout = document.createElement('option');
	    ajout.text = h;
	    try {
	        duration.add(ajout,null);
	    }
	    catch(err) {
	        duration.add(ajout);
	    }
	}
	la_duree = Math.min (max_hour - 1, default_duration);
	duration.selectedIndex = la_duree;
}
