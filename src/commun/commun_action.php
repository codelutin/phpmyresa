<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/commun_action.php
*
* This file is included in efface.php, modifie.php and valide.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


error_reporting(E_ALL);

function GetClassProperties($DB_request_condition){
	global $connexionDB;

	$icalTab = array();
	$classeTab = array();
	$valideTab = array();
	$final = array();
	$k = 0;
	$j = 0;
	$i = 0;
	$DB_request = "SELECT DISTINCT O.pubIcal, C.nom, O.status FROM reservation R, objet O, classe C ";
	$DB_request .= " WHERE R.idobjet = O.id AND O.id_classe = C.id AND $DB_request_condition AND R.state=0";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)){
		$classeResa = database_get_from_object($row, 'nom');
		if ( ! in_array($classeResa, $classeTab)){
			$classeTab[$j] = $classeResa;
			$j++;
		}
		if ( ! in_array($classeResa, $icalTab)){
			if (database_get_from_object($row, 'pubIcal') == 1){
				$icalTab[$k] = $classeResa;
				$k++;
			}
		}
		if ( ! in_array($classeResa, $valideTab)){
			if (database_get_from_object($row, 'status') == 1){
				$valideTab[$i] = $classeResa;
				$i++;
			}
		}
	}
	$final[0] = $icalTab;
	$final[1] = $classeTab;
	$final[2] = $valideTab;
	return $final;
}

function getIdmultiFromId($id){
	global $connexionDB;

	$DB_request = "SELECT idmulti FROM reservation WHERE id=$id";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat);
	return database_get_from_object($row, 'idmulti');
}

function reservationIsContinuous($idmulti){
	global $connexionDB;

	if ($idmulti == 0) return false;
	else {
		$DB_request = "SELECT distinct debut, duree FROM reservation WHERE idmulti=$idmulti AND state=0";
		$res = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
		$compteur = 0;
		while (database_fetch_object($res)) $compteur++;
		if ($compteur == 1) return false; else return true;
	}
}

function computeTitleInAction($minDate, $maxDate, $continuous, $count){
	global $week_end_enabled;

	$tab = array();

	if ($maxDate == ""){
		$tab[0] = $_SESSION['s_language']['single_day_reservation']." ".displayCurrentLanguageDate($minDate, 0);
		$tab[1] = -1;
		return $tab;
	}

	$messageInter = $_SESSION['s_language']['execresa_from']." ".displayCurrentLanguageDate($minDate, 0)." ".$_SESSION['s_language']['execresa_to']." ".displayCurrentLanguageDate($maxDate, 0);
	$period = -1;
	if ($continuous){
		$message = $_SESSION['s_language']['reservation_continue']." ".$messageInter;
	} else{
		$message = $_SESSION['s_language']['reservation_periodic_strict']." ";
		$dayDiff = date_diff(new DateTimeImmutable($minDate), new DateTimeImmutable($maxDate))->format("%R%a");
		if (($count - $dayDiff) == 1){
			$message .= $_SESSION['s_language']['modify_1jour']." ".$messageInter;
			$period = 1;
		} elseif (($count -1)*7 == $dayDiff){
			$message .= $messageInter." ".$_SESSION['s_language']['modify_1semaine'];
			$period = 7;
		} elseif (($count -1)*14 == $dayDiff){
			$message .= $messageInter." ".$_SESSION['s_language']['modify_2semaines'];
			$period = 14;
		} elseif ( ! $week_end_enabled){
			$dayDiff = date_diff_without_we($minDate, $maxDate);
			if ($count == $dayDiff){
				$message .= $_SESSION['s_language']['modify_1jour']." ".$messageInter;
				$period = 1;
			}else $message .= $messageInter." - ".$_SESSION['s_language']['modify_periodicity_not_defined'];
		}
		else $message .= $messageInter." - ".$_SESSION['s_language']['modify_periodicity_not_defined'];
	}
	$tab[0] = $message;
	$tab[1] = $period;
	return $tab;
}

?>
