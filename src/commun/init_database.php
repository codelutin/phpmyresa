<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/init_database.php
*
* This file loads the database parameters and set them into session variables
* If everything's OK, nothing is displayed. If there is an incorrect configuration,
* the user is redirected to the config web pages
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author 	Emilie Deloustal
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


if (ini_get('session.auto_start') == "0") session_start();
require_once('database.php');

function go_configure(){
	Header("Location:./config/");
	exit();
}

if ( ! isset($database) || empty($database))  $database = "MySQL";
else if (($database != "MySQL") && ($database != "PostgreSQL") && ($database != "Oracle")) go_configure();

if ( ! defined('HOST') || ! constant('HOST')) go_configure();
if ( ! defined('USER') || ! constant('USER')) go_configure();
if ( ! defined('PWD')  || ! constant('PWD')) go_configure();
if ( ($database != "Oracle") && (! defined('BD')   || ! constant('BD'))) go_configure();


//***********************************************************
// VÃ©rification de la connexion Ã  la base de donnÃ©es
//***********************************************************
if ($database == "MySQL"){
	$connexionDB = @mysql_connect(HOST, USER, PWD);
	$pb_num = mysql_errno();
	if ($pb_num == 2013) exit("2013: ".mysql_error());
	if ( ! $connexionDB) go_configure();
	if ( ! mysql_select_db(BD,$connexionDB)) go_configure();
} else if ($database == "PostgreSQL"){
	$connexionDB = @pg_connect("host=".HOST." dbname=".BD." user=".USER." password=".PWD);
	if ( ! $connexionDB) go_configure();
} else if ($database == "Oracle"){
	$connexionDB = @oci_connect(USER, PWD, HOST);
	if ( ! $connexionDB) go_configure();
}


//***********************************************************
// VÃ©rification de l'existence de tables dans la base de donnÃ©es
//***********************************************************
if ($database == "MySQL"){
	$DB_request = "SHOW TABLES";
	$result = mysql_query($DB_request, $connexionDB) or die($error_message_db1.$DB_request.$error_message_db2);
	$nbTable = mysql_num_rows($result);
} else if ($database == "PostgreSQL"){
	$DB_request = "select table_name from information_schema.tables where table_type='BASE TABLE' and table_name not like 'pg_%' and table_name not like 'sql_%'";
	$result = @pg_query($connexionDB, $DB_request) or die($error_message_db1.$DB_request.$error_message_db2);
	$nbTable = pg_num_rows($result);
} else if ($database == "Oracle"){
	$DB_request = "SELECT table_name FROM user_tables";
	$DB_parse = oci_parse($connexionDB, $DB_request) or die($error_message_db1.$DB_request.$error_message_db2);
	@oci_execute($DB_parse, OCI_COMMIT_ON_SUCCESS) or die($error_message_db1.$DB_request.$error_message_db2);
	$nbTable = oci_fetch_all($DB_parse, $result);
}
if ($nbTable == 0) go_configure();


//***********************************************************
// Recherche de la table languages
//***********************************************************
$need_installation = true;
if ($database == "MySQL"){
	for ($i = 0; $i < $nbTable; $i++) {
		$tmp_tablename = mysql_tablename($result, $i);
		if($tmp_tablename == "languages") $need_installation = false;
	}
} else if ($database == "PostgreSQL"){
	while ($field = pg_fetch_object($result)){
		$tmp_tablename = $field->table_name;
		if ($tmp_tablename == "languages") $need_installation = false;
	}
} else if ($database == "Oracle"){
	$row = array();
	$row = $result['TABLE_NAME'];
	for ($i = 0; $i < count($row); $i++){
		if ($row[$i] == "LANGUAGES") $need_installation = false;
	}
}
if ($need_installation) go_configure();


//***********************************************************
// Recherche de la colonne german de la table languages
//***********************************************************
$need_upgrade = true;
if ($database == "MySQL"){
	$DB_request = "DESCRIBE languages";
	$result = mysql_query($DB_request, $connexionDB) or die($error_message_db1.$DB_request.$error_message_db2);
	while ($field = mysql_fetch_object($result)){
		if (strtoupper($field->Field) == 'GERMAN') $need_upgrade = false;
	}
} else if ($database == "PostgreSQL"){
	$DB_request = "select column_name from information_schema.columns where table_name = 'languages'";
	$result = @pg_query($connexionDB, $DB_request ) or die($error_message_db1.$DB_request.$error_message_db2);
	while ($field = pg_fetch_object($result)){
		if (strtoupper($field->column_name) == 'GERMAN') $need_upgrade = false;
	}
} else if ($database == "Oracle"){
	$DB_request = "SELECT COLUMN_NAME FROM user_tab_columns where table_name='".strtoupper(languages)."'";
	$result = oci_parse($connexionDB, $DB_request) or die($error_message_db1.$DB_request.$error_message_db2);
	$test = @oci_execute($result, OCI_COMMIT_ON_SUCCESS) or die($error_message_db1.$DB_request.$error_message_db2);
	while ($field = oci_fetch_object($result)){
		if (strtoupper($field->COLUMN_NAME) == 'GERMAN') $need_upgrade = false;
	}
}
if ($need_upgrade) go_configure();


//***********************************************************
// La table language doit etre remplie
//***********************************************************
$DB_request = "SELECT count(*) AS nb FROM languages";
if ($database == "MySQL"){
	$result = mysql_query($DB_request, $connexionDB) or die($error_message_db1.$DB_request.$error_message_db2);
	$resultat = mysql_fetch_object($result);
	$nbResult = $resultat->nb;
} else if ($database == "PostgreSQL"){
	$result = @pg_query($connexionDB, $DB_request) or die($error_message_db1.$DB_request.$error_message_db2);
	$resultat = pg_fetch_object($result);
	$nbResult = $resultat->nb;
} else if ($database == "Oracle"){
	$result = oci_parse($connexionDB, $DB_request) or die($error_message_db1.$DB_request.$error_message_db2);
	@oci_execute($result, OCI_COMMIT_ON_SUCCESS) or die($error_message_db1.$DB_request.$error_message_db2);
	$resultat = oci_fetch_object($result);
	$nbResult = $resultat->NB;
}
if ($nbResult == 0) go_configure();
?>
