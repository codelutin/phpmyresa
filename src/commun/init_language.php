<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/init_language.php
*
* This file loads the language data
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


if ( ! isset($_SESSION['s_current_language']) ){
	$save_path = session_save_path();
	if ($save_path != ""){
		if ( ! is_writable($save_path)){
			if ( ! file_exists($save_path)){
				$test = mkdir($save_path, 0700);
				if ( ! $test) exit($error_message_begin."The $save_path directory can not be created. Please correct this!".$error_message_end);
		    	} else exit($error_message_begin."The $save_path directory is not writable. Please correct this!".$error_message_end);
		}
	}
	init_config_language();
}

if ( ! isset($_SESSION['s_load_of_db_language']) || (! $_SESSION['s_load_of_db_language'])){
	init_language();
	$_SESSION['s_les_mois'] = array($_SESSION['s_language']['mois1'], $_SESSION['s_language']['mois2'], $_SESSION['s_language']['mois3'], $_SESSION['s_language']['mois4'], $_SESSION['s_language']['mois5'], $_SESSION['s_language']['mois6'], $_SESSION['s_language']['mois7'], $_SESSION['s_language']['mois8'], $_SESSION['s_language']['mois9'], $_SESSION['s_language']['mois10'], $_SESSION['s_language']['mois11'], $_SESSION['s_language']['mois12']);

	$_SESSION['s_les_jours'] = array();
	if ($_SESSION['s_current_language'] == 'english'){
	    $_SESSION['s_les_jours'][0] = $_SESSION['s_language']['day7'];
	    $_SESSION['s_les_jours'][1] = $_SESSION['s_language']['day1'];
	    $_SESSION['s_les_jours'][2] = $_SESSION['s_language']['day2'];
	    $_SESSION['s_les_jours'][3] = $_SESSION['s_language']['day3'];
	    $_SESSION['s_les_jours'][4] = $_SESSION['s_language']['day4'];
	    $_SESSION['s_les_jours'][5] = $_SESSION['s_language']['day5'];
	    $_SESSION['s_les_jours'][6] = $_SESSION['s_language']['day6'];
	    $_SESSION['s_jour_travail'][0] = false;
	    $_SESSION['s_jour_travail'][1] = true;
	    $_SESSION['s_jour_travail'][2] = true;
	    $_SESSION['s_jour_travail'][3] = true;
	    $_SESSION['s_jour_travail'][4] = true;
	    $_SESSION['s_jour_travail'][5] = true;
	    $_SESSION['s_jour_travail'][6] = false;
	    $_SESSION['s_jour_offset'] = 0;
	} else {
	    $_SESSION['s_les_jours'][0] = $_SESSION['s_language']['day1'];
	    $_SESSION['s_les_jours'][1] = $_SESSION['s_language']['day2'];
	    $_SESSION['s_les_jours'][2] = $_SESSION['s_language']['day3'];
	    $_SESSION['s_les_jours'][3] = $_SESSION['s_language']['day4'];
	    $_SESSION['s_les_jours'][4] = $_SESSION['s_language']['day5'];
	    $_SESSION['s_les_jours'][5] = $_SESSION['s_language']['day6'];
	    $_SESSION['s_les_jours'][6] = $_SESSION['s_language']['day7'];
	    $_SESSION['s_jour_travail'][0] = true;
	    $_SESSION['s_jour_travail'][1] = true;
	    $_SESSION['s_jour_travail'][2] = true;
	    $_SESSION['s_jour_travail'][3] = true;
	    $_SESSION['s_jour_travail'][4] = true;
	    $_SESSION['s_jour_travail'][5] = false;
	    $_SESSION['s_jour_travail'][6] = false;
	    $_SESSION['s_jour_offset'] = -1;	//0 is set to sunday in javascript
	}
}

function init_config_language(){
	global $error_message_begin, $error_message_end;

	require_once('config.php');
	if (isset($_COOKIE['resa_lang'])) $default_language = $_COOKIE['resa_lang'];
	else{
	    if ( ! isset($default_language))  $default_language = "english";
	    else{
	        $langues = getAvailableLanguages();
	        if ( ! in_array($default_language, $langues)){
	            $mes = $error_message_begin."The value you gave the the default_language variable ($default_language) is not in the following list: ";
	            $inter = "";
	            for ($i = 0 ; $i < count($langues) ; $i++) $inter .= $langues[$i].", ";
	            $mes .= substr($inter, 0, -2).$error_message_end;
	            exit($mes);
	        }
	    }
	    setcookie("resa_lang", $default_language, time()+3600*24*30*12);
	}
	$_SESSION['s_current_language'] = $default_language;
}

function init_language(){

	global $connexionDB;

	$DB_request = "SELECT name, ".$_SESSION['s_current_language']." FROM languages ORDER BY name";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)) $_SESSION['s_language'][database_get_from_object($row, 'name')] = database_get_from_object($row, $_SESSION['s_current_language']);

	$_SESSION['s_load_of_db_language'] = true;
}
?>
