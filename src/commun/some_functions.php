<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/some_functions.php
*
* This file contains some localisation of files functions
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2007,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


function does_file_exists($url){

	global $isFromSOAP;

	if (substr($url,0,4) != "http") return file_exists($url);
	else{
	    $webServerResa = $_SERVER['SERVER_NAME'];
	    $lengthWebServerResa = strlen($webServerResa);
	    $temp = substr($url, 7);		// l'URL sans le 'http://'
	    $webServerURL = substr($temp, 0, strpos($temp, '/'));
	    if ($webServerURL == $webServerResa){
	        // In this case the file is not a remore one but a local one => now we try to search the path
	        $absoluteResa_URL = substr(WEB, $lengthWebServerResa+8);
	        $absoluteURL = substr($url, $lengthWebServerResa+8);
	        $beginOfAbsoluteResa_URL = substr($absoluteResa_URL, 0, strpos($absoluteResa_URL, "/")+1);	// le nom du premier rÃ©pertoire Ã  la racine
	        $beginOfAbsoluteURL = substr($absoluteURL, 0, strpos($absoluteURL, "/")+1);			// le nom du premier rÃ©pertoire Ã  la racine

	        if ( (substr($beginOfAbsoluteResa_URL, 0, 1) == '~') || (substr($beginOfAbsoluteURL, 0, 1) == '~') ) return remote_file_exists($url);
	        else {
			$tmp = "";
			for ($i = 0; $i < substr_count($absoluteResa_URL, "/") ; $i++) $tmp .= "../";
			$relativeURL = $tmp.$absoluteURL;
			if ($isFromSOAP) $relativeURL = "../".$relativeURL;
			return file_exists($relativeURL);
	        }
	    }
	}

	return remote_file_exists($url);
}


function remote_file_exists($url){

	//here is the traitment for remote file (not local one)
	$url_p = parse_url($url);

	if (isset ($url_p["host"])) $host = $url_p["host"];
	else return false;

	if (isset ($url_p["path"])) $path = $url_p["path"];
	else $path = "";

	if (isset ($url_p["port"])) $port = $url_p["port"];
	else $port = 80;

	$fp = @fsockopen($host, $port, $errno, $errstr, 30);
	if (!$fp) return false;
	else{
	    fputs($fp, "GET ".$path." HTTP/1.1\r\n");
	    fputs($fp, "HOST: ".$host."\r\n");
	    fputs($fp, "Connection: close\r\n\r\n");
	    $headers = "";
	    while (!feof ($fp)) $headers .= fgets ($fp, 128);
	}
	fclose($fp);
	$arr_headers = explode("\n", $headers);
	$return = false;
	if (isset ($arr_headers[0])) $return = strpos($arr_headers[0], "404") === false;
	return $return;
}


?>
