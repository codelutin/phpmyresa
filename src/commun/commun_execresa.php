<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/commun_execresa.php
*
* This file is included in execresa.php and execresa2.php
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


error_reporting(E_ALL);

function getMailMessageForAdminInSpecifiedLanguage($langue){
	global $connexionDB;

	$DB_request = "SELECT name, $langue
	      FROM languages
	      WHERE name = 'execresa_mail_admin'
	        OR name = 'ponctuation'
	        OR name = 'execresa_mail_date'
	        OR name = 'execresa_mail_start_time'
	        OR name = 'execresa_mail_duration'
	        OR name = 'execresa_mail_title'
	        OR name = 'execresa_mail_user'
	        OR name = 'execresa_mail_priority'
	        OR name = 'execresa_mail_no_priority'
	        OR name = 'execresa_mail_subject'
	        OR name = 'execresa_mail_valid'
	        OR name = 'reservation_periodic_strict'
	        OR name = 'reservation_continue'
	        OR name = 'title_array12'
	        OR name = 'execresa_from'
	        OR name = 'execresa_from2'
	        OR name = 'execresa_to'
	        OR name = 'execresa_booking'
	        OR name = 'pendant'
	        OR name = 'par'
	        OR name = 'title_array9'
	        OR name = 'execresa_mail_wifi'
	        OR name = 'execresa2_dates'";
	$res = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$inter = array();
	while ($row = database_fetch_object($res)){
	    $inter[database_get_from_object($row, 'name')] = database_get_from_object($row, $langue);
	}
	return $inter;
}

function getDayInWeek($ladatetxt){
	$date_txt = getdate(mktimeFromTXTdate($ladatetxt));
	$nb_day = $date_txt["wday"] + $_SESSION['s_jour_offset'];
	if ($nb_day == -1) $nb_day = 6;
	return $nb_day;
}

function getPriorityMessageInSpecifiedLanguage($objets, $langue){
	global $connexionDB;

	if ($langue == $_SESSION['s_current_language']) return getPriorityMessage($objets);

	$DBname = "";
	for($i = 0 ; $i < count($objets) ; $i++){
	    $DBname .= "'".getPriorityMessageName($objets[$i])."', ";
	}
	$DBname = substr($DBname, 0, -2);

	$DB_request = "SELECT name, $langue
	      FROM languages
	      WHERE name IN ($DBname)";
	$res = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$message = "";
	while ($row = database_fetch_object($res)){
	    $message .= database_get_from_object($row, $langue).", ";
	}
	$message = substr($message, 0, -2);
	$message = replaceLastOccurenceOfComaInSpecifiedLanguage($message, 'or', $langue);
	return $message;
}

function replaceLastOccurenceOfComaSpecial($text, $replace){

	if ( ($pos = strrpos($text,",")) !== FALSE ) return substr($text, 0, $pos)." $replace ".substr($text, $pos+2);
	else return $text;
}

function replaceLastOccurenceOfComaInSpecifiedLanguage($text, $conjonction, $langue){
	global $connexionDB;

	if ($langue == $_SESSION['s_current_language']) return replaceLastOccurenceOfComa($text, $conjonction);
	else {
	    $DB_request = "SELECT name, $langue
	          FROM languages
	          WHERE name = '$conjonction'";
	    $res = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $row = database_fetch_object($res);
	    $conjonctionInSpecifiedLanguage = database_get_from_object($row, $langue);
	    if ( ($pos = strrpos($text,",")) !== FALSE ) return substr($text, 0, $pos)." $conjonctionInSpecifiedLanguage ".substr($text, $pos+2);
	    else return $text;
	}
}

?>
