<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun_all.php
*
* This file is used store common includes. It is the only file used by both resa and resa/config
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


function database_query($DB_request, $connexionDB){
	global $database;

	if ($database == "MySQL") return mysql_query($DB_request, $connexionDB);
	else if ($database == "PostgreSQL") return @pg_query($connexionDB, $DB_request);
	else if ($database == "Oracle"){
	    $DB_parse = oci_parse($connexionDB, $DB_request);
	    $test = @oci_execute($DB_parse, OCI_COMMIT_ON_SUCCESS);
	    if ( ! $test) return false;
	    else return $DB_parse;
	}
}

function database_fetch_object($result){
	global $database;

	if ($database == "MySQL") return mysql_fetch_object($result);
	else if ($database == "PostgreSQL") return pg_fetch_object($result);
	else if ($database == "Oracle") return oci_fetch_object($result);
}

function database_get_from_object($row, $column){
	global $database;

	if ($database == "Oracle") $column = strtoupper($column);
	if (isset($row->$column)) return $row->$column; else return '';
}

function database_real_escape_string($text){
	global $database;

	$text = stripslashes($text);
	if ($database == "MySQL") return mysql_real_escape_string($text);
	else if ($database == "PostgreSQL") return pg_escape_string($text);
	else if ($database == "Oracle"){
		if ( ! ini_get('magic_quotes_sybase')){
			if (get_magic_quotes_gpc()) return str_replace("\'", "''", $text);
			else return str_replace("'", "''", $text);
		}
	}
}

function validate_email($email){

	$valid = true;
	if (version_compare(PHP_VERSION, '5.2.0', '>')){
		if( ! filter_var($email, FILTER_VALIDATE_EMAIL) ) $valid = false;
	} else{
		if (strpos($email, '@') === FALSE) $valid = false;
		if (strpos($email, '.') === FALSE) $valid = false;
	}

	return $valid;
}
?>
