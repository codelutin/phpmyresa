<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File commun/init_config.php
*
* This file loads the configuration parameters and set them into session variables
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core_commun
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require('config.php');
require_once('some_functions.php');

$temp = $_SERVER['SCRIPT_NAME'];
$temp = substr($temp, 0, strrpos($temp, '/') + 1);
if (substr($temp, -5) == "SOAP/") $prefix = "../"; else $prefix = "";

$do_database_test = true;


/************************************************************************************************************************
*															*
*															*
*				1st part: test on the config.php file							*
*															*
*															*
*************************************************************************************************************************/
if ( ! isset($technical_contact) || empty($technical_contact) || ! isset($technical_email) || empty($technical_email) || ! isset($technical_tel) || empty($technical_tel)){
	Header("Location:./config");
	exit();
}


//*******************************************************************************
// partie configuration gÃ©nÃ©rale
//*******************************************************************************
$message = "";

if ( ! isset($titre)) $titre = "PHPMyResa";
if ( ! isset($bookmark)) $bookmark = "PHPMyResa";
if ( ! isset($image_titre)) $image_titre = "";
else if (! empty($image_titre)){
	$location = $prefix.'img/'.$image_titre;
	if ( ! file_exists($location)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_image'], $image_titre, $location)."</li>\n";
}
if ( ! isset($image_pdf)) $image_pdf = "";
else if (! empty($image_pdf)){
	$location = $prefix.'img/'.$image_pdf;
	if ( ! file_exists($location)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_image'], $image_pdf, $location)."</li>\n";
	else if ( (substr($image_pdf, -4) != '.jpg') && (substr($image_pdf, -5) != '.jpeg') ) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_fpdf_image_format'], $image_pdf)."</li>\n";
}
if ( ! isset($page_accueil)) $page_accueil = "vueMois.php";
else{
	if ( ($page_accueil != 'vueMois.php') && ($page_accueil != 'vide.php') && ($page_accueil != 'today.php') ) $page_accueil = "vueMois.php";
}
if ( ! isset($email_domain)) $email_domain = "";
if ( ! isset($capacite)) $capacite = false;
if ( ! isset($modification_enable)) $modification_enable = false;
// $default_language is checked in the init_language.php file, because it is needed there before the call of init_config.php
if ( ! isset($display_tomorrow)) $display_tomorrow = false;
if ( ! isset($several_object_possible)) $several_object_possible = false;
if ( ! isset($week_end_enabled)) $week_end_enabled = true;
if ( ! isset($tree)) $tree = false;
else if ( ! isset($tree_scrollable)) $tree_scrollable = true;
if ( ! isset($mail_subject)) $mail_subject = "medium";
else if ( ($mail_subject != 'short') && ($mail_subject != 'medium') && ($mail_subject != 'long')) $mail_subject = "medium";
if ( ! isset($default_class_for_feed)) $default_class_for_feed = "";
if ( ! isset($use_diffusion_flag)) $use_diffusion_flag = false;

if ( ! isset($read_only_enable)) $read_only_enable = false;
else if ($read_only_enable){
	$IP_set = true;
	$domain_set = true;
	if ( ( ! isset($IP_regex)) || ($IP_regex == "") ){
	    $IP_set = false;
	}
	if ( ! isset($enabled_domain) || (count($enabled_domain) == 0) || ( ( (count($enabled_domain) == 1) && ($enabled_domain == "") ))){
	    $domain_set = false;
	}
	if ( ! $IP_set && ! $domain_set) $message .= "<li>".$_SESSION['s_language']['init_config_read_only_enable']."</li>\n";
}

if (! isset($default_beginning_hour)) $default_beginning_hour = 8;
if (! isset($default_beginning_mihour)) $default_beginning_mihour = 0;
if (! isset($default_duration)) $default_duration = 1;
if (! isset($default_miduration)) $default_miduration = 0;
if (! isset($default_end_hour)) $default_end_hour = 18;
if (! isset($default_end_mihour)) $default_end_mihour = 0;
if (! isset($default_comment)) $default_comment = "";

if (! isset($mail_server)) $mail_server = "";
if (! isset($web_server)) $web_server = "";
if (($mail_server == "") && ($web_server != "")) $message .= "<li>".$_SESSION['s_language']['init_config_mail_server_2']."</li>\n";
if (($mail_server != "") && ($web_server == "")) $message .= "<li>".$_SESSION['s_language']['init_config_mail_server_1']."</li>\n";

displayErrorsIfAny($message, "", sprintf($_SESSION['s_language']['init_config_general_msg3'], './config/configure_main.php'));


//*******************************************************************************
// partie logiciels annexes
//*******************************************************************************
$message = "";

if ( ! isset($iCal)) $iCal = false;
else if ($iCal){
	if ( ! isset($URL_iCal) || empty($URL_iCal)) $message .= "<li>".$_SESSION['s_language']['init_config_iCal_URL']."</li>\n";
	else{
	    if (substr($URL_iCal,-1) != '/')  $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_URL_msg'], $URL_iCal)."</li>\n";
	    if ( ! does_file_exists($prefix.$URL_iCal) &&  ! does_file_exists($URL_iCal)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_URL_error'], $URL_iCal)."</li>\n";
	}

	if ( ! isset($CALENDARS_file_location) || empty($CALENDARS_file_location)) $message .= "<li>".$_SESSION['s_language']['init_config_iCal_file_URL']."</li>\n";
	else{
	    if (substr($CALENDARS_file_location,-1) != '/') $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_file_msg'], $CALENDARS_file_location)."</li>\n";
	    if ( ! file_exists($CALENDARS_file_location)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_file_URL_error'], $CALENDARS_file_location)."</li>\n";
	    else if ( ! is_readable($CALENDARS_file_location)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_file_user_read'], $CALENDARS_file_location)."</li>\n";
	    else if ( ! is_writable($CALENDARS_file_location)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_iCal_file_user_write'], $CALENDARS_file_location)."</li>\n";
	}
	if ( ! isset($CALENDARS_file_beginning_of_name) || empty($CALENDARS_file_beginning_of_name)) $message .= "<li>".$_SESSION['s_language']['init_config_iCal_file_beginning']."</li>\n";
	if ( ! isset($CALENDARS_end_of_uid) || empty($CALENDARS_end_of_uid)) $message .= "<li>".$_SESSION['s_language']['init_config_iCal_file_end']."</li>\n";
	if ( ! isset($iCal_delay) || ! is_numeric($iCal_delay)) $iCal_delay = -1;
}

if (@file('Spreadsheet/Excel/Writer.php', FILE_USE_INCLUDE_PATH) === FALSE) $generation_excel = false;
else{
	$generation_excel = true;
	$repertoire = getcwd()."/".$prefix."stats";
	if ( ! is_writable($repertoire)) $message .= "<li>".sprintf($_SESSION['s_language']['init_config_excel_file_user'], $repertoire)."</li>\n";
}

displayErrorsIfAny($message, "", sprintf($_SESSION['s_language']['init_config_general_msg3'], './config/configure_annexe.php'));

$URL_overlib = "external_softwares/overlib/";
$fpdf_location = "external_softwares/fpdf/";


/************************************************************************************************************************
*															*
*															*
*				2nd part: test on existence of database	table and a minimum of data			*
*					and test on consistency of the database tables data				*
*															*
*															*
*************************************************************************************************************************/

if ($do_database_test) require_once('database_checks.php');



/************************************************************************************************************************
*															*
*															*
*				3rd part: everything is OK, is the user has read-only access or not ?			*
*															*
*															*
*************************************************************************************************************************/

$remoteIP = $_SERVER['REMOTE_ADDR'];
if (strstr($remoteIP, ', ')) {
	$ips = explode(', ', $remoteIP);
	$remoteIP = $ips[0];
}

if ( ! $read_only_enable) $read_only = false;
else{
	$inter = true;
	if ( (isset($IP_regex)) && ($IP_regex != "") ){
	    preg_match("/".$IP_regex."/", $remoteIP, $matches);
	    if (count($matches) == 1) $inter = false;
	}
	if ($inter){
	    if ( isset($enabled_domain) && (count($enabled_domain) != 0) && ( ! ( (count($enabled_domain) == 1) && ($enabled_domain == "") ))){
	        $fullhost = gethostbyaddr($remoteIP);
	        for ($i = 0; $i < count($enabled_domain); $i++){
	            if (substr($fullhost, strlen($enabled_domain[$i])*(-1)) == $enabled_domain[$i]) $inter = false;
	        }
	    }
	}
	$read_only = $inter;
}













/************************************************************************************************************************
*															*
*															*
*				4rd part: Functions									*
*															*
*															*
*************************************************************************************************************************/

function exitMsg($msg){
	global $error_message_begin, $body_end;

	exit($error_message_begin.$msg.$body_end);
}


function displayErrorsIfAny($message, $javascript, $link){
	if (! empty($message)){
	    if (substr_count($message, '<li>') != 1) $message = $_SESSION['s_language']['init_config_general_msg1']."</p>\n<ul class='red'>\n".$message."</ul></li>";
	    else $message = $_SESSION['s_language']['init_config_general_msg2']."</p>\n<ul class='red'>\n".$message."</ul>";
	    if ($javascript != '') $message = $javascript.$message;
	    if ($link != '') $message = $message.$link;
	    exitMsg($message);
	}
}

function constructionListe($resultat, $column){

	$list = "";
	while ($row = database_fetch_object($resultat)){
	    $list .= "'".database_get_from_object($row, $column)."', ";
	}
	$list = substr($list, 0, -2);
	if (empty($list)) $list = "''";
	return $list;
}
?>
