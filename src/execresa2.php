<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File execresa2.php
*
* This file is used to execute the creation a periodical reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
require_once('commun/commun_execresa.php');
if ($read_only)	exit($exit_message_authentification);
if ($iCal) require_once('wiCal.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 21) || (count($_GET) != 0)) exitWrongSignature('execresa2.php');
$availableClass = getAvailableClass();
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	$tab = createObjectList($objet);
	$objets = $tab[0];
	$nbObjets = $tab[1];
	$objets_list = $tab[2];
	$objetDisplay = $tab[3];
	$availableObjects = getAvailableObjects();
	for ($i = 0 ; $i < count($objets) ; $i++){
		if ( ! in_array($objets[$i], $availableObjects) ) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');
if (isset($_POST['date'])){
	$date = $_POST['date'];
	if ( ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $date)) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['email'])){
	$email = $_POST['email'];
	if ( ! validate_email($email)) exitWrongSignature('execresa2.php');
	if (strlen($email) > 128) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['pass'])){
	$pass = $_POST['pass'];
	if (strlen($pass) > 20) exitWrongSignature('execresa2.php');
	$pass = database_real_escape_string($pass);
	$pass = htmlspecialchars($pass);
} else exitWrongSignature('execresa2.php');
if (isset($_POST['debut'])){
	$debut = $_POST['debut'];
	if ( ( ! ereg ("([0-9]{2}):([0-9]{2}):([0-9]{2})", $debut)) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['titre'])){
	$titre = $_POST['titre'];
	if (strlen($titre) > 64) exitWrongSignature('execresa2.php');
	$titre = database_real_escape_string($titre);
	$titre = htmlspecialchars($titre);
} else exitWrongSignature('execresa2.php');
if (isset($_POST['commentaire'])){
	$commentaire = $_POST['commentaire'];
	if (strlen($commentaire) > 255) exitWrongSignature('execresa2.php');
	$commentaire = database_real_escape_string($commentaire);
	$commentaire = htmlspecialchars($commentaire);
} else exitWrongSignature('execresa2.php');
if (isset($_POST['multi'])){
	$multi = $_POST['multi'];
	if ( ! in_array($multi, array('0', '1', '7', '14', '-1')) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['datefintxt'])){
	$datefintxt = $_POST['datefintxt'];
	if ( ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $datefintxt)) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['classeResa'])){
	$classeResa = $_POST['classeResa'];	// usefull only if $nbObjets = 1
	if ( ! in_array($classeResa, $availableClass) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['objetWithPriorityManagementExist'])){
	$objetWithPriorityManagementExist = $_POST['objetWithPriorityManagementExist'];
	if ( ! in_array($objetWithPriorityManagementExist, array('0', '1')) && ($objetWithPriorityManagementExist != "") ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['priority'])){
	$priority = $_POST['priority'];
	if ( ! in_array($priority, array('0', '1')) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['nbConflit'])){
	$nbConflit = $_POST['nbConflit'];
	if ( ! ctype_digit($nbConflit) || ($nbConflit == '') ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['wifi'])){
	$wifi = $_POST['wifi'];
	if ( ! in_array($wifi, array('0', '1')) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if (isset($_POST['diffusion'])){
	$diffusion = $_POST['diffusion'];
	if ( ! in_array($diffusion, array('0', '1')) ) exitWrongSignature('execresa2.php');
} else exitWrongSignature('execresa2.php');
if ($multi == '-1'){
	if (isset($_POST['fin'])){
		$fin = $_POST['fin'];
		if ( ( ! ereg ("([0-9]{2}):([0-9]{2}):([0-9]{2})", $fin)) ) exitWrongSignature('execresa2.php');
	} else exitWrongSignature('execresa2.php');
} else {
	if (isset($_POST['duree'])){
		$duree = $_POST['duree'];
		if ( ( ! ereg ("([0-9]{2}):([0-9]{2}):([0-9]{2})", $duree)) ) exitWrongSignature('execresa2.php');
	} else exitWrongSignature('execresa2.php');
}
if (isset($_POST['objectParameter'])){
	$objectParameter = unserialize(urldecode(stripslashes($_POST['objectParameter'])));
	for ($i = 0 ; $i < count($objectParameter) ; $i++){
		$temp_array = $objectParameter[$i];
		$idobjet_tmp = $temp_array['idobjet'];
		if ( ! ctype_digit($idobjet_tmp) || ($idobjet_tmp == '') ) exitWrongSignature('execresa2.php');
		if ( ! in_array($temp_array['objetPriority'], array('0', '1')) ) exitWrongSignature('execresa2.php');
		if ( ! in_array($temp_array['objetWifi'], array('0', '1')) ) exitWrongSignature('execresa2.php');
		if ( ! in_array($temp_array['valide'], array('0', '1')) ) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');
if (isset($_POST['classeForICal'])){
	$classeForICal = unserialize(urldecode(stripslashes($_POST['classeForICal'])));
	for ($i = 0 ; $i < count($classeForICal) ; $i++){
		if ( ! in_array($classeForICal[$i], $availableClass) ) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');
if (isset($_POST['adminMail'])){
	$adminMail = unserialize(urldecode(stripslashes($_POST['adminMail'])));
	for ($i = 0 ; $i < count($adminMail) ; $i++){
		if ( ! validate_email($adminMail[$i])) exitWrongSignature('execresa2.php');
		if (strlen($adminMail[$i]) > 128) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');
if (isset($_POST['langueAdminMail'])){
	$langueAdminMail = unserialize(urldecode(stripslashes($_POST['langueAdminMail'])));
	$availableLanguages = getAvailableLanguages();
	for ($i = 0 ; $i < count($langueAdminMail) ; $i++){
		if ( ! in_array($langueAdminMail[$i], $availableLanguages) ) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');
if (isset($_POST['classeAdminMail'])){
	$classeAdminMail = unserialize(urldecode(stripslashes($_POST['classeAdminMail'])));
	for ($i = 0 ; $i < count($classeAdminMail) ; $i++){
		if ( ! in_array($classeAdminMail[$i], $availableClass) ) exitWrongSignature('execresa2.php');
	}
} else exitWrongSignature('execresa2.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$nbAdminMail = count($adminMail);

echo $entete;
?>

<body style='font-size:small'>

<?php
$idebut = substr($debut,0,2);
$midebut = substr($debut,3,2);
if ($midebut == "30") $idebut = $idebut + .5;

if ($multi == -1) {
	$ifin = substr($fin,0,2);
	$mi = substr($fin,3,2);
	if ($mi == "30") $ifin = $ifin + .5;
} else {
	$iduree = substr($duree,0,2);
	$mi = substr($debut,3,2);
	if ($mi == "30") $iduree = $iduree + .5;
	$ifin = $idebut + $iduree;
}

$listedates = "";
$listedatesAdmin = array();
$dateBeginAdmin = array();
$dateBegin = "";
$cpt = 0;
$cptCollision = 0;
$ladate = getdate(mktimeFromTXTdate($date));
$ladatebegintxt = $date;
$ladatetxt = $ladatebegintxt;
while ($ladatetxt <= $datefintxt){

	if ( ! $week_end_enabled){
	    if ( ! $_SESSION['s_jour_travail'][getDayInWeek($ladatetxt)]){
	        $tmp = computeNextDayToReserve($multi, $ladate);
	        $ladate = $tmp[0];
	        $ladatetxt = $tmp[1];
	        continue;
	    }
	}

	/***************************************************************************
	****                Check if time available                             ****
	***************************************************************************/
	$valable = check_time_available_on_multiple_reservation_for_one_day($ladatetxt, $objets_list, $idebut, $ifin, $multi, $ladatebegintxt, $datefintxt);

	/***************************************************************************
	****                Insert into database                                ****
	***************************************************************************/
	if($valable == 1){
	    $date1 = displayCurrentLanguageDate($ladatetxt, 0);

	    if($cpt == 0){
	        if ($multi == -1){
	            $duree = 24-$idebut;
	            if ($midebut=="30"){
	                $duree = $duree - 0.5;
	                $duree = fillWithZero($duree);
	                $duree .= ":30:00";
	            } else{
	                $duree = fillWithZero($duree);
	                $duree .= ":00:00";
	            }
	            $dateBegin = $date1." (".substr($debut,0,5).")";
	        } else $listedates .= "- ".$date1." \n";

	        $DB_request = "INSERT INTO reservation (idobjet,jour,debut,duree,email,titre,commentaire,valide,pass,priority,wifi,state,diffusion)";
	        $DB_request .= "VALUES ('".$objectParameter[0]['idobjet']."','$ladatetxt','$debut','$duree','$email','$titre','$commentaire','".$objectParameter[0]['valide']."','$pass','".$objectParameter[0]['objetPriority']."','".$objectParameter[0]['objetWifi']."',0, $diffusion)";
	        database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	        $DB_request = "SELECT R.id FROM reservation R, objet O WHERE R.idobjet = O.id AND R.jour = '$ladatetxt' AND O.nom IN ($objets_list) AND debut='$debut'";
	        $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	        $Inforesa = database_fetch_object($resultat);
	        $infoidmulti= database_get_from_object($Inforesa, 'id');
	        $DB_request = "UPDATE reservation SET idmulti='$infoidmulti' WHERE id='$infoidmulti'";
	        database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	        for ($i = 1 ; $i < $nbObjets ; $i++){
	            $DB_request = "INSERT INTO reservation (idobjet,jour,debut,duree,email,titre,commentaire,valide,pass,priority,idmulti,wifi,state,diffusion)";
	            $DB_request .= "VALUES ('".$objectParameter[$i]['idobjet']."','$ladatetxt','$debut','$duree','$email','$titre','$commentaire','".$objectParameter[$i]['valide']."','$pass','".$objectParameter[$i]['objetPriority']."','$infoidmulti','".$objectParameter[$i]['objetWifi']."', 0, $diffusion)";
	            database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	        }

	        for ($i = 0 ; $i < $nbAdminMail ; $i++){
	            $date2 = displayDate($ladatetxt, $langueAdminMail[$i], 0);
	            if ($multi == -1) $dateBeginAdmin[$i] = "$date2 (".substr($debut,0,5).")";
	            else $listedatesAdmin[$i] = "- ".$date2." \n";
	        }

	    } else {
	        if ($multi == -1){
	            $debut = "00:00:00";
	            if ($ladatetxt == $datefintxt){
	                $duree = $fin;
	            } else {
	                $duree = "24:00:00";
	            }
	            $dateEnd = $date1." (".substr($fin,0,5).")";
	        } else $listedates .= "- ".$date1." \n";

	        for ($i = 0 ; $i < $nbObjets ; $i++){
	            $DB_request = "INSERT INTO reservation (idobjet,jour,debut,duree,email,titre,commentaire,valide,pass,priority,idmulti,wifi,state,diffusion)";
	            $DB_request .= "VALUES ('".$objectParameter[$i]['idobjet']."','$ladatetxt','$debut','$duree','$email','$titre','$commentaire','".$objectParameter[$i]['valide']."','$pass','".$objectParameter[$i]['objetPriority']."','$infoidmulti','".$objectParameter[$i]['objetWifi']."',0, $diffusion)";
	            database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	        }

	        for ($i = 0 ; $i < $nbAdminMail ; $i++){
	            $date2 = displayDate($ladatetxt, $langueAdminMail[$i], 0);
	            if ($multi == -1) $dateEndAdmin[$i] = "$date2 (".substr($fin,0,5).")";
	            else $listedatesAdmin[$i] .= "- ".$date2." \n";
	        }
	    }
	    $cpt++;
	} else $cptCollision++;

	$tmp = computeNextDayToReserve($multi, $ladate);
	$ladate = $tmp[0];
	$ladatetxt = $tmp[1];
}


// La rÃ©servation est annulÃ©e si aucune date n'a Ã©tÃ© retenue ou s'il s'agit d'une rÃ©servation continue si au moins une date Ã©tait impossible
if (($cpt == 0) || (($multi == -1) && ($cptCollision != 0))){
	if ($cpt != 0){
	    $DB_request = "DELETE FROM reservation WHERE idmulti = '$infoidmulti'";
	    database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	}?>
<div>
	<span class='lblue'>
	    <?php echo $_SESSION['s_language']['execresa2_all_conflit'];?>
	    </span>
	    <br /><br />
	<?php echo $_SESSION['s_language']['execresa2_conflit'];?>
</div>
<?php
} else {
	/**********************************************************************************************************
	****                        Part email send                                                            ****
	***********************************************************************************************************/
	$from = "From: PHPMyResa";
	$lien = WEB."invitevalide.php?idmulti=$infoidmulti";

	/***************************************************************************
	****                        Mail for the administrator                  ****
	****************************************************************************/
	$sendAdminList = "";
	$sendAdminErrorList = "";
	for ($i = 0 ; $i < $nbAdminMail ; $i++){
	    $inter = getMailMessageForAdminInSpecifiedLanguage($langueAdminMail[$i]);

	    $sujet = $inter['execresa_mail_subject']." ".$objetDisplay;
	    if ($mail_subject != "short") $sujet .= ", '$titre'";
	    if ($mail_subject == "long"){
	        $sujet .= " ".$inter['par']." $email - ";
	        if ($multi == -1){
	            $sujet .= $inter['reservation_continue']." ";
	            $sujet .= $inter['execresa_from']." $dateBeginAdmin[$i] ";
	            $sujet .= $inter['execresa_to']." $dateEndAdmin[$i]";
	        } else{
	            $sujet .= $inter['reservation_periodic_strict'];
	            $sujet .= " ".$inter['execresa_from2']." ".substr($debut,0,5)." ".$inter['pendant']." ".substr($duree,0,5).", ";
	            $temp = explode('-', $listedatesAdmin[$i]);
	            $sujet .= $inter['execresa_from']." ".trim($temp[1])." ".$inter['execresa_to']." ".trim($temp[count($temp)-1]);
	            $sujet .= ", ".$inter['title_array9'].$inter['ponctuation']." $multi";
	        }
	    }
	    $sujet = sans_accent($sujet);

	    $contenu = sprintf($inter['execresa_mail_admin'], $classeAdminMail[$i])."\n\n";
	    $contenu .= sprintf($_SESSION['s_language']['execresa_booking'], $objetDisplay)."\n";
	    $contenu .= $inter['execresa_mail_title']." $titre\n";

	    if ($multi == -1){
	        $contenu .= $inter['reservation_continue']." ";
	        $contenu .= $inter['execresa_from']." $dateBeginAdmin[$i] ";
	        $contenu .= $inter['execresa_to']." $dateEndAdmin[$i]\n\n";
	    } else {
	        $contenu .= $inter['reservation_periodic_strict']."\n";
	        $contenu .= $inter['execresa_mail_start_time']." ".substr($debut,0,5)."\n";
	        $contenu .= $inter['execresa_mail_duration']." ".substr($duree,0,5)."\n";
	        $contenu .= $inter['execresa2_dates']." \n$listedatesAdmin[$i]\n\n";
	    }

	        $contenu .= $inter['execresa_mail_user']." $email\n";
	    if ($commentaire != '') $contenu .= $inter['title_array12']." $commentaire\n";
	    if ($objetWithPriorityManagementExist){
	        $messagePriority = getPriorityMessageInSpecifiedLanguage($objets, $langueAdminMail[$i]);
	        if ($priority == '1') $contenu .= "\n".$inter['execresa_mail_priority']." ($messagePriority).\n";
	        else $contenu .= "\n".$inter['execresa_mail_no_priority']." ($messagePriority).\n";
	    }
	    if ($wifi == '1') $contenu .= "\n".$inter['execresa_mail_wifi']."\n";
	    $contenu .= "\n".sprintf($inter['execresa_mail_valid'], $lien)."\n";
	    $contenu = ereg_replace("\\\'","'",$contenu);
	    $contenu = ereg_replace("\\\\\"","\"",$contenu);
	    $destinataire = str_replace($web_server, $mail_server, $adminMail[$i]);
	    $sendAdmin = mail($destinataire,$sujet,$contenu,$from);
	    if ($sendAdmin) $sendAdminList .= $adminMail[$i].", ";
	    else $sendAdminErrorList .= $adminMail[$i].", ";
	}

	$temp = $_SESSION['s_language']['and'].' '.$_SESSION['s_language']['to'];
	if ($sendAdminList != ""){
	    $sendAdminList = substr($sendAdminList, 0, -2);
	    $sendAdminList = replaceLastOccurenceOfComaSpecial($sendAdminList, $temp);
	}
	if ($sendAdminErrorList != ""){
	    $sendAdminErrorList = substr($sendAdminErrorList, 0, -2);
	    $sendAdminErrorList = replaceLastOccurenceOfComaSpecial($sendAdminErrorList, $temp);
	}

	/***************************************************************************
	****            Mail for the user                                       ****
	***************************************************************************/

	$sujet = $_SESSION['s_language']['execresa_mail_subject']." ".$objetDisplay;
	if ($mail_subject != "short") $sujet .= ", '$titre', ";
	if ($mail_subject == "long"){
	    if ($multi == -1){
	        $sujet .= $_SESSION['s_language']['reservation_continue']." ";
	        $sujet .= $_SESSION['s_language']['execresa_from']." $dateBegin ";
	        $sujet .= $_SESSION['s_language']['execresa_to']." $dateEnd";
	    } else {
	        $sujet .= $_SESSION['s_language']['reservation_periodic_strict'];
	        $sujet .= " ".$_SESSION['s_language']['execresa_from2']." ".substr($debut,0,5)." ".$_SESSION['s_language']['pendant']." ".substr($duree,0,5).", ";
	        $temp = explode('-', $listedates);
	        $sujet .= $_SESSION['s_language']['execresa_from']." ".trim($temp[1])." ".$_SESSION['s_language']['execresa_to']." ".trim($temp[count($temp)-1]);
	        $sujet .= ", ".$inter['title_array9'].$inter['ponctuation']." $multi";
	    }
	}
	$sujet = sans_accent($sujet);

	$contenu = $_SESSION['s_language']['execresa_mail_to_user']."\n\n";
	if ($nbAdminMail == '0') $contenu .= $_SESSION['s_language']['execresa_mail_request']."\n\n";
	else $contenu .= $_SESSION['s_language']['execresa_mail_validation']."\n\n";
	$contenu .= sprintf($_SESSION['s_language']['execresa_booking'], $objetDisplay)."\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_title']." $titre\n";
	if ($multi == -1){
	    $contenu .= $_SESSION['s_language']['reservation_continue']." ";
	    $contenu .= $_SESSION['s_language']['execresa_from']." $dateBegin ";
	    $contenu .= $_SESSION['s_language']['execresa_to']." $dateEnd\n\n";
	} else {
	    $contenu .= $_SESSION['s_language']['reservation_periodic_strict']."\n";
	    $contenu .= $_SESSION['s_language']['execresa_mail_start_time']." ".substr($debut,0,5)." \n";
	    $contenu .= $_SESSION['s_language']['execresa_mail_duration']." ".substr($duree,0,5)." \n";
	    $contenu .= $_SESSION['s_language']['execresa2_dates']." \n$listedates\n\n";
	}
	if ($commentaire != '') $contenu .= $_SESSION['s_language']['title_array12']." $commentaire\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_pass']." $pass\n";
	if ($objetWithPriorityManagementExist){
	    $messagePriority = getPriorityMessage($objets);
	    if ($priority=='1') $contenu .= "\n".$_SESSION['s_language']['execresa_mail_priority']." ($messagePriority).\n";
	    else $contenu .= "\n".$_SESSION['s_language']['execresa_mail_no_priority']." ($messagePriority). ".$_SESSION['s_language']['execresa_mail_no_priority_bis']."\n";
	}
	if ($wifi == '1') {
	    $wifiPassword = strrev(substr($pass, 1, -1));
	    $contenu .= "\n".$_SESSION['s_language']['execresa_mail_wifi']." ".sprintf($_SESSION['s_language']['execresa_mail_wifi2'], $wifiPassword)."\n";
	}
	$contenu .= "\n";
	if ($modification_enable) $contenu .= sprintf($_SESSION['s_language']['execresa_mail_modify4'], $lien);
	else $contenu .= sprintf($_SESSION['s_language']['execresa_mail_modify3'], $lien);
	$contenu = ereg_replace("\\\'","'",$contenu);
	$contenu = ereg_replace("\\\\\"","\"",$contenu);
	$destinataire = str_replace($web_server, $mail_server, $email);
	$sendUser = mail($destinataire,$sujet,$contenu,$from);

	/***************************************************************************
	****                   Display in PHPMyResa                             ****
	****************************************************************************/
?>
<div>
	<span class='lblue'>
	    <?php echo $_SESSION['s_language']['execresa_mail_done'];?>
	</span>
	<br /><br />
<?php
	if ($nbAdminMail != 0){
		echo $_SESSION['s_language']['execresa_mail_validation']."<br />";
		if ($sendAdminList != "") echo " ".$_SESSION['s_language']['execresa_mail_envoye']." ".$sendAdminList;
		if ($sendAdminErrorList != "") echo " ".$_SESSION['s_language']['execresa_mail_mal_envoye']." ".$sendAdminErrorList." ".$_SESSION['s_language']['commun_error_mysql_contact1']." ".$technical_contact." (".$technical_tel.") ".$_SESSION['s_language']['commun_error_mysql_contact2'];
		echo "<br /><br />";
	}
	echo $_SESSION['s_language']['execresa_mail_pass1'];?>

	<span class='bred' style='font-size:medium'><?php echo $pass;?></span><br />

	<?php
	if ($modification_enable)
	    echo $_SESSION['s_language']['execresa_mail_modify7'];
	else
	    echo $_SESSION['s_language']['execresa_mail_modify8'];
	echo "<br />";

	if ($sendUser)
	    echo $_SESSION['s_language']['execresa_mail_pass4'];
	else
	    echo $_SESSION['s_language']['execresa_mail_pass5']." ".$_SESSION['s_language']['execresa_mail_pass6']." (".$email.").";

	if ($nbConflit != $cptCollision) {?>
	<br /><br />
	<b><?php echo $_SESSION['s_language']['execresa2_conflit2'];?></b>
<?php
	}?>
</div>

<?php
}?>
<div>
	<br /><br /><br />
	<a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
</div>

<?php
if ($iCal){
	for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
}

echo $body_end;
?>
