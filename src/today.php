<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File today.php
*
* This file is used to display the planing of the current date
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Daniel Charnay
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if (( (count($_GET) != 1) && (count($_GET) != 0) ) || (count($_POST) != 0)) exitWrongSignature('today.php');
if (isset($_GET['offset'])){
	$offset = $_GET['offset'];
	if ($offset == '-') exitWrongSignature('today.php');
	else if (substr($offset, 0, 1) == '-'){
		$tmp_offset = substr($offset, 1);
		if ( ! ctype_digit($tmp_offset) || ($tmp_offset == '') ) exitWrongSignature('today.php');
	} else if ( ! ctype_digit($offset) || ($offset == '') ) exitWrongSignature('today.php');

}else $offset = 0;

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

echo $entete;
echo "<body>";

$dateChoisie = date("Y-m-d", strtotime($offset." days",strtotime(date("Y-m-d"))));
$dateChoisieDisplay = displayCurrentLanguageDate($dateChoisie, 0);

$DB_request = "SELECT count(*) AS count FROM reservation WHERE jour = '$dateChoisie' AND state=0";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$row = database_fetch_object($resultat);
$nb = database_get_from_object($row, 'count');

if ($offset == 0)       $msg1 = $_SESSION['s_language']['calendrier_menu2'];
else if ($offset == -1) $msg1 = $_SESSION['s_language']['calendrier_menu4'];
else if ($offset == 1)  $msg1 = $_SESSION['s_language']['calendrier_menu5'];
else $msg1 = ucfirst($dateChoisieDisplay);

if ($nb <2) $msg2 = $_SESSION['s_language']['reservation'];
else        $msg2 = $_SESSION['s_language']['reservations'];

echo "<h2 class='centered'>$msg1 $nb $msg2<br /></h2>";

echo "<table class='noborder' style='width:100%'>";
$nbColumn = 2;
$DB_request = "SELECT id, nom FROM classe ORDER BY id";
$resultatClasse = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($rowClasse = database_fetch_object($resultatClasse)) {
	echo "<tr><td colspan='$nbColumn'>&nbsp;</td></tr>";
	echo "<tr style='background-color:gray'>";
	echo "<td colspan='$nbColumn' style='font-weight:bold;color:black'>";
	echo $_SESSION['s_language']['today_classe']," '",database_get_from_object($rowClasse, 'nom')."', ",$dateChoisieDisplay;
	echo "</td>";
	echo "</tr>";
	echo "<tr><td colspan='$nbColumn'>&nbsp;</td></tr>";

	$DB_request = "SELECT O.nom, R.titre, R.debut, R.duree, R.email, R.commentaire, R.valide, R.priority FROM reservation R, objet O, classe C ";
	$DB_request .= "WHERE jour = '$dateChoisie' AND R.idobjet = O.id AND O.id_classe = C.id AND C.id = ".database_get_from_object($rowClasse, 'id')." AND R.state=0 ";
	$DB_request .= "ORDER BY O.nom, R.debut";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	if ($row = database_fetch_object($resultat)) {
	    $couleur="#dddddd";
	    displayLine($row, $couleur);
	} else echo "<tr><td colspan='$nbColumn;>".$_SESSION['s_language']['today_nothing']."</td></tr>";

	while ($row = database_fetch_object($resultat)) {
	    if ($couleur=="#dddddd") $couleur="#cccccc"; else $couleur="#dddddd";
	    displayLine($row, $couleur);
	}
	echo "<tr><td colspan='$nbColumn'>&nbsp;</td></tr>";
}
echo "</table>";

echo $body_end;

function displayLine($row, $couleur){
	echo "  <tr bgcolor='$couleur'>";
	echo "<td><b>".database_get_from_object($row, 'nom')."</b></td>";
	echo "<td>'".database_get_from_object($row, 'titre')."' ".$_SESSION['s_language']['a']." ".
	    str_replace(':','h',substr(database_get_from_object($row, 'debut'), 0, 5))." ".$_SESSION['s_language']['pendant']." ".
	    str_replace(':','h',substr(database_get_from_object($row, 'duree'), 0, 5))." ".$_SESSION['s_language']['par']." ".
	    database_get_from_object($row, 'email');
	if (database_get_from_object($row, 'commentaire') != "") echo " ".(database_get_from_object($row, 'commentaire'));
	if (database_get_from_object($row, 'valide') == 0) echo " - ".$_SESSION['s_language']['today_not_yet_validated'];
	echo "</td>";
	echo "</tr>\n";
}
?>
