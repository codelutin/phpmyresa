<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File genexcel.php
*
* This file is used to generate an excel file from the result of a search
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @copyright	2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
require_once('Spreadsheet/Excel/Writer.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 4) || (count($_GET) != 0)) exitWrongSignature('genexcel.php');
if (isset($_POST['critere'])){
	$critere = $_POST['critere'];
	if ( ! in_array($critere, array('date', 'titre', 'email', 'objet', 'classe'))) exitWrongSignature('chercher.php');
} else exitWrongSignature('genexcel.php');
if (isset($_POST['type'])){
	$type = $_POST['type'];
	if ( ! ctype_digit($type) || ($type == '') ) exitWrongSignature('chercher.php');
	if ( $type > 4 ) exitWrongSignature('chercher.php');
} else exitWrongSignature('genexcel.php');
if (isset($_POST['valeurcritere'])){
	$valeurcritere = $_POST['valeurcritere'];
	$valeurcritere = database_real_escape_string($valeurcritere);
	$valeurcritere = htmlspecialchars($valeurcritere);
} else exitWrongSignature('genexcel.php');
if (isset($_POST['toutVoir'])){
	$toutVoir = $_POST['toutVoir'];
	if ( ($toutVoir != 'on') && ($toutVoir != '') ) exitWrongSignature('chercher.php');
} else exitWrongSignature('genexcel.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$DB_request = createSearchDBrequest($critere, $type, $valeurcritere, $toutVoir);
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$reservation = database_fetch_object($resultat) ;

echo $entete;
echo "<body>";

if ($reservation == NULL) echo $_SESSION['s_language']['genexcel_error'];
else {
	$fname = 'stats/tab.xls';
	$xls =& new Spreadsheet_Excel_Writer($fname);
	$sheet =& $xls->addWorksheet('Resa');
	$sheet->freezePanes(array(4, 0, 0, 0));

	$sheet->setColumn(1,1,10);
	$sheet->setColumn(2,2,18);
	$sheet->setColumn(3,3,10);

	$titleFormat =& $xls->addFormat();
	$titleFormat->setFontFamily('Helvetica');
	$titleFormat->setBold();
	$titleFormat->setSize('13');
	$titleFormat->setColor('navy');
	$titleFormat->setBottomColor('navy');

	$sheet->write(0, 0, strtoupper($_SESSION['s_language']['reservations']), $titleFormat);
	$sheet->write(2, 0, $_SESSION['s_language']['title_array1'], $titleFormat);
	$sheet->write(2, 1, $_SESSION['s_language']['title_array2'], $titleFormat);
	$sheet->write(2, 2, $_SESSION['s_language']['title_array3'], $titleFormat);
	$sheet->write(2, 3, $_SESSION['s_language']['title_array4'], $titleFormat);
	$sheet->write(2, 4, $_SESSION['s_language']['title_array6'], $titleFormat);
	$sheet->write(2, 5, $_SESSION['s_language']['title_array5'], $titleFormat);
	$sheet->write(2, 6, $_SESSION['s_language']['title_array8'], $titleFormat);

	$i=4;
	$maxLenght0 = 8;
	$maxLenght4 = 20;
	$maxLenght5 = 10;
	$maxLenght6 = 16;
	do{
		$objet = database_get_from_object($reservation, 'objet');
		$sheet->write($i, 0, $objet);
		$len0 = strlen($objet);
		if ($len0 > $maxLenght0) $maxLenght0 = $len0;
		$sheet->write($i, 1, database_get_from_object($reservation, 'jour'));
		$sheet->write($i, 2, database_get_from_object($reservation, 'debut'));
		$sheet->write($i, 3, database_get_from_object($reservation, 'duree'));
		$email = database_get_from_object($reservation, 'email');
		$sheet->write($i, 4, $email);
		$len4 = strlen($email);
		if ($len4 > $maxLenght4) $maxLenght4 = $len4;
		$titre = database_get_from_object($reservation, 'titre');
		$sheet->write($i, 5, $titre);
		$len5 = strlen($titre);
		if ($len5 > $maxLenght5) $maxLenght5 = $len5;
		$commentaire = database_get_from_object($reservation, 'commentaire');
		$sheet->write($i, 6, $commentaire);
		$len6 = strlen($commentaire);
		if ($len6 > $maxLenght6) $maxLenght6 = $len6;
		$reservation = database_fetch_object($resultat);
		$i++;
	} while($reservation != NULL);

	$sheet->setColumn(0,0,$maxLenght0);
	$sheet->setColumn(4,4,$maxLenght4);
	$sheet->setColumn(5,5,$maxLenght5);
	$sheet->setColumn(6,6,$maxLenght6);
	$xls->close();

	echo $_SESSION['s_language']['genexcel_download']." <a href='$fname'>$fname</a>";
}
?>
