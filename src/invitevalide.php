<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File invitevalide.php
*
* This file is used to ask confirmation and password to validate a periodical or a non periodical reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 Frédéric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only) exit($exit_message_authentification);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ((count($_GET) != 1) || (count($_POST) != 0)) exitWrongSignature('invitevalide.php');
if (isset($_GET['id'])){
	$id = $_GET['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('invitevalide.php');
	$DB_request0 = "SELECT R.titre, R.email FROM reservation R WHERE R.id=$id AND R.state=0";
	$DB_request1 = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority FROM reservation R, objet O WHERE R.idobjet = O.id AND R.id=$id AND R.state=0";
} else if (isset($_GET['idmulti'])){
	$idmulti = $_GET['idmulti'];
	if ( ! ctype_digit($idmulti) || ($idmulti == '') ) exitWrongSignature('invitevalide.php');
	$DB_request0 = "SELECT R.titre, R.email FROM reservation R WHERE R.idmulti=$idmulti AND R.state=0";
	$DB_request1 = "SELECT R.id, O.nom AS objet, R.jour, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority FROM reservation R, objet O WHERE R.idobjet = O.id AND R.idmulti=$idmulti ORDER BY jour ASC, nom AND R.state=0";
} else exitWrongSignature('invitevalide.php');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 	 	 **************
**********************************************************************************************/

echo $entete;
?>
<body style='font-size:small'>

<script type='text/javascript' src='<?php echo $URL_overlib;?>overlib.js'></script>
<div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>

<div>
<?php
$resultat = database_query($DB_request0, $connexionDB) or errorDB($DB_request0);
if ($reservation = database_fetch_object($resultat)){
	$title = database_get_from_object($reservation, 'titre');
	$title = ereg_replace("\\\\\"","\"",$title);
	$email = database_get_from_object($reservation, 'email');
	echo getResaJavascriptContent()."\n";
	echo getResaFormContent();


	$resultat = database_query($DB_request1, $connexionDB) or errorDB($DB_request1);
	$tab = getResaContent($resultat, false, true, true);

	$emailCrypte = PHPcrypt($email);
?>
	<span style='font-size:large'><?php echo $_SESSION['s_language']['execefface_mail_resa'];?> <?php echo $title;?></span>
	<br /><br />
	<?php echo $_SESSION['s_language']['invitevalide_you_can'];?>

	<ul>
	    <li><?php echo $_SESSION['s_language']['invitevalide_choix1'];?></li>
<?php
	if ($modification_enable) {?>
	    <li><?php echo $_SESSION['s_language']['invitevalide_choix2'];?></li>
<?php
	}?>
	    <li><?php echo $_SESSION['s_language']['invitevalide_choix3'];?></li>
	</ul>

	<p><span class='red'><?php echo $_SESSION['s_language']['invitevalide_comment'];?></span></p>

	<?php echo $tab[0];?>

	<p>
	    <b>
	        <script type='text/javascript'>
	        <!--
	            decrypt('', '<?php echo $emailCrypte;?>', '')
	        //-->
	       </script>
	    </b> <?php echo $_SESSION['s_language']['invitevalide_mail_notification'];?>
	</p>
<?php
} else {?>
	<span style='color:red;font-size:large' ><?php echo $_SESSION['s_language']['invitevalide_not_exists'];?></span>
	<br /><br />
<?php
}
?>

<a href='index.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
</div>
<?php echo $body_end;?>
