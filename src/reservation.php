<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File reservation.php
*
* This file is used to create a new reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only) exit($exit_message_authentification);

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ((($nbpar != 4) && ($nbpar != 5)) || (count($_GET) != 0)) exitWrongSignature('reservation.php');
if (isset($_POST['annee'])){
	$annee = $_POST['annee'];
	if (strlen($annee) > 4) exitWrongSignature('reservation.php');
	if ( ! ctype_digit($annee) || ($annee == '') ) exitWrongSignature('reservation.php');
} else exitWrongSignature('reservation.php');
if (isset($_POST['mois'])){
	$mois = $_POST['mois'];
	if ( ! ctype_digit($mois) || ($mois == '') ) exitWrongSignature('reservation.php');
	if ($mois > 12) exitWrongSignature('reservation.php');
} else exitWrongSignature('reservation.php');
if (isset($_POST['jour'])){
	$jour = $_POST['jour'];
	if ( ! ctype_digit($jour) || ($jour == '') ) exitWrongSignature('reservation.php');
	if ($jour > 31) exitWrongSignature('reservation.php');
} else exitWrongSignature('reservation.php');
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	$tab = createObjectList($objet);
	$objets = $tab[0];
	$nbObjets = $tab[1];
	$objets_list = $tab[2];
	$objetDisplay = $tab[3];
	$availableObjects = getAvailableObjects();
	for ($i = 0 ; $i < count($objets) ; $i++){
		if ( ! in_array($objets[$i], $availableObjects) ) exitWrongSignature('reservation.php');
	}
} else exitWrongSignature('reservation.php');
if (isset($_POST['periodicityType'])){
	$periodicityType  = $_POST['periodicityType'];
	if ( ! in_array($periodicityType, array('repetitif', 'continue')) ) exitWrongSignature('reservation.php');
} else $periodicityType = 'repetitif';

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$debut = $default_beginning_hour;
$midebut = $default_beginning_mihour;
if (isset($_COOKIE['resa_email'])) $email = $_COOKIE['resa_email']; else $email = $email_domain;
$titre = '';
$commentaire = $default_comment;


// Pour Patricia
/*
$DB_request = "SELECT C.nom FROM classe C, objet O WHERE O.nom = '$objet' AND O.id_classe = C.id";
$resultat = mysql_query($DB_request, $connexionDB) or errorDB($DB_request);
$row = mysql_fetch_object($resultat);
$objectClasse = $row->nom;
$objetVehicule = false;
if (strlen($objectClasse) >= 8){
	if (substr($objectClasse,0,8) == "VEHICULE") $objetVehicule = true;
}
if ($objetVehicule) $commentaire ='Conducteur1 :
Conducteur2 :
Destination :
Groupe :
Passager :
';
else $commentaire ='Laboratoire ou entitÃ© :
Groupe :
';
*/


function displayMessageWithURL($msg, $link, $URL) {
// fonction utlisÃ©e pour documenter le choix entre rÃ©servations continues et rÃ©servations ponctuelles ou rÃ©pÃ©titives
	$msg = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$msg);
	return "<a href=\"$link\" onmouseover=\"return overlib('$msg', CAPTION, '', CENTER, OFFSETY, 20, TEXTSIZE, '12px', FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);\" onmouseout='return nd();' class='es3'>$URL</a>";
}


$objectPriority = false;
$objectwifi = false;
$DB_request = "SELECT priority, wifi FROM objet WHERE nom IN ($objets_list)";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)) {
	if (database_get_from_object($row, 'priority') == 1) $objectPriority = true;
	if (database_get_from_object($row, 'wifi') == 1) $objectwifi = true;
}
if ($objectPriority) $priorityMessage = getPriorityMessage($objets);

echo $entete;
?>

<body style='font-size:small'>

<script type='text/javascript' src='<?php echo $URL_overlib;?>overlib.js'></script>

<div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>


<script type='text/javascript'>
<!--

function sComplet() {
	msg = "";
	titre = document.forms.resa.titre.value;
	if (titre == "") {
	    msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_error_title_J'];?>"+"\n";
	}
	if (titre.length > 64) {
	    msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_error_title2_J'];?>"+"\n";
	}
	var email = document.forms.resa.email.value;
	if (email == "" || email == "<?php echo $email_domain;?>") {
	    msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_error_email_1_J'];?>"+"\n";
	}
	if ( ! emailIsCorrect(email)) {
	    msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_error_email_2_J'];?>"+"\n";
	}

<?php
	if ($periodicityType == 'continue') { ?>
	    if (document.forms.resa.dateFin.value=='') {
	        msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_message_continuous_end_date_J'];?>"+"\n";
	    }
	    if (document.forms.resa.fin.options[document.forms.resa.fin.selectedIndex].value=='0' && document.forms.resa.mifin.options[document.forms.resa.mifin.selectedIndex].value=='00') {
	        msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_message_continuous_end_time_J'];?>"+"\n";
	    }
	    if (document.forms.resa.fin.options[document.forms.resa.fin.selectedIndex].value=='24' && document.forms.resa.mifin.options[document.forms.resa.mifin.selectedIndex].value=='00') {
	        if (document.forms.resa.debut.options[document.forms.resa.debut.selectedIndex].value=='0' && document.forms.resa.midebut.options[document.forms.resa.midebut.selectedIndex].value=='00') {
	            msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_message_continuous_bad_times_J'];?>"+"\n";
	        }
	    }
<?php
	} else { ?>
	    if (document.forms.resa.multi.options[document.forms.resa.multi.selectedIndex].value!='0' && document.forms.resa.dateFin.value=='') {
	        msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_message_end_date_J'];?>"+"\n";
	    }
	    if (document.forms.resa.duree.options[document.forms.resa.duree.selectedIndex].value=='0' && document.forms.resa.miduree.options[document.forms.resa.miduree.selectedIndex].value=='00') {
	        msg = msg+"- "+"<?php echo $_SESSION['s_language']['reservation_error_duration_J'];?>"+"\n";
	    }

	    total_heures = Number(document.forms.resa.debut.options[document.forms.resa.debut.selectedIndex].value)
	                 + Number(document.forms.resa.duree.options[document.forms.resa.duree.selectedIndex].value);
	    total_minutes = Number(document.forms.resa.midebut.options[document.forms.resa.midebut.selectedIndex].value)
	                  + Number(document.forms.resa.miduree.options[document.forms.resa.miduree.selectedIndex].value);
	    if (total_minutes == 0) {
	        terminaison = total_heures;
	    } else if (total_minutes == 30) {
	        terminaison = total_heures + 0.5;
	    } else if (total_minutes == 60) {
	        terminaison = total_heures + 1;
	    }
	    if ( terminaison > 24) {
	        msg = msg+"- "+"<?php echo $_SESSION['s_language']['execresa_error'];?>  ("+
	        total_heures+ " h  "+total_minutes+" mn) \n";
	    }
<?php
	} ?>
	if (msg != "") {
	    alert (msg);
	} else {
	    document.forms.resa.submit();
	}
	return;
}

function chargerType(type) {
	document.forms.refreshFromReservationPeriodicityChange.periodicityType.value = type;
}

function calendarDateFin(test) {
	// test = true si la rÃ©servation est rÃ©pÃ©titive, false si elle est continue
	if (test && (document.forms.resa.multi.selectedIndex == 0)) {
	    alert("<?php echo $_SESSION['s_language']['reservation_periodic_J'];?>");
	    return;
	}
	url = 'selectionDateFin.php';
	if (document.forms.resa.aa.value != '' && document.forms.resa.aa.value != '  ') url = url+"?annee="+document.forms.resa.aa.value+"&mois="+document.forms.resa.mm.value+"&type=0";
	else url = url+"?annee=<?php echo $annee;?>&mois=<?php echo $mois;?>"+"&type=0";

	<?php if ($ie) echo "width = 200; height = 240;"; else echo "width = 185; height = 200;";?>
	x = (640 - width)/2, y = (480 - height)/2;
	if (screen)
	{
	    y = (screen.availHeight - height)/2;
	    x = (screen.availWidth - width)/2;
	    if (screen.availWidth > 1800) x = ((screen.availWidth/2) - width)/2;
	}
	window.open(url,'Date_fin','width='+width+',height='+height+',screenX='+x+',screenY='+y+',top='+y+',left='+x+',resizable=no,scrollbars=no');
}

function multiChange() {
	if (document.forms.resa.multi.selectedIndex == 0) {
	    document.forms.resa.dateFin.value='';
	    document.forms.resa.jj.value='';
	    document.forms.resa.mm.value='';
	    document.forms.resa.aa.value='';
	}
}

function changerFond(id, couleur) {
	    document.getElementById("td_"+id).style.background = couleur;
}

//-->
</script>

<?php

echo getResaJavascriptContent();

$arrSearch = Array ('\\\'','\"');
$arrReplace = Array('\'','"');
$titre = str_replace($arrSearch, $arrReplace, $titre);
$commentaire = str_replace($arrSearch, $arrReplace, $commentaire);

$currentDate = displayCurrentLanguageDate($annee."-".$mois."-".$jour, 0);
?>

<form id='refreshFromReservationPeriodicityChange' method='post' action='reservation.php'>
<p>
	<input type='hidden' name='annee' value='<?php echo $annee;?>' />
	<input type='hidden' name='mois' value='<?php echo $mois;?>' />
	<input type='hidden' name='jour' value='<?php echo $jour;?>' />
	<input type='hidden' name='objet' value='<?php echo $objet;?>' />
	<input type='hidden' name='periodicityType' value='<?php echo $periodicityType;?>' />
</p>
</form>

<div>
<span  style='font-size:large'><?php echo $objetDisplay, " ", $currentDate;?></span>
<br />
<?php
if ($objectPriority) {
	if ($nbObjets == 1) {
	    echo $_SESSION['s_language']['reservation_priority_message'];
	} else {
	    echo $_SESSION['s_language']['reservation_priority_message2'];
	}
} ?>

</div>

<?php
$date = makeDateWithZero($annee, $mois, $jour);
$DB_request = "SELECT R.id, R.debut, R.duree, R.commentaire, R.titre, R.email, R.valide, R.priority, O.nom AS objet FROM reservation R, objet O ";
$DB_request .= "WHERE R.idobjet = O.id AND R.jour = '$date' AND O.nom IN ($objets_list) AND R.state=0 ORDER BY R.debut";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
if ($nbObjets == 1)
    $tab = getResaContent($resultat, true, false, false);
else
    $tab = getResaContent($resultat, true, false, true);

if ($tab[1] == 0) {
	echo "<p style='font-size:large'>".$_SESSION['s_language']['reservation_no_reservation']."</p>";
} else {
	echo "<p style='font-size:small'>".$_SESSION['s_language']['reservation_deja_reserve']."<br />";
	if ($objectPriority){
	    echo "<span style='font-size:x-small'>".$_SESSION['s_language']['priority_1']." (".getPriorityMessage($tab[2]).")</span>\n";
	    echo "<br />";
	}
	echo $tab[0];
	echo "</p>\n";
} ?>

<div>
	<?php echo getResaFormContent();?>
	<hr /><br />
</div>


<table style='border:double thin lightgrey;width:100%' rules='none' frame='box'>
	<tr>
	    <td>
	        <table class='noborder' style='width:100%'>
<?php
if ($periodicityType == 'continue') $nbColumn = 2; else $nbColumn = 3;

if ($periodicityType == 'continue'){ ?>
	            <tr>
	                <td class='center' id='td_p'
	                    style='width:50%;background-color:#f5f5f5;text-decoration:underline;font-weight:bold;cursor:pointer;'
	                    onclick="javascript:chargerType('repetitif');document.forms.refreshFromReservationPeriodicityChange.submit();"
	                    onmouseover="javascript:changerFond('p','#d3dce3');
	                    return overlib('<?php echo $_SESSION['s_language']['reservation_periodic2'];?>', CAPTION, '', CENTER, OFFSETY, 20, FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);"
	                    onmouseout="javascript:changerFond('p','#f5f5f5'); return nd();" >
	                    <?php echo $_SESSION['s_language']['reservation_periodic'];?>
	                </td>
	                <td class='center'
	                    style='width:50%;background-color:#cccccc;text-decoration:underline;font-weight:bold;cursor:pointer;'
	                    onmouseover="javascript:return overlib('<?php echo $_SESSION['s_language']['reservation_continue2'];?>', CAPTION, '', CENTER, OFFSETY, 20, FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);"
	                    onmouseout="javascript:return nd();">
	                    <?php echo $_SESSION['s_language']['reservation_continue'];?>
	                </td>
	            </tr>
<?php
} else {?>
	            <tr>
	                <td class='center'
	                    style='width:50%;background-color:#cccccc;text-decoration:underline;font-weight:bold;cursor:pointer;'
	                    onmouseover="javascript:return overlib('<?php echo $_SESSION['s_language']['reservation_periodic2'];?>', CAPTION, '', CENTER, OFFSETY, 20, FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);"
	                    onmouseout="javascript:return nd();">
	                    <?php echo $_SESSION['s_language']['reservation_periodic'];?>
	                </td>
	                <td class='center' id='td_c'
	                    style='width:50%;background-color:#f5f5f5;text-decoration:underline;font-weight:bold;cursor:pointer;'
	                    onclick="javascript:chargerType('continue');document.forms.refreshFromReservationPeriodicityChange.submit();"
	                    onmouseover="javascript:changerFond('c','#d3dce3');
	                    return overlib('<?php echo $_SESSION['s_language']['reservation_continue2'];?>', CAPTION, '', CENTER, OFFSETY, 20, FGCOLOR, '#eaeff0', BGCOLOR, '#346099', BORDER, 2);"
	                    onmouseout="javascript:changerFond('c','#f5f5f5'); return nd();">
	                    <?php echo $_SESSION['s_language']['reservation_continue'];?>
	                </td>
	            </tr>
<?php
}
?>
	        </table>
	    </td>
	</tr>
</table>
<div><br /><br /></div>


<form id='resa' action='execresa.php' method='post'>
<table class='noborder' style='width:100%' cellpadding='3' cellspacing='1'>
	<tr>
	    <th class='gauche' colspan='<?php echo $nbColumn;?>'><?php echo $_SESSION['s_language']['reservation_new'];?>
	    </th>
	</tr>

<?php
if ($periodicityType == 'continue') {
?>
	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array13'];?> <span class='star'>*</span>
	    </th>
	    <th><?php echo $_SESSION['s_language']['title_array3'];?> <span class='star'>*</span>
	    </th>
	</tr>
	<tr style='background-color:#cccccc'>
	    <td class='center'><?php echo $currentDate;?></td>
	    <td class='center'>
	        <select name='debut'>
<?php
	for ($i=0; $i<24; $i++) {
	    if ($i == $debut) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>
	        &nbsp;
	        <select name='midebut'>
<?php
	if ($midebut == 0) {?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else {?>
	            <option value='00'>0</option>
	            <option  value='30'selected='selected'>30</option>
<?php
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array14'];?> <span class='star'>*</span>
	    </th>
	    <th><?php echo $_SESSION['s_language']['title_array15'];?> <span class='star'>*</span>
	    </th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td>
	        <table class='center'>
	            <tr>
	               <td>
	                    <a href='javascript:calendarDateFin(false)'>
	                    <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
	                </td>
	                <td>
	                    <input name='dateFin' disabled='disabled' value='' onfocus='this.blur();' size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>
	    <td class='center'>
	        <select name='fin'>
<?php
	for ($i=0; $i<24; $i++) {
	    if ($i == $default_end_hour) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else {?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>
	        &nbsp;
	        <select name='mifin'>
<?php
	if ($default_end_mihour == 0){?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else {?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	   <th class='gauche' colspan='2'><?php echo $_SESSION['s_language']['title_array7'];?> <span class='star'>*</span>
	   </th>
	</tr>
<?php
	$emailCrypte = PHPcrypt($email);
?>
	<tr style='background-color:#cccccc'>
	    <td colspan='2'>
	        <script type='text/javascript'>
	        <!--
	            decrypt("<input name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	        //-->
	        </script>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array5'];?> <span class='star'>*</span>
	    </th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array8'];?>
	    </th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td style='vertical-align:top'><textarea name='titre' cols='20' rows='5'><?php echo $titre;?></textarea>
	    </td>
	    <td><textarea name='commentaire' rows='5' cols='20'><?php echo $commentaire;?></textarea>
	    </td>
	</tr>
	<tr>
	    <td colspan='2'><span class='star'>*</span> = <?php echo $_SESSION['s_language']['reservation_mandatory'];?>
	    </td>
	</tr>


<?php
} else { ?>


	<tr style='background-color:#d3dce3'>
	    <th><?php echo $_SESSION['s_language']['title_array3'];?> <span class='star'>*</span>
	    </th>
	    <th><?php echo $_SESSION['s_language']['title_array4'];?> <span class='star'>*</span>
	    </th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array7'];?> <span class='star'>*</span>
	    </th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td class='center'>
	        <select name='debut' onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	for ($i=0; $i<24; $i++){
	    if ($i == $debut) {?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else { ?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	}?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>&nbsp;

	        <select name='midebut' onchange='javascript:fixeDuree(<?php echo $default_duration;?>);'>
<?php
	    if ($midebut == 0){ ?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	    } else { ?>
	            <option value='00'>0</option>
	            <option value='30' selected='selected'>30</option>
<?php
	    }?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>
	    <td class='center'>
	        <select name='duree'>
<?php
	if ($midebut == 0) $maximum = 25 - $debut;
	else $maximum = 25 - $debut - 1;
	for ($i=0; $i<$maximum; $i++){
	    if ($i == $default_duration) { ?>
	            <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	    } else { ?>
	            <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	    }
	} ?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_heure'];?>&nbsp;

	        <select name='miduree'>
<?php
	if ($default_miduration == 0){ ?>
	            <option value='00' selected='selected'>0</option>
	            <option value='30'>30</option>
<?php
	} else { ?>
	            <option value='00'>0</option>
	            <option  value='30'selected='selected'>30</option>
<?php
	} ?>
	        </select>
	        <?php echo $_SESSION['s_language']['reservation_minute'];?>
	    </td>

<?php
	$emailCrypte = PHPcrypt($email);
?>
	    <td>
	        <script type='text/javascript'>
	        <!--
	            decrypt("<input name='email' size='20' value='", "<?php echo $emailCrypte;?>", "' />")
	        //-->
	        </script>
	    </td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th class='gauche'  colspan='2' ><?php echo $_SESSION['s_language']['title_array5'];?> <span class='star'>*</span>
	    </th>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array8'];?>
	    </th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td colspan='2' style='vertical-align:top'>
	        <textarea name='titre' cols='20' rows='5'><?php echo $titre;?></textarea>
	    </td>
	    <td><textarea name='commentaire' rows='5' cols='20'><?php echo $commentaire;?></textarea>
	    </td>
	</tr>

	<tr>
	    <td><span class='star'>*</span> = <?php echo $_SESSION['s_language']['reservation_mandatory'];?>
	    </td>
	    <td colspan='2'>&nbsp;</td>
	</tr>

	<tr><td colspan='3'>&nbsp;</td>
	</tr>

	<tr>
	    <th class='gauche'><?php echo $_SESSION['s_language']['reservation_repeat'];?>
	    </th>
	    <td colspan='2'>&nbsp;</td>
	</tr>

	<tr style='background-color:#d3dce3'>
	    <th class='gauche'><?php echo $_SESSION['s_language']['title_array9'];?>
	    </th>
	    <th class='gauche' colspan='2'><?php echo $_SESSION['s_language']['title_array10'];?>
	    </th>
	</tr>

	<tr style='background-color:#cccccc'>
	    <td style='vertical-align:middle'>
	        <select name='multi' onchange='multiChange();'>
	            <option value='0'><?php echo $_SESSION['s_language']['reservation_period_no'];?></option>
	            <option value='1'><?php echo $_SESSION['s_language']['reservation_period_1day'];?></option>
	            <option value='7'><?php echo $_SESSION['s_language']['reservation_period_1week'];?></option>
	            <option value='14'><?php echo $_SESSION['s_language']['reservation_period_2week'];?></option>
	        </select>
	    </td>

	    <td colspan='2' style='vertical-align:middle'>
	        <table class='noborder'>
	            <tr>
	                <td>
	                    <a href='javascript:calendarDateFin(true)'>
	                    <img src='img/calendar.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['title_array10'];?>" />
	                    </a>
	                </td>
	                <td>
	                    <input name='dateFin' disabled='disabled' value='' onfocus='this.blur();' size='30' />
	                </td>
	            </tr>
	        </table>
	    </td>
	</tr>

<?php
}


$pass = substr(uniqid(""),7,6);
?>

<?php
if ($objectPriority){?>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
	<tr>
	    <th class='gauche' style='background-color:#d3dce3' colspan='<?php echo $nbColumn;?>'>
	        <?php echo sprintf($_SESSION['s_language']['priority_2'], $priorityMessage);?>
	    </th>
	</tr>
	<tr>
	    <td style='background-color:#cccccc;vertical-align:middle' colspan='<?php echo $nbColumn;?>'>
	        <input type='radio' name='priority' value='1' /><?php echo $_SESSION['s_language']['yes'];?>
	        &nbsp; &nbsp;
	        <input type='radio' name='priority' value='0' checked='checked' /><?php echo $_SESSION['s_language']['no'];?>
	    </td>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
<?php
}

if ($use_diffusion_flag) {
$temp = $_SESSION['s_language']['reservation_diffusion1']." (";
$temp .= $_SESSION['s_language']['reservation_diffusion3'];
if ($iCal) $temp .= " ".$_SESSION['s_language']['and']." ".$_SESSION['s_language']['reservation_diffusion4'];
$temp .= ") ".$_SESSION['s_language']['reservation_diffusion2'];
?>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
	<tr>
	    <th class='gauche' style='background-color:#d3dce3' colspan='<?php echo $nbColumn;?>'>
	        <?php echo $temp;?> &nbsp; <input type='checkbox' name='diffusion'/>
	    </th>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
<?php
}

if ($objectwifi) {?>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
	<tr>
	    <th class='gauche' style='background-color:#d3dce3' colspan='<?php echo $nbColumn;?>'>
	        <?php echo $_SESSION['s_language']['reservation_wifi'];?>
	    </th>
	</tr>
	<tr>
	    <td style='background-color:#cccccc;vertical-align:middle' colspan='<?php echo $nbColumn;?>'>
	        <input type='radio' name='wifi' value='1' /><?php echo $_SESSION['s_language']['yes'];?>
	        &nbsp; &nbsp;
	        <input type='radio' name='wifi' value='0' checked='checked' /><?php echo $_SESSION['s_language']['no'];?>
	    </td>
	</tr>
	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
<?php
} ?>

	<tr><td colspan='<?php echo $nbColumn;?>'>&nbsp;</td></tr>
	<tr><th class='gauche' colspan='<?php echo $nbColumn;?>'><?php echo $_SESSION['s_language']['reservation_go'];?></th></tr>

	<tr class='center' style='background-color:#dddddd'>
	    <td colspan='<?php echo $nbColumn;?>'>
	        <input type='button' name='soumettre' value="<?php echo $_SESSION['s_language']['reservation_submit'];?>"
	        onclick="javascript:return sComplet();" />
	        &nbsp;
	        <input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	        onclick="javascript:window.open('<?php echo $page_accueil;?>', '_self')" />
	    </td>
	</tr>
</table>
<div>
	<input type='hidden' name='pass'  value='<?php echo $pass;?>' />
	<input type='hidden' name='objet' value='<?php echo $objet;?>' />
	<input type='hidden' name='date'  value='<?php echo $date;?>' />
	<input type='hidden' name='jj' />
	<input type='hidden' name='mm' />
	<input type='hidden' name='aa' />
<?php
if ($periodicityType == 'continue') echo "<input type='hidden' name='multi' value='-1' />";
if ( ! $objectPriority) echo "<input type='hidden' name='priority' value='0' />";
if ( ! $objectwifi) echo "<input type='hidden' name='wifi' value='0' />";
if ( ! $use_diffusion_flag) echo "<input type='hidden' name='duffusion' value='1' />";
?>

</div>
</form>

<?php echo $body_end;?>
