<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File SOAP/index.php
*
* This file is used to shown all available Web Services based on the SOAP protocol
*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Michel Avenier <michel.avenier@lpsc.in2p3.fr>
*
* @copyright	2006,2007,2008 FrÃ©dÃ©ric Melot
* @copyright	2006,2007 Michel Avenier
*
* @package	PHPMyResa
* @subpackage	SOAP
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('../commun/commun.php');


/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 0) || (count($_GET) != 0)) exitWrongSignature('index.php');

/***********************************************************************************************
**************		  Fin de vÃ©rification des paramÃštres en entrÃ©e	 	  **************
**********************************************************************************************/


$tab_extension = get_loaded_extensions();
$trouve = false;
for ($i = 0 ; $i < count($tab_extension) ; $i++)
    if (strtoupper($tab_extension[$i]) == 'SOAP'){
        $trouve = true;
        break;
    }
if ( ! $trouve) exit($_SESSION['s_language']['init_config_soap']);

$wsdl = WEB."SOAP/resa.wsdl";

echo $entete;

echo "<body>\n";
echo "<p><br />WSDL : <a href='$wsdl'>$wsdl</a><br /><br /></p>";

if (!isset($HTTP_RAW_POST_DATA)){
    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
}

try {
	$soapClient = new SoapClient($wsdl);
	$functions = $soapClient->__getFunctions();
?>

<div>
	<table class='soap'>
	<tr style="background-color:#CCCCFF" class='soap'>
	    <th colspan='3' class='soap'><h2><b>Method Summary</b></h2>
	    </th>
	</tr>
	<tr style="background-color:white;" class='soap'>
	    <th>Return</th>
	    <th>Name</th>
	    <th>Descriptor</th>
	</tr>
<?php
	for ($i = 0 ; $i < count($functions) ; $i++){
	    $temp = $functions[$i];
	    $return = substr($temp, 0, strpos($temp, ' '));
	    $temp = substr($temp, strpos($temp, ' '));
	    $methodName = substr($temp, 0, strpos($temp, '('));
	    $signature = substr($temp, strpos($temp, '('));
	    $signature = str_replace('$', '', $signature);
	    $methodDesc = "";
	    ?>
	<tr style="background-color:white;" class='soap'>
	    <td align='right' class='soap'>
	    	<code>&nbsp;<?php echo $return;?></code>
	    </td>
	    <td class='soap'>
	    	<code><b><a href='#'><?php echo $methodName;?></a></b><?php echo $signature;?></code>
	    </td>
	    <td align='left' class='soap'>
	        <code>&nbsp;<?php echo $methodDesc;?></code>
	    </td>
	</tr>

<?php
	}?>

	</table>
<br />
</div>

<?php
	$types = $soapClient->__getTypes();
	echo "<p>";
	echo "Types disponibles : ";
	print_r($types);
	echo "</p>";
} catch (SoapFault $soapFault) {
	echo $soapFault;
}

echo "</body>\n";
echo "</html>\n";
?>
