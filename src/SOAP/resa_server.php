<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File SOAP/index.php
*
* This file is the implementation of the SOAP procedures
*
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Michel Avenier <michel.avenier@lpsc.in2p3.fr>
*
* @copyright	2006,2007,2008 FrÃ©dÃ©ric Melot
* @copyright	2006,2007 Michel Avenier
*
* @package	PHPMyResa
* @subpackage	SOAP
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once("../commun/commun.php");

class myResaWebService{

	function Get_Info4wifi_byID ($idresa){

	    global $connexionDB;

	    $myquery = "select titre, min(jour) as minjour, max(jour) as maxjour, idmulti, pass, email, idobjet
	                from reservation
	                where (id = '$idresa' or idmulti = '$idresa')
	                group by titre";

	    $result = database_query ($myquery, $connexionDB);
	    $response = array();
	    if (!$result) {
	        $response[0] = false;
	    } else {
	        $object = database_fetch_object ($result);
		$response[0] = database_get_from_object($object, 'titre');
		$response[1] = database_get_from_object($object, 'minjour');
		$response[2] = database_get_from_object($object, 'maxjour');
		$response[3] = database_get_from_object($object, 'idmulti');
		$temp = database_get_from_object($object, 'pass');
		$response[4] = sha1(strrev (substr ($temp, 1, -1)));
		$response[5] = database_get_from_object($object, 'email');
		$response[6] = database_get_from_object($object, 'idobjet');
	    }
	    return $response;
	}

	function List_Resa4wifi (){

	    global $connexionDB;

	    $myquery = "select titre, min(jour) as debut, max(jour) as jour, idmulti, id, pass
	                from reservation
	                where wifi = 1 and state = 0
	                and (
	                    ( idmulti in
	                        (
	                        select idmulti
	                        from reservation
	                        where jour = curdate( )
	                        and idmulti <>0
	                        )
	                    )
	                or ( idmulti = 0 and jour = curdate() )
			    )
	                group by titre
	                order by debut";

	    $result = database_query ($myquery, $connexionDB);

	    $response = array();
	    if ( ! $result) {
	        $response[0] = false;
	        $response[1] = "mauvaise reservation";
	    } else {
	        while ($object = database_fetch_object ($result)) {
		    $row = array();
		    $row[0] = database_get_from_object($object, 'titre');
		    $row[1] = database_get_from_object($object, 'debut');
		    $row[2] = database_get_from_object($object, 'jour');
		    $row[3] = database_get_from_object($object, 'idmulti');
		    $row[4] = database_get_from_object($object, 'id');
		    $temp = database_get_from_object($object, 'pass');
		    $row[5] = sha1(strrev (substr ($temp, 1, -1)));
	            array_push ($response, $row);
	        }
	    }
	    return $response;
	}

}


if (!isset($HTTP_RAW_POST_DATA)){
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');
}
try {
	ini_set('soap.wsdl_cache_enabled', FALSE);
	$server = new SoapServer(WEB."SOAP/resa.wsdl");
	$server -> setclass('myResaWebService');
} catch (Exception $e) {
	echo $e;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") $server->handle();
?>
