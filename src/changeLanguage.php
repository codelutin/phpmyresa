<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File changeLanguage.php
*
* This file is used to change the current language
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2004,2005,2006,2008 FrÃ©dÃ©ric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 1) || (count($_GET) != 0)) exitWrongSignature('changeLanguage.php');
if (isset($_POST['lang'])){
	$lang = $_POST['lang'];
	if ( ! in_array($lang, getAvailableLanguages())) exitWrongSignature('changeLanguage.php');
} else $lang = "english";

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$_SESSION['s_current_language'] = $lang;
$_SESSION['s_load_of_db_language'] = false;

setcookie("resa_lang", $lang, time()+3600*24*30*12);
?>

<script type='text/javascript'>
<!--
	window.top.location.href="index.php"
//-->
</script>
