<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File tree.php
*
* This file is used to create a tree for loading objects
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Laurent Capelli <capelli@in2p3.fr>
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
*
* @copyright	2005 Laurent Capelli
* @copyright	2005 Daniel Charnay
* @copyright	2006 Frédéric Melot
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


class TREE{
	var $opened;
	var $selected;
	var $htmltree;
	var $baseframe;
	var $basetarget;
	var $foreignframe;
	var $tablewidth;
	var $defaultspace;
	var $iconpath;
	var $icons;
	var $entries;
	var $last_element;

function TREE($opened, $selected) {
	settype($opened, "array");
	settype($selected, "array");

	$this->opened = $opened;
	$this->selected = $selected;
	$this->htmltree = "";
	$this->baseframe = "_self";
	$this->basetarget = $_SERVER["PHP_SELF"];
	$this->foreignframe = "_self";
	$this->tablewidth = 0;
	$this->defaultspace = 17;
	$this->iconpath = "";
	$this->icons = array();
	$this->icons['ROOT'] = "folderroot.gif";
	$this->icons['OPEN'] = "folderopen.gif";
	$this->icons['CLOSED'] = "folderclosed.gif";
	$this->icons['DOC'] = "file.gif";
	$this->icons['DOC2'] = "file_selected.gif";
	$this->icons['BLANK'] = "vide.gif";
	$this->icons['NODE'] = "tree_node.gif";
	$this->icons['LASTNODE'] = "tree_lastnode.gif";
	$this->icons['OPENFIRSTNODE'] = "tree_mfirstnode.gif";
	$this->icons['OPENLASTNODE'] = "tree_mlastnode.gif";
	$this->icons['CLOSEDNODE'] = "tree_pnode.gif";
	$this->icons['CLOSEDLASTNODE'] = "tree_plastnode.gif";
	$this->icons['CLOSEDFIRSTNODE'] = "tree_pfirstnode.gif";
	$this->entries = array();
	$this->last_element = array();
}

function setIconPath($path=".") {
	$this->iconpath = $path."/";
}

function setTableWidth($width){
	$this->tablewidth = $width;
}

function openTree($title, $target=FALSE, $frame=FALSE){
	$target = ( $target ) ? $target : $this->basetarget;
	$frame = ( $frame ) ? $frame : $this->foreignframe;
	$this->entries[1] = array (0, $title, $target, $frame, 0, 0);
	$this->last_element[0] = 1;
	return (1);
}

function addFolder($root, $title, $target=FALSE, $frame=FALSE){
	$frame = ( $frame ) ? $frame : $this->foreignframe;
	$retval = array_keys($this->entries);
	asort($retval);
	$retval = array_pop($retval)+1;
	$this->entries[$retval] = array($root, $title, $target, $frame, 0, 1);
	// for icon choosing and linking
	if ( !isSet($this->last_element[$root]) ) {
	    $this->last_element[$root] = array($retval, 0);
	} else {
	    $count = $this->last_element[$root][1]+1;
	    $this->last_element[$root] = array($retval, $count);
	}
	return ($retval);
}

function addDocument($root, $title, $target=FALSE, $frame=FALSE, $icon=FALSE){
	$frame = ( $frame ) ? $frame : $this->foreignframe;
	$icon = ( $icon ) ? $icon : $this->icons['DOC'];
	$retval = array_keys($this->entries);
	asort($retval);
	$retval = array_pop($retval)+1;
	$this->entries[$retval] = array($root, $title, $target, $frame, $icon, 1);
	// for icon choosing and linking
	if ( !isSet($this->last_element[$root]) ) {
	    $this->last_element[$root] = array($retval, 0);
	} else {
	    $count = $this->last_element[$root][1]++;
	    $this->last_element[$root] = array($retval, $count);
	}
}

function closeTree(){
	$this->generateTree(0,$this->entries,0);
}

function getTree(){
	return $this->htmltree;
}

function generateTree($closed, $entry, $sp_count){
	$sp_width = $sp_count*$this->defaultspace+1;
	$sp_count++;
	$iconarray = array($this->icons['CLOSED'], $this->icons['OPEN'], $this->icons['DOC']);
	foreach ($entry as $rowid=>$rowdata) {
	    if ( $rowdata[0] == $closed ) {

	        $this->htmltree .= "<table cellpadding='0' cellspacing='0' border='0'";
	        $this->htmltree .= ( $this->tablewidth > 0 ) ? " width='".$this->tablewidth."'>\n" : ">\n";
	        $this->htmltree .= "<tr>\n";
	        $this->htmltree .= "<td><a name='$rowid'><img src='".$this->iconpath.$this->icons['BLANK']."' width='".$sp_width."' height='5' alt='tree_element' /></a></td>\n";

	        if ( in_array($rowid, $this->opened) ) $rowdata[4]++;

	        $start_a_F = "<a href=\"javascript:go('$rowid', 'folder')\" target='".$this->baseframe."'>";
	        $start_a_D = "<a href=\"javascript:go('$rowid', 'document')\" target='".$this->baseframe."'>";

	        if ( is_int($rowdata[4]) ) {	// only folders
	            if ( $rowdata[4] == 1 || !isSet($this->last_element[$rowid][1]) ) {
	                if ( $rowdata[5] == 0 ) {
	                    $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['OPENFIRSTNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	                } else {
	                    $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['OPENLASTNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	                }
	            } else if ( $rowdata[4] == 0) {
	                if ( $rowid == $this->last_element[$rowdata[0]][0] ) {
	                    $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['CLOSEDLASTNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	                } else {
	                    if ( $rowdata[5] == 0 ) {
	                        $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['CLOSEDFIRSTNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	                    } else {
	                        $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['CLOSEDNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	                    }
	                }
	            }
	            if ( $rowdata[5] == 0 ) {
	                $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$this->icons['ROOT']."' class='noborder' alt='tree_element' /></a></td>\n";
	            } else {
	                $this->htmltree .= "<td>$start_a_F<img src='".$this->iconpath.$iconarray[$rowdata[4]]."' class='noborder' alt='tree_element' /></a></td>\n";
	            }
	        } else {	// only documents
	            if ( $rowid == $this->last_element[$rowdata[0]][0] ) {
	                $this->htmltree .= "<td>$start_a_D<img src='".$this->iconpath.$this->icons['LASTNODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	            } else {
	                $this->htmltree .= "<td>$start_a_D<img src='".$this->iconpath.$this->icons['NODE']."' class='noborder' alt='tree_element' /></a></td>\n";
	            }
	            $inter = $rowdata[4];
	            if ( ($inter == $this->icons['DOC']) && (in_array($rowid, $this->selected)) ){
	                $inter = $this->icons['DOC2'];
	            }
	            $this->htmltree .= "<td>$start_a_D<img src='".$this->iconpath.$inter."' class='noborder' alt='tree_element' /></a></td>\n";
	        }
	        $this->htmltree .= "<td><img src='".$this->iconpath.$this->icons['BLANK']."' width='5' height='5' alt='tree_element' /></td>\n";

	        if ( is_int($rowdata[4]) ) $this->htmltree .= "<td>$start_a_F".$rowdata[1]."</a></td>\n";
	        else if (in_array($rowid, $this->selected)) $this->htmltree .= "<td>$start_a_D<b>".$rowdata[1]."</b></a></td>\n";
	        else $this->htmltree .= "<td>$start_a_D".$rowdata[1]."</a></td>\n";

	        $this->htmltree .= "</tr>\n</table>\n\n";

	        if ( in_array($rowid, $this->opened) ) $this->generateTree($rowid, $entry, $sp_count);
	    }
	}// end foreach $entry
}// end function

} // end class
?>
