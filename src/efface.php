<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File efface.php
*
* This file is used to ask confirmation and password to delete reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	Frédéric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 Frédéric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
require_once('commun/commun_action.php');
if ($read_only)	exit($exit_message_authentification);

/***********************************************************************************************
**************		 Début de vérification des paramètres en entrée 	  **************
**********************************************************************************************/

if ((count($_POST) != 1) || (count($_GET) != 0)) exitWrongSignature('efface.php');
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('efface.php');
} else exitWrongSignature('efface.php');

/***********************************************************************************************
**************		 Fin de vérification des paramètres en entrée 		  **************
**********************************************************************************************/

$DB_request = "SELECT jour FROM reservation WHERE id=$id AND state=0";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$row = database_fetch_object($resultat);
$selectedDate = database_get_from_object($row, 'jour');

$idmulti = getIdmultiFromId($id);
if ($idmulti == 0) $DB_request_condition = "R.id=$id"; else $DB_request_condition = "R.idmulti=$idmulti";
$continuous = reservationIsContinuous($idmulti);

$tab = GetClassProperties($DB_request_condition);
$classeForICal = $tab[0];
$classe = $tab[1];

$DB_request = "SELECT DISTINCT O.nom FROM reservation R, objet O WHERE R.idobjet = O.id AND R.state=0 AND ".$DB_request_condition;
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$inter = "";
$i = 0;
while ($row = database_fetch_object($resultat)){
	$res = database_get_from_object($row, 'nom');
	if (strpos($inter, $res) === FALSE){
	    $inter .= $res.",";
	    $i++;
	}
}
$nbObjets = $i;

$tab = createObjectList(substr($inter, 0, -1));
$objets = $tab[0];
$nbObjets = $tab[1];
$objetDisplay = $tab[3];

$minDate = "";
$maxDate = "";
$i = 0;
$DB_request = "SELECT DISTINCT jour, titre FROM reservation R WHERE R.state=0 AND $DB_request_condition ORDER BY jour";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if ($i == 0){
	    $minDate = database_get_from_object($row, 'jour');
	} else {
	    $maxDate = database_get_from_object($row, 'jour');
	}
	$titre = database_get_from_object($row, 'titre');
	$i++;
}
$nbJours = $i;

echo $entete;
echo "<body style='font-size:small'>";

$today = date("Y-m-d");
if ( (($maxDate == "") && ($minDate < $today)) || (($maxDate != "") && ($maxDate < $today)) ){
	exit($_SESSION['s_language']['past_reservation']."<div style='font-size:small'><br /><br /><br /><a href='vueMois.php'>".$_SESSION['s_language']['invitevalide_planning']."</a></div>");
}

$tab = computeTitleInAction($minDate, $maxDate, $continuous, $nbJours);
$titleTypeAndDate = $tab[0];
$titleObjects = strtoupper($objetDisplay);
?>
<div>
<img src='img/poub.gif' class='noborder' alt="<?php echo $_SESSION['s_language']['efface_submit'];?>" /> <span style='font-size:large'><?php echo $titleObjects,"  ",$titleTypeAndDate;?></span><br /><br />
<hr />

<b><?php echo $_SESSION['s_language']['delete_ask'];?></b>
<ul>
	<?php
	echo "<li>".$_SESSION['s_language']['ask_user_password']."</li>\n";
	echo "<li>".$_SESSION['s_language']['or']." ";
	if (count($classe) == 1){
		$SPECIAL_classe_to_display = $classe[0];
		$text = $_SESSION['s_language']['ask_admin_password_for_one_class'];
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_one_class']."\";" );
		echo $text;
	} else{
		$SPECIAL_classe_to_display = "";
		for ($i = 0 ; $i < count($classe) ; $i++) $SPECIAL_classe_to_display .= "&#39;".$classe[$i]."&#39;, ";
		$SPECIAL_classe_to_display = replaceLastOccurenceOfComa(substr($SPECIAL_classe_to_display, 0, -2), 'or');
		eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_several_classes']."\";" );
		echo $text;
		echo "<br /><br />";
	} ?>
	</li>
</ul>
</div>

<form id='executeEfface' action='execefface.php' method='post'>

<div>
<?php
// Erase all objects?
if ($nbObjets == 1) {
	echo "<input type='hidden' name='objet' value='-1' />\n";
} else {
	echo $_SESSION['s_language']['efface_multiple_objets']."&nbsp;";
	echo "<select name='objet'>\n";
	    echo "<option value='-1'>".$_SESSION['s_language']['validate_all']."</option>\n";
	for ($i=0; $i<$nbObjets; $i++) {
	    echo "<option value=\"".$objets[$i]."\">".$objets[$i]."</option>\n";
	}
	echo "</select>\n <br />";
	if (count($classe) != 1)
	    echo "(",$_SESSION['s_language']['deletion_multiple_objets'],")<br /><br /><br />";
} ?>
</div>


<div>
<?php
// Erase all dates (only for periodic reservations)?
if ($idmulti == 0){
	echo "<input type='hidden' name='multi' value='oui' />\n";
} else if ($nbJours == 1){
	echo "<input type='hidden' name='multi' value='oui' />\n";
} else {
	echo "<br />";
	if ($continuous) {
	    echo $_SESSION['s_language']['continuous_reservation']." ".$_SESSION['s_language']['efface_all'];
	    echo "<input type='hidden' name='multi' value='oui' />\n";
	} else {
	    echo "<div>\n";
	    echo $_SESSION['s_language']['periodic_reservation'];
	    echo "<br />";
	    echo sprintf($_SESSION['s_language']['efface_all_reservations'], displayCurrentLanguageDate($selectedDate, 0));
	    echo "&nbsp; &nbsp;";
	    echo "<select name='multi'>\n";
	    echo "<option value='non'>".$_SESSION['s_language']['no']."</option>\n";
	    echo "<option value='oui' selected='selected'>".$_SESSION['s_language']['yes']."</option>\n";
	    echo "</select>\n\n";
	    echo "</div>\n";
	}

	echo "<br /><br /><br />";
} ?>

</div>

<?php
$what =  "'index.php', '_parent'";
if (isset($_SERVER["HTTP_REFERER"])){
	$temp = $_SERVER["HTTP_REFERER"];
	if (strpos($temp, 'invitevalide.php') === FALSE) $what = "'".$page_accueil."', '_self'";
} ?>

<div>
	<?php echo $_SESSION['s_language']['explanation'];?> <br />
	<textarea name='raison' rows='4' cols='40'></textarea><br />
	<?php echo $_SESSION['s_language']['password'];?> <input name='motdepasse' type='password' /> &nbsp;
	<input type='hidden' name='id' value='<?php echo $id;?>' />
	<input type='hidden' name='idmulti' value='<?php echo $idmulti;?>' />
	<input type='hidden' name='titleObjects' value='<?php echo $titleObjects;?>' />
	<input type='hidden' name='titleTypeAndDate' value='<?php echo $titleTypeAndDate;?>' />
	<input type='hidden' name='nbJours' value='<?php echo $nbJours;?>' />
	<input type='hidden' name='selectedDate' value='<?php echo $selectedDate;?>' />
	<input type='hidden' name='classeForICal' value='<?php echo addslashes(urlencode(serialize($classeForICal)));?>' />
	<input type='hidden' name='classe' value='<?php echo addslashes(urlencode(serialize($classe)));?>' />
	<input type='submit' value="<?php echo $_SESSION['s_language']['efface_submit'];?>" /> &nbsp;
	<input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	    onclick="javascript:window.open(<?php echo $what;?>)" />
</div>


</form>

<?php echo $body_end;?>
