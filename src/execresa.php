<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File execresa.php
*
* This file is used to execute the creation of a non-periodical
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//*******************************************************************************


require_once('commun/commun.php');
require_once('commun/commun_execresa.php');
if ($read_only)  exit($exit_message_authentification);

if ($iCal) require_once('wiCal.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
// 16, 17 ou 18 en fonction du navigateur car l'input dateFin est disabled et diffusion existe ou non
if (( ($nbpar != 16) && ($nbpar != 17) && ($nbpar != 18) ) || (count($_GET) != 0)) exitWrongSignature('execresa.php');
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	$tab = createObjectList($objet);
	$objets = $tab[0];
	$nbObjets = $tab[1];
	$objets_list = $tab[2];
	$objetDisplay = $tab[3];
	$availableObjects = getAvailableObjects();
	for ($i = 0 ; $i < count($objets) ; $i++){
		if ( ! in_array($objets[$i], $availableObjects) ) exitWrongSignature('execresa.php');
	}
} else exitWrongSignature('execresa.php');
if (isset($_POST['date'])){
	$date = $_POST['date'];
	if ( ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $date)) ) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['debut'])){
	$debut = $_POST['debut'];
	if (strlen($debut) > 2) exitWrongSignature('execresa.php');
	if ( ! ctype_digit($debut) || ($debut == '') ) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['midebut'])){
	$midebut = $_POST['midebut'];
	if (strlen($midebut) > 2) exitWrongSignature('execresa.php');
	if ( ! ctype_digit($midebut) || ($midebut == '') ) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['email'])){
	$email = $_POST['email'];
	if ( ! validate_email($email)) exitWrongSignature('execresa.php');
	if (strlen($email) > 128) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['titre'])){
	$titre = $_POST['titre'];
	if (strlen($titre) > 64) exitWrongSignature('execresa.php');
	$titre = database_real_escape_string($titre);
	$titre = htmlspecialchars($titre);
} else exitWrongSignature('execresa.php');
if (isset($_POST['commentaire'])){
	$commentaire = $_POST['commentaire'];
	if (strlen($commentaire) > 255) exitWrongSignature('execresa.php');
	$commentaire = database_real_escape_string($commentaire);
	$commentaire = htmlspecialchars($commentaire);
} else exitWrongSignature('execresa.php');
if (isset($_POST['pass'])){
	$pass = $_POST['pass'];
	if (strlen($pass) > 20) exitWrongSignature('execresa.php');
	$pass = database_real_escape_string($pass);
	$pass = htmlspecialchars($pass);
} else exitWrongSignature('execresa.php');
if (isset($_POST['jj'])){
	$jj = $_POST['jj'];
	if ($jj != ""){
		if ( ! ctype_digit($jj) || ($jj == '') ) exitWrongSignature('execresa.php');
		if ($jj > 31) exitWrongSignature('execresa.php');
	}
} else exitWrongSignature('execresa.php');
if (isset($_POST['mm'])){
	$mm = $_POST['mm'];
	if ($mm != ""){
		if ( ! ctype_digit($mm) || ($mm == '') ) exitWrongSignature('execresa.php');
		if ($mm > 12) exitWrongSignature('execresa.php');
	}
} else exitWrongSignature('execresa.php');
if (isset($_POST['aa'])){
	$aa = $_POST['aa'];
	if ($aa != ""){
		if (strlen($aa) > 4) exitWrongSignature('execresa.php');
		if ( ! ctype_digit($aa) || ($aa == '') ) exitWrongSignature('execresa.php');
	}
} else exitWrongSignature('execresa.php');
if (isset($_POST['priority'])){
	$priority = $_POST['priority'];
	if ( ! in_array($priority, array('0', '1')) ) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['multi'])){
	$multi = $_POST['multi'];
	if ( ! in_array($multi, array('0', '1', '7', '14', '-1')) ) exitWrongSignature('execresa.php');
} else exitWrongSignature('execresa.php');
if (isset($_POST['wifi'])){
	$wifi = $_POST['wifi'];
	if ( ! in_array($wifi, array('0', '1')) ) exitWrongSignature('execresa.php');
} else $wifi = 0;
if ($multi == '-1'){
	if (isset($_POST['fin'])){
		$fin = $_POST['fin'];
		if (strlen($fin) > 2) exitWrongSignature('execresa.php');
		if ( ! ctype_digit($fin) || ($fin == '') ) exitWrongSignature('execresa.php');
	} else exitWrongSignature('execresa.php');
	if (isset($_POST['mifin'])){
		$mifin = $_POST['mifin'];
		if (strlen($mifin) > 2) exitWrongSignature('execresa.php');
		if ( ! ctype_digit($mifin) || ($mifin == '') ) exitWrongSignature('execresa.php');
	} else exitWrongSignature('execresa.php');
} else {
	if (isset($_POST['duree'])){
		$duree = $_POST['duree'];
		if (strlen($duree) > 2) exitWrongSignature('execresa.php');
		if ( ! ctype_digit($duree) || ($duree == '') ) exitWrongSignature('execresa.php');
	} else exitWrongSignature('execresa.php');
	if (isset($_POST['miduree'])){
		$miduree = $_POST['miduree'];
		if (strlen($miduree) > 2) exitWrongSignature('execresa.php');
		if ( ! ctype_digit($miduree) || ($miduree == '') ) exitWrongSignature('execresa.php');
	} else exitWrongSignature('execresa.php');
}
if (isset($_POST['diffusion'])){
	$diffusion = $_POST['diffusion'];
	if ($diffusion == 'on') $diffusion = 0; else $diffusion = 1;
} else $diffusion = 1;

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

setcookie("resa_email", $email, time()+3600*24*30*12);

$idebut = $debut;
if ($midebut == "30") $idebut = $idebut + .5;
$debut = fillWithZero($debut).":".$midebut.":00";

if ($multi == -1){
	$ifin = $fin;
	if ($mifin == "30") $ifin = $ifin + .5;
	$fin = fillWithZero($fin).":".$mifin.":00";
} else {
	$iduree = $duree;
	if ($miduree == "30") $iduree = $iduree + .5;
	$duree = fillWithZero($duree).":".$miduree.":00";
	$ifin = $idebut+$iduree;
}

echo $entete;
?>

<body style='font-size:small'>

<?php
/********************************************************************************
***                 Getting all information from DB                           ***
********************************************************************************/
$objectParameter = array();
$objetWithPriorityManagementExist = false;
$adminToContact = array();
$langueAdminToContact = array();
$classeAdminToContact = array();
$classeForICal = array();
$j = 0;
$k = 0;
for ($i = 0 ; $i < $nbObjets ; $i++) {
	$DB_request = "SELECT O.status, O.pubical, O.priority, O.id AS idobjet, O.wifi, C.id AS idclasse, C.nom AS nomclasse, A.mail AS adminmail, A.default_language ";
	$DB_request .= "FROM objet O, classe C, administrateur A WHERE O.nom = '$objets[$i]' AND O.id_classe = C.id AND A.id_classe = C.id";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat);
	$classeResa = database_get_from_object($row, 'nomclasse');
	if ( ! in_array($classeResa, $classeForICal)) {
	    if (database_get_from_object($row, 'pubical') == 1) {
	        $classeForICal[$k] = $classeResa;
	        $k++;
	    }
	}
	$adminMail = database_get_from_object($row, 'adminmail');
	$langue = database_get_from_object($row, 'default_language');
	if (database_get_from_object($row, 'status') == 0)
	    $valide = 1;
	else {
	    $valide = 0;
	    if ( ! in_array($classeResa, $classeAdminToContact)){
	        $adminToContact[$j] = $adminMail;
	        $langueAdminToContact[$j] = $langue;
	        $classeAdminToContact[$j] = $classeResa;
	        $j++;
	    }
	}

	$objectParameter[$i]['idobjet'] = database_get_from_object($row, 'idobjet');
	$objectParameter[$i]['objetPriority'] = database_get_from_object($row, 'priority');
	$objectParameter[$i]['objetWifi'] = database_get_from_object($row, 'wifi');
	$objectParameter[$i]['valide'] = $valide;

	if ($objectParameter[$i]['objetPriority']) {
	    $objectParameter[$i]['objetPriority'] = $priority;
	    $objetWithPriorityManagementExist = true;
	}
	if ($objectParameter[$i]['objetWifi']) $objectParameter[$i]['objetWifi'] = $wifi;
}
$nbClasseAskForValidation = $j;


/********************************************************************************
***                 Computing on those information                            ***
********************************************************************************/

/*
	In fact $adminToContact, $langueAdminToContact and $classeAdminToContact were just temporarly arrays
	$adminMail, $langueAdminMail and $classeAdminMail are arrays to use
	$nbClasseAskForValidation is also replaced by $nbAdminMail
*/

$adminMail = array();
$langueAdminMail = array();
$classeAdminMail = array();
$j = 0;
$nbAdminMail = 0;
if ($nbClasseAskForValidation > 1) {
	for ($i = 0 ; $i < $nbClasseAskForValidation ; $i++) {
	    if ( ! in_array($adminToContact[$i], $adminMail)) {
	        $adminMail[$j] = $adminToContact[$i];
	        $langueAdminMail[$j] = $langueAdminToContact[$i];
	        $classeAdminMail[$j] = $classeAdminToContact[$i];
	        $j++;
	    } else {
	        for ($k = 0 ; $k <= $j ; $k++) {
	            if ($adminMail[$k] == $adminToContact[$i]) {
	                $classeAdminMail[$k] .= ", ".$classeAdminToContact[$i];
	                break;
	            }
	        }
	    }
	}
	$nbAdminMail = $j;
} else if ($nbClasseAskForValidation == 1) {
	$adminMail[0] = $adminToContact[0];
	$langueAdminMail[0] = $langueAdminToContact[0];
	$classeAdminMail[0] = $classeAdminToContact[0];
	$nbAdminMail = 1;
}
if ($multi == 0) {
	/****************************************************************************
	***                     Check if time available                           ***
	****************************************************************************/
	$tab = check_time_available_on_single_reservation($date, $objets_list, $idebut, $ifin, "");
	$compteur = $tab[0];
	if ($compteur != 0) {
	    $message = $tab[1];
	    if ($compteur == 1)
	        $messageToSend = $_SESSION['s_language']['execresa_conflict'];//."  $message";
	    else
	        $messageToSend = $_SESSION['s_language']['execresa_conflicts'];//." $message";
	    exit("<div style='font-size=:large'><span class='red'>$messageToSend</span> $message</div>");
	}

	/***************************************************************************
	***                     Insert into database                             ***
	****************************************************************************/
	for ($i = 0 ; $i < $nbObjets ; $i++) {
	    if ($i == 0) {
	        $DB_request = "INSERT INTO reservation (idobjet,jour,debut,duree,email,titre,commentaire,valide,pass,priority,wifi,state,diffusion)";
	        $DB_request .= "VALUES('".$objectParameter[$i]['idobjet']."','$date','$debut','$duree','$email','$titre','$commentaire','".$objectParameter[$i]['valide']."','$pass','".$objectParameter[$i]['objetPriority']."','".$objectParameter[$i]['objetWifi']."', 0, $diffusion)";
	        database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

	        $DB_request = "SELECT R.id FROM reservation R, objet O WHERE R.idobjet = O.id AND O.nom='$objets[$i]' AND R.jour='$date' AND R.debut='$debut' AND R.pass='$pass'";
	        $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	        $reservation = database_fetch_object($resultat);
	        $id = database_get_from_object($reservation, 'id');

	        if ($nbObjets > 1) {
	            $DB_request = "UPDATE reservation SET idmulti='$id' WHERE id='$id'";
	            database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	        }

	    } else {
	        $DB_request = "INSERT INTO reservation (idmulti, idobjet,jour,debut,duree,email,titre,commentaire,valide,pass,priority,wifi,state,diffusion)";
	        $DB_request .= "VALUES($id,'".$objectParameter[$i]['idobjet']."','$date','$debut','$duree','$email','$titre','$commentaire','".$objectParameter[$i]['valide']."','$pass','".$objectParameter[$i]['objetPriority']."','".$objectParameter[$i]['objetWifi']."', 0, $diffusion)";
	        database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    }
	}


	/****************************************************************************
	***                     Part email send                                   ***
	****************************************************************************/
	$from = "From: PHPMyResa";
	$headers  = "Date: ".date("l j F Y, G:i")."\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/plain; charset=utf8\n";
	$headers = $from."\n";
	$lien = WEB;
	if ($nbObjets == 1) $lien .= "invitevalide.php?id=$id";
	else $lien .= "invitevalide.php?idmulti=$id";

	/****************************************************************************
	***                     Mail for the administrator(s)                     ***
	****************************************************************************/
	$sendAdminList = "";
	$sendAdminErrorList = "";
	for ($i = 0 ; $i < $nbAdminMail ; $i++) {
	    $inter = getMailMessageForAdminInSpecifiedLanguage($langueAdminMail[$i]);
	    $customDate = displayDate($date, $langueAdminMail[$i], 0);

	    $sujet = $inter['execresa_mail_subject']." ".$objetDisplay;
	    if ($mail_subject != "short") $sujet .= ", '$titre'";
	    if ($mail_subject == "long") $sujet .= " ".$inter['par']." $email - $customDate ".$inter['execresa_from2']." ".substr($debut,0,5)." ".$inter['pendant']." ".substr($duree,0,5);
	    $sujet = sans_accent($sujet);

	    $contenu = sprintf($inter['execresa_mail_admin'], $classeAdminMail[$i])."\n\n";
	    $contenu .= sprintf($_SESSION['s_language']['execresa_booking'], $objetDisplay)."\n";
	    $contenu .= $inter['execresa_mail_title']." $titre\n";
	    $contenu .= $inter['execresa_mail_date']." $customDate\n";
	    $contenu .= $inter['execresa_mail_start_time']." ".substr($debut,0,5)." \n";
	    $contenu .= $inter['execresa_mail_duration']." ".substr($duree,0,5)." \n";
	    $contenu .= $inter['execresa_mail_user']." $email\n";
	    if ($commentaire != '') $contenu .= $inter['title_array12']." $commentaire\n";
	    if ($objetWithPriorityManagementExist) {
	        $messagePriority = getPriorityMessageInSpecifiedLanguage($objets, $langueAdminMail[$i]);
	        if ($priority == '1')
	            $contenu .= "\n".$inter['execresa_mail_priority']." ($messagePriority).\n";
	        else
	            $contenu .= "\n".$inter['execresa_mail_no_priority']." ($messagePriority).\n";
	    }
	    if ($wifi == '1') $contenu .= "\n".$inter['execresa_mail_wifi']."\n";
	    $contenu .= "\n".sprintf($inter['execresa_mail_valid'], $lien)."\n";
	    $contenu = ereg_replace("\\\'","'",$contenu);
	    $contenu = ereg_replace("\\\\\"","\"",$contenu);
	    $destinataire = str_replace($web_server, $mail_server, $adminMail[$i]);
	    $sendAdmin = mail($destinataire,$sujet,$contenu,$headers);
	    if ($sendAdmin) $sendAdminList .= $adminMail[$i].", ";
	    else $sendAdminErrorList .= $adminMail[$i].", ";
	}

	$temp = $_SESSION['s_language']['and'].' '.$_SESSION['s_language']['to'];
	if ($sendAdminList != "") {
	    $sendAdminList = substr($sendAdminList, 0, -2);
	    $sendAdminList = replaceLastOccurenceOfComaSpecial($sendAdminList, $temp);
	}
	if ($sendAdminErrorList != "") {
	    $sendAdminErrorList = substr($sendAdminErrorList, 0, -2);
	    $sendAdminErrorList = replaceLastOccurenceOfComaSpecial($sendAdminErrorList, $temp);
	}

	/****************************************************************************
	***                     Mail for the user                                 ***
	****************************************************************************/
	$customDate = displayCurrentLanguageDate($date, 0);

	$sujet = $_SESSION['s_language']['execresa_mail_subject']." ".$objetDisplay;
	if ($mail_subject != "short") $sujet .= ", '$titre'";
	if ($mail_subject == "long") $sujet .= " - $customDate ".$_SESSION['s_language']['execresa_from2']." ".substr($debut,0,5)." ".$_SESSION['s_language']['pendant']." ".substr($duree,0,5);
	$sujet = sans_accent($sujet);

	$contenu = $_SESSION['s_language']['execresa_mail_to_user']."\n\n";
	if ($nbAdminMail == '0') $contenu .= $_SESSION['s_language']['execresa_mail_request']."\n\n";
	else $contenu .= $_SESSION['s_language']['execresa_mail_validation']."\n\n";
	$contenu .= sprintf($_SESSION['s_language']['execresa_booking'], $objetDisplay)."\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_title']." $titre\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_date']." $customDate\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_start_time']." ".substr($debut,0,5)." \n";
	$contenu .= $_SESSION['s_language']['execresa_mail_duration']." ".substr($duree,0,5)." \n";
	if ($commentaire != '') $contenu .= $_SESSION['s_language']['title_array12']." $commentaire\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_pass']." $pass\n";
	if ($objetWithPriorityManagementExist) {
	    $messagePriority = getPriorityMessage($objets);
	    if ($priority=='1')
	        $contenu .= "\n".$_SESSION['s_language']['execresa_mail_priority']." ($messagePriority).\n";
	    else
	        $contenu .= "\n".$_SESSION['s_language']['execresa_mail_no_priority']." ($messagePriority). ".$_SESSION['s_language']['execresa_mail_no_priority_bis']."\n";
	}
	if ($wifi == '1') {
	    $wifiPassword = strrev(substr($pass, 1, -1));
	    $contenu .= "\n".$_SESSION['s_language']['execresa_mail_wifi']." ".sprintf($_SESSION['s_language']['execresa_mail_wifi2'], $wifiPassword)."\n";
	}
	$contenu .= "\n";
	if ($modification_enable) $contenu .= sprintf($_SESSION['s_language']['execresa_mail_modify2'], $lien);
	else $contenu .= sprintf($_SESSION['s_language']['execresa_mail_modify1'], $lien);
	$contenu = ereg_replace("\\\'","'",$contenu);
	$contenu = ereg_replace("\\\\\"","\"",$contenu);
	$destinataire = str_replace($web_server, $mail_server, $email);
	$sendUser = mail($destinataire,$sujet,$contenu,$headers);

	/****************************************************************************
	***                     Display in PHPMyResa                              ***
	****************************************************************************/
?>
<div>
<span class='lblue'>
	<?php echo $_SESSION['s_language']['execresa_mail_done'];?>
</span>
	<br /><br />
<?php
	if ($nbAdminMail != 0) {
	    echo$_SESSION['s_language']['execresa_mail_validation']."<br />";
	    if ($sendAdminList != "") echo $_SESSION['s_language']['execresa_mail_envoye']." ".$sendAdminList;
	    if ($sendAdminErrorList != "") echo " ".$_SESSION['s_language']['execresa_mail_mal_envoye']." ".$sendAdminErrorList." ".$_SESSION['s_language']['commun_error_mysql_contact1']." ".$technical_contact." ".$technical_tel." ".$_SESSION['s_language']['commun_error_mysql_contact2'];
	    echo "<br /><br />";
	}

	echo $_SESSION['s_language']['execresa_mail_pass1'];?>
	<span class='bred' style='font-size:medium'><?php echo $pass;?></span><br />
<?php

	if ($modification_enable) echo $_SESSION['s_language']['execresa_mail_modify5'];
	else echo $_SESSION['s_language']['execresa_mail_modify6'];
	echo "<br />";

	if ($sendUser) echo $_SESSION['s_language']['execresa_mail_pass4'];
	else echo $_SESSION['s_language']['execresa_mail_pass5']." ".$_SESSION['s_language']['execresa_mail_pass6']." ($email) ";
	?>
	<br /><br /><br />
	<span class='blue'>
	<a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
	</span>
</div>

<?php
	if ($iCal) {
	    for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
	}


} else {

	/*
	    $multi = 1, 7 or 14 ( or -1 for continuous reservations)
	    $date is the departure date in format YYYY-MM-DD
	    $datefintxt is the arrival date in format YYYY-MM-DD
	*/

	$datefintxt = makeDateWithZero($aa, $mm, $jj);

	/****************************************************************************
	***                 Check if time available                               ***
	****************************************************************************/
	$tab = check_time_available_on_multiple_reservation($multi, $date, $datefintxt, $objets_list, $idebut, $ifin, "");
	$nbJoursOK = $tab[0];
	$nbJoursConflit = $tab[1];
	$nbConflit = $tab[2];
	$message = $tab[3];

	/****************************************************************************
	***                 Display in PHPMyResa                                  ***
	****************************************************************************/
	$departureDepart = displayCurrentLanguageDate($date, 0);
	$arrivalDate = displayCurrentLanguageDate($datefintxt, 0);
?>
<div>
	<span style='font-size:large'>
	    <?php echo $objetDisplay." ".$_SESSION['s_language']['execresa_from']." ".$departureDepart." ".$_SESSION['s_language']['execresa_to']." ".$arrivalDate;?>
	</span>
	<br />
	<span style='font-style:italic'>
	    <?php echo stripslashes($titre);?>
	</span>
	<br />

<?php
	if ($multi == 1) {
	    echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_1day']."<br />";
	} else if ($multi == 7) {
	    echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_1week']."<br />";
	} else if ($multi == 14) {
	    echo ucfirst($_SESSION['s_language']['reservation'])." ".$_SESSION['s_language']['reservation_period_2week']."<br />";
	} else if ($multi == -1) {
	    echo $_SESSION['s_language']['reservation_continue']."<br />";
	}
	if ($multi == -1) {
	    echo $_SESSION['s_language']['title_array3'].$_SESSION['s_language']['ponctuation']." ".(int)substr($debut,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($debut,3,2)." ".$_SESSION['s_language']['reservation_minute']." ".($departureDepart)."<br />";
	    echo $_SESSION['s_language']['title_array15'].$_SESSION['s_language']['ponctuation']." ".(int)substr($fin,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($fin,3,2)." ".$_SESSION['s_language']['reservation_minute']." ".($arrivalDate)."<br />";
	} else {
	    echo $_SESSION['s_language']['execresa_mail_start_time']." ".(int)substr($debut,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($debut,3,2)." ".$_SESSION['s_language']['reservation_minute']." - ";
	    echo $_SESSION['s_language']['execresa_mail_duration']  ." ".(int)substr($duree,0,2)." ".$_SESSION['s_language']['reservation_heure']." ".(int)substr($duree,3,2)." ".$_SESSION['s_language']['reservation_minute']."<br />";
	} ?>

</div>

<?php
	if ($nbConflit > 0) {?>

<div><br />
	<table class='noborder' cellpadding='3'>
<?php
	    $color = "#eeeeee";
	    for($i = 0; $i < $nbConflit; $i++) {?>
	    <tr style='background-color:<?php echo $color;?>'>
<?php
	        if ($nbObjets != 1) {?>
	        <td><?php echo $message[$i][0];?></td>
<?php
	        }?>
	        <td><?php echo $message[$i][1];?></td>
	        <td><?php echo $message[$i][2];?></td>
	        <td class='gauche'>
	            <span class='bred'><?php echo $_SESSION['s_language']['execresa_not_available'];?></span>
	            <i>(<?php echo $message[$i][3];?>)</i></td>
	    </tr>
<?php
	        if ($color=="#eeeeee") $color="#d3dce3"; else $color="#eeeeee";
	    }?>
	</table>


	<?php echo $_SESSION['s_language']['execresa_nb_possible'];?>
	<span class='bblue'><?php echo $nbJoursOK;?></span>
	<?php echo $_SESSION['s_language']['execresa_nb_not_possible'];?>
	<span class='bred'><?php echo $nbJoursConflit;?></span>
<?php
	    if ($nbObjets != 1) {
	        if ($nbConflit == 1) {?>
	(<?php echo $nbConflit." ".$_SESSION['s_language']['execresa_conflict2'];?>)
<?php
	        } else {?>
	(<?php echo $nbConflit." ".$_SESSION['s_language']['execresa_conflict3'];?>)
<?php
	        }
	    }
	} else {?>
	<?php echo $_SESSION['s_language']['execresa_nb_OK'];?> :&nbsp;
	<span class='bblue'><?php echo $nbJoursOK;?></span>
<?php
	}?>
	<br /><br />
</div>

<?php
	$titre = ereg_replace("'","&#39;",stripslashes($titre));
	$emailCrypte = PHPcrypt($email);
?>

	<form id='valid' action='execresa2.php' method='post'>
	<div>
	    <input type='hidden' name='objet' value='<?php echo $objet;?>' />
	    <input type='hidden' name='date' value='<?php echo $date;?>' />
<?php
//	$emailCrypte = PHPcrypt($email);
//	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<input name='email' type='hidden' value='\", \"$emailCrypte\", \"'>\")</script>";
//	echo $afficheEmailCrypte;
?>
	    <script type='text/javascript'> <!--
	        decrypt("<input type='hidden' name='email' value='", "<?php echo $emailCrypte;?>", "' />")
	    //-->
	    </script>

	    <input type='hidden' name='pass' value='<?php echo $pass;?>' />
	    <input type='hidden' name='debut' value='<?php echo $debut;?>' />
	    <input type='hidden' name='titre' value='<?php echo $titre;?>' />
	    <input type='hidden' name='commentaire' value='<?php echo ereg_replace("'","&#39;",stripslashes ($commentaire));?>'>
	    <input type='hidden' name='multi' value='<?php echo $multi;?>' />
	    <input type='hidden' name='datefintxt' value='<?php echo $datefintxt;?>' />
	    <input type='hidden' name='classeResa' value='<?php echo $classeResa;?>' /> 	<!-- usefull only if $nbObjets = 1 -->
	    <input type='hidden' name='objetWithPriorityManagementExist' value='<?php echo $objetWithPriorityManagementExist;?>' />
	    <input type='hidden' name='priority' value='<?php echo $priority;?>' />
	    <input type='hidden' name='nbConflit' value='<?php echo $nbConflit;?>' />
	    <input type='hidden' name='wifi' value='<?php echo $wifi;?>' />
	    <input type='hidden' name='diffusion' value='<?php echo $diffusion;?>' />
<?php
	if ($multi == -1)
	{?>
	    <input type="hidden" name="fin" value="<?php echo $fin;?>" />
<?php
	} else {?>
	    <input type="hidden" name="duree" value="<?php echo $duree;?>" />
<?php
	}?>

	    <input type='hidden' name='objectParameter' value='<?php echo addslashes(urlencode(serialize($objectParameter)));?>' />
	    <input type='hidden' name='classeForICal' value='<?php echo addslashes(urlencode(serialize($classeForICal)));?>' />
	    <input type='hidden' name='adminMail' value='<?php echo addslashes(urlencode(serialize($adminMail)));?>' />
	    <input type='hidden' name='langueAdminMail' value='<?php echo addslashes(urlencode(serialize($langueAdminMail)));?>' />
	    <input type='hidden' name='classeAdminMail' value='<?php echo addslashes(urlencode(serialize($classeAdminMail)));?>' />

<?php
	if ($multi == -1) {
	    if ($nbJoursConflit == 0) {
	    echo "<input type=\"submit\" value=\"".$_SESSION['s_language']['execresa_confirm']."\" />\n";
	    }
	} else {
	    if ($nbJoursOK > 0) {
	    echo "<input type=\"submit\" value=\"".$_SESSION['s_language']['execresa_confirm']."\" />\n";
	    }
	}?>
	    <input type="button" value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	        onclick="javascript:window.open('<?php echo $page_accueil;?>', '_self')" />
	</div>
	</form>
<?php
}

echo $body_end;
?>
