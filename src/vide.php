<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File vide.php
*
* This file is the help file
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 0) || (count($_GET) != 0)) exitWrongSignature('vide.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e	 	  **************
**********************************************************************************************/


$image_suppress = "<img src='img/poub.gif' align='middle' alt='".$_SESSION['s_language']['help_suppress']."' />";
$image_edit = "<img src='img/edit.gif' align='middle' alt='".$_SESSION['s_language']['help_edit']."' />";
$image_validate = "<img src='img/cadenasO.gif' align='middle' alt='".$_SESSION['s_language']['help_validate']."' />";
$image_validate_closed = "<img src='img/cadenasFr.gif' alt='".$_SESSION['s_language']['help_validate']."' />";

echo $entete;

echo "<body>";
echo "<font size='+1'>".sprintf($_SESSION['s_language']['help_titre'], $version)."</font>";
echo "<p align='justify'><font size='-1'>";
echo $_SESSION['s_language']['help_javascript']."<br /><br />";


/***********************************************************************************************
**************				 General presentation	 		  **************
**********************************************************************************************/
if ($read_only_enable) echo $_SESSION['s_language']['help_1.restriction']; else echo $_SESSION['s_language']['help_1'];
echo " ";

if ($modification_enable) echo sprintf($_SESSION['s_language']['help_1.2'], $image_suppress, $image_edit);
else echo sprintf($_SESSION['s_language']['help_1.1'], $image_suppress);

echo "<br /><br />";


/***********************************************************************************************
**************			 Objects and validation	 			  **************
**********************************************************************************************/
if (get_objectValidationExists()){
	$listeOfObjects = "";
	$DB_request = "SELECT distinct nom FROM objet WHERE status = 1";
	$resultat_objet = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat_objet)) $listeOfObjects .= ", <i>".database_get_from_object($row, 'nom')."</i>";
	$listeOfObjects = substr($listeOfObjects,2);
	$listeOfObjects = replaceLastOccurenceOfComa($listeOfObjects, 'or');

	echo sprintf($_SESSION['s_language']['help_2.0'], $listeOfObjects);
	echo " ";
	echo sprintf($_SESSION['s_language']['help_2.1'], $image_validate, $image_suppress);
	echo "<br />".$_SESSION['s_language']['help_5']."<br />".$_SESSION['s_language']['help_6']."<br />".$_SESSION['s_language']['help_7']."<br />";
} else echo $_SESSION['s_language']['help_2.no_validation']."<br />";

echo $_SESSION['s_language']['help_8']."<br />";
echo sprintf($_SESSION['s_language']['help_9'], $image_validate_closed)."<br /><br />";


/***********************************************************************************************
**************			 Class not available on the current day		  **************
**********************************************************************************************/
$DB_request = "SELECT nom FROM classe WHERE jour_meme = 0";
$resultat_classe = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$list_of_class = "";
$nb = 0;
while ($row = database_fetch_object($resultat_classe)){
	$list_of_class .= ", <i>".database_get_from_object($row, 'nom')."</i>";
	$nb++;
}
if ($nb != 0){
	$list_of_class = substr($list_of_class,2);
	if ($nb == 1){
		echo sprintf($_SESSION['s_language']['help_classe'], $list_of_class);
	} else{
		echo sprintf($_SESSION['s_language']['help_classes'], replaceLastOccurenceOfComa($list_of_class, 'or'));
	}
	echo "<br /><br />";
}


/***********************************************************************************************
**************			 Objects with priority		 		  **************
**********************************************************************************************/
if (get_priorityExists()){
	$DB_request = "SELECT distinct nom FROM objet WHERE priority = 1";
	$resultat_objet = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$nb = 0;
	$inter = "";
	while ($row = database_fetch_object($resultat_objet)){
		$inter .= ", <i>".database_get_from_object($row, 'nom')."</i>";
		$nb++;
	}
	$inter = substr($inter,2);
	if ($nb == 1){
		echo sprintf($_SESSION['s_language']['help_priority'], $inter);
	} else{
		echo sprintf($_SESSION['s_language']['help_prioritys'], replaceLastOccurenceOfComa($inter, 'and'));
	}
	echo "<br /><br />";
}


/***********************************************************************************************
**************			 Objects with wifi		 		  **************
**********************************************************************************************/
$DB_request = "SELECT nom FROM objet WHERE wifi = 1";
$resultat_wifi = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

$nb = 0;
$inter = "";
while ($row = database_fetch_object($resultat_wifi)){
	$inter .= ", <i>".database_get_from_object($row, 'nom')."</i>";
	$nb++;
}
if ($nb != 0){
	$inter = substr($inter,2);
	if ($nb == 1){
		echo sprintf($_SESSION['s_language']['help_wifi'], $inter);
	} else{
		echo sprintf($_SESSION['s_language']['help_wifis'], replaceLastOccurenceOfComa($inter, 'and'));
	}
	echo "<br /><br />";
}


/***********************************************************************************************
**************			 Some extra general presentations 		  **************
**********************************************************************************************/
//echo $_SESSION['s_language']['help_vehicule']."<br /><br />";

echo $_SESSION['s_language']['help_11'];

echo "<ul>";
echo "<li>".$_SESSION['s_language']['help_12']."</li>";
echo "<li>".sprintf($_SESSION['s_language']['help_13'], $_SESSION['s_language']['calendrier_recherche'])."</li>";
echo "</ul>";
echo "<p>";

echo $_SESSION['s_language']['help_14'];
echo "<ul>";
echo "<li>".$_SESSION['s_language']['help_15']."</li>";
echo "<li>".$_SESSION['s_language']['help_16']."</li>";
echo "<li>".$_SESSION['s_language']['help_17']."</li>";
echo "<li>".$_SESSION['s_language']['help_18']."</li>";
echo "</ul>";
echo "<p>";


/***********************************************************************************************
**************			 How to find help		 		  **************
**********************************************************************************************/
echo "<br />\n";
echo $_SESSION['s_language']['help_21']."<p>";
echo "<ul>\n";
$DB_request = "SELECT distinct C.nom AS nomclasse, A.nom, A.prenom, A.mail, A.telephone FROM administrateur A, classe C WHERE A.id_classe = C.id ORDER BY C.nom";
$resultat_admin = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat_admin)){
	$emailCrypte = PHPcrypt("mailto:".database_get_from_object($row, 'mail')." ?subject=".$_SESSION['s_language']['help_22']);
	$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<a href='\", \"$emailCrypte\", \"'>\")</script>";
	$personne = database_get_from_object($row, 'prenom')." ".database_get_from_object($row, 'nom');
	if ($personne == " "){
		$emailCrypte2 = PHPcrypt(database_get_from_object($row, 'mail'));
		$afficheEmailCrypte2 = "<script type='text/javascript'>decrypt(\"\", \"$emailCrypte2\", \"\")</script>";
		$personne = $afficheEmailCrypte2;
	}
	$tel = database_get_from_object($row, 'telephone');
	if ($tel == "") $messageTel = ""; else $messageTel = " - ".$_SESSION['s_language']['help_23']." ".$tel;
	echo "<li><align=left>".database_get_from_object($row, 'nomclasse').$_SESSION['s_language']['ponctuation']." $afficheEmailCrypte <font size='-1'>$personne</font></a>$messageTel<br />"."</li>";
}
echo "</ul>\n";

$emailCrypte = PHPcrypt("mailto:".$technical_email."?subject=".$_SESSION['s_language']['help_25']);
$afficheEmailCrypte = "<script type='text/javascript'>decrypt(\"<a href='\", \"$emailCrypte\", \"'>\")</script>";
echo "<p>".$_SESSION['s_language']['help_24']." $afficheEmailCrypte";
echo $technical_contact."</a> - ".$_SESSION['s_language']['help_23']." ".$technical_tel."</p>\n";

$help_file = "commun/tutorialReservation_".$_SESSION['s_current_language'].".pdf";
if ( ! file_exists($help_file)) $help_file = "commun/tutorialReservation_english.pdf";
echo "<p><a href='$help_file' target='_blank'>".$_SESSION['s_language']['help_26']."</a><br /><br />";

if ($page_accueil != "vide.php") echo "<a href=$page_accueil>".$_SESSION['s_language']['help_27']."</a>";

echo $body_end;
?>
