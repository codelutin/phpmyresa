<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File execefface.php
*
* This file is used to execute the deletion of a reservation
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');
if ($read_only)	exit($exit_message_authentification);

if ($iCal) require_once('wiCal.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_POST) != 12) || (count($_GET) != 0)) exitWrongSignature('execefface.php');
$availableClass = getAvailableClass();
if (isset($_POST['id'])){
	$id = $_POST['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['idmulti'])){
	$idmulti = $_POST['idmulti'];
	if ( ! ctype_digit($idmulti) || ($idmulti == '') ) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['multi'])){
	$multi = $_POST['multi'];
	if ( ! in_array($multi, array("non", "oui"))) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['raison'])){
	$raison = $_POST['raison'];		// que pour l'envoi de mail
	$raison = htmlspecialchars($raison);
} else exitWrongSignature('execefface.php');
if (isset($_POST['objet'])){
	$objet = $_POST['objet'];
	if ( ! in_array($objet, getAvailableObjects()) && ($objet != -1) ) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['motdepasse'])){
	$motdepasse= $_POST['motdepasse'];
	$motdepasse = database_real_escape_string($motdepasse);
} else exitWrongSignature('execefface.php');
if (isset($_POST['titleObjects'])){
	$titleObjects = $_POST['titleObjects'];			// que pour l'envoi d'email
	$titleObjects = htmlspecialchars($titleObjects);
} else exitWrongSignature('execefface.php');
if (isset($_POST['titleTypeAndDate'])){
	$titleTypeAndDate = $_POST['titleTypeAndDate'];		// que pour l'envoi d'email
	$titleTypeAndDate = htmlspecialchars($titleTypeAndDate);
} else exitWrongSignature('execefface.php');
if (isset($_POST['nbJours'])){
	$nbJours = $_POST['nbJours'];				// que pour l'envoi d'email
	if ( ! ctype_digit($nbJours) || ($nbJours == '') ) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['selectedDate'])){
	$selectedDate = $_POST['selectedDate'];
	if ( ! ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $selectedDate)) exitWrongSignature('execefface.php');
} else exitWrongSignature('execefface.php');
if (isset($_POST['classeForICal'])){
	$classeForICal = unserialize(urldecode(stripslashes($_POST['classeForICal'])));
	for ($i = 0 ; $i < count($classeForICal) ; $i++){
		if ( ! in_array($classeForICal[$i], $availableClass) ) exitWrongSignature('execefface.php');
	}
} else exitWrongSignature('execefface.php');
if (isset($_POST['classe'])){
	$classe = unserialize(urldecode(stripslashes($_POST['classe'])));
	for ($i = 0 ; $i < count($classe) ; $i++){
		if ( ! in_array($classe[$i], $availableClass) ) exitWrongSignature('execefface.php');
	}
} else exitWrongSignature('execefface.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

if ($idmulti == 0) $DB_request_condition = "R.id=$id"; else $DB_request_condition = "R.idmulti=$idmulti";

$password_OK = false;
$passwordUser_OK = false;
$listClassId = "";
$listAdminEmail = "";
$classNameOK = array();
$i = 0;
$DB_request = "SELECT DISTINCT A.pass, C.id, C.nom, A.mail, R.pass AS userpass FROM administrateur A, reservation R, objet O, classe C ";
$DB_request .= "WHERE R.idobjet = O.id AND O.id_classe = C.id AND A.id_classe = C.id AND R.state=0 AND ".$DB_request_condition;
if ($objet != -1) $DB_request .= " AND O.nom = '$objet'";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
while ($row = database_fetch_object($resultat)){
	if ($motdepasse == database_get_from_object($row, 'userpass')){
	    $password_OK = true;
	    $passwordUser_OK = true;
	    break;
	}
	if ($motdepasse == database_get_from_object($row, 'pass')){
	    $password_OK = true;
	    $className = database_get_from_object($row, 'nom');
	    if ( ! in_array($className, $classNameOK)){
	        $listClassId .= "'".database_get_from_object($row, 'id')."',";
	        $classNameOK[$i] = $className;
	        $email = database_get_from_object($row, 'mail');
	        if (strpos($listAdminEmail, $email) === FALSE) $listAdminEmail .= $email.",";
	    }
	    $i++;
	}
}
$listClassId = substr($listClassId, 0, -1);
$listAdminEmail = substr($listAdminEmail, 0, -1);

echo $entete;
?>

<body style='font-size:small'>

<?php
if ($password_OK) {
	/***************************************************************************
	****                Case of RIGHT password                              ****
	****************************************************************************/
	// Get all required information on the reservation(s) before the cancellation
	$DB_request = "SELECT DISTINCT email, titre, commentaire FROM reservation R WHERE R.state=0 AND ".$DB_request_condition;
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	$row = database_fetch_object($resultat);
	$to = database_get_from_object($row, 'email');
	$title = database_get_from_object($row, 'titre');
	$commentaire = database_get_from_object($row, 'commentaire');

	if ($idmulti == 0) 	// case of reservations of a single object for only one day
	    $DB_request = "UPDATE reservation SET state=1 WHERE id=$id";
	else { 	        // case of reservations of multiple objects for only one day or continuous reservations or periodic reservations
	    $DB_request = "SELECT DISTINCT R.id FROM reservation R, objet O, classe C WHERE R.state=0 AND R.idmulti=$idmulti ";
	    if (($objet == -1) && ! $passwordUser_OK)
	        $DB_request .= "AND R.idobjet = O.id AND O.id_classe = C.id AND C.id IN ($listClassId) ";
	    else if ($objet != -1)
	        $DB_request .= "AND R.idobjet = O.id AND O.nom = '$objet' ";
	    if ($multi == 'non') $DB_request .= " AND R.jour = '$selectedDate'";
	    $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $listId = "";
	    while ($row = database_fetch_object($resultat)){
	        $listId .= database_get_from_object($row, 'id').",";
	    }
	    if ($listId == ""){
	        $mes = "<div><span class='lblue'>".$_SESSION['s_language']['execefface_not_found']."</span><br /><br />\n";
	        $mes .= "<br /><br /><br /><a href='vueMois.php'>".$_SESSION['s_language']['invitevalide_planning']."</a></div>\n";
	        exit($mes);
	    }
	    $listId = substr($listId, 0, -1);
	    $DB_request = "UPDATE reservation SET state=1 WHERE id IN ($listId)";
	}
	database_query($DB_request, $connexionDB) or errorDB($DB_request, false);


	/***************************************************************************
	****                    Part email send                                 ****
	****************************************************************************/
	$sujet = $_SESSION['s_language']['execefface_mail_subject']." $titleObjects, '$title'";
	$contenu  = $_SESSION['s_language']['execefface_mail_resa']."\n\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_subject']." ".strtolower($titleObjects)."\n";
	$contenu .= $_SESSION['s_language']['execresa_mail_title']." $title\n";
	$contenu .= $_SESSION['s_language']['reservation_kind']." $titleTypeAndDate\n";
	if ($commentaire != '') $contenu .= $_SESSION['s_language']['title_array12']." $commentaire\n";
	$contenu .= "\n";

	if ($idmulti == 0){
	    if ($passwordUser_OK) $contenu .= $_SESSION['s_language']['execefface_by_user']."\n\n";
	    else $contenu .= $_SESSION['s_language']['execefface_by_admin']."\n\n";
	} else {
	    $DB_request = "SELECT count(*) AS nb FROM reservation WHERE state=0 AND idmulti=$idmulti";
	    $resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	    $row = database_fetch_object($resultat);
	    $nb = database_get_from_object($row, 'nb');
	    if ($nb == 0){
	        if ($passwordUser_OK) $contenu .= $_SESSION['s_language']['execefface_by_user']."\n\n";
	        else $contenu .= $_SESSION['s_language']['execefface_by_admin']."\n\n";
	    } else {
	        if ($passwordUser_OK) $contenu .= $_SESSION['s_language']['execefface_by_user2']."\n\n";
	        else $contenu .= $_SESSION['s_language']['execefface_by_admin2']."\n\n";
	    }
	    if ($nbJours != 1){
	        if ($multi == 'non') $contenu .= $_SESSION['s_language']['execefface_only_one_date']." ".displayCurrentLanguageDate($selectedDate, 0)."\n\n";
	        else $contenu .= $_SESSION['s_language']['execefface_all']." \n\n";
	    }
	    if ($objet != -1) $contenu .= $_SESSION['s_language']['execefface_only_one_object']." $objet\n\n";
	}

	if ($raison != "") $contenu .= $_SESSION['s_language']['execefface_raison']."\n".$raison."\n\n";
	$contenu .= "\n".sprintf($_SESSION['s_language']['execefface_new_resa'], WEB)."\n";
	$contenu = ereg_replace("\\\'","'",$contenu);
	$contenu = ereg_replace("\\\\\"","\"",$contenu);
	if ($passwordUser_OK) $from = "From: $to"; else $from = "From: $listAdminEmail";
	$destinataire = str_replace($web_server, $mail_server, $to);
	$sendUser = mail($destinataire,$sujet,$contenu,$from);


	/***************************************************************************
	****                Display in PHPMyResa                                ****
	****************************************************************************/
?>
<div>
	<span class='lblue'><?php echo $_SESSION['s_language']['execefface_done'];?></span>
	<br /><br />
<?php
	if ($sendUser) {
	    echo $_SESSION['s_language']['execefface_notification']." $to \n";
	} else {
	    echo $_SESSION['s_language']['execefface_notification_false']." $to ".$_SESSION['s_language']['execefface_notification_false_2']." \n";
	}?>
	<br /><br /><br />
	<a href='vueMois.php'><?php echo $_SESSION['s_language']['invitevalide_planning'];?></a>
</div>
<?php
	if ($iCal){
	    for ($i = 0; $i < count($classeForICal); $i++) wiCal($classeForICal[$i]);
	}

} else {

	/***************************************************************************
	****                Case of WRONG password                              ****
	****************************************************************************/
?>
<div>
	<span class='bred'><?php echo $_SESSION['s_language']['incorrect_password'];?></span>
	<br /><br />
	<b><?php echo $_SESSION['s_language']['delete_ask'];?></b>
	<ul>
		<?php
		echo "<li>".$_SESSION['s_language']['ask_user_password']."</li>\n";
		echo "<li>".$_SESSION['s_language']['or']." ";
		if (count($classe) == 1){
			$SPECIAL_classe_to_display = $classe[0];
			$text = $_SESSION['s_language']['ask_admin_password_for_one_class'];
			eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_one_class']."\";" );
			echo $text;
		} else{
			$SPECIAL_classe_to_display = "";
			for ($i = 0 ; $i < count($classe) ; $i++) $SPECIAL_classe_to_display .= "&#39;".$classe[$i]."&#39;, ";
			$SPECIAL_classe_to_display = replaceLastOccurenceOfComa(substr($SPECIAL_classe_to_display, 0, -2), 'or');
			eval( "\$text = \"".$_SESSION['s_language']['ask_admin_password_for_several_classes']."\";" );
			echo $text;
			echo "<br /><br />";
		} ?>
		</li>
	</ul>
	<br />
</div>


	<form id='executeEfface' action='execefface.php' method='post'>
	<div>
	    <?php echo $_SESSION['s_language']['password'];?> <input name='motdepasse' type='password' value='' /> &nbsp;
	    <input type='hidden'name='multi' value='<?php echo $multi;?>' />
	    <input type='hidden' name='id' value='<?php echo $id;?>' />
	    <input type='hidden' name='idmulti' value='<?php echo $idmulti;?>' />
	    <input name='raison' type='hidden' value='<?php echo $raison;?>' />
	    <input type='hidden' name='objet' value='<?php echo $objet;?>' />
	    <input type='hidden' name='titleObjects' value='<?php echo $titleObjects;?>' />
	    <input type='hidden' name='titleTypeAndDate' value='<?php echo $titleTypeAndDate;?>' />
	    <input type='hidden' name='nbJours' value='<?php echo $nbJours;?>' />
	    <input type='hidden' name='selectedDate' value='<?php echo $selectedDate;?>' />
	    <input type='hidden' name='classeForICal' value="<?php echo addslashes(urlencode(serialize($classeForICal)));?>" />
	    <input type='hidden' name='classe' value="<?php echo addslashes(urlencode(serialize($classe)));?>" />
	    <input type='submit' value="<?php echo $_SESSION['s_language']['efface_submit'];?>" /> &nbsp;
	    <input type='button' value="<?php echo $_SESSION['s_language']['reservation_cancel'];?>"
	        onclick="javascript:window.open('index.php', '_parent')" />
	</div>
	</form>
<?php
}
echo $body_end;
?>
