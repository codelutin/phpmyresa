<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File calendrier.php
*
* This file is used by the left frame of the application. It contains main menus.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Gilbert Federico <g.federico@saxxo.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004,2006 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Gilbert Federico
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link	    http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if (count($_GET) != 0) exitWrongSignature('calendrier.php');
if ($tree){
	if (($nbpar != 6) && ($nbpar != 5) && ($nbpar != 0)) exitWrongSignature('calendrier.php');
} else {
	//at the beginning there is no value, after a reload from a date change, there are two cases: with or without selected object
	if (($nbpar != 2) && ($nbpar != 0) && ($nbpar != 3)) exitWrongSignature('calendrier.php');
}

if (isset($_POST['opened'])){
	$opened = $_POST['opened'];
	for ($i = 0 ; $i < count($opened) ; $i++) if ( ! ctype_digit($opened[$i]) && ($opened[$i] != "")) exitWrongSignature('calendrier.php');
} else $opened = array("0", "1");
if (isset($_POST['selected'])){
	$selected = $_POST['selected'];
	for ($i = 0 ; $i < count($selected) ; $i++) if ( ! ctype_digit($selected[$i]) && ($selected[$i] != "")) exitWrongSignature('calendrier.php');
} else $selected = array("");
if (isset($_POST['treeType'])){
	$treeType = $_POST['treeType'];
	if ( ! in_array($treeType, array("", "folder", "document"))) exitWrongSignature('calendrier.php');
} else $treeType = '';
if (isset($_POST['treeObjet'])){
	$treeObjet = $_POST['treeObjet'];
	if ( ! ctype_digit($treeObjet) && ($treeObjet != "") ) exitWrongSignature('calendrier.php');
} else $treeObjet = '';
if (isset($_POST['objet'])){
	$temp = $_POST['objet'];
	if ($several_object_possible) $objectsTab = $temp;
	else $objectsTab = array($temp);
	$availableObjects = getAvailableObjects();
	for ($i = 0 ; $i < count($objectsTab) ; $i++){
		if ( ! in_array($objectsTab[$i], $availableObjects) ) exitWrongSignature('calendrier.php');
	}
} else $objectsTab = array("");
if (isset($_POST['moisSelect'])){
	$mois = $_POST['moisSelect'];
	if ( ! ctype_digit($mois)|| ($mois == '') ) exitWrongSignature('calendrier.php');
	if ($mois > 12) exitWrongSignature('calendrier.php');
} else $mois = $moisCourant;
if (isset($_POST['anneeSelect'])){
	$annee = $_POST['anneeSelect'];
	if ( ! ctype_digit($annee)|| ($annee == '') ) exitWrongSignature('calendrier.php');
	if (strlen($annee) > 4) exitWrongSignature('calendrier.php');
} else $annee = $anneeCourante;

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e	 	  **************
**********************************************************************************************/

/**
* This function is used to suppress a value from an array
* @param int the value to suppress
* @param array[int] the array to investigate
* @return void nothing, the array is directly changed
*/
function array_remove($value, &$array){
	$j = 0;
	$temp = array();
	for ($i = 0 ; $i < count($array) ; $i++){
	    if ($array[$i] != $value){
	        $temp[$j] = $array[$i];
	        $j++;
	    }
	}
	$array = $temp;
}

if ($treeType == "folder"){
	if (in_array($treeObjet, $opened)) array_remove($treeObjet, $opened);
	else $opened[count($opened)] = $treeObjet;
} else if ($treeType == "document"){
	if ($several_object_possible){
	    if (in_array($treeObjet, $selected)) array_remove($treeObjet, $selected);
	    else{
	        if (count($selected) != 1 || $selected[0] != "") $selected[count($selected)] = $treeObjet;
	        else $selected[0] = $treeObjet;
	    }
	} else $selected[0] = $treeObjet;
}

/***********************************************************************************************************
****	    RÃ©cupÃ©ration des caractÃ©ristiques des objets et des classes dans la bd	        ****
***********************************************************************************************************/
$objet_tab = array();
$idx=0;
$oldclasse='';
$DB_request = "SELECT C.nom AS nomclasse, O.nom AS nomobjet, O.capacite, O.available FROM classe C, objet O WHERE C.id = O.id_classe AND O.visible = 1 ORDER BY C.id, O.id";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, true);
while ($row = database_fetch_object($resultat)){
	$classe = strtoupper(database_get_from_object($row, 'nomclasse'));
	if ($oldclasse == $classe){
	    $objet = database_get_from_object($row, 'nomobjet');
	    if (database_get_from_object($row, 'available') == 0) $objet .= ' '.$_SESSION['s_language']['calendrier_dispo'];
	    $objet_tab["type"][$idx] = "objet";
	    $objet_tab["nom"][$idx] = $objet;
	    $objet_tab["capacite"][$idx] = database_get_from_object($row, 'capacite');
	    $idx++;
	} else {
	    $objet_tab["type"][$idx] = "classe";
	    $objet_tab["nom"][$idx] = $classe;
	    $objet_tab["capacite"][$idx] = database_get_from_object($row, 'capacite');
	    $idx++;
	    $oldclasse = $classe;

	    $objet = database_get_from_object($row, 'nomobjet');
	    if (database_get_from_object($row, 'available') == 0) $objet .= ' '.$_SESSION['s_language']['calendrier_dispo'];
	    $objet_tab["type"][$idx] = "objet";
	    $objet_tab["nom"][$idx] = $objet;
	    $objet_tab["capacite"][$idx] = database_get_from_object($row, 'capacite');
	    $idx++;
	}
}


/***********************************************************************************************************
****            RÃ©cupÃ©ration des caractÃ©ristiques des objets pour lesquels jour_meme = 0                ****
***********************************************************************************************************/
$DB_request = "SELECT count(*) AS res FROM classe WHERE jour_meme=0";
$resultat_objet = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$row = database_fetch_object($resultat_objet);
if (database_get_from_object($row, 'res') == 0) $jourMemeExists = false; else $jourMemeExists = true;

if ($jourMemeExists){
	$objet_jour_meme = array();
	$k = 0;
	$DB_request = "SELECT O.nom, A.telephone FROM objet O, classe C, administrateur A WHERE C.jour_meme = 0 AND C.id = O.id_classe AND C.id = A.id_classe";
	$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
	while ($row = database_fetch_object($resultat)){
	    $objet_jour_meme['nom'][$k] = database_get_from_object($row, 'nom');
	    $objet_jour_meme['tel'][$k] = database_get_from_object($row, 'telephone');
	    $k++;
	}
	if (count($objet_jour_meme) == 0) $jourMemeExists = false;	// C'est le cas d'une classe sans object
}

echo $entete;
?>

<body style='font-size:small'>

<?php
if ($tree){

	/**
	* Used to load each value
	* @param int to write
	* @param int to write
	* @return void to write
	*/
	function load_value(&$item, $key){
	    global $objet_tab;
	    $item = $objet_tab["nom"][$item-2];
	}

	require_once('tree.php');
	if (count($selected) != 1 || $selected[0] != ""){
	    $objectsTab = $selected;
	    array_walk($objectsTab, 'load_value');
	}
	else $objectsTab= array();
	echo "\n<style type='text/css'>.innerb {height:14em; overflow:auto;}</style>\n";
}
?>

<script type='text/javascript' src='./commun/sprintf.js'>\n</script>

<script type="text/javascript"><!--

function go(rowid, type){
	document.forms.genererCalendrier.action += '#'+rowid;
	document.forms.genererCalendrier.treeType.value = type;
	document.forms.genererCalendrier.treeObjet.value = rowid;
	document.forms.genererCalendrier.submit();
}

function startResa(jour){

	var form = document.forms.genererCalendrier;
	var moisSelectionne = form.moisSelect.options[form.moisSelect.selectedIndex].value;
	var anneeSelectionnee = form.anneeSelect.options[form.anneeSelect.selectedIndex].value;

	var date1 = new Date();
	var anneeCourante = parseInt(date1.getFullYear());
	var moisCourant = parseInt(date1.getMonth()+1);
	var jourCourant = parseInt(date1.getDate());

	<?php
	if ($tree){
	    echo "var objets = new Array();\n";
	    for ($i = 0 ; $i < count($objectsTab) ; $i++) echo "objets[$i] = \"$objectsTab[$i]\";\n";
	} else { ?>

	var objet = form.elements[0];
	var objets = new Array();
	var j = 0;
	for (i=0; i<objet.options.length; i++){
	    if (objet.options[i].selected){
	        var inter = objet.options[i].value;
	        if ( (inter == '-') || (inter == 'titre') ){
	            alert ("<?php echo $_SESSION['s_language']['calendrier_error1_J'];?>");
	            return;
	        }
	        objets[j] = inter;
	        j++;
	    }
	}

	<?php  }?>

	if (objets.length == 0){
	    alert ("<?php echo $_SESSION['s_language']['calendrier_error1_J'];?>");
	    return;
	}

	for (i=0; i<objets.length; i++){
	    var inter = objets[i];
	    <?php $nb_dipo = strlen($_SESSION['s_language']['calendrier_dispo']);?>
	    if (inter.length > <?php echo $nb_dipo+2;?>){
	        if (inter.substr(inter.length-<?php echo $nb_dipo.','.$nb_dipo;?>) == '<?php echo $_SESSION['s_language']['calendrier_dispo'];?>'){
	            inter = inter.substr(0, inter.indexOf(' ('));
	            alert(sprintf("<?php echo $_SESSION['s_language']['calendrier_dispo_msg_J'];?>", inter));
	            return;
	        }
	    }

	    <?php
	    if ($jourMemeExists){
	        echo "if ( (jour == jourCourant) && (moisSelectionne == moisCourant) && (anneeSelectionnee == anneeCourante) ){\n";
	        for ($i = 0; $i < count($objet_jour_meme['nom']); $i++){
	            echo "  	            ";
	            echo "if (inter == '".$objet_jour_meme['nom'][$i]."') alert(\"".$_SESSION['s_language']['calendrier_error3.1_J']." '".$objet_jour_meme['nom'][$i]."' ".$_SESSION['s_language']['calendrier_error3.2_J']." ";
	            $tel = $objet_jour_meme['tel'][$i];
	            if ($tel == "") echo $_SESSION['s_language']['calendrier_error3.4_J']."\");\n";
	        else echo $_SESSION['s_language']['calendrier_error3.3_J']." ".$objet_jour_meme['tel'][$i]."\");\n";
	        }
	        echo "	        }\n\n";
	    }
	?>
	}

	if (compareDate(anneeSelectionnee, anneeCourante, moisSelectionne, moisCourant, jour, jourCourant, 'false') == 'false'){
	    alert("<?php echo $_SESSION['s_language']['calendrier_error2_J'];?>");
	    return;
	}

	document.forms.resa.objet.value = objets.toString();
	document.forms.resa.jour.value = jour;
	document.forms.resa.mois.value = moisSelectionne;
	document.forms.resa.annee.value = anneeSelectionnee;

	<?php
	$emailCrypte = PHPcrypt($technical_email);
	if ($read_only) echo "alert(\"".$exit_message_javascript_authentification." (\" + decryptJ(\"\", \"$emailCrypte\", \"\") + \")\")";
	else echo "document.forms.resa.submit()\n";
	?>
}

function goChangeLanguage(lang){
	document.forms.changeLanguage.lang.value=lang;
	document.forms.changeLanguage.submit();
}

function helpDate(){
	if (document.forms.chercheresa.critere.options[document.forms.chercheresa.critere.selectedIndex].value == 'date') alert ("<?php echo $_SESSION['s_language']['calendrier_help_date_J'];?>");
}

<?php
if ($capacite && ! $tree){
	echo "\n";
	echo "function infoObjet(index){
	    var i;
	    var count = 0;
	    var objet = document.forms.genererCalendrier.elements[0];
	    for (i=0; i<objet.options.length; i++){
	        if (objet.options[i].selected){
	            count++;
	        }
	    }
	    var capacite = CapaciteObjet[index];
	    if (count > 1) capacite = 0;
	    if (capacite != 0) document.forms.info.capacite.value = capacite;
	    else document.forms.info.capacite.value = '';\n";
	echo "}\n\n";
}
?>

//-->
</script>


<form id='changeLanguage' action='changeLanguage.php' method='post'>
<table class='noborder' style='width:100%'>
    <tr>
        <td class='gauche' style='width:65%;'><input type='hidden' name='lang' value='english' />
<?php
if ( ! empty($image_titre)) echo "<img src='img/$image_titre' alt=\"".$_SESSION['s_language']['title_array5']."\" /><br />";
if ( ! empty($titre)) echo "<span style='font-size:large'>".strtoupper($titre)."</span>";
?>
	</td>
	</tr>
</table>
</form>

<?php
echo "<div><br /><br />";
echo "<b>".$_SESSION['s_language']['calendrier_explication1']."</b></div>\n";
echo "<ol>\n";
if ($several_object_possible) {
	echo "<li>".$_SESSION['s_language']['calendrier_explication5']."</li>\n";
} else {
	echo "<li>".$_SESSION['s_language']['calendrier_explication2']."</li>\n";
}
	echo "<li>".$_SESSION['s_language']['calendrier_explication3']."</li>\n";
	echo "<li>".$_SESSION['s_language']['calendrier_explication4']."</li>\n";
echo "</ol>\n";
?>

<table class='noborder' cellpadding='1' cellspacing='1'>
	<tr style='background-color:#eeeeee'>
	    <td>
	        <form id='genererCalendrier' action='calendrier.php' method='post'>
	        <table>
	            <tr>
	                <td style='vertical-align:top'>

<?php
/***********************************************************************************************************
****            CrÃ©ation de la liste des classes et des objets                                          ****
***********************************************************************************************************/
$capacite_courante = 0;
?>
<?php
if ($tree){
	$tree = new TREE($opened, $selected);
	$tree->setIconPath("img/tree");

	$tree_id = array();
	$tree_id['ROOT'] = $tree->openTree("<b>".$_SESSION['s_language']['liste_objets']."</b>");

	for ($i = 0 ; $i < count($objet_tab["nom"]) ; $i++){
	    $nomObjet = $objet_tab["nom"][$i];
	    if ($objet_tab["type"][$i] == "classe") $tree_id['site'] = $tree->addFolder($tree_id['ROOT'], $nomObjet, "#");
	    else{
	        $tree->addDocument($tree_id['site'], "$nomObjet", "",  "_self");
	        if (in_array($nomObjet, $objectsTab)) $capacite_courante = $objet_tab["capacite"][$i];
	    }
	}
	if (count($selected) != 1) $capacite_courante = "";
	$tree->closeTree();
?>
	                    <table class='noborder'>
	                        <tr>
	                            <td>
<?php
	if ($tree_scrollable) { ?>
	                                <div class='innerb'>
<?php
	}
	                                    echo  $tree->getTree();
	if ($tree_scrollable) { ?>
	                                </div>
<?php
	} ?>
	                            </td>
	                        </tr>
	                    </table>

	                    <div>
<?php
	for ($i = 0 ; $i < count($selected) ; $i ++) {?>
	                        <input type='hidden' name='selected[]' value='<?php echo $selected[$i];?>' />
<?php
	}
	for ($i = 0 ; $i < count($opened) ; $i ++) {?>
	                        <input type='hidden' name='opened[]' value='<?php echo $opened[$i];?>' />
<?php
	}?>
	                        <input type='hidden' name='treeType' value='' />
	                        <input type='hidden' name='treeObjet' value='' />
	                    </div>

<?php
} else { ?>
<?php
	$name = "objet";
	if ($several_object_possible) $name .= "[]";
	?>
	                    <select name='<?php echo $name;?>' size='10'
	                        <?php if ($capacite) echo " onchange='infoObjet(this.selectedIndex);'";?>
	                        <?php if ($several_object_possible) echo " multiple='multiple'";?>  >
<?php
	for ($i = 0 ; $i < count($objet_tab["nom"]) ; $i++){
	    if ($objet_tab["type"][$i] == "classe"){ ?>
	                        <option value='titre' style='background-color:gray;color:white;' disabled='disabled'>
	                            <?php echo $objet_tab["nom"][$i];?></option>
<?php
	    } else {
	        $objet = $objet_tab["nom"][$i];
	        if (in_array($objet, $objectsTab)){
	            $capacite_courante = $objet_tab["capacite"][$i];
	            ?>
	                        <option value='<?php echo $objet;?>' selected='selected'><?php echo $objet;?></option>
<?php
	        } else { ?>
	                        <option value='<?php echo $objet;?>'><?php echo $objet;?></option>
<?php
	        }
	    }
	} ?>
	                    </select>


	        <!-- transfert du tableau PHP vers un tableau JavaScript //-->
	        <script type='text/javascript'><!--
	            var CapaciteObjet=new Array();
<?php
	            for ($idx=0 ; $idx<count($objet_tab["nom"]) ; $idx++){
	                $temp = $objet_tab["capacite"][$idx];
	                echo "	CapaciteObjet[$idx]='$temp";
	                if (is_numeric($temp) && ($temp != 0)) echo " ".$_SESSION['s_language']['calendrier_place'];
	                echo "';\n";
	            } ?>
	        //-->
	        </script>
<?php
} ?>
	                </td>

<?php
/***********************************************************************************************************
****            CrÃ©ation de la liste des mois et des dates                                               ****
***********************************************************************************************************/
?>
	                <td style='vertical-align:top'>
                        <select name='moisSelect' onchange='document.forms.genererCalendrier.submit();'>
<?php
for ($i=1; $i<13; $i++){
	if ($i == $mois) {?>
	                        <option value='<?php echo $i;?>' selected='selected'><?php echo $_SESSION['s_les_mois'][$i-1];?></option>
<?php
	} else {?>
	                        <option value='<?php echo $i;?>'><?php echo $_SESSION['s_les_mois'][$i-1];?></option>
<?php
	}
}?>
	                    </select>

	                    <select name='anneeSelect' onchange='document.forms.genererCalendrier.submit();'>
<?php
for ($i=$anneeCourante; $i<$anneeCourante+4; $i++){
	if ($i == $annee){?>
	                        <option value='<?php echo $i;?>' selected='selected'><?php echo $i;?></option>
<?php
	} else {?>
	                        <option value='<?php echo $i;?>'><?php echo $i;?></option>
<?php
	}
}?>
	                    </select>

<?php
/***********************************************************************************************************
****                CrÃ©ation de la liste du calendrier                                                  ****
***********************************************************************************************************/
calendrier($mois,$annee);
?>
	                </td>
	            </tr>
	        </table>
	        </form>
	    </td>
	</tr>

<?php
if ($capacite){
	if ($capacite_courante == '0') $capacite_courante = "";
	if (is_numeric($capacite_courante) && ($capacite_courante != 0))
	    $capacite_courante .= " ".$_SESSION['s_language']['calendrier_place'];
	?>
	<tr>
	    <td colspan='2'  style='background-color:#d3dce3'>
	    <form id='info' action=''>
	    <table class='all'>
	        <tr>
	            <td style='vertical-align:middle'>
	                <input name='capacite' onfocus='this.blur();' value='<?php echo $capacite_courante;?>' size='30' />
	            </td>
	        </tr>
	    </table>
	    </form>
	    </td>
	</tr>
<?php
	}
?>
</table>

<hr />

<?php
/***********************************************************************************************************
****                CrÃ©ation du module de recherche                                                     ****
***********************************************************************************************************/
?>
<div><b><?php echo $_SESSION['s_language']['calendrier_recherche'];?></b></div>

<form id='chercheresa' action='chercher.php' method='post' target='droite'>
<table class='noborder' style='background-color:#d3dce3' cellpadding='4' cellspacing='0'>
	<tr>
	    <td>
	        <select name='critere' onchange='helpDate();'>
	            <option value='date'><?php echo $_SESSION['s_language']['calendrier_recherche0'];?></option>
	            <option value='titre' selected='selected'><?php echo $_SESSION['s_language']['calendrier_recherche1'];?></option>
	            <option value='email'><?php echo $_SESSION['s_language']['calendrier_recherche2'];?></option>
	            <option value='objet'><?php echo $_SESSION['s_language']['calendrier_recherche3'];?></option>
	            <option value='classe'><?php echo $_SESSION['s_language']['calendrier_recherche4'];?></option>
	        </select>
	    </td>
	    <td>
	        <select name='type'>
	            <option value='1'><?php echo $_SESSION['s_language']['calendrier_recherche_text1'];?></option>
	            <option value='2'><?php echo $_SESSION['s_language']['calendrier_recherche_text3'];?></option>
	            <option value='3'><?php echo $_SESSION['s_language']['calendrier_recherche_text4'];?></option>
	            <option value='4'><?php echo $_SESSION['s_language']['calendrier_recherche_text5'];?></option>
	        </select>
	    </td>
	    <td>
	        <input name='valeurcritere' size='16' />
	    </td>
	    <td>
	        <input type='submit' value='OK' />
	    </td>
	</tr>
</table>
</form>

<div style='font-size:x-small'><?php echo $_SESSION['s_language']['calendrier_recherche_text2'];?>
<br /><br />
<hr />

</div>

<?php
/***********************************************************************************************************
****                Ajout de liens hypertextes                                                          ****
***********************************************************************************************************/

echo "<div>";
echo "<table style='width:100%'><tr><td>";
echo "<a href='vueMois.php' target='droite'>".$_SESSION['s_language']['calendrier_menu1']."</a>";
if ($iCal) echo " - <a href='$URL_iCal' target='droite'>iCal</a>";
// Pour Patricia
//echo " - <a href='today.php?offset=-1' target='droite'>".$_SESSION['s_language']['calendrier_menu4']."</a>";
echo " - <a href='today.php?offset=0' target='droite'>".$_SESSION['s_language']['calendrier_menu2']."</a>";
if ($display_tomorrow) echo " - <a href='today.php?offset=1' target='droite'>".$_SESSION['s_language']['calendrier_menu5']."</a>";
$help_file = "commun/tutorialReservation_".$_SESSION['s_current_language'].".pdf";
if ( ! file_exists($help_file)) $help_file = "commun/tutorialReservation_english.pdf";
echo " - <a href='$help_file' target='droite'>".ucfirst($_SESSION['s_language']['calendrier_tutorial'])."</a>";
echo " - <a href='vide.php' target='droite'>".$_SESSION['s_language']['calendrier_menu3']."<span style='font-weight:bold;font-size:medium'>?</span></a><br />";
echo "</td><td>";

echo "</td>";
echo "<td class='droite'>";

$tab = getAvailableLanguages();
for ($i = 0; $i < count($tab); $i++){
	if ($_SESSION['s_current_language'] != $tab[$i]) echo "<img src='./img/drapeaux/".$tab[$i].".png' height='15pt' alt='".$tab[$i]."' onclick=\"javascript:goChangeLanguage('".$tab[$i]."')\" /> \n";
}
echo "</td></tr></table>";

$img_file = "img/pbi_".$_SESSION['s_current_language'].".gif";
if ( ! file_exists($img_file)) $img_file = "img/pbi_english.gif";
?>
<a href='http://phpmyresa.in2p3.fr' target='_blanck'><img src='<?php echo $img_file;?>' class='noborder' alt='http://phpmyresa.in2p3.fr' /></a>
</div>

<form id='resa' action='reservation.php' target='droite' method='post'>
<p>
	<input name='annee' type='hidden' value='' />
	<input name='mois' type='hidden' value='' />
	<input name='jour' type='hidden' value='' />
	<input name='objet' type='hidden' value='' />
</p>
</form>

<?php echo $body_end;?>
