<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File chercher.php
*
* This file is used to display the result of a search among reservations
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Laurent Quenoy <lquenoy@netcourrier.com>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2003 Laurent Quenoy
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_POST);
if ((($nbpar != 3) && ($nbpar != 4)) || (count($_GET) != 0)) exitWrongSignature('chercher.php');
if (isset($_POST['critere'])){
	$critere = $_POST['critere'];
	if ( ! in_array($critere, array('date', 'titre', 'email', 'objet', 'classe'))) exitWrongSignature('chercher.php');
} else exitWrongSignature('chercher.php');
if (isset($_POST['type'])){
	$type = $_POST['type'];
	if ( ! ctype_digit($type) || ($type == '') ) exitWrongSignature('chercher.php');
	if ( $type > 4 ) exitWrongSignature('chercher.php');
} else exitWrongSignature('chercher.php');
if (isset($_POST['valeurcritere'])){
	$valeurcritere = $_POST['valeurcritere'];
	$valeurcritere = database_real_escape_string($valeurcritere);
	$valeurcritere = htmlspecialchars($valeurcritere);
} else exitWrongSignature('chercher.php');
if (isset($_POST['toutVoir'])){
	$toutVoir = $_POST['toutVoir'];
	if ( $toutVoir != 'on' ) exitWrongSignature('chercher.php');
} else $toutVoir = false;

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

echo $entete;
?>

<body style='font-size:small'>

<script type='text/javascript' src='<?php echo $URL_overlib;?>overlib.js'></script>
<div id='overDiv' style='position:absolute; visibility:hidden; z-index:1000;'></div>

<?php
echo getResaJavascriptContent();

$DB_request = createSearchDBrequest($critere, $type, $valeurcritere, $toutVoir);
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$tab = getResaContent($resultat, true, true, true);
?>

<table class='all'>
	<tr>
	    <td class='gauche' style='width:60%;'>
	        <span style='font-size:large;'><?php echo $_SESSION['s_language']['chercher_title1'];?></span>

<?php
$nb = $tab[1];
if ($nb == 1){
	echo " (1 ".$_SESSION['s_language']['reservation'].")";
} else if ($nb != 0){
	echo " ($nb ".$_SESSION['s_language']['reservations'].")";
}
?>

	    </td>
	    <td style='width:40%;'>
	        <form id='voirHistorique' action='chercher.php' method='post'>
	        <table class='all'>
	            <tr>
	                <td class='droite' style='font-size:x-small'>
	                        <input type='hidden' name='critere' value='<?php echo $critere;?>' />
	                        <input type='hidden' name='type' value='<?php echo $type;?>' />
	                        <input type='hidden' name='valeurcritere' value='<?php echo $valeurcritere;?>' />
	                        <?php echo $_SESSION['s_language']['chercher_historic'];?>&nbsp;
	                        <input type='checkbox' name='toutVoir' onclick='submit();'
	                            <?php if ($toutVoir == true) echo " checked='checked'";?> />
	                </td>
	            </tr>
	        </table>
	        </form>
	    </td>
	</tr>
</table>
<div> <br /> </div>

<?php
if ($nb == 0){
	echo "<p class ='red' style='font-size:large'>".$_SESSION['s_language']['chercher_no_result']."</p>";
} else{
	if ($generation_excel){
	?>
	    <form id='genexcel' action='genexcel.php' method='post'>
	    <table class='all' style='background-color:#D3DCE3'>
	        <tr>
	            <td class='center'>
	                <input type='hidden' name='critere' value='<?php echo $critere;?>' />
	                <input type='hidden' name='type' value='<?php echo $type;?>' />
	                <input type='hidden' name='valeurcritere' value='<?php echo $valeurcritere;?>' />
	                <input type='hidden' name='toutVoir' value='<?php echo $toutVoir;?>' />
	                <input type='submit' value="<?php echo $_SESSION['s_language']['chercher_save_excel'];?>" />
	            </td>
	        </tr>
	        <tr>
	            <td class='center'>
	                <?php echo$_SESSION['s_language']['chercher_print1'];?>
	                <a href='javascript:this.print();'><?php echo $_SESSION['s_language']['chercher_print2'];?></a>
	            </td>
	        </tr>
	    </table>
	    </form>
	    <div> <br /></div>
<?php
	}

	if (get_priorityExists()){
		$message = getPriorityMessage($tab[2]);
		if ($message != ""){
			echo "<div style='font-size:x-small'>";
			echo $_SESSION['s_language']['priority_1']." (".$message.")<br /><br />";
			echo "</div>";
		}
	}
	echo $tab[0];
}

echo getResaFormContent();
echo $body_end;
?>
