<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File rss.php
*
* This file is used to create rss export
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Warin-Charpentier Patricia <Patricia.Warin-Charpentier@lpnhep.in2p3.fr>
*
* @copyright	2008 Daniel Charnay
* @copyright	2008 FrÃ©dÃ©ric Melot
* @copyright	2008 Patricia Warin-Charpentier
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

$nbpar = count($_GET);
if (( ($nbpar != 0) && ($nbpar != 1) && ($nbpar != 2) ) || (count($_POST) != 0)) exitWrongSignature('rss.php');
if (isset($_GET['offset'])){
	$offset = $_GET['offset'];
	if ($offset == '-') exitWrongSignature('rss.php');
	else if (substr($offset, 0, 1) == '-'){
		$tmp_offset = substr($offset, 1);
		if ( ! ctype_digit($tmp_offset) || ($tmp_offset == '') ) exitWrongSignature('rss.php');
	} else if ( ! ctype_digit($offset) || ($offset == '') ) exitWrongSignature('rss.php');
} else $offset = 0;
if (isset($_GET['class'])){
	$class = $_GET['class'];
	if ( ! in_array($class, getAvailableClass()) ) exitWrongSignature('rss.php');
} else $class = $default_class_for_feed;

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$dateChoisie = date("Y-m-d", strtotime($offset." days",strtotime(date("Y-m-d"))));
$date = displayCurrentLanguageDate($dateChoisie, 0);

$DB_request = "SELECT O.nom, R.titre, R.debut, R.duree, R.id, R.commentaire FROM reservation R, objet O, classe C ";
$DB_request .= "WHERE jour  = '$dateChoisie' AND R.idobjet = O.id AND O.available = 1 AND R.state = 0 AND R.diffusion = 1 AND O.id_classe = C.id ";
if ($class != "") $DB_request .= " AND C.nom = '$class' ";
$DB_request .= "ORDER BY O.nom, R.debut";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);

header("Content-Type: application/rss+xml; charset=utf8");
echo "<?xml version='1.0' encoding='utf8' ?>\n";
echo "<rss version='2.0'>\n";
echo "<channel>\n";
echo "<title>PHPMyResa</title>\n";
echo "<link>".WEB."</link>\n";
$temp = $_SESSION['s_language']['rss_description_1']." ".$date." ".$_SESSION['s_language']['rss_description_2']." '$class'";
echo "<description>$temp</description>\n";

while ($row = database_fetch_object ($resultat)) {
	displayFeed($row, $dateChoisie);
}

echo "</channel>\n";
echo "</rss>\n";

function displayFeed($row,$dateChoisie){
// crÃ©ation des items du feed : Titre de la rÃ©union, nom de la salle, date et heure, Ã©ventuellement le commentaire
	global $date;

	echo "<item>\n";
	echo "<title>".database_get_from_object($row, 'titre')."</title>\n";
	$debut = substr(database_get_from_object($row, 'debut'),0,5);
	$debut = ereg_replace (":","h",$debut);
	$duree = substr(database_get_from_object($row, 'duree'),0,5);
	$duree = ereg_replace (":","h",$duree);
	echo "<description>".database_get_from_object($row, 'nom')." - ".$date." ".$_SESSION['s_language']['a']." $debut ".$_SESSION['s_language']['pendant']." $duree";
	$comment = database_get_from_object($row, 'commentaire');
	if ($comment != "") echo " ($comment)";
	echo "</description>\n";
	echo "</item>\n";
}
?>
