<?php
//**********************************************************************************************
/**
* Project PHPMyResa / File affichePDF.php
*
* This file is used to create a PDF output. It is useful especially for room meetings.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* @license 	http://opensource.org/licenses/gpl-license.php GNU Public License
*
* @author	Daniel Charnay <charnay@in2p3.fr>
* @author	FrÃ©dÃ©ric Melot <frederic.melot@lpsc.in2p3.fr>
* @author	Patricia Warin-Charpentier <patricia.warin-charpentier@lpnhep.in2p3.fr>
* @author	Emilie Deloustal
*
* @copyright	2001,2002,2003,2004 Daniel Charnay
* @copyright	2002,2003,2004 Patricia Warin-Charpentier
* @copyright	2003,2004,2005,2006,2008 FrÃ©dÃ©ric Melot
* @copyright	2005 Emilie Deloustal
*
* @package	PHPMyResa
* @subpackage	core
* @link		http://phpmyresa.in2p3.fr
* @version	4.0
*/
//**********************************************************************************************


require_once('commun/commun.php');

/***********************************************************************************************
**************		 DÃ©but de vÃ©rification des paramÃštres en entrÃ©e 	  **************
**********************************************************************************************/

if ((count($_GET) != 1) || (count($_POST) != 0)) exitWrongSignature('affichePDF.php');
if (isset($_GET['id'])){
	$id = $_GET['id'];
	if ( ! ctype_digit($id) || ($id == '') ) exitWrongSignature('affichePDF.php');
} else exitWrongSignature('affichePDF.php');

/***********************************************************************************************
**************		 Fin de vÃ©rification des paramÃštres en entrÃ©e 		  **************
**********************************************************************************************/

$DB_request = "SELECT O.libelle, R.titre,  R.debut, R.duree, R.jour FROM reservation R, objet O WHERE R.id = '$id' AND R.idobjet = O.id";
$resultat = database_query($DB_request, $connexionDB) or errorDB($DB_request, false);
$reservation = database_fetch_object($resultat);

$inter = database_get_from_object($reservation, 'jour');
$date = displayCurrentLanguageDate($inter, 0);

$libelle = database_get_from_object($reservation, 'libelle');
$titre = database_get_from_object($reservation, 'titre');
$titre = ereg_replace("(\r\n)|(\n)|(\r)","<br />",$titre);

$heureAffiche=substr(database_get_from_object($reservation, 'debut'),0,2);
$heureDecimale=$heureAffiche;
$minuteAffiche=substr(database_get_from_object($reservation, 'debut'),3,2);
if ($minuteAffiche=="30")$heureDecimale=$heureDecimale+.5;
$heureFinAffiche=substr(database_get_from_object($reservation, 'duree'),0,2);
$minuteFinAffiche=substr(database_get_from_object($reservation, 'duree'),3,2);

$debut=(int)($heureAffiche)." h ".$minuteAffiche;
if ($minuteAffiche=="00")$debut=(int)($heureAffiche)." h";

$heureFin=$heureFinAffiche+$heureDecimale;
if ($minuteFinAffiche=="30")$heureFin=$heureFin+.5;
if (($heureFin - (int)($heureFin))>0) $heureFin=" - ".(int)($heureFin)." h 30";
else $heureFin=" - ".$heureFin." h";


require_once($fpdf_location.'fpdf.php');
define('FPDF_FONTPATH',$fpdf_location.'font/');

$pdf=new FPDF('L','mm','A3');
$pdf->Open();
$pdf->AddPage();

// - Logo & Objet
if (! empty($image_pdf)) $pdf->Image("img/".$image_pdf,10,30,50);
$pdf->SetFont('Arial','B',60);
$pdf->Cell(420,40,'',0,1);
$pdf->SetTextColor(255,100,000);
$pdf->Cell(400,50,$libelle,0,1,'C');

// - Date
$pdf->SetFont('Arial','B',60);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(400,40,$date,0,1,'C');

// - Debut et fin
$pdf->SetTextColor(100,100,100);
$temps=$debut."  ".$heureFin;
$pdf->Cell(400,20,$temps,0,1,'C');

// - Titre
$titre=stripslashes($titre);
$pdf->SetFont('Arial','B',60);
$pdf->SetTextColor(0,0,0);


if (strlen($titre)<36){
	$pdf->Cell(400,20,"",0,1,'C');
	while ( ($pos = strpos($titre,"<br />")) !== FALSE){
	    $ligne = substr($titre,0,$pos);
	    $titre = substr($titre,$pos+4);
	    $pdf->Cell(400,20,$ligne,0,1,'C');
	}
	$pdf->Cell(400,20,$titre,0,1,'C');
} else {
	$pdf->Cell(400,20,"",0,1,'C');
	while (true){
	    $ligne = substr($titre,0,35);
	    if ( ($posBR = strpos($ligne,"<br />")) !== FALSE){
	        $ligne = substr($ligne,0,$posBR);
	        $titre = substr($titre,$posBR+4);
	        $pdf->Cell(400,20,$ligne,0,1,'C');
	        continue;
	    }
	    $pos = strrpos($ligne," ");
	    if (strlen($titre) > 34) $ligne = substr($ligne,0,$pos);
	    $pdf->Cell(400,20,$ligne,0,1,'C');
	    if (strlen($titre) > 34) $titre = substr($titre,$pos); else break;
	}
}
$pdf->Output();
?>
