#!/usr/bin/env bash

set -e

if [ ! -f /config/config.php ]; then
  cp -v /var/www/html/commun/config.php.sample /config/config.php
fi

if [ ! -f /config/database.php ]; then
  cp -v /var/www/html/commun/database.php.sample /config/database.php
fi

ln -s /config/config.php /var/www/html/commun/config.php
ln -s /config/database.php /var/www/html/commun/database.php

exec apache2-foreground
