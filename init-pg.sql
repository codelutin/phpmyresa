-- Based on http://web.archive.org/web/20100414075025/http://phpmyresa.in2p3.fr/installation/installationV4.0/INSTALL_creation_db_fr.php
--
-- Changed some columns to 'integer' to avoid this kind of errors:
--   ERROR:  foreign key constraint "fk_administrateur_id_classe" cannot be implemented
--   DETAIL:  Key columns "id_classe" and "id" are of incompatible types: numeric and integer
-- Original script caused those errors even on Postgres 8.4, the oldest version tested

CREATE TABLE classe (
    id serial NOT NULL,
    nom varchar(50) NOT NULL,
    jour_meme numeric(1) DEFAULT 1 NOT NULL,
    CONSTRAINT pk_classe_id PRIMARY KEY (id),
    CONSTRAINT un_classe_nom UNIQUE (nom)
);


CREATE TABLE administrateur (
    id_classe integer NOT NULL,
    nom varchar(40) NOT NULL,
    prenom varchar(40) NOT NULL,
    pass varchar(32) NOT NULL,
    mail varchar(128) NOT NULL,
    telephone varchar(20) NOT NULL,
    default_language varchar(30) DEFAULT 'english' NOT NULL,
    CONSTRAINT pk_administrateur_id_classe PRIMARY KEY (id_classe),
    CONSTRAINT fk_administrateur_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)
);


CREATE TABLE objet (
    id serial NOT NULL,
    id_classe integer NOT NULL,
    nom varchar(128) NOT NULL,
    capacite varchar(30) DEFAULT 0 NOT NULL,
    status numeric(1) DEFAULT 0 NOT NULL,
    libelle varchar(255) NOT NULL,
    priority numeric(1) DEFAULT 0 NOT NULL,
    pubical numeric(1) DEFAULT 1 NOT NULL,
    available numeric(1) DEFAULT 1 NOT NULL,
    visible numeric(1) DEFAULT 1 NOT NULL,
    wifi numeric(1) DEFAULT 0 NOT NULL,
    CONSTRAINT pk_objet_id PRIMARY KEY (id),
    CONSTRAINT un_objet_nom UNIQUE (nom),
    CONSTRAINT fk_objet_id_classe FOREIGN KEY (id_classe) REFERENCES classe(id)
);
CREATE INDEX in_objet_id_classe ON objet (id_classe);


CREATE TABLE reservation (
    id serial NOT NULL,
    idmulti numeric(16) DEFAULT 0 NOT NULL,
    idobjet integer NOT NULL,
    titre varchar(64) NOT NULL,
    jour varchar(10) NOT NULL,
    debut varchar(8) NOT NULL,
    duree varchar(8) NOT NULL,
    email varchar(128) NOT NULL,
    commentaire varchar(255) NOT NULL,
    pass varchar(20) NOT NULL,
    valide numeric(1) DEFAULT 0 NOT NULL,
    priority numeric(1) DEFAULT 0 NOT NULL,
    wifi numeric(1) DEFAULT 0 NOT NULL,
    state numeric(1) DEFAULT 0 NOT NULL,
    diffusion numeric(1) DEFAULT 1 NOT NULL,
    CONSTRAINT pk_reservation_id PRIMARY KEY (id),
    CONSTRAINT fk_reservation_idobjet FOREIGN KEY (idobjet) REFERENCES objet(id)
);
CREATE INDEX in_reservation_idobjet ON reservation (idobjet);
CREATE INDEX in_reservation_idmulti ON reservation (idmulti);
CREATE INDEX in_reservation_jour ON reservation (jour);
CREATE INDEX in_reservation_titre ON reservation (titre);
CREATE INDEX in_reservation_email ON reservation (email);
CREATE INDEX in_reservation_state ON reservation (state);


CREATE TABLE languages (
    name varchar(50) NOT NULL,
    french text NOT NULL,
    english text NOT NULL,
    spanish text NOT NULL,
    german text NOT NULL,
    CONSTRAINT pk_languages_name PRIMARY KEY (name)
);
